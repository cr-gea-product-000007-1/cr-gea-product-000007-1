// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace Path
{
	const FString UnderMessageWidgetPath = "/Game/Widget/System/WBP_UnderMessage.WBP_UnderMessage_C";
	const FString MessageBoxWidgetPath = "/Game/Widget/System/WBP_MessageBox.WBP_MessageBox_C";
	const FString NormalFadeWidgetPath = "/Game/Widget/System/WBP_NormalFade.WBP_NormalFade_C";
	const FString StatusBarWidgetPath = "/Game/Widget/System/WBP_StatusBar.WBP_StatusBar_C";
	const FString PlayerScreenViewPath = "/Game/Blueprint/UI/BP_PlayerScreenView.BP_PlayerScreenView_C";

	const FString OneLineSelectWidgetPath = "/Game/Widget/System/WBP_OneLineSelect.WBP_OneLineSelect_C";
	const FString OneLinePageSelectWidgetPath = "/Game/Widget/System/WBP_OneLinePageSelect.WBP_OneLinePageSelect_C";
	const FString TwoLineSelectWidgetPath = "/Game/Widget/System/WBP_TwoLineSelect.WBP_TwoLineSelect_C";
	const FString DescriptionWidgetPath = "/Game/Widget/System/WBP_Description.WBP_Description_C";
	const FString ValueWidgetPath = "/Game/Widget/System/WBP_Value.WBP_Value_C";
	const FString OnePlayerEquipmentStatusWidgetPath = "/Game/Widget/System/WBP_OnePlayerEquipmentStatus.WBP_OnePlayerEquipmentStatus_C";
	const FString AllPlayerEquipmentStatusWidgetPath = "/Game/Widget/System/WBP_AllPlayerEquipmentStatus.WBP_AllPlayerEquipmentStatus_C";

	const FString TitleMenuWidgetPath = "/Game/Widget/Title/WBP_TitleMenu.WBP_TitleMenu_C";
	const FString CreditWidgetPath = "/Game/Widget/Title/WBP_Credit.WBP_Credit_C";
	const FString SettingWidgetPath = "/Game/Widget/Title/WBP_TitleSetting.WBP_TitleSetting_C";

	const FString MenuStatusWidgetPath = "/Game/Widget/Menu/WBP_MenuStatus.WBP_MenuStatus_C";
	const FString MenuSettingWidgetPath = "/Game/Widget/Menu/WBP_MenuSetting.WBP_MenuSetting_C";
	const FString MenuGameEndWidgetPath = "/Game/Widget/Menu/WBP_MenuGameEnd.WBP_MenuGameEnd_C";

	const FString ShopItemCountWidgetPath = "/Game/Widget/Shop/WBP_ShopItemCount.WBP_ShopItemCount_C";

	const FString ChectTargetWidgetPath = "/Game/Widget/FieldUI/WBP_CheckTarget.WBP_CheckTarget_C";
	const FString TutorialWidgetPath = "/Game/Widget/FieldUI/WBP_Tutorial.WBP_Tutorial_C";
	const FString GoldWidgetPath = "/Game/Widget/FieldUI/WBP_Gold.WBP_Gold_C";

	const FString DamageWidgetPath = "/Game/Widget/Damage/WBP_Damage.WBP_Damage_C";
	const FString BattleResultWidgetPath = "/Game/Widget/BattleUI/WBP_BattleResult.WBP_BattleResult_C";

	const FString CommonSoundPath = "/Game/Widget/BattleUI/WBP_StatusBar.WBP_StatusBar_C";
	const FString BattleCameraPath = "/Game/BluePrint/Camera/BP_BattleCameraManager.BP_BattleCameraManager_C";

	const FString PlayerStatusDataPath = "/Game/Data/PlayerDataTable.PlayerDataTable";
	const FString EnemyStatusDataPath = "/Game/Data/EnemyDataTable.EnemyDataTable";
	const FString SkillDataPath = "/Game/Data/SkillDataTable.SkillDataTable";
	const FString ItemDataPath = "/Game/Data/ItemDataTable.ItemDataTable";
	const FString WeaponDataPath = "/Game/Data/WeaponDataTable.WeaponDataTable";
	const FString ProtectorDataPath = "/Game/Data/ProtectorDataTable.ProtectorDataTable";
	const FString OtherEquipmentDataPath = "/Game/Data/OtherEquipmentDataTable.OtherEquipmentDataTable";
	const FString CommonSoundDataPath = "/Game/Data/CommonSoundDataTable.CommonSoundDataTable";
	const FString StartParameterDataPath = "/Game/Data/StartParameterDataTable.StartParameterDataTable";
	const FString DebugParameterDataPath = "/Game/Data/DebugParameterDataTable.DebugParameterDataTable";

	const FString TitleBGMPath = "/Game/Sound/Original/Title.Title";
	const FString FieldBGMPath = "/Game/Sound/Original/Dungeon.Dungeon";
	const FString EncountSoundPath = "/Game/Sound/Original/Encount.Encount";
};