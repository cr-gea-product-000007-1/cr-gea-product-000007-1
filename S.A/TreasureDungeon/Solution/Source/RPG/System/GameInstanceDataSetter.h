﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../RPGGameInstance.h"
#include "GameInstanceDataSetter.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UGameInstanceDataSetter : public UObject
{
	GENERATED_BODY()

		enum class ELoadDataType
	{
		None,
		Player,
		Enemy,
		Skill,
		Item,
		Equipment,
		Sound,
		Finish,
	};

public:
	/// <summary>
	/// 読み込み開始
	/// </summary>
	void LoadStart();

	/// <summary>
	/// データテーブルのデータをゲームインスタントに読み込む
	/// </summary>
	void LoadDataUpdate();

	/// <summary>
	/// 読み込み完了しているか
	/// </summary>
	/// <returns>読み込み完了 = true</returns>
	bool IsLoadFinish() { return LoadDataType == ELoadDataType::Finish; };

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	URPGGameInstance* GameInstance;

	/// <summary>
	/// 読み込むデータの種類
	/// </summary>
	ELoadDataType LoadDataType;
};
