﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstanceDataSetter.h"
#include "../PathConstant.h"

#include "../Skill/Skill.h"
#include "../Item/Item.h"
#include "../Item/Weapon.h"
#include "../Item/Protector.h"
#include "../Sound/CommonSoundObject.h"

/// <summary>
/// 読み込み開始
/// </summary>
void UGameInstanceDataSetter::LoadStart()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	LoadDataType = ELoadDataType::Player;
}

/// <summary>
/// データテーブルのデータをゲームインスタントに読み込む
/// </summary>
void UGameInstanceDataSetter::LoadDataUpdate()
{
	if (GameInstance == nullptr) return;
	TArray<FName> RowNames;

	switch (LoadDataType)
	{
	case ELoadDataType::Player:
	{
		GameInstance->RootPlayerData.Empty();
		UDataTable* PlayerDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::PlayerStatusDataPath));
		RowNames = PlayerDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			FPlayerParameter* PlayerParameter = PlayerDataTable->FindRow<FPlayerParameter>(RowName, "");
			GameInstance->RootPlayerData.Add(*PlayerParameter);
		}

		LoadDataType = ELoadDataType::Enemy;
		break;
	}
	case ELoadDataType::Enemy:
	{
		GameInstance->RootEnemyData.Empty();
		UDataTable* EnemyDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::EnemyStatusDataPath));
		RowNames = EnemyDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			FEnemyParameter* EnemyParameter = EnemyDataTable->FindRow<FEnemyParameter>(RowName, "");
			GameInstance->RootEnemyData.Add(*EnemyParameter);
		}

		LoadDataType = ELoadDataType::Skill;
		break;
	}
	case ELoadDataType::Skill:
	{
		GameInstance->RootSkillData.Empty();
		UDataTable* SkillDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::SkillDataPath));
		RowNames = SkillDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			USkill* Skill = NewObject<USkill>();
			FSkillParameter* SkillParameter = SkillDataTable->FindRow<FSkillParameter>(RowName, "");
			Skill->SetSkillParameter(*SkillParameter);
			GameInstance->RootSkillData.Add(Skill);
		}

		LoadDataType = ELoadDataType::Item;
		break;
	}
	case ELoadDataType::Item:
	{
		GameInstance->RootItemData.Empty();
		UDataTable* ItemDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::ItemDataPath));
		RowNames = ItemDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			UItem* Item = NewObject<UItem>();
			FItemParameter* ItemParameter = ItemDataTable->FindRow<FItemParameter>(RowName, "");
			Item->SetItemParameter(*ItemParameter);
			GameInstance->RootItemData.Add(Item);
		}

		LoadDataType = ELoadDataType::Equipment;
		break;
	}
	case ELoadDataType::Equipment:
	{
		//武器
		GameInstance->RootWeaponData.Empty();
		UDataTable* WeaponDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::WeaponDataPath));
		RowNames = WeaponDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			UWeapon* Weapon = NewObject<UWeapon>();
			FWeaponParameter* WeaponParameter = WeaponDataTable->FindRow<FWeaponParameter>(RowName, "");
			Weapon->SetWeaponParameter(*WeaponParameter);
			GameInstance->RootWeaponData.Add(Weapon);
		}
		//防具
		GameInstance->RootProtectorData.Empty();
		UDataTable* ProtectorDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::ProtectorDataPath));
		RowNames = ProtectorDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			UProtector* Protector = NewObject<UProtector>();
			FProtectorParameter* ProtectorParameter = ProtectorDataTable->FindRow<FProtectorParameter>(RowName, "");
			Protector->SetProtectorParameter(*ProtectorParameter);
			GameInstance->RootProtectorData.Add(Protector);
		}
		//装備なし
		UDataTable* OtherEquipmentDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::OtherEquipmentDataPath));
		RowNames = OtherEquipmentDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			UEquipment* OtherEquipment = NewObject<UEquipment>();
			FEquipmentParameter* EquipmentParameter = OtherEquipmentDataTable->FindRow<FEquipmentParameter>(RowName, "");
			OtherEquipment->SetEquipmentParameter(*EquipmentParameter);
			GameInstance->NoEquipment = OtherEquipment;
		}

		LoadDataType = ELoadDataType::Sound;
		break;
	}
	case ELoadDataType::Sound:
	{
		TArray<FCommonSoundObject*> ComonSoundObjects;
		UDataTable* SoundDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::CommonSoundDataPath));
		RowNames = SoundDataTable->GetRowNames();
		for (FName RowName : RowNames)
		{
			FCommonSoundObject* ComonSoundObject = SoundDataTable->FindRow<FCommonSoundObject>(RowName, "");
			ComonSoundObjects.Add(ComonSoundObject);
		}
		FCommonSoundObject* DecisionSound = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::Decision; });
		GameInstance->DecisionSound = DecisionSound->Sound;

		FCommonSoundObject* CancelSound = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::Cancel; });
		GameInstance->CancelSound = CancelSound->Sound;

		FCommonSoundObject* CursorMoveSound = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::CursorMove; });
		GameInstance->CursorMoveSound = CursorMoveSound->Sound;

		FCommonSoundObject* VictorySound = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::Victory; });
		GameInstance->VictorySound = VictorySound->Sound;

		FCommonSoundObject* FanfareSound = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::Fanfare; });
		GameInstance->FanfareSound = FanfareSound->Sound;

		FCommonSoundObject* NormalBattleBGM = *ComonSoundObjects.FindByPredicate([](FCommonSoundObject* Sound) { return Sound->SoundID == ECommonSoundID::NormalBattleBGM; });
		GameInstance->NormalBattleBGM = NormalBattleBGM->Sound;

		if (!GameInstance->bIsFirst)
		{
			GameInstance->BGM_Volue = 0.7f;
			GameInstance->SE_Volue = 0.7f;
			GameInstance->bIsFirst = true;
		}

		LoadDataType = ELoadDataType::Finish;
		break;
	}
	}
}