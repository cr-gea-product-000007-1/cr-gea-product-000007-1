// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Item/ItemID.h"
#include "Engine/DataTable.h"
#include "StartParameter.generated.h"



USTRUCT(BlueprintType)
struct FStartParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// 所持品
	/// </summary>
	UPROPERTY(EditAnywhere)
	TMap<EItemID, int> HaveItems;

	/// <summary>
	/// 所持ゴールド
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Gold;
};