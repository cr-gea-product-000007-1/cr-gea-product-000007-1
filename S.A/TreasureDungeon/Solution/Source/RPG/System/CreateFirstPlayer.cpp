﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CreateFirstPlayer.h"
#include "../PathConstant.h"
#include "StartParameter.h"
#include "../Debug/DebugParameter.h"


/// <summary>
/// 初期化
/// </summary>
void UCreateFirstPlayer::Initialize()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}

/// <summary>
/// 初期プレイヤーを生成する
/// </summary>
void UCreateFirstPlayer::PlayerCreate()
{
	if (!IsValid(GameInstance)) return;

	GameInstance->PlayerStatuses.Empty();
	TArray<FPlayerParameter> PlayerParameters = GameInstance->RootPlayerData;
	for (FPlayerParameter PlayerParameter : PlayerParameters)
	{
		UBattlePlayerStatus* PlayerStatus = NewObject<UBattlePlayerStatus>();
		PlayerStatus->SetPlayerStatus(PlayerParameter, GameInstance);
		PlayerStatus->AddSkill(GetSkill(ESkillID::Heal1));
		//初期装備
		PlayerStatus->EquipItem(EItemType::Weapon, GetWeapon(PlayerParameter.Weapon));
		PlayerStatus->EquipItem(EItemType::Shield, GetProtector(PlayerParameter.Weapon));
		PlayerStatus->EquipItem(EItemType::Helmet, GetProtector(PlayerParameter.Helmet));
		PlayerStatus->EquipItem(EItemType::Armor, GetProtector(PlayerParameter.Armor));
		PlayerStatus->EquipItem(EItemType::Accessory, GetProtector(PlayerParameter.Accessory));

		GameInstance->PlayerStatuses.Add(PlayerStatus);
	}

	bool bIsDebug = false;
	UDataTable* StartParameterDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::StartParameterDataPath));
	UDataTable* DebugParameterDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *Path::DebugParameterDataPath));
	FStartParameter* StartParameter = StartParameterDataTable->FindRow<FStartParameter>(*StartParameterDataTable->GetRowNames().begin(), "");
	FDebugParameter* DebugParameter = DebugParameterDataTable->FindRow<FDebugParameter>(*DebugParameterDataTable->GetRowNames().begin(), "");

#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
	bIsDebug = DebugParameter->bIsDebug;
#endif

	//デバッグプレイヤー設定
	if (bIsDebug)
	{
		UBattlePlayerStatus* PlayerStatus = *GameInstance->PlayerStatuses.begin();
		PlayerStatus->SetLoadDataInterface(this);
		PlayerStatus->AddLevel(this, DebugParameter->StartLevel - 1);
		PlayerStatus->EquipItem(EItemType::Weapon, GetWeapon(DebugParameter->Weapon));
		PlayerStatus->EquipItem(EItemType::Shield, GetProtector(DebugParameter->Shield));
		PlayerStatus->EquipItem(EItemType::Helmet, GetProtector(DebugParameter->Helmet));
		PlayerStatus->EquipItem(EItemType::Armor, GetProtector(DebugParameter->Armor));
		PlayerStatus->EquipItem(EItemType::Accessory, GetProtector(DebugParameter->Accessory));
	}

	//初期アイテム
	GameInstance->ItemStorage = NewObject<UItemStorage>();
	TMap<EItemID, int> HaveItems;
	if (bIsDebug)
		HaveItems = DebugParameter->HaveItems;
	else
		HaveItems = StartParameter->HaveItems;
	for(TPair<EItemID, int> HaveItem : HaveItems)
		GameInstance->ItemStorage->InsertItem(GetItem(HaveItem.Key), HaveItem.Value);

	//所持金
	if (bIsDebug)
		GameInstance->Gold = DebugParameter->Gold;
	else
		GameInstance->Gold = StartParameter->Gold;

	//フラグ設定
	GameInstance->bInBattle = false;
	GameInstance->bGameClear = false;
	GameInstance->bBattleLose = false;

	if (bIsDebug)
	{
		GameInstance->bTutorialFinish = true;
		GameInstance->PlayerStartPoint = DebugParameter->PlayerStartPoint;
		GameInstance->bIsEncount = DebugParameter->bIsEncount;
	}
	else {
		GameInstance->bTutorialFinish = false;
		GameInstance->PlayerStartPoint = EPlayerStartPoint::Start;
		GameInstance->bIsEncount = true;
	}
}

/// <summary>
/// スキルを取得
/// </summary>
/// <param name="SkilID">スキルのID</param>
USkill* UCreateFirstPlayer::GetSkill(ESkillID SkillID)
{
	if (!IsValid(GameInstance)) return nullptr;

	TArray<USkill*>& SkillData = GameInstance->RootSkillData;
	int32 Index = SkillData.IndexOfByPredicate([SkillID](USkill* Skill) {
		return Skill->GetSkillParameter().SkillID == SkillID;
		});

	if (Index == -1) return nullptr;
	return SkillData[Index];
}

/// <summary>
/// アイテムを取得
/// </summary>
/// <param name="ItemID">アイテムのID</param>
UItem* UCreateFirstPlayer::GetItem(EItemID ItemID)
{
	if (!IsValid(GameInstance)) return nullptr;

	TArray<UItem*>& ItemData = GameInstance->RootItemData;
	int32 Index = ItemData.IndexOfByPredicate([ItemID](UItem* Item) {
		return Item->GetItemParameter().ItemID == ItemID;
		});

	if (Index == -1) return nullptr;
	return ItemData[Index];
}

/// <summary>
/// 武器を取得
/// </summary>
/// <param name="WeaponID">武器のID</param>
UWeapon* UCreateFirstPlayer::GetWeapon(EItemID WeaponID)
{
	if (!IsValid(GameInstance)) return nullptr;

	TArray<UWeapon*>& WeaponData = GameInstance->RootWeaponData;
	int32 Index = WeaponData.IndexOfByPredicate([WeaponID](UWeapon* Weapon) {
		return Weapon->GetEquipmentParameter().ItemID == WeaponID;
		});

	if (Index == -1) return nullptr;
	return WeaponData[Index];
}

/// <summary>
/// 防具を取得
/// </summary>
/// <param name="ProtectorID">防具のID</param>
UProtector* UCreateFirstPlayer::GetProtector(EItemID ProtectorID)
{
	if (!IsValid(GameInstance)) return nullptr;

	TArray<UProtector*>& ProtectorData = GameInstance->RootProtectorData;
	int32 Index = ProtectorData.IndexOfByPredicate([ProtectorID](UProtector* Protector) {
		return Protector->GetEquipmentParameter().ItemID == ProtectorID;
		});

	if (Index == -1) return nullptr;
	return ProtectorData[Index];
}

