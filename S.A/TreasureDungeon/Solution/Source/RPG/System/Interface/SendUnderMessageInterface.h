﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Interface/ReceiveUnderMessageInterface.h"
#include "SendUnderMessageInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(meta = (CannotImplementInterfaceInBlueprint))
class USendUnderMessageInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API ISendUnderMessageInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// メッセージ表示
	/// </summary>
	UFUNCTION(BlueprintCallable)
		virtual void ShowMessage(const FString& Message);

	/// <summary>
	/// メッセージを受け取るインターフェース設定
	/// </summary>
	void SetReceiveUnderMessageInterface(IReceiveUnderMessageInterface* Interface) { ReceiveUnderMessageInterface = Interface; };

private:
	/// <summary>
	/// メッセージを受け取るインターフェース
	/// </summary>
	IReceiveUnderMessageInterface* ReceiveUnderMessageInterface;
};
