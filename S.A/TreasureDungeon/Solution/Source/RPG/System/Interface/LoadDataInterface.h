﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../../RPGGameInstance.h"

#include "../../Character/StatusData/EnemyParameter.h"
#include "../../Skill/Skill.h"
#include "../../Item/HaveItem.h"
#include "../../Item/Item.h"
#include "../../Item/Weapon.h"
#include "../../Item/Protector.h"

#include "LoadDataInterface.generated.h"

class UBattlePlayerStatus;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class ULoadDataInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API ILoadDataInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// プレイヤーのステータスリストを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <returns>プレイヤーのステータスリスト</returns>
	TArray<UBattlePlayerStatus*> GetPlayerStatuses(UObject* Obj);

	/// <summary>
	/// 所持アイテムリストを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <returns>所持アイテムリスト</returns>
	TArray<UHaveItem*> GetHaveItems(UObject* Obj);

	/// <summary>
	/// 所持武器リストを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <returns>所持武器リスト</returns>
	TArray<UHaveItem*> GetHaveWeapons(UObject* Obj);

	/// <summary>
	/// 所持防具リストを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <returns>所持防具リスト</returns>
	TArray<UHaveItem*> GetHaveProtectors(UObject* Obj);

	/// <summary>
	/// 敵情報を取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="SkillID">敵ID</param>
	/// <returns>敵情報</returns>
	FEnemyParameter GetEnemyParameter(UObject* Obj, EEnemyID EnemyID);

	/// <summary>
	/// スキルを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="SkillID">取得するスキルID</param>
	/// <returns>スキル</returns>
	USkill* GetSkill(UObject* Obj, ESkillID SkillID);

	/// <summary>
	/// アイテムの親クラスを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="WeaponID">取得するアイテムID</param>
	/// <returns>アイテムの親クラス</returns>
	UBaseItem* GetBaseItem(UObject* Obj, EItemID ItemID);

	/// <summary>
	/// アイテムを取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="WeaponID">取得するアイテムID</param>
	/// <returns>アイテム</returns>
	UItem* GetItem(UObject* Obj, EItemID ItemID);

	/// <summary>
	/// 武器を取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="WeaponID">取得するアイテムID</param>
	/// <returns>武器</returns>
	UWeapon* GetWeapon(UObject* Obj, EItemID ItemID);

	/// <summary>
	/// 防具を取得
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <param name="SkillID">取得するアイテムID</param>
	/// <returns>防具</returns>
	UProtector* GetProtector(UObject* Obj, EItemID ItemID);

private:
	/// <summary>
	/// ゲームインスタンスを読み込む
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	/// <returns>読み込み成功 = true</returns>
	bool LoadGameInstance(UObject* Obj);

private:
	/// <summary>
	/// データを読み込んでいるか
	/// </summary>
	bool bIsLoad = false;

protected:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	URPGGameInstance* GameInstance;
};
