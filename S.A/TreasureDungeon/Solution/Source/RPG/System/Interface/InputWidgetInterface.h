﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InputWidgetInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInputWidgetInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IInputWidgetInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	virtual void OnDecide() {};

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	virtual void OnCancel() {};

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	virtual void OnMenu() {};

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	virtual void OnUp() {};

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	virtual void OnDown() {};

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	virtual void OnLeft() {};

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	virtual void OnRight() {};
};
