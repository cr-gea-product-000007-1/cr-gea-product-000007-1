﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "LoadDataInterface.h"
#include "../../Item/ItemStorage.h"
#include "../../Character/BattlePlayerStatus.h"
#include "Kismet/GameplayStatics.h"

// Add default functionality here for any ILoadDataInterface functions that are not pure virtual.


/// <summary>
/// プレイヤーのステータスリストを取得
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>プレイヤーのステータスリスト</returns>
TArray<UBattlePlayerStatus*> ILoadDataInterface::GetPlayerStatuses(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return TArray<UBattlePlayerStatus*>();

	return GameInstance->PlayerStatuses;
}

/// <summary>
/// 所持アイテムリストを取得
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>所持アイテムリスト</returns>
TArray<UHaveItem*> ILoadDataInterface::GetHaveItems(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return TArray<UHaveItem*>();

	return GameInstance->ItemStorage->GetInItems();
}

/// <summary>
/// 所持武器リストを取得
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>所持武器リスト</returns>
TArray<UHaveItem*> ILoadDataInterface::GetHaveWeapons(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return TArray<UHaveItem*>();

	return GameInstance->ItemStorage->GetInWeapons();
}

/// <summary>
/// 所持防具リストを取得
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>所持防具リスト</returns>
TArray<UHaveItem*> ILoadDataInterface::GetHaveProtectors(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return TArray<UHaveItem*>();

	return GameInstance->ItemStorage->GetInProtectors();
}

/// <summary>
/// 敵情報を取得
/// </summary>
/// <param name="SkillID">敵ID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>敵情報</returns>
FEnemyParameter ILoadDataInterface::GetEnemyParameter(UObject* Obj, EEnemyID EnemyID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return FEnemyParameter();

	TArray<FEnemyParameter> EnemyParameters = GameInstance->RootEnemyData;
	if (EnemyParameters.Num() == 0) return FEnemyParameter();

	int32 Index = EnemyParameters.IndexOfByPredicate([EnemyID](FEnemyParameter EnemyParameter) {
		return EnemyParameter.ID == EnemyID;
		});

	if (Index == -1) return FEnemyParameter();
	return EnemyParameters[Index];
}

/// <summary>
/// スキルを取得
/// </summary>
/// <param name="SkillID">取得するスキルID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>スキル</returns>
USkill* ILoadDataInterface::GetSkill(UObject* Obj, ESkillID SkillID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return nullptr;

	TArray<USkill*> SkillData = GameInstance->RootSkillData;
	if (SkillData.Num() == 0) return nullptr;

	int32 Index = SkillData.IndexOfByPredicate([SkillID](USkill* Skill) {
		return Skill->GetSkillParameter().SkillID == SkillID;
		});

	if (Index == -1) return nullptr;
	return SkillData[Index];
}

/// <summary>
/// アイテムの親クラスを取得
/// </summary>
/// <param name="WeaponID">取得するアイテムID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>アイテムの親クラス</returns>
UBaseItem* ILoadDataInterface::GetBaseItem(UObject* Obj, EItemID ItemID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return nullptr;

	TArray<UItem*> ItemData = GameInstance->RootItemData;
	if (ItemData.Num() > 0)
	{
		int32 Index = ItemData.IndexOfByPredicate([ItemID](UItem* Item) {
			return Item->GetItemParameter().ItemID == ItemID;
			});
		if (Index >= 0)
			return ItemData[Index];
	}

	TArray<UWeapon*> WeaponData = GameInstance->RootWeaponData;
	if (WeaponData.Num() > 0)
	{
		int32 Index = WeaponData.IndexOfByPredicate([ItemID](UWeapon* Weapon) {
			return Weapon->GetEquipmentParameter().ItemID == ItemID;
			});
		if (Index >= 0)
			return WeaponData[Index];
	}

	TArray<UProtector*> ProtectorData = GameInstance->RootProtectorData;
	if (ProtectorData.Num() > 0)
	{
		int32 Index = ProtectorData.IndexOfByPredicate([ItemID](UProtector* Protector) {
			return Protector->GetEquipmentParameter().ItemID == ItemID;
			});
		if (Index >= 0)
			return ProtectorData[Index];
	}
	return nullptr;
}

/// <summary>
/// アイテムを取得
/// </summary>
/// <param name="WeaponID">取得するアイテムID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>アイテム</returns>
UItem* ILoadDataInterface::GetItem(UObject* Obj, EItemID ItemID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return nullptr;

	TArray<UItem*> ItemData = GameInstance->RootItemData;
	if (ItemData.Num() == 0) return nullptr;

	int32 Index = ItemData.IndexOfByPredicate([ItemID](UItem* Item) {
		return Item->GetItemParameter().ItemID == ItemID;
		});

	if (Index == -1) return nullptr;
	return ItemData[Index];
}

/// <summary>
/// 武器を取得
/// </summary>
/// <param name="WeaponID">取得するアイテムID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>武器</returns>
UWeapon* ILoadDataInterface::GetWeapon(UObject* Obj, EItemID ItemID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return nullptr;

	TArray<UWeapon*> WeaponData = GameInstance->RootWeaponData;
	if (WeaponData.Num() == 0) return nullptr;

	int32 Index = WeaponData.IndexOfByPredicate([ItemID](UWeapon* Weapon) {
		return Weapon->GetEquipmentParameter().ItemID == ItemID;
		});

	if (Index == -1) return nullptr;
	return WeaponData[Index];
}

/// <summary>
/// 防具を取得
/// </summary>
/// <param name="SkillID">取得するアイテムID</param>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>防具</returns>
UProtector* ILoadDataInterface::GetProtector(UObject* Obj, EItemID ItemID)
{
	if (!bIsLoad)
		if (!LoadGameInstance(Obj)) return nullptr;

	TArray<UProtector*> ProtectorData = GameInstance->RootProtectorData;
	if (ProtectorData.Num() == 0) return nullptr;

	int32 Index = ProtectorData.IndexOfByPredicate([ItemID](UProtector* Protector) {
		return Protector->GetEquipmentParameter().ItemID == ItemID;
		});

	if (Index == -1) return nullptr;
	return ProtectorData[Index];
}

/// <summary>
/// ゲームインスタンスを読み込む
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
/// <returns>読み込み成功 = true</returns>
bool ILoadDataInterface::LoadGameInstance(UObject* Obj)
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(Obj->GetWorld()));
	if (!IsValid(GameInstance)) return false;

	bIsLoad = true;
	return true;
}