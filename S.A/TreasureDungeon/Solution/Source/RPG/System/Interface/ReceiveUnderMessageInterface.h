﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ReceiveUnderMessageInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UReceiveUnderMessageInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IReceiveUnderMessageInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// メッセージ表示
	/// </summary>
	/// <param name="Message">表示するメッセージ</param>
	virtual void ShowMessage(const FString& Message) = 0;
};
