﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SendUnderMessageInterface.h"

// Add default functionality here for any ISendUnderMessageInterface functions that are not pure virtual.

/// <summary>
/// メッセージ表示
/// </summary>
void ISendUnderMessageInterface::ShowMessage(const FString& Message)
{
	if (ReceiveUnderMessageInterface != nullptr)
	{
		ReceiveUnderMessageInterface->ShowMessage(Message);
	}
}