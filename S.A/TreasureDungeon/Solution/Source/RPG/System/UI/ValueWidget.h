﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OpenWidget.h"
#include "ValueWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UValueWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter);

	/// <summary>
	/// 数値設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetNum(int Num);

	/// <summary>
	/// 個数を単位に設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetItemUnit();

	/// <summary>
	/// MPを単位に設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetMPUnit();

public:
	/// <summary>
	/// ウインドウの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FVector2D WindowSize;
};
