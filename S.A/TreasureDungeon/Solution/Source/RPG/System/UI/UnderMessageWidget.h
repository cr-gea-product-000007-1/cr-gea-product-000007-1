﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UnderMessageWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UUnderMessageWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 表示するメッセージ追加
	/// </summary>
	void AddMessage(FString Message) { Messages.Add(Message); };

	/// <summary>
	/// 表示中かを返す
	/// </summary>
	/// <returns>表示中　= true</returns>
	bool IsViewMessage() { return bIsViewMessage; };

public:
	/// <summary>
	/// 表示するメッセージ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	TArray<FString> Messages;

	/// <summary>
	/// 表示中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsViewMessage;
};
