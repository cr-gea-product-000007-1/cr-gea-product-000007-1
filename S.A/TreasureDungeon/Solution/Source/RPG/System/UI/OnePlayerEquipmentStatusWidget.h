﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/Equipment.h"
#include "OpenWidget.h"
#include "WindowParameter.h"
#include "OnePlayerEquipmentStatusWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UOnePlayerEquipmentStatusWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter, FVector2D SideWindowSize);

	/// <summary>
	/// 装備するプレイヤーを設定する
	/// </summary>
	/// <param name="NewPlayerStatus">装備するプレイヤー</param>
	void SetPlayerStatus(UBattlePlayerStatus* NewPlayerStatus) { PlayerStatus = NewPlayerStatus; };

	/// <summary>
	/// 表示装備を設定
	/// </summary>
	/// <param name="NewHaveEquipments">表示装備</param>
	void SetEquipment(UEquipment* NewEquipment) { Equipment = NewEquipment; };

	/// <summary>
	/// 表示装備部位を設定
	/// </summary>
	/// <param name="NewHaveEquipments">表示装備</param>
	void SetEquipmentType(EItemType NewEquipmentType);

	/// <summary>
	/// ウインドウを更新
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateWindow();
	
public:
	/// <summary>
	/// 装備するプレイヤー
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	UBattlePlayerStatus* PlayerStatus;

	/// <summary>
	/// 表示する装備
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	UEquipment* Equipment;

	/// <summary>
	/// 表示する装備の部位
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	EItemType EquipmentType;

	/// <summary>
	/// メインのステータス
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	EStatusType MainStatusType;
};
