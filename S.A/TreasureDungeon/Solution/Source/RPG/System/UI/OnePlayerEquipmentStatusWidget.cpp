﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OnePlayerEquipmentStatusWidget.h"


/// <summary>
/// 表示装備部位を設定
/// </summary>
/// <param name="NewHaveEquipments">表示装備</param>
void UOnePlayerEquipmentStatusWidget::SetEquipmentType(EItemType NewEquipmentType) 
{
	EquipmentType = NewEquipmentType;
	if (EquipmentType == EItemType::Weapon)
		MainStatusType = EStatusType::Attack;
	else
		MainStatusType = EStatusType::Vitality;
};