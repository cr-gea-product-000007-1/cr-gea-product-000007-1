﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TwoLineSelectWidget.h"


/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UTwoLineSelectWidget::SetSelectIndex(int Index)
{
	if (Index < 0) return;
	if (Index > MaxIndex) return;

	SelectIndex = Index;
	UpdatePageIndex();
	UpdateWindow();
}

/// <summary>
/// 選んでいるインデックスを範囲内に合わせる
/// </summary>
void UTwoLineSelectWidget::ClampSelectIndex()
{
	if (SelectIndex > MaxIndex)
	{
		SelectIndex = MaxIndex;
	}
	if (SelectIndex < 0)
	{
		SelectIndex = 0;
	}
	UpdatePageIndex();
}

/// <summary>
/// インデックスの最大値を設定
/// </summary>
/// <param name="Index">インデックス</param>
void UTwoLineSelectWidget::SetMaxIndex(int Index)
{
	MaxIndex = Index;
	PageMaxNum = FMath::CeilToInt((float)(MaxIndex + 1) / (float)(PageXNum * PageYNum));
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
bool UTwoLineSelectWidget::OnUp()
{
	PageYIndex--;
	if (PageYIndex < 0)
		PageYIndex = PageYNum - 1;
	UpdateSelectIndex();

	while (SelectIndex > MaxIndex)
	{
		SelectIndex -= PageXNum;
		PageYIndex--;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
bool UTwoLineSelectWidget::OnDown()
{
	PageYIndex++;
	if (PageYIndex >= PageYNum)
		PageYIndex = 0;
	UpdateSelectIndex();

	if (SelectIndex > MaxIndex)
	{
		PageYIndex = 0;
		UpdateSelectIndex();
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
bool UTwoLineSelectWidget::OnLeft()
{
	PageXIndex--;
	//ページ変更
	if (PageXIndex >= 0)
	{
		UpdateSelectIndex();
		UpdateWindow();
		return true;
	}
	PageXIndex = PageXNum - 1;
	PageNum--;
	//ページループ
	if (PageNum >= 0)
	{
		UpdateSelectIndex();
		UpdateWindow();
		return true;
	}
	PageNum = PageMaxNum - 1;
	UpdateSelectIndex();

	//インデックスはみ出し
	if (SelectIndex <= MaxIndex)
	{
		UpdateWindow();
		return true;
	}
	SelectIndex = MaxIndex;
	UpdatePageIndex();

	UpdateWindow();
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
bool UTwoLineSelectWidget::OnRight()
{
	PageXIndex++;
	//ページ変更
	if (PageXIndex >= PageXNum)
	{
		PageXIndex = 0;
		PageNum++;
	}
	UpdateSelectIndex();
	//ページループ
	if (PageNum >= PageMaxNum)
	{
		PageNum = 0;
		UpdateSelectIndex();
	}
	while (SelectIndex > MaxIndex)
	{
		SelectIndex -= PageXNum;
		PageYIndex--;
	}
	if (SelectIndex < 0)
	{
		SelectIndex = 0;
		PageXIndex = 0;
		PageYIndex = 0;
	}
	else
	{
		UpdateSelectIndex();
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 選んでいるインデックスをページ位置から設定する
/// </summary>
void UTwoLineSelectWidget::UpdateSelectIndex()
{
	//ページ数＊ページ項目数＋現在の位置
	SelectIndex = PageNum * PageXNum * PageYNum + PageXIndex + PageYIndex * PageXNum;
}

/// <summary>
/// ページ位置を選んでいるインデックスから設定する
/// </summary>
void UTwoLineSelectWidget::UpdatePageIndex()
{
	int PageItemNum = PageXNum * PageYNum;
	if (PageItemNum == 0)
	{
		PageNum = 0;
		PageXIndex = 0;
		PageYIndex = 0;
		return;
	}
	PageNum = SelectIndex / (PageItemNum);
	int PageIndex = SelectIndex - PageNum * (PageItemNum);
	PageXIndex = PageIndex % PageXNum;
	PageYIndex = PageIndex / PageXNum;
}