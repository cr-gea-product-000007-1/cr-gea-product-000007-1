﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WindowParameter.h"
#include "OpenWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UOpenWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 開いた時のイベント
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Open();

	/// <summary>
	/// 閉じた時のイベント
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Close();

	/// <summary>
	/// 閉じ終わった時のイベント
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Closed();

	/// <summary>
	/// ウィジットを開いているかを取得する
	/// </summary>
	bool IsOpen() { return bIsOpen; }

	/// <summary>
	/// ウインドウを隠す
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Hide();

public:
	/// <summary>
	/// ウィジットを開いているか
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	bool bIsOpen = false;
};
