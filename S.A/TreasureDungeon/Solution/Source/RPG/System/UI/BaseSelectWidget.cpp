﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseSelectWidget.h"


/// <summary>
/// 表示テキストを設定
/// </summary>
void UBaseSelectWidget::SetText(const TArray <FString>& Texts)
{
	SetMaxIndex(Texts.Num() - 1);
	TextParameters.Empty();

	for (FString Text : Texts)
	{
		FSelectWidgetTextParameter TextParameter;
		TextParameter.NameText = FText::FromString(Text);
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示テキストを追加
/// </summary>
void UBaseSelectWidget::AddText(const FString& Text)
{
	SetMaxIndex(MaxIndex + 1);

	FSelectWidgetTextParameter TextParameter;
	TextParameter.NameText = FText::FromString(Text);
	TextParameters.Add(TextParameter);
	UpdateWindow();
}

/// <summary>
/// 表示キャラクターを設定
/// </summary>
void UBaseSelectWidget::SetCharacter(const TArray<UBattleCharacter*>& NewCharacters)
{
	SetMaxIndex(NewCharacters.Num() - 1);
	TextParameters.Empty();

	for (UBattleCharacter* Character : NewCharacters)
	{
		FSelectWidgetTextParameter TextParameter;
		TextParameter.NameText = FText::FromString(Character->GetStatus()->GetCharacterName());
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示プレイヤーを設定
/// </summary>
void UBaseSelectWidget::SetPlayerStatus(const TArray<UBattlePlayerStatus*>& NewStatuses)
{
	SetMaxIndex(NewStatuses.Num() - 1);
	TextParameters.Empty();

	for (UBattleCharacterStatus* Status : NewStatuses)
	{
		FSelectWidgetTextParameter TextParameter;
		TextParameter.NameText = FText::FromString(Status->GetCharacterName());
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示アイテムを設定
/// </summary>
void UBaseSelectWidget::SetItems(const TArray<UBaseItem*>& NewBaseItems, ESelectViewType SelectViewType)
{
	SetMaxIndex(NewBaseItems.Num() - 1);
	TextParameters.Empty();

	for (UBaseItem* BaseItem : NewBaseItems)
	{
		FSelectWidgetTextParameter TextParameter;
		switch (SelectViewType)
		{
		case ESelectViewType::NameOnly:
		case ESelectViewType::NameAndCount:
		case ESelectViewType::NameAndMP:
			TextParameter.NameText = FText::FromString(BaseItem->GetItemName());
			break;

		case ESelectViewType::NameAndValue:
			TextParameter.NameText = FText::FromString(BaseItem->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(BaseItem->GetValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;

		case ESelectViewType::NameAndCountAndSellValue:
			TextParameter.NameText = FText::FromString(BaseItem->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(BaseItem->GetValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;
		}
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示装備を設定
/// </summary>
void UBaseSelectWidget::SetEquipments(const TArray<UEquipment*>& NewEquipments, ESelectViewType SelectViewType)
{
	SetMaxIndex(NewEquipments.Num() - 1);
	TextParameters.Empty();
	
	for (UEquipment* Equipment : NewEquipments)
	{
		FSelectWidgetTextParameter TextParameter;
		switch (SelectViewType)
		{
		case ESelectViewType::NameOnly:
		case ESelectViewType::NameAndCount:
		case ESelectViewType::NameAndMP:
			TextParameter.NameText = FText::FromString(Equipment->GetItemName());
			break;

		case ESelectViewType::NameAndValue:
			TextParameter.NameText = FText::FromString(Equipment->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(Equipment->GetValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;

		case ESelectViewType::NameAndCountAndSellValue:
			TextParameter.NameText = FText::FromString(Equipment->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(Equipment->GetValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;
		}
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示所持アイテムを設定
/// </summary>
void UBaseSelectWidget::SetHaveItems(const TArray<UHaveItem*>& NewHaveItems, ESelectViewType SelectViewType)
{
	SetMaxIndex(NewHaveItems.Num() - 1);
	TextParameters.Empty();

	for (UHaveItem* HaveItem : NewHaveItems)
	{
		FSelectWidgetTextParameter TextParameter;
		switch (SelectViewType)
		{
		case ESelectViewType::NameOnly:
		case ESelectViewType::NameAndMP:
			TextParameter.NameText = FText::FromString(HaveItem->GetItem()->GetItemName());
			break;

		case ESelectViewType::NameAndCount:
			TextParameter.NameText = FText::FromString(HaveItem->GetItem()->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveItem->GetNum()));
			TextParameter.Unit1 = FText::FromString(TEXT("個"));
			break;

		case ESelectViewType::NameAndValue:
			TextParameter.NameText = FText::FromString(HaveItem->GetItem()->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveItem->GetItem()->GetSellValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;

		case ESelectViewType::NameAndCountAndSellValue:
			TextParameter.NameText = FText::FromString(HaveItem->GetItem()->GetItemName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveItem->GetItem()->GetSellValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			TextParameter.Value2 = FText::FromString(FString::FromInt(HaveItem->GetNum()));
			TextParameter.Unit2 = FText::FromString(TEXT("個"));
			break;
		}
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示所持装備を設定
/// </summary>
void UBaseSelectWidget::SetHaveEquipments(const TArray<FHaveEquipment>& NewHaveEquipments, ESelectViewType SelectViewType)
{
	SetMaxIndex(NewHaveEquipments.Num() - 1);
	TextParameters.Empty();

	for (FHaveEquipment HaveEquipment : NewHaveEquipments)
	{
		FSelectWidgetTextParameter TextParameter;
		switch (SelectViewType)
		{
		case ESelectViewType::NameOnly:
		case ESelectViewType::NameAndMP:
			TextParameter.NameText = FText::FromString(HaveEquipment.Equipment->GetItemName());
			break;

		case ESelectViewType::NameAndCount:
			TextParameter.NameText = FText::FromString(HaveEquipment.Equipment->GetItemName());
			if (HaveEquipment.Equipment->GetItemType() == EItemType::NoEquipmwnt)
				break;
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveEquipment.Num));
			TextParameter.Unit1 = FText::FromString(TEXT("個"));
			break;

		case ESelectViewType::NameAndValue:
			TextParameter.NameText = FText::FromString(HaveEquipment.Equipment->GetItemName());
			if (HaveEquipment.Equipment->GetItemType() == EItemType::NoEquipmwnt)
				break;
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveEquipment.Equipment->GetSellValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			break;

		case ESelectViewType::NameAndCountAndSellValue:
			TextParameter.NameText = FText::FromString(HaveEquipment.Equipment->GetItemName());
			if (HaveEquipment.Equipment->GetItemType() == EItemType::NoEquipmwnt)
				break;
			TextParameter.Value1 = FText::FromString(FString::FromInt(HaveEquipment.Equipment->GetSellValue()));
			TextParameter.Unit1 = FText::FromString(TEXT("G"));
			TextParameter.Value2 = FText::FromString(FString::FromInt(HaveEquipment.Num));
			TextParameter.Unit2 = FText::FromString(TEXT("個"));
			break;
		}
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}

/// <summary>
/// 表示スキルを設定
/// </summary>
void UBaseSelectWidget::SetSkills(const TArray<USkill*>& NewSkill, ESelectViewType SelectViewType)
{
	SetMaxIndex(NewSkill.Num() - 1);
	TextParameters.Empty();

	for (USkill* Skill : NewSkill)
	{
		FSelectWidgetTextParameter TextParameter;
		switch (SelectViewType)
		{
		case ESelectViewType::NameOnly:
		case ESelectViewType::NameAndCount:
		case ESelectViewType::NameAndValue:
		case ESelectViewType::NameAndCountAndSellValue:
			TextParameter.NameText = FText::FromString(Skill->GetSkillName());
			break;

		case ESelectViewType::NameAndMP:
			TextParameter.NameText = FText::FromString(Skill->GetSkillName());
			TextParameter.Value1 = FText::FromString(FString::FromInt(Skill->GetSkillParameter().Mp));
			break;
		}
		TextParameters.Add(TextParameter);
	}
	UpdateWindow();
}
