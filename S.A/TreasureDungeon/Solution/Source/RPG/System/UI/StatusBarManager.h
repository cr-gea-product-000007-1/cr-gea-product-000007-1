﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../../Character/BattlePlayerStatus.h"
#include "StatusBarWidget.h"
#include "StatusBarManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UStatusBarManager : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ウィジット生成
	/// </summary>
	/// <param name="Players">設定するプレイヤー</param>
	void CreateStatusWidgets(TArray<UBattlePlayerStatus*> Players);

	/// <summary>
	/// ステータスを設定
	/// </summary>
	/// <param name="Player">設定するプレイヤー</param>
	void SetStatus(UBattlePlayerStatus* Player);

	/// <summary>
	/// 全員のステータスを設定
	/// </summary>
	/// <param name="Player">設定するプレイヤー</param>
	void SetAllStatus(TArray<UBattlePlayerStatus*> Players);

	/// <summary>
	/// 表示位置にスライドする
	/// </summary>
	void ViewSlide();

	/// <summary>
	/// 非表示位置にスライドする
	/// </summary>
	void NonViewSlide();

	/// <summary>
	/// 非表示位置にする
	/// </summary>
	void Hide();

private:
	/// <summary>
	/// ステータスバー
	/// </summary>
	UPROPERTY()
	TMap<UBattlePlayerStatus*, UStatusBarWidget*> Statuses;
};
