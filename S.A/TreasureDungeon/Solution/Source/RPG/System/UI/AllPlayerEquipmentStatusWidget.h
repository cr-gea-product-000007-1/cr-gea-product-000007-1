﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/Equipment.h"
#include "OpenWidget.h"
#include "WindowParameter.h"
#include "AllPlayerEquipmentStatusWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UAllPlayerEquipmentStatusWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter, FVector2D SideWindowSize);

	/// <summary>
	/// 装備するプレイヤーを設定する
	/// </summary>
	/// <param name="NewPlayerStatus">装備するプレイヤー</param>
	void SetPlayerStatuses(TArray<UBattlePlayerStatus*> NewPlayerStatuses) { PlayerStatuses = NewPlayerStatuses; };

	/// <summary>
	/// 表示装備を設定
	/// </summary>
	/// <param name="NewHaveEquipments">表示装備</param>
	void SetEquipment(UEquipment* NewEquipment) { Equipment = NewEquipment; };

	/// <summary>
	/// ウインドウを更新
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateWindow();

	/// <summary>
	/// 描画位置の設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetCanvasPannelRenderTranslation(FVector2D RenderTranslation);

	/// <summary>
	/// 左上の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetUpperRightCorner();

public:
	/// <summary>
	/// 装備するプレイヤー
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	TArray<UBattlePlayerStatus*> PlayerStatuses;

	/// <summary>
	/// 表示する装備
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	UEquipment* Equipment;

	/// <summary>
	/// ウインドウの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FVector2D WindowSize;
};
