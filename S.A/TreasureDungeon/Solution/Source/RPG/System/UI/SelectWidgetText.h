﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SelectWidgetText.generated.h"



USTRUCT(Blueprintable)
struct FSelectWidgetTextParameter
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// 名前
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText NameText;

	/// <summary>
	/// 数値1
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Value1;

	/// <summary>
	/// 数値2
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Value2;

	/// <summary>
	/// 単位1
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Unit1;

	/// <summary>
	/// 単位2
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Unit2;
};

UCLASS()
class RPG_API USelectWidgetText : public UUserWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// テキストの大きさを指定
	/// </summary>
	/// <param name="FontSize">文字の大きさ</param>
	/// <param name="Lenght">テキストの長さ</param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetTextParameter(int FontSize, float Lenght);

	/// <summary>
	/// 表示テキストを設定
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetText(FSelectWidgetTextParameter TextParameter);

public:
	/// <summary>
	/// テキストの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	FVector2D TextSize;

private:
	/// <summary
	/// 表示するテキスト
	/// </summary
	FSelectWidgetTextParameter NameText;
};
