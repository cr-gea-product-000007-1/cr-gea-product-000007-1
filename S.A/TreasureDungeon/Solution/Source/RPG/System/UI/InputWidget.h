﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OpenWidget.h"
#include "InputWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UInputWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	virtual bool OnDecide() { return false; };

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	virtual bool OnUp() { return false; };

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	virtual bool OnDown() { return false; };

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	virtual bool OnLeft() { return false; };

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	virtual bool OnRight() { return false; };

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	virtual void ActiveArrow() {};

	/// <summary>
	/// 矢印を止める
	/// </summary>
	virtual void StopArrow() {};

	/// <summary>
	/// ウインドウを更新する
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void UpdateWindow();
};
