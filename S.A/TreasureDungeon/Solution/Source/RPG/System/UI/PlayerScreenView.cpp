﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerScreenView.h"
#include "../../Character/CharacterMeshPath.h"


/// <summary>
/// プレイヤーを生成
/// </summary>
void APlayerScreenView::CreatePlayer(TArray<UBattleCharacterStatus*> PlayerStatuses)
{
	FString Path;
	for (UBattleCharacterStatus* PlayerStatus : PlayerStatuses)
	{
		EPlayerID PlayerID = (EPlayerID)PlayerStatus->GetCharacterID();
		if (Path::PlayerMeshPaths.Contains(PlayerID))
		{
			Path = Path::PlayerMeshPaths[PlayerID];
			TSubclassOf<ABattleCharacterMesh> MeshClass = TSoftClassPtr<ABattleCharacterMesh>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(MeshClass))
			{
				ABattleCharacterMesh* PlayerMesh = GetWorld()->SpawnActor<ABattleCharacterMesh>(MeshClass);
				PlayerMesh->SetActorLocation(GetActorLocation());
			}
		}
	}
}