﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputWidget.h"
#include "MessageBoxWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UMessageBoxWidget : public UInputWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// メッセージ表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewMessage(const TArray<FString>& Message);

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void MoveArrow();

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopArrow();

	/// <summary>
	/// ウィジットのアニメーション中かを返す
	/// </summary>
	/// <returns>ウィジットのアニメーション中 = true</returns>
	bool IsWidgetAnimation() { return bIsWidgetAnimation; };

	/// <summary>
	/// 文字のアニメーション中かを返す
	/// </summary>
	/// <returns>文字のアニメーション中 = true</returns>
	bool IsTextAnimation() { return bIsTextAnimation; };

	/// <summary>
	/// 文字を左揃えに
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetTextJustificationLeft();

	/// <summary>
	/// 文字を中央揃えに
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetTextJustificationCenter();

public:
	/// <summary>
	/// ウィジットのアニメーション中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsWidgetAnimation;

	/// <summary>
	/// 文字のアニメーション中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsTextAnimation;
};
