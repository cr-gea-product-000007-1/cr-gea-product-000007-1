﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OpenWidget.h"
#include "WindowParameter.h"
#include "DescriptionWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UDescriptionWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter);

	/// <summary>
	/// 表示を設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetText(const FString& String);

	/// <summary>
	/// 描画位置の設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetCanvasPannelRenderTranslation(FVector2D RenderTranslation);

	/// <summary>
	/// ウインドウの大きさ取得
	/// </summary>
	FVector2D GetWindowSize() { return WindowSize; };

	/// <summary>
	/// 右上の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetUpperRightCorner();

	/// <summary>
	/// 左下の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetLowerLeftCorner();

public:
	/// <summary>
	/// ウインドウの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FVector2D WindowSize;
};
