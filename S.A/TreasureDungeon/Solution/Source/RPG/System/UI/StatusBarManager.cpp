﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusBarManager.h"
#include "../../PathConstant.h"


/// <summary>
/// ウィジット生成
/// </summary>
/// <param name="Players">設定するプレイヤー</param>
void UStatusBarManager::CreateStatusWidgets(TArray<UBattlePlayerStatus*> Players)
{
	FString Path = Path::StatusBarWidgetPath;
	int Num = 0;
	for (UBattlePlayerStatus* Player : Players)
	{
		TSubclassOf<UStatusBarWidget> StatusBarWidgetClass = TSoftClassPtr<UStatusBarWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(StatusBarWidgetClass))
		{
			UStatusBarWidget* StatusBarWidget = CreateWidget<UStatusBarWidget>(GetWorld(), StatusBarWidgetClass);
			StatusBarWidget->SetRenderTranslation(FVector2D(1550.0f, 850 - Num * 300));
			StatusBarWidget->AddToViewport();
			StatusBarWidget->SetStatus(Player, true);
			Statuses.Add(TTuple<UBattlePlayerStatus*, UStatusBarWidget*>(Player, StatusBarWidget));
		};
		Num++;
	}
}

/// <summary>
/// ステータスを設定
/// </summary>
/// <param name="Players">設定するプレイヤー</param>
void UStatusBarManager::SetStatus(UBattlePlayerStatus* Player)
{
	if (Statuses.Contains(Player))
		Statuses[Player]->SetStatus(Player);
}

/// <summary>
/// 全員のステータスを設定
/// </summary>
/// <param name="Player">設定するプレイヤー</param>
void UStatusBarManager::SetAllStatus(TArray<UBattlePlayerStatus*> Players)
{
	for (UBattlePlayerStatus* Player : Players)
	{
		if (Statuses.Contains(Player))
			Statuses[Player]->SetStatus(Player);
	}
}

/// <summary>
/// 表示位置にスライドする
/// </summary>
void UStatusBarManager::ViewSlide()
{
	for (TTuple<UBattleCharacterStatus*, UStatusBarWidget*>Status : Statuses)
	{
		Status.Value->ViewSlide();
	}
}

/// <summary>
/// 非表示位置にスライドする
/// </summary>
void UStatusBarManager::NonViewSlide()
{
	for (TTuple<UBattleCharacterStatus*, UStatusBarWidget*>Status : Statuses)
	{
		Status.Value->NonViewSlide();
	}
}

/// <summary>
/// 非表示位置にする
/// </summary>
void UStatusBarManager::Hide()
{
	for (TTuple<UBattleCharacterStatus*, UStatusBarWidget*>Status : Statuses)
	{
		Status.Value->Hide();
	}
}