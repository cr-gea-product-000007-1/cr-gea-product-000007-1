﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OneLineSelectWidget.h"


/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UOneLineSelectWidget::SetSelectIndex(int Index)
{
	SelectIndex = Index;
	PageIndex = 0;
	UpdateWindow();
}

/// <summary>
/// 選んでいるインデックスを範囲内に合わせる
/// </summary>
void UOneLineSelectWidget::ClampSelectIndex()
{
	if (SelectIndex > MaxIndex)
	{
		SelectIndex = MaxIndex;
		PageIndex = FMath::Min(PageNum - 1, MaxIndex);
	}
	if (SelectIndex < 0)
	{
		SelectIndex = 0;
		PageIndex = 0;
	}
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UOneLineSelectWidget::OnUp()
{
	SelectIndex--;
	if (PageIndex > 0)
		PageIndex--;
	//上下ループ
	if (SelectIndex < 0)
	{
		SelectIndex = MaxIndex;
		PageIndex = FMath::Min(PageNum - 1, MaxIndex);
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UOneLineSelectWidget::OnDown()
{
	SelectIndex++;
	if (PageIndex < PageNum - 1)
		PageIndex++;
	//上下ループ
	if (SelectIndex > MaxIndex)
	{
		SelectIndex = 0;
		PageIndex = 0;
	}
	UpdateWindow();
	return true;
}
