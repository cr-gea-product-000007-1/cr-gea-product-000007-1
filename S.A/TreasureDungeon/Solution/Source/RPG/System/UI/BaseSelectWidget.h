﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputWidget.h"
#include "WindowParameter.h"
#include "../../Character/BattleCharacter.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/BaseItem.h"
#include "../../Item/Equipment.h"
#include "../../Item/HaveItem.h"
#include "../../Skill/Skill.h"
#include "SelectWidgetText.h"
#include "BaseSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UBaseSelectWidget : public UInputWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter);

	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	virtual void SetSelectIndex(int Index) {};

	/// <summary>
	/// 選んでいるインデックスを範囲内に合わせる
	/// </summary>
	virtual void ClampSelectIndex() {};

	/// <summary>
	/// インデックスの最大値を設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	virtual void SetMaxIndex(int Index) { MaxIndex = Index; };

	/// <summary>
	/// 選んでいるインデックスを取得
	/// </summary>
	/// <return> 選んでいるインデックス </return>
	int GetSelectIndex() { return SelectIndex; };

	/// <summary>
	/// 表示テキストを設定
	/// </summary>
	void SetText(const TArray <FString>& Texts);

	/// <summary>
	/// 表示テキストを追加
	/// </summary>
	void AddText(const FString& Text);

	/// <summary>
	/// 表示キャラクターを設定
	/// </summary>
	void SetCharacter(const TArray<UBattleCharacter*>& NewCharacters);

	/// <summary>
	/// 表示プレイヤーを設定
	/// </summary>
	void SetPlayerStatus(const TArray<UBattlePlayerStatus*>& NewStatuses);

	/// <summary>
	/// 表示アイテムを設定
	/// </summary>
	void SetItems(const TArray<UBaseItem*>& NewBaseItems, ESelectViewType SelectViewType);

	/// <summary>
	/// 表示装備を設定
	/// </summary>
	void SetEquipments(const TArray<UEquipment*>& NewEquipments, ESelectViewType SelectViewType);

	/// <summary>
	/// 表示所持アイテムを設定
	/// </summary>
	void SetHaveItems(const TArray<UHaveItem*>& NewHaveItems, ESelectViewType SelectViewType);

	/// <summary>
	/// 表示所持装備を設定
	/// </summary>
	void SetHaveEquipments(const TArray<FHaveEquipment>& NewHaveEquipments, ESelectViewType SelectViewType);

	/// <summary>
	/// 表示スキルを設定
	/// </summary>
	void SetSkills(const TArray<USkill*>& NewSkill, ESelectViewType SelectViewType);

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ActiveArrow() override;

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopArrow() override;

	/// <summary>
	/// ウインドウの大きさを取得
	/// </summary>
	FVector2D GetWindowSize() { return WindowSize; };

	/// <summary>
	/// 描画位置の設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetCanvasPannelRenderTranslation(FVector2D RenderTranslation);

	/// <summary>
	/// 描画位置の中心設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetCanvasPannelRenderTranslationCenter(FVector2D RenderTranslation);

	/// <summary>
	/// 左上の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetUpperLeftCorner();

	/// <summary>
	/// 右上の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetUpperRightCorner();

	/// <summary>
	/// 左下の角の座標取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FVector2D GetLowerLeftCorner();

	/// <summary>
	/// 矢印の中心座標取得
	/// </summary>
	FVector2D GetArrowCorner() { return ArrowCenter; };

	/// <summary>
	/// 追加ウインドウ設定
	/// </summary>
	/// <param name="SubWidget"></param>
	void AddSubWidget(UOpenWidget* SubWidget) { SubWidgets.Add(SubWidget); };

public:
	/// <summary>
	/// 現在のインデックス
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	int SelectIndex;

	/// <summary>
	/// インデックスの最大値
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	int MaxIndex;

	/// <summary>
	/// 表示するテキスト
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	TArray<FSelectWidgetTextParameter> TextParameters;

	/// <summary>
	/// ウインドウの大きさ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FVector2D WindowSize;

	/// <summary>
	/// 矢印の初期中心位置
	/// </summary>
	UPROPERTY(BlueprintreadWrite)
	FVector2D ArrowCenter;

	/// <summary>
	/// 追加ウインドウ
	/// </summary>
	UPROPERTY(BlueprintreadWrite)
	TArray<UOpenWidget*> SubWidgets;
};

