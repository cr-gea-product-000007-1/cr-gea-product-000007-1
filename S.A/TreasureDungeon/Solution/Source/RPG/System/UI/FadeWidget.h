﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OpenWidget.h"
#include "FadeWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UFadeWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// フェードインを再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayFadeIn();

	/// <summary>
	/// フェードアウトを再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayFadeOut();

	/// <summary>
	/// アニメーションの再生中か
	/// </summary>
	bool IsAnimation() { return bIsAnimation; };

public:
	/// <summary>
	/// アニメーションの再生中か
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	bool bIsAnimation = false;
};
