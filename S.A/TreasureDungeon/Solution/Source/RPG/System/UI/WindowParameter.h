﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "WindowParameter.generated.h"


USTRUCT(Blueprintable)
struct FSelectWidgetParameter
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// 文字のフォントサイズ
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FontSize;

	/// <summary>
	/// 文字の間隔
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int TextSpace;

	/// <summary>
	/// 項目数
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int ItemNum;

	/// <summary>
	/// 最大文字数
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxTextNum;
};

/// <summary>
/// デフォルトのフォントサイズ
/// </summary>
const int SelectTextFontSize = 28;

/// <summary>
/// デフォルトの行間
/// </summary>
const int SelectTextSpace = 50;

UENUM(BlueprintType)
enum class ESelectViewType : uint8
{
	NameOnly,                   //名前のみ
	NameAndCount,               //名前と個数
	NameAndMP,                  //名前と消費MP
	NameAndValue,               //名前と買値
	NameAndCountAndSellValue,   //名前と所持個数と売値
};
