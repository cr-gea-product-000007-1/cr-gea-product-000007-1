﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "StatusBarWidget.generated.h"

class UBattlePlayerStatus;

/**
 * 
 */
UCLASS()
class RPG_API UStatusBarWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ステータスを設定
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetStatus(UBattlePlayerStatus* Status, bool bSlideComplete = false);

	/// <summary>
	/// 表示位置にスライドする
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewSlide();

	/// <summary>
	/// 非表示位置にスライドする
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void NonViewSlide();

	/// <summary>
	/// 非表示位置にする
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Hide();
};
