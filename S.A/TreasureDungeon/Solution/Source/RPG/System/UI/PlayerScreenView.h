﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Character/BattleCharacterMesh.h"
#include "../../Character/BattleCharacterStatus.h"
#include "PlayerScreenView.generated.h"

UCLASS()
class RPG_API APlayerScreenView : public AActor
{
	GENERATED_BODY()
	
public:	
	/// <summary>
	/// プレイヤーを生成
	/// </summary>
	void CreatePlayer(TArray<UBattleCharacterStatus*> PlayerStatuses);

private:
	/// <summary>
	/// プレイヤーメッシュ
	/// </summary>
	UPROPERTY()
	TArray<ABattleCharacterMesh*> PlayerMeshs;
};
