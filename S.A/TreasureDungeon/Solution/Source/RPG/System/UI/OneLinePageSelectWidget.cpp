﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OneLinePageSelectWidget.h"


/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UOneLinePageSelectWidget::SetSelectIndex(int Index)
{
	if (Index < 0) return;
	if (Index > MaxIndex) return;

	SelectIndex = Index;
	UpdatePageIndex();
	UpdateWindow();
}

/// <summary>
/// 選んでいるインデックスを範囲内に合わせる
/// </summary>
void UOneLinePageSelectWidget::ClampSelectIndex()
{
	if (SelectIndex > MaxIndex)
	{
		SelectIndex = MaxIndex;
	}
	if (SelectIndex < 0)
	{
		SelectIndex = 0;
	}
	UpdatePageIndex();
}

/// <summary>
/// インデックスの最大値を設定
/// </summary>
/// <param name="Index">インデックス</param>
void UOneLinePageSelectWidget::SetMaxIndex(int Index)
{
	MaxIndex = Index;
	PageMaxNum = FMath::CeilToInt((float)(MaxIndex + 1) / (float)PageItemNum);
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
bool UOneLinePageSelectWidget::OnUp()
{
	PageIndex--;
	if (PageIndex < 0)
		PageIndex = PageItemNum - 1;
	UpdateSelectIndex();

	if (SelectIndex > MaxIndex)
	{
		int OverIndex = SelectIndex - MaxIndex;
		SelectIndex -= OverIndex;
		PageIndex -= OverIndex;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
bool UOneLinePageSelectWidget::OnDown()
{
	PageIndex++;
	if (PageIndex >= PageItemNum)
		PageIndex = 0;
	UpdateSelectIndex();

	if (SelectIndex > MaxIndex)
	{
		PageIndex = 0;
		UpdateSelectIndex();
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
bool UOneLinePageSelectWidget::OnLeft()
{
	PageNum--;
	//ページループ
	if (PageNum >= 0)
	{
		UpdateSelectIndex();
		UpdateWindow();
		return true;
	}
	PageNum = PageMaxNum - 1;
	UpdateSelectIndex();

	//インデックスはみ出し
	if (SelectIndex <= MaxIndex)
	{
		UpdateWindow();
		return true;
	}
	SelectIndex = MaxIndex;
	UpdatePageIndex();

	UpdateWindow();
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
bool UOneLinePageSelectWidget::OnRight()
{
	PageNum++;
	UpdateSelectIndex();
	//ページループ
	if (PageNum >= PageMaxNum)
	{
		PageNum = 0;
		UpdateSelectIndex();
	}
	if (SelectIndex > MaxIndex)
	{
		int OverIndex = SelectIndex - MaxIndex;
		SelectIndex -= OverIndex;
		PageIndex -= OverIndex;
	}
	if (SelectIndex < 0)
	{
		SelectIndex = 0;
		PageIndex = 0;
	}
	else
	{
		UpdateSelectIndex();
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 選んでいるインデックスをページ位置から設定する
/// </summary>
void UOneLinePageSelectWidget::UpdateSelectIndex()
{
	//ページ数＊ページ項目数＋現在の位置
	SelectIndex = PageItemNum * PageNum + PageIndex;
}

/// <summary>
/// ページ位置を選んでいるインデックスから設定する
/// </summary>
void UOneLinePageSelectWidget::UpdatePageIndex()
{
	if (PageMaxNum == 0)
	{
		PageNum = 0;
		PageIndex = 0;
		return;
	}
	PageNum = SelectIndex / PageItemNum;
	PageIndex = SelectIndex - PageNum * PageItemNum;
}