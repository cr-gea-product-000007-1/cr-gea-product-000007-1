﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSelectWidget.h"
#include "OneLineSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UOneLineSelectWidget : public UBaseSelectWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	UFUNCTION(BlueprintCallable)
	void SetSelectIndex(int Index) override;

	/// <summary>
	/// 選んでいるインデックスを範囲内に合わせる
	/// </summary>
	void ClampSelectIndex() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnDown() override;

public:
	/// <summary>
	/// テキストの間隔
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	float TextSpace;

	/// <summary>
	/// 1ページの表示数
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageNum;

	/// <summary>
	/// ページ内のインデックス
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageIndex;
};
