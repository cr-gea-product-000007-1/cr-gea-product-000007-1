﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSelectWidget.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/BaseItem.h"
#include "../../Item/Equipment.h"
#include "../../Item/HaveItem.h"
#include "../../Skill/Skill.h"
#include "TwoLineSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UTwoLineSelectWidget : public UBaseSelectWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	UFUNCTION(BlueprintCallable)
	void SetSelectIndex(int Index) override;

	/// <summary>
	/// 選んでいるインデックスを範囲内に合わせる
	/// </summary>
	void ClampSelectIndex() override;

	/// <summary>
	/// インデックスの最大値を設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	 void SetMaxIndex(int Index) override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnRight() override;

private:
	/// <summary>
	/// 選んでいるインデックスをページ位置から設定する
	/// </summary>
	void UpdateSelectIndex();

	/// <summary>
	/// ページ位置を選んでいるインデックスから設定する
	/// </summary>
	void UpdatePageIndex();

public:
	/// <summary>
	/// 現在のページ数
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageNum;

	/// <summary>
	/// ページの総数
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageMaxNum;

	/// <summary>
	/// ページ内のXインデックス
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageXIndex;

	/// <summary>
	/// ページ内のYインデックス
	UPROPERTY(BlueprintReadwrite)
	int PageYIndex;

	/// <summary>
	/// 1ページのX個数
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageXNum;

	/// <summary>
	/// 1ページのY個数
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int PageYNum;

	/// <summary>
	/// テキストのX間隔
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	float TextXSpace;

	/// <summary>
	/// テキストのY間隔
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	float TextYSpace;
};

