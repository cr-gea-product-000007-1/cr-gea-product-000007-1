﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/GameplayStatics.h"
#include "../RPGGameInstance.h"
#include "../Character/BattlePlayerStatus.h"
#include "../Skill/Skill.h"
#include "../Item/Weapon.h"
#include "../Item/Protector.h"
#include "../System/Interface/LoadDataInterface.h"
#include "CreateFirstPlayer.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UCreateFirstPlayer : public UObject, public ILoadDataInterface
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// 初期プレイヤーを生成する
	/// </summary>
	void PlayerCreate();

private:
	/// <summary>
	/// スキルを取得
	/// </summary>
	/// <param name="SkilID">スキルのID</param>
	USkill* GetSkill(ESkillID SkillID);

	/// <summary>
	/// アイテムを取得
	/// </summary>
	/// <param name="ItemID">アイテムのID</param>
	UItem* GetItem(EItemID ItemID);

	/// <summary>
	/// 武器を取得
	/// </summary>
	/// <param name="WeaponID">武器のID</param>
	UWeapon* GetWeapon(EItemID WeaponID);

	/// <summary>
	/// 防具を取得
	/// </summary>
	/// <param name="ProtectorID">防具のID</param>
	UProtector* GetProtector(EItemID ProtectorID);

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	URPGGameInstance* GameInstance;
};
