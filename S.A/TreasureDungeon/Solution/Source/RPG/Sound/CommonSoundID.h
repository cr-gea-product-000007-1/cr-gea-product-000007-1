﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonSoundID.generated.h"

UENUM(BlueprintType)
enum class ECommonSoundID : uint8
{
    None,
	Decision,
	Cancel,
	CursorMove,
	Victory,
	Fanfare,
	NormalBattleBGM,
};
