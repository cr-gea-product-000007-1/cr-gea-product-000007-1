﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "Sound/SoundBase.h"
#include "CommonSoundID.h"
#include "CommonSoundObject.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FCommonSoundObject : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// サウンドID
	/// </summary>
	UPROPERTY(EditAnywhere)
	ECommonSoundID SoundID;

	/// <summary>
	/// サウンド
	/// </summary>
	UPROPERTY(EditAnywhere)
	USoundBase* Sound;
};