﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonSoundObject.h"
#include "SoundPlayer.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API USoundPlayer : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 決定サウンド再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayDecisionSound(UObject* Obj);

	/// <summary>
	/// キャンセルサウンド再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayCancelSound(UObject* Obj);

	/// <summary>
	/// カーソル移動サウンド再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayCursorMoveSound(UObject* Obj);

	/// <summary>
	/// 勝利サウンド再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayVictorySound(UObject* Obj);

	/// <summary>
	/// ファンファーレサウンド再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayFanfareSound(UObject* Obj);

	/// <summary>
	/// サウンドの再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	/// <param name="Sound">再生するサウンド</param>
	/// <param name="FadeTime">フェードインする時間</param>
	/// <param name="Pitch">サウンドのピッチ</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlaySound(UObject* Obj, USoundBase* Sound, float FadeTime = 0.0f, float Pitch = 1.0f);

private:
	/// <summary>
	/// サウンドの読み込み
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	bool LoadSound(UObject* Obj);

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
private:
	/// <summary>
	/// データを読み込んでいるか
	/// </summary>
	bool bIsLoad = false;

	/// <summary>
	/// 決定サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* DecisionSound;

	/// <summary>
	/// キャンセルサウンド
	/// </summary>
	UPROPERTY()
	USoundBase* CancelSound;

	/// <summary>
	/// カーソル移動サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* CursorMoveSound;

	/// <summary>
	/// 勝利サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* VictorySound;

	/// <summary>
	/// ファンファーレサウンド
	/// </summary>
	UPROPERTY()
	USoundBase* FanfareSound;

	/// <summary>
	/// 音量
	/// </summary>
	float* Volue;
	
};
