﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SoundPlayer.h"
#include "../RPGGameInstance.h"
#include "Kismet/GameplayStatics.h"


/// <summary>
/// 決定サウンド再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
void USoundPlayer::PlayDecisionSound(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;
	PlaySound(Obj, DecisionSound);
}

/// <summary>
/// キャンセルサウンド再生
/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
void USoundPlayer::PlayCancelSound(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;
	PlaySound(Obj, CancelSound);
}

/// <summary>
/// カーソル移動サウンド再生
/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
void USoundPlayer::PlayCursorMoveSound(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;
	PlaySound(Obj, CursorMoveSound);
}

/// <summary>
/// 勝利サウンド再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
void USoundPlayer::PlayVictorySound(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;
	PlaySound(Obj, VictorySound);
}

/// <summary>
/// ファンファーレサウンド再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
void USoundPlayer::PlayFanfareSound(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;
	PlaySound(Obj, FanfareSound);
}

/// <summary>
/// サウンドの再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
/// <param name="Sound">再生するサウンド</param>
/// <param name="Pitch">サウンドのピッチ</param>
void USoundPlayer::PlaySound(UObject* Obj, USoundBase* Sound, float FadeTime, float Pitch)
{
	if (!bIsLoad)
		if (!LoadSound(Obj)) return;

	if (FadeTime == 0.0f)
	{
		UGameplayStatics::PlaySound2D(Obj->GetWorld(), Sound, *Volue, Pitch, 0.0f);
	}
	else {
		UAudioComponent* Audio = UGameplayStatics::SpawnSound2D(Obj->GetWorld(), Sound, *Volue, Pitch, 0.0f);
		Audio->FadeIn(FadeTime, *Volue, 0.0f);
	}
}

/// <summary>
/// サウンドの読み込み
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
bool USoundPlayer::LoadSound(UObject* Obj)
{
	URPGGameInstance* GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(Obj->GetWorld()));
	if (!IsValid(GameInstance)) return false;

	DecisionSound = GameInstance->DecisionSound;
	CancelSound = GameInstance->CancelSound;
	CursorMoveSound = GameInstance->CursorMoveSound;
	VictorySound = GameInstance->VictorySound;
	FanfareSound = GameInstance->FanfareSound;
	Volue = &GameInstance->SE_Volue;

	bIsLoad = true;

	return true;
}
