﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Components/AudioComponent.h"
#include "BGMPlayer.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UBGMPlayer : public UObject
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 通常戦闘曲の再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayNormalBattleBGM(UObject* Obj);

	/// <summary>
	/// BGMの再生
	/// </summary>
	/// <param name="Obj">再生するオブジェクト</param>
	/// <param name="BGM">再生するBGM</param>
	/// <param name="FadeTime">フェードインする時間</param>
	/// <param name="Pitch">BGMのピッチ</param>
	UFUNCTION(BlueprintCallable)
	virtual void PlayBGM(UObject* Obj, USoundBase* BGM, float FadeTime = 0.0f, float Pitch = 1.0f);

	/// <summary>
	/// BGMの停止
	/// </summary>
	UFUNCTION(BlueprintCallable)
	virtual void StopBGM();

	/// <summary>
	/// BGMの音量更新
	/// </summary>
	UFUNCTION(BlueprintCallable)
	virtual void VolumeUpdate();

private:
	/// <summary>
	/// BGMの読み込み
	/// </summary>
	/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
	bool LoadBGM(UObject* Obj);

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
private:
	/// <summary>
	/// データを読み込んでいるか
	/// </summary>
	bool bIsLoad = false;

	/// <summary>
	/// 再生中か
	/// </summary>
	bool bIsPlay = false;

	/// <summary>
	/// 通常戦闘曲
	/// </summary>
	UPROPERTY()
	USoundBase* NormalBattleBGM;

	/// <summary>
	/// 再生しているBGM
	/// </summary>
	UPROPERTY()
	UAudioComponent* PlayingBGM;

	/// <summary>
	/// 音量
	/// </summary>
	float* Volue;
};
