﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BGMPlayer.h"
#include "../RPGGameInstance.h"
#include "Kismet/GameplayStatics.h"

// Add default functionality here for any UBGMPlayer functions that are not pure virtual.

/// <summary>
/// 通常戦闘曲の再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
void UBGMPlayer::PlayNormalBattleBGM(UObject* Obj)
{
	if (!bIsLoad)
		if (!LoadBGM(Obj)) return;
	PlayBGM(Obj, NormalBattleBGM);
}

/// <summary>
/// BGMの再生
/// </summary>
/// <param name="Obj">再生するオブジェクト</param>
/// <param name="BGM">再生するBGM</param>
/// <param name="Pitch">BGMのピッチ</param>
void UBGMPlayer::PlayBGM(UObject* Obj, USoundBase* BGM, float FadeTime, float Pitch)
{
	if (!bIsLoad)
		if (!LoadBGM(Obj)) return;
	PlayingBGM = UGameplayStatics::SpawnSound2D(Obj->GetWorld(), BGM, *Volue, Pitch, 0.0f, nullptr, false, true);

	if (FadeTime == 0.0f)
	{
		PlayingBGM->Play();
		PlayingBGM->AdjustVolume(0.0f, *Volue);
	}
	else {
		PlayingBGM->FadeIn(FadeTime, *Volue, 0.0f);
	}
	bIsPlay = true;
}

/// <summary>
/// BGMの停止
/// </summary>
void UBGMPlayer::StopBGM()
{
	PlayingBGM->Stop();
	bIsPlay = false;
}

/// <summary>
/// BGMの音量更新
/// </summary>
void UBGMPlayer::VolumeUpdate()
{
	if (bIsPlay)
		PlayingBGM->AdjustVolume(1.0f, *Volue);
	else
		PlayingBGM->FadeIn(0.5f, *Volue, 0.0f);

	if (*Volue == 0.0f)
		bIsPlay = false;
	else
		bIsPlay = true;;
}

/// <summary>
/// BGMの読み込み
/// </summary>
/// <param name="Obj">GameInstanceを取得するオブジェクト</param>
bool UBGMPlayer::LoadBGM(UObject* Obj)
{
	URPGGameInstance* GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(Obj->GetWorld()));
	if (!IsValid(GameInstance)) return false;

	NormalBattleBGM = GameInstance->NormalBattleBGM;
	Volue = &GameInstance->BGM_Volue;

	bIsLoad = true;

	return true;
}
