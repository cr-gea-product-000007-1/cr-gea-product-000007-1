// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayBGMLevelScriptActor.h"


void APlayBGMLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	BGMPlayer = NewObject<UBGMPlayer>();
	BGMPlayer->PlayBGM(this, BGM, FadeInTime);
}