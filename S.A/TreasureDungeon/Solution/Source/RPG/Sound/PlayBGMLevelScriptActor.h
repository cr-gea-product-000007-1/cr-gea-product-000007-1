﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "BGMPlayer.h"
#include "PlayBGMLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API APlayBGMLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

public:
	void BeginPlay() override;

private:
	/// <summary>
	/// BGM
	/// </summary>
	UPROPERTY(EditAnywhere)
	USoundBase* BGM;

	/// <summary>
	/// BGMがフェードインする時間
	/// </summary>
	UPROPERTY(EditAnywhere)
	float FadeInTime;

	/// <summary>
	/// BGMを再生する
	/// </summary>
	UPROPERTY()
	UBGMPlayer* BGMPlayer;
};
