﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Character/BattleCharacter.h"
#include "BattleCameraActor.h"
#include "BattleCameraManager.generated.h"


UCLASS()
class RPG_API ABattleCameraManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABattleCameraManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// カメラ取得
	/// </summary>
	ABattleCameraActor* GetCamera();

	/// <summary>
	/// 後ろからプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewBackPlayers();

	/// <summary>
	/// 前からプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewFrontPlayers();

	/// <summary>
	/// 後ろから敵陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewBackEnemys();

	/// <summary>
	/// 前から敵陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewFrontEnemys();

	/// <summary>
	/// 後ろから指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewBackCharacter(UBattleCharacter* Character);

	/// <summary>
	/// 前から指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ViewFrontCharacter(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewPlayerBackCharacter(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターの陣営を後ろから写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewBackCharacters(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターの陣営を前から写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewFrontCharacters(UBattleCharacter* Character);

public:
	/// <summary>
	/// カメラ
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	ABattleCameraActor* Camera;
};
