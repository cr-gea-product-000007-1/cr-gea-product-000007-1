﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../../Character/StatusData/EnemyParameter.h"
#include "LoadBattleDataInterface.generated.h"

class UBattleCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class ULoadBattleDataInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API ILoadBattleDataInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// プレイヤーキャラリストを取得
	/// </summary>
	virtual TArray<UBattleCharacter*> GetPlayerCharacters() = 0;

	/// <summary>
	/// 敵キャラリストを取得
	/// </summary>
	virtual TArray<UBattleCharacter*> GetEnemyCharacters() = 0;

	/// <summary>
	/// 生きているプレイヤーキャラリストを取得
	/// </summary>
	virtual TArray<UBattleCharacter*> GetLivingPlayerCharacters() = 0;

	/// <summary>
	/// 生きている敵キャラリストを取得
	/// </summary>
	virtual TArray<UBattleCharacter*> GetLivingEnemyCharacters() = 0;
};
