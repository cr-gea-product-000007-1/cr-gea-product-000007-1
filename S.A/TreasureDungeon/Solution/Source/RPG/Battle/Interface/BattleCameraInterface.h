﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BattleCameraInterface.generated.h"

class UBattleCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBattleCameraInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IBattleCameraInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 後ろからプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	virtual void ViewBackPlayers() = 0;

	/// <summary>
	/// 前からプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	virtual void ViewFrontPlayers() = 0;

	/// <summary>
	/// 後ろから敵陣営を写す位置にカメラ移動
	/// </summary
	virtual	void ViewBackEnemys() = 0;

	/// <summary>
	/// 前から敵陣営を写す位置にカメラ移動
	/// </summary
	virtual	void ViewFrontEnemys() = 0;

	/// <summary>
	/// 後ろから指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	virtual	void ViewBackCharacter(UBattleCharacter* Character) = 0;

	/// <summary>
	/// 前から指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	virtual	void ViewFrontCharacter(UBattleCharacter* Character) = 0;

	/// <summary>
	/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	virtual	void ViewPlayerBackCharacter(UBattleCharacter* Character) = 0;

	/// <summary>
	/// キャラクターの陣営を後ろから写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	virtual void ViewBackCharacters(UBattleCharacter* Character) = 0;

	/// <summary>
	/// キャラクターの陣営を前から写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	virtual void ViewFrontCharacters(UBattleCharacter* Character) = 0;
};
