﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleLevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "../Sound/CommonSoundObject.h"
#include "../PathConstant.h"


// コンストラクタ
ABattleLevelScriptActor::ABattleLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABattleLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	BattlePhase = ABattleLevelScriptActor::BattlePhaseType::Initialize;

	BattleManager = NewObject<UBattleManager>(this);
}

void ABattleLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(BattleManager))
	{
		UE_LOG(LogTemp, Display, TEXT("BattleManager Error"));
		return;
	}
	//フェーズごとの処理
	switch (BattlePhase)
	{
	case ABattleLevelScriptActor::Initialize:
		//テストデータ読み込み
		LoadDataForGameInstance();

		if(BattleManager->InitializeEvent())
			BattlePhase = ABattleLevelScriptActor::Create;
		break;

	case ABattleLevelScriptActor::Create:
		if (BattleManager->CreateEvent())
			BattlePhase = ABattleLevelScriptActor::FadeOut;
		break;

	case ABattleLevelScriptActor::FadeOut:
		if (BattleManager->FadeOutEvent())
			BattlePhase = ABattleLevelScriptActor::Battle;
		break;

	case ABattleLevelScriptActor::Battle:
		if (BattleManager->BattleEvent())
		{
			if(BattleManager->IsWin())
				BattlePhase = ABattleLevelScriptActor::Win;

			if (BattleManager->IsLose())
				BattlePhase = ABattleLevelScriptActor::Lose;
		}
		break;

	case ABattleLevelScriptActor::Win:
		if (BattleManager->WinEvent(DeltaTime))
			BattlePhase = ABattleLevelScriptActor::FadeIn;
		break;

	case ABattleLevelScriptActor::Lose:
		if (BattleManager->LoseEvent())
			BattlePhase = ABattleLevelScriptActor::FadeIn;
		break;

	case ABattleLevelScriptActor::FadeIn:
		if (BattleManager->FadeInEvent())
			BattlePhase = ABattleLevelScriptActor::Finalize;
		break;

	case ABattleLevelScriptActor::Finalize:
		BattleManager->FinalizeEvent();
		break;
	}
}

/// <summary>
/// ゲームインスタンスのデータをデータテーブルから読み込む
/// （テスト用）
/// </summary>
void ABattleLevelScriptActor::LoadDataForGameInstance()
{
}
