﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleManager.h"
#include "Kismet/GameplayStatics.h"
#include "../Skill/Skill.h"
#include "BattleController.h"
#include "BattleConstant.h"
#include "../PathConstant.h"
#include "Engine/DataTable.h"


const float PlayerY = -400.0f;
const float EnemyXSpace = 250.0f;
const float EnemyY = 400.0f;

/// <summary>
/// 開始時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::InitializeEvent()
{
	switch (StartPhase)
	{
	case EStartPhase::Initialize:
	{
		GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		TurnNum = 1;

		//フェード生成
		FString Path = Path::NormalFadeWidgetPath;
		TSubclassOf<UFadeWidget> FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(FadeWidgetClass))
		{
			FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
			FadeWidget->AddToViewport(1);
		};
		BGMPlayer = NewObject<UBGMPlayer>();

		StartPhase = EStartPhase::AnimationStart;
		break;
	}
	case EStartPhase::AnimationStart:
		StartPhase = EStartPhase::AnimationPlaying;
		break;

	case EStartPhase::AnimationPlaying:
		StartPhase = EStartPhase::Finish;
		break;

	case EStartPhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// 生成の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::CreateEvent()
{
	if (!IsValid(GameInstance)) return false;
	switch (CreatePhase)
	{
	case ECreatePhase::PlayerCreate:
	{
		UBattleCharacter* PlayerCharacter = NewObject<UBattleCharacter>(this);
		UBattlePlayerStatus* PlayerStatus = NewObject<UBattlePlayerStatus>(this);
		PlayerStatus = *GameInstance->PlayerStatuses.begin();

		PlayerCharacter->SetStatus(PlayerStatus);
		PlayerCharacter->SetBattleCharacterInterface(this);
		PlayerCharacter->SetReceiveUnderMessageInterface(this);
		PlayerStatus->SetLoadDataInterface(this);
		ABattleCharacterMesh* Mesh = PlayerCharacter->CreateMesh();
		Mesh->SetActorLocation(FVector(0.0f, PlayerY, 0.0f));
		Mesh->FirstLocationSetting();

		PlayerCharacters.Add(PlayerCharacter);

		CreatePhase = ECreatePhase::EnemyCreate;
		break;
	}
	case ECreatePhase::EnemyCreate:
	{
		TArray<EEnemyID>EncountEnemyIDGroup = GameInstance->EncountEnemyIDGroup;
		TMap<EEnemyID, int>EnemyTypeCount;
		int Num = 0;
		for(EEnemyID EnemyID : EncountEnemyIDGroup)
		{
			UBattleCharacter* EnemyCharacter = NewObject<UBattleCharacter>(this);
			UBattleEnemyStatus* EnemyStatus = NewObject<UBattleEnemyStatus>(this);
			int Count = 0;
			if (EnemyTypeCount.Contains(EnemyID))
				Count = EnemyTypeCount[EnemyID];
			else
				EnemyTypeCount.Add(EnemyID);
			EnemyTypeCount[EnemyID]++;
			EnemyStatus->SetEnemyStatus(GetEnemyParameter(this, EnemyID), Count);
			EnemyCharacter->SetReceiveUnderMessageInterface(this);
			EnemyStatus->SetLoadDataInterface(this);
			EnemyCharacter->SetStatus(EnemyStatus);
			ABattleCharacterMesh* Mesh = EnemyCharacter->CreateMesh();
			float X = ((float)EncountEnemyIDGroup.Num() / 2.0f - Num - 0.5f) * EnemyXSpace;
			Mesh->SetActorLocation(FVector(X, EnemyY, 0.0f));
			Mesh->FirstLocationSetting();
			EnemyCharacters.Add(EnemyCharacter);
			Num++;
		}

		CreatePhase = ECreatePhase::UICreate;
		break;
	}
	case ECreatePhase::UICreate:
	{
		FString Path;
		//メッセージ生成
		Path = Path::UnderMessageWidgetPath;
		TSubclassOf<UUnderMessageWidget> UnderMessageWidgetClass = TSoftClassPtr<UUnderMessageWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(UnderMessageWidgetClass))
		{
			UnderMessageWidget = CreateWidget<UUnderMessageWidget>(GetWorld(), UnderMessageWidgetClass);
			UnderMessageWidget->AddToViewport();
		};

		//ステータスUI生成
		CommandManager = NewObject<UCommandManager>(this);
		CommandManager->WidgetCreate();
		CommandManager->SetLoadDataInterface(this);
		CommandManager->SetLoadBattleDataInterface(this);

		StatusBarManager = NewObject<UStatusBarManager>(this);
		TArray<UBattlePlayerStatus*> Players = GetPlayerStatuses();
		StatusBarManager->CreateStatusWidgets(Players);

		//戦闘リザルト生成
		BattleResultManager = NewObject<UBattleResultManager>(this);
		BattleResultManager->WidgetCreate();
		BattleResultManager->SetBattleCharacterInterface(this);

		ABattleController* BattleController = Cast<ABattleController>(GetWorld()->GetFirstPlayerController());
		if (IsValid(BattleController))
		{
			BattleController->SetCommandInputInterface(CommandManager);
			BattleController->SetBattleResultInterface(BattleResultManager);
		}
		CreatePhase = ECreatePhase::CameraCreate;
		break;
	}
	case ECreatePhase::CameraCreate:
	{
		FString Path = Path::BattleCameraPath;
		TSubclassOf<ABattleCameraManager> CameraClass = TSoftClassPtr<ABattleCameraManager>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(CameraClass))
		{
			BattleCameraManager = GetWorld()->SpawnActor<ABattleCameraManager>(CameraClass);
		}

		CreatePhase = ECreatePhase::Finish;
		return true;
	}
	case ECreatePhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// フェードアウトの処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::FadeOutEvent()
{
	switch (FadeOutPhase)
	{
	case EFadeOutPhase::Start:
		FadeWidget->PlayFadeOut();
		BGMPlayer->PlayNormalBattleBGM(this);
		FadeOutPhase = EFadeOutPhase::FadeOut;
		break;

	case EFadeOutPhase::FadeOut:
		if(!FadeWidget->IsAnimation())
			FadeOutPhase = EFadeOutPhase::Finish;
		break;

	case EFadeOutPhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// 戦闘中の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::BattleEvent()
{
	switch (TurnPhase)
	{
	case ETurnPhase::TurnStart:
		ViewBackPlayers();
		TurnPhase = ETurnPhase::CommandStart;
		break;

	case ETurnPhase::CommandStart:
	{
		UBattleCharacter* Player = *PlayerCharacters.begin();
		UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(Player->GetStatus());
		if (IsValid(PlayerStatus))
		{
			CommandManager->SetPlayerStatus(PlayerStatus);
		}
		CommandManager->SetUserPlayer(Player);
		CommandManager->SetHaveItem();

		//最後に置くこと
		CommandManager->CommandSelectStart();

		TurnPhase = ETurnPhase::CommandInput;
		break;
	}
	case ETurnPhase::CommandInput:
	{
		if(CommandManager->IsSelectSkill())
			TurnPhase = ETurnPhase::CommandFinish;
		break;
	}
	case ETurnPhase::CommandFinish:
	{
		UUseTurnSkill* UseSkill = CommandManager->GetSelectSkill();
		UBattleCharacter* PlayerCharacter = *PlayerCharacters.begin();
		UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(PlayerCharacter->GetStatus());
		if(IsValid(PlayerStatus))
			UseSkill->SetPriority(PlayerStatus->GetFinallyStatus().Agility);
		UseTurnSkills.Add(UseSkill);
		TurnPhase = ETurnPhase::EnemyActionSelect;
		break;
	}
	case ETurnPhase::EnemyActionSelect:
	{
		for (int i = 0; i < EnemyCharacters.Num(); i++)
		{
			UBattleCharacter* EnemyCharacter = EnemyCharacters[i];
			UBattleEnemyStatus* EnemyStatus = Cast<UBattleEnemyStatus>(EnemyCharacters[i]->GetStatus());
			if (!IsValid(EnemyStatus)) continue;
			if (EnemyStatus->IsDie()) continue;

			UUseTurnSkill* UseSkill = NewObject<UUseTurnSkill>(this);

			ESkillID SkillID = EnemyStatus->GetUseSkillID(0);
			USkill* Skill = GetSkill(this, SkillID);

			TArray<UBattleCharacter*>Targets = GetEnemySkillTarget(EnemyCharacter, Skill->GetSkillParameter().SkillTarget);

			UseSkill->SetUseSkill(Skill);
			UseSkill->SetSkillUser(EnemyCharacter);
			UseSkill->SetSkillTargets(Targets);
			UseSkill->SetPriority(EnemyStatus->GetFinallyStatus().Agility);

			UseTurnSkills.Add(UseSkill);
		}

		TurnPhase = ETurnPhase::ActionSort;
		break;
	}
	case ETurnPhase::ActionSort:
		//早い順に並び替え
		for (int i = 0; i < UseTurnSkills.Num() - 1; i++) {

			for (int j = i + 1; j < UseTurnSkills.Num(); j++) {
				UUseTurnSkill*& Skill1 = UseTurnSkills[i];
				UUseTurnSkill*& Skill2 = UseTurnSkills[j];
				if (Skill1->GetPriority() > Skill2->GetPriority()) continue;
				//同速ならランダム
				if (Skill1->GetPriority() == Skill2->GetPriority() && FMath::RandBool()) continue;
				UUseTurnSkill* Tmp = Skill1;
				Skill1 = Skill2;
				Skill2 = Tmp;
			}
		}
		TurnPhase = ETurnPhase::Action;
		break;

	case ETurnPhase::Action:
		if(ActionEvent())
			if(!UnderMessageWidget->IsViewMessage())
				TurnPhase = ETurnPhase::NextAction;
		break;

	case ETurnPhase::NextAction:
		//戦闘終了判定
		BattleJudge();
		if (BattleFinishType != EBattleFinishType::None)
			return true;

		//次の行動へ
		if (UseTurnSkills.Num() > 0)
		{
			ActionPhase = EActionPhase::ActionStart;
			TurnPhase = ETurnPhase::Action;
		}
		else
			TurnPhase = ETurnPhase::TrunEnd;
		break;

	case ETurnPhase::TrunEnd:
		TurnNum++;
		TurnPhase = ETurnPhase::TurnStart;
		break;
	}

	return false;

}

/// <summary>
/// 勝利時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::WinEvent(float DeltaTime)
{
	switch (WinPhase)
	{
	case EWinPhase::Start:
	{
		BGMPlayer->StopBGM();
		ViewFrontPlayers();
		EnemyDrop();
		//経験値獲得
		TArray<UBattleCharacter*>LevelUpCharacters;
		for (UBattleCharacter* PlayerCharacter : PlayerCharacters)
		{
			UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(PlayerCharacter->GetStatus());
			if (!IsValid(PlayerStatus)) continue;
			if (PlayerStatus->IsDie()) continue;
			if (PlayerStatus->AddExp(DropExp))
				LevelUpCharacters.Add(PlayerCharacter);
		}
		//ゴールド獲得
		GameInstance->Gold += DropGold;
		//アイテム情報取得
		TArray<UBaseItem*>DropItems;
		for (EItemID DropItemId : DropItemIds)
		{
			UBaseItem* DropItem = GetBaseItem(this, DropItemId);
			GameInstance->ItemStorage->InsertBaseItem(DropItem, 1);
			DropItems.Add(DropItem);
		}
		BattleResultManager->WinOpen();
		BattleResultManager->SetDropExp(DropExp);
		BattleResultManager->SetDropGold(DropGold);
		BattleResultManager->SetDropItems(DropItems);
		BattleResultManager->SetLevelUpPlayers(LevelUpCharacters);
		WinPhase = EWinPhase::Result;
		break;
	}
	case EWinPhase::Result:
		BattleResultManager->Update(DeltaTime);
		if(BattleResultManager->IsFinish())
			WinPhase = EWinPhase::Finish;
		break;

	case EWinPhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// 敗北時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::LoseEvent()
{
	switch (LosePhase)
	{
	case ELosePhase::Start:
		ViewFrontPlayers();
		BattleResultManager->LoseOpen();
		GameInstance->bBattleLose = true;
		LosePhase = ELosePhase::LoseMessage;
		break;

	case ELosePhase::LoseMessage:
		BattleResultManager->Update(0);
		if (BattleResultManager->IsFinish())
			LosePhase = ELosePhase::Finish;
		break;

	case ELosePhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// フェードインの処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::FadeInEvent()
{
	switch (FadeInPhase)
	{
	case EFadeInPhase::Start:
		FadeWidget->PlayFadeIn();
		FadeInPhase = EFadeInPhase::FadeIn;

	case EFadeInPhase::FadeIn:
		if (!FadeWidget->IsAnimation())
		FadeInPhase = EFadeInPhase::Finish;
		break;

	case EFadeInPhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// 終了時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::FinalizeEvent()
{
	if (GameInstance->bBattleLose)
	{
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("GameOverLevel"), true);
		return true;
	}

	if (!GameInstance->bGameClear)
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("FieldLevel"), true);
	else
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("EndingLevel"), true);
	return true;
}

/// <summary>
/// 行動の処理
/// </summary>
/// <return> 終了 = true </return>
bool UBattleManager::ActionEvent()
{
	switch (ActionPhase)
	{
	case EActionPhase::ActionStart:
	{
		//スキル取得
		ActionSkill = *(UseTurnSkills.begin());
		USkill* UseSkill = ActionSkill->GetUseSkil();

		//使用者取得
		UBattleCharacter* User = ActionSkill->GetSkillUser();
		UBattleCharacterStatus* UserStatus = User->GetStatus();

		//死んでいたら飛ばす
		if (UserStatus->IsDie())
		{
			//使用スキル削除
			UseTurnSkills.Remove(ActionSkill);

			ActionPhase = EActionPhase::ActionFinish;
			break;
		}

		//使用者の状態で実際に使用するスキル取得
		EUseSkillType UseSkillType = UserStatus->GetActionSkill(UseSkill);
		switch (UseSkillType)
		{
		case EUseSkillType::DefaultUseSkill:
			if (UseSkill->GetSkillParameter().SkillID == BattleConstant::DefaultNormalAttackSkill)
			{
				NormalMessage(UserStatus);
			}
			else {
				SkillMessage(UserStatus, UseSkill);
			}
			break;

		case EUseSkillType::AbilityFumble:
			SkillMessage(UserStatus, UseSkill);
			FumbleMessage();
			UseSkill = GetSkill(this,  ESkillID::AbilityFumble);
			break;

		case EUseSkillType::MagicFumble:
			SkillMessage(UserStatus, UseSkill);
			FumbleMessage();
			UseSkill = GetSkill(this, ESkillID::MagicFumble);
			break;
		}

		//MP消費
		UserStatus->UseMp(UseSkill->GetSkillParameter().Mp);
		if (User->IsPlayerCharacter())
		{
			UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(User->GetStatus());
			if (IsValid(PlayerStatus))
				UpdateStatusBar(PlayerStatus);
		}
		//アイテム使用
		UBaseItem* UseItem = ActionSkill->GetUseItem();
		if (IsValid(UseItem))
			GameInstance->ItemStorage->LostBaseItem(UseItem, 1);

		//対象者取得
		TArray <UBattleCharacter*> TargetCharacters = ActionSkill->GetAllSkillTargets();

		//ダメージ判定
		for (UBattleCharacter* Target : TargetCharacters)
		{
			ABattleCharacterMesh* TargetMesh = Target->GetMesh();
			UBattleCharacterStatus* TargetStatus = Target->GetStatus();

			if (TargetStatus->IsDie()) continue;

			int Damage = 0;
			if (UseSkill->IsHitSkill(UserStatus, TargetStatus))
				Damage = UseSkill->GetDamage(UserStatus, TargetStatus);

			TargetStatus->GiveDamage(Damage);
		}

		//エフェクト再生
		PlaytEffect = UseSkill->UseEffect(User, TargetCharacters);
		PlaytEffect->SetBattleCameraInterface(this);

		//使用スキル削除
		UseTurnSkills.Remove(ActionSkill);

		ActionPhase = EActionPhase::ActionEffect;
		break;
	}
	case EActionPhase::ActionEffect:
		if (PlaytEffect->IsFinish())
		{
			PlaytEffect->Destroy();
			ActionPhase = EActionPhase::ActionFinish;
		}
		break;

	case EActionPhase::ActionFinish:
		return true;
	}

	return false;
}

/// <summary>
/// 戦闘終了判定
/// </summary>
void UBattleManager::BattleJudge()
{
	bool bIsLose = true;
	for (UBattleCharacterStatus* PlayerStatus : GetPlayerStatuses())
	{
		if (!PlayerStatus->IsDie())
			bIsLose = false;
	}
	if (bIsLose)
	{
		BattleFinishType = EBattleFinishType::Lose;
		return;
	}

	bool bIsWin = true;
	for (UBattleCharacterStatus* EnemyStatus : GetEnemyStatuses())
	{
		if (!EnemyStatus->IsDie())
			bIsWin = false;
	}
	if (bIsWin)
	{
		BattleFinishType = EBattleFinishType::Win;
	}
}

/// <summary>
/// 味方のステータス一覧取得
/// </summary>
/// <returns>味方のステータス一覧</returns>
TArray<UBattlePlayerStatus*> UBattleManager::GetPlayerStatuses()
{
	TArray<UBattlePlayerStatus*>Statuses;
	for (UBattleCharacter* PlayerCharacter : PlayerCharacters)
	{
		UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(PlayerCharacter->GetStatus());
		if(IsValid(PlayerStatus))
			Statuses.Add(PlayerStatus);
	}
	return Statuses;
}

/// <summary>
/// 敵のステータス一覧取得
/// </summary>
/// <returns>敵のステータス一覧</returns>
TArray<UBattleEnemyStatus*> UBattleManager::GetEnemyStatuses()
{
	TArray<UBattleEnemyStatus*>Statuses;
	for (UBattleCharacter* EnemyCharacter : EnemyCharacters)
	{
		UBattleEnemyStatus* EnemyStatus = Cast<UBattleEnemyStatus>(EnemyCharacter->GetStatus());
		if (IsValid(EnemyStatus))
			Statuses.Add(EnemyStatus);
	}
	return Statuses;
}

/// <summary>
/// 敵の行動の対象選択
/// </summary>
TArray<UBattleCharacter*> UBattleManager::GetEnemySkillTarget(UBattleCharacter* User, ESkillTarget Target)
{
	TArray<UBattleCharacter*>Targets;

	switch (Target)
	{
	case ESkillTarget::My:
		Targets.Add(User);
		break;

	case ESkillTarget::OneMember:
	{
		UBattleCharacter* EnemyCharacter = EnemyCharacters[FMath::Rand() % EnemyCharacters.Num()];
		Targets.Add(EnemyCharacter);
		break;
	}
	case ESkillTarget::AllMenbers:
		Targets = GetEnemyCharacters();
		break;

	case ESkillTarget::OneEnemy:
	{
		UBattleCharacter* PlayerCharacter = PlayerCharacters[FMath::Rand() % PlayerCharacters.Num()];
		Targets.Add(PlayerCharacter);
		break;
	}
	case ESkillTarget::AllEnemys:
		Targets = GetPlayerCharacters();
		break;
	}

	return Targets;
}

/// <summary>
/// 通常攻撃のメッセージの表示
/// </summary>
void UBattleManager::NormalMessage(UBattleCharacterStatus* UserStatus)
{
	ShowMessage(UserStatus->GetCharacterName() + TEXT(" の 攻撃 !!"));
}

/// <summary>
/// スキル使用のメッセージの表示
/// </summary>
/// <param name="User">使用者</param>
/// <param name="UseSkill">スキル</param>
void UBattleManager::SkillMessage(UBattleCharacterStatus* UserStatus, USkill* UseSkill)
{
	ShowMessage(UserStatus->GetCharacterName() + TEXT(" の ") + UseSkill->GetSkillParameter().Name + TEXT("発動 !!"));
}

/// <summary>
/// MP不足のメッセージの表示
/// </summary>
void UBattleManager::FumbleMessage()
{
	ShowMessage(TEXT("MPが足りず 失敗 ..."));
}

/// <summary>
/// 敵からの獲得物設定
/// </summary>
void UBattleManager::EnemyDrop()
{
	DropExp = 0;
	DropGold = 0;
	DropItemIds.Empty();

	for (UBattleEnemyStatus* EnemyStatus : GetEnemyStatuses())
	{
		DropExp += EnemyStatus->GetExp();
		DropGold += EnemyStatus->GetGold();
		EItemID DropItemId = EnemyStatus->GetDropItemID();
		if (DropItemId != EItemID::None)
			DropItemIds.Add(DropItemId);
	}
}


//------------------------------------------
// インターフェースイベント
//------------------------------------------


/// <summary>
/// プレイヤーキャラリストを取得
/// </summary>
/// <param name="SkillID">プレイヤーキャラリスト</param>
TArray<UBattleCharacter*> UBattleManager::GetPlayerCharacters()
{
	return PlayerCharacters;
}

/// <summary>
/// 敵キャラリストを取得
/// </summary>
/// <param name="SkillID">敵キャラリスト</param>
TArray<UBattleCharacter*> UBattleManager::GetEnemyCharacters()
{
	return EnemyCharacters;
}

/// <summary>
/// 生きているプレイヤーキャラリストを取得
/// </summary>
TArray<UBattleCharacter*> UBattleManager::GetLivingPlayerCharacters()
{
	TArray<UBattleCharacter*> CopyPlayerCharacters = GetPlayerCharacters();
	TArray<UBattleCharacter*> LivingPlayerCharacters = CopyPlayerCharacters.FilterByPredicate([](UBattleCharacter* Player)
		{ return Player->GetStatus(); });

	return LivingPlayerCharacters;
}

/// <summary>
/// 生きている敵キャラリストを取得
/// </summary>
TArray<UBattleCharacter*> UBattleManager::GetLivingEnemyCharacters()
{
	TArray<UBattleCharacter*> CopyEnemyCharacters = GetEnemyCharacters();
	TArray<UBattleCharacter*> LivingEnemyCharacters = CopyEnemyCharacters.FilterByPredicate([](UBattleCharacter* Enemy)
		{ return !Enemy->GetStatus()->IsDie(); });

	return LivingEnemyCharacters;
}

/// <summary>
/// メッセージ表示
/// </summary>
/// <param name="Message">表示するメッセージ</param>
void UBattleManager::ShowMessage(const FString& Message)
{
	if (IsValid(UnderMessageWidget))
	{
		UnderMessageWidget->AddMessage(Message);
	}
}

/// <summary>
/// ステータスバーを更新するイベント
/// </summary>
void UBattleManager::UpdateStatusBar(UBattlePlayerStatus* Status)
{
	StatusBarManager->SetStatus(Status);
}

/// <summary>
/// 後ろからプレイヤーを写す位置にカメラ移動
/// </summary>
void UBattleManager::ViewBackPlayers()
{
	BattleCameraManager->ViewBackPlayers();
}

/// <summary>
/// 前からプレイヤー陣営を写す位置にカメラ移動
/// </summary>
void UBattleManager::ViewFrontPlayers()
{
	BattleCameraManager->ViewFrontPlayers();
}

/// <summary>
/// 後ろから敵を写す位置にカメラ移動
/// </summary>
void UBattleManager::ViewBackEnemys()
{
	BattleCameraManager->ViewBackEnemys();
}

/// <summary>
/// 前から敵陣営を写す位置にカメラ移動
/// </summary>
void UBattleManager::ViewFrontEnemys()
{
	BattleCameraManager->ViewFrontEnemys();
}

/// <summary>
/// 後ろから指定したキャラクターを写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void UBattleManager::ViewBackCharacter(UBattleCharacter* Character)
{
	BattleCameraManager->ViewBackCharacter(Character);
}

/// <summary>
/// 前から指定したキャラクターを写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void UBattleManager::ViewFrontCharacter(UBattleCharacter* Character)
{
	BattleCameraManager->ViewFrontCharacter(Character);
}

/// <summary>
/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void UBattleManager::ViewPlayerBackCharacter(UBattleCharacter* Character)
{
	BattleCameraManager->ViewPlayerBackCharacter(Character);
}

/// <summary>
/// キャラクターの陣営を後ろから写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void UBattleManager::ViewBackCharacters(UBattleCharacter* Character)
{
	BattleCameraManager->ViewBackCharacters(Character);
}

/// <summary>
/// キャラクターの陣営を前から写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void UBattleManager::ViewFrontCharacters(UBattleCharacter* Character)
{
	BattleCameraManager->ViewFrontCharacters(Character);
}