﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleController.h"


ABattleController::ABattleController()
{
    bAutoManageActiveCameraTarget = false;
}

//SetupInputComponentの継承
void ABattleController::SetupInputComponent()
{
    Super::SetupInputComponent();
    check(InputComponent); //UE4のアサートマクロ

    if (IsValid(InputComponent))
    {
        InputComponent->BindAction("Decide", IE_Pressed, this, &ABattleController::OnDecide);
        InputComponent->BindAction("Cancel", IE_Pressed, this, &ABattleController::OnCancel);
        InputComponent->BindAction("Up", IE_Pressed, this, &ABattleController::OnUp);
        InputComponent->BindAction("Down", IE_Pressed, this, &ABattleController::OnDown);
        InputComponent->BindAction("Left", IE_Pressed, this, &ABattleController::OnLeft);
        InputComponent->BindAction("Right", IE_Pressed, this, &ABattleController::OnRight);
    }
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ABattleController::OnDecide()
{
    if (CommandInputInterface == nullptr) return;
    if (BattleResultInterface == nullptr) return;

    CommandInputInterface->OnDecide();
    BattleResultInterface->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void ABattleController::OnCancel()
{
    if (CommandInputInterface == nullptr) return;
    CommandInputInterface->OnCancel();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void ABattleController::OnUp()
{
    if (CommandInputInterface == nullptr) return;

    CommandInputInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void ABattleController::OnDown()
{
    if (CommandInputInterface == nullptr) return;

    CommandInputInterface->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void ABattleController::OnLeft()
{
    if (CommandInputInterface == nullptr) return;

    CommandInputInterface->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void ABattleController::OnRight()
{
    if (CommandInputInterface == nullptr) return;

    CommandInputInterface->OnRight();
}
