﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../System/Interface/InputWidgetInterface.h"
#include "BattleController.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ABattleController : public APlayerController
{
	GENERATED_BODY()

	ABattleController();

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft();

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight();

public:
	/// <summary>
	/// コマンド入力インターフェースを設定する
	/// </summary>
	void SetCommandInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { CommandInputInterface = Interface; };

	/// <summary>
	/// 戦闘リザルトインターフェースを設定する
	/// </summary>
	void SetBattleResultInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { BattleResultInterface = Interface; };

private:
	/// <summary>
	/// コマンド入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> CommandInputInterface;

	/// <summary>
	/// 戦闘リザルトインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> BattleResultInterface;
};
