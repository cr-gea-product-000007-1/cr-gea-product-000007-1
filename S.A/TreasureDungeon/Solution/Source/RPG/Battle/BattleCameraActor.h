﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "BattleCameraType.h"
#include "../Character/BattleCharacter.h"
#include "BattleCameraActor.generated.h"


UENUM(BlueprintType)
enum class EBattleCameraType : uint8
{
	None,
	GoTotarget,
	FollowBackCharacter,
	FollowFrontCharacter,
	TeleportBackCharacter,
	TeleportFrontCharacter,
};

UCLASS()
class RPG_API ABattleCameraActor : public ACameraActor
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 写すキャラクターを指定
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void SetViewCharacter(UBattleCharacter* Character) { ViewCharacter = Character; };

	/// <summary>
	/// カメラの描画方法を指定
	/// </summary>
	/// <param name="NewBattleCameraType">カメラの描画方法</param>
	UFUNCTION(BlueprintCallable)
	void SetBattleCameraType(EBattleCameraType NewBattleCameraType) { BattleCameraType = NewBattleCameraType; };

public:
	/// <summary>
	/// 写す位置
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FTransform ViewTransform;

	/// <summary>
	/// 写すキャラクター
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	UBattleCharacter* ViewCharacter;
	
	/// <summary>
	/// カメラの描画方法
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	EBattleCameraType BattleCameraType;
};
