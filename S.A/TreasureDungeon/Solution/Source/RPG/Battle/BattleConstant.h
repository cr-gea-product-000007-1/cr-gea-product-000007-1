// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Skill/SkillID.h"

namespace BattleConstant
{
	const ESkillID DefaultNormalAttackSkill = ESkillID::NormalAttack;
	const ESkillID MagicFumbleSkill = ESkillID::MagicFumble;
	const ESkillID AbilityFumbleSkill = ESkillID::AbilityFumble;
}