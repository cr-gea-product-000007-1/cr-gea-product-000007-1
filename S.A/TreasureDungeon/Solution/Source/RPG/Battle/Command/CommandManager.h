﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "../../System/Interface/InputWidgetInterface.h"
#include "../../System/Interface/LoadDataInterface.h"
#include "../Interface/LoadBattleDataInterface.h"

#include "../../Character/BattleCharacter.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Skill/UseTurnSkill.h"

#include "../../System/UI/BaseSelectWidget.h"
#include "../../System/UI/DescriptionWidget.h"
#include "../../System/UI/ValueWidget.h"
#include "CommandIndexs.h"

#include "../../Sound/SoundPlayer.h"

#include "CommandManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UCommandManager : public UObject, public IInputWidgetInterface
{
	GENERATED_BODY()

	enum class EActiveWidgetType
	{
		None,
		CommandSelect,
		SkillSelect,
		ItemSelect,
		PlayerTargetSelect,
		EnemyTargetSelect,
	};

public:
	/// <summary>
	/// ウィジットを生成
	/// </summary>
	void WidgetCreate();

	/// <summary>
	/// コマンド入力を始める
	/// </summary>
	void CommandSelectStart();

	/// <summary>
	/// ウィジットを全て閉じる
	/// </summary>
	void AllClose();

	/// <summary>
	/// 表示するプレイヤーを設定する
	/// </summary>
	/// <param name="Status">プレイヤー</param>
	void SetPlayerCharacter(UBattleCharacter* Character) { Player = Character; };

	/// <summary>
	/// 表示するプレイヤーのステータスを設定する
	/// </summary>
	/// <param name="Status">プレイヤーのステータス</param>
	void SetPlayerStatus(UBattlePlayerStatus* Status);

	/// <summary>
	/// 使用プレイヤーを設定する
	/// </summary>
	/// <param name="Status">プレイヤー</param>
	void SetUserPlayer(UBattleCharacter* Character) { Player = Character; };

	/// <summary>
	/// 所持アイテムを設定する
	/// </summary>
	void SetHaveItem();

	/// <summary>
	/// スキルが選ばれているか
	/// </summary>
	/// <returns選んだスキル></returns>
	bool IsSelectSkill() { return SelectSkill->IsUsable(); };

	/// <summary>
	/// 選んだスキルを取得する
	/// </summary>
	/// <returns選んだスキル></returns>
	UUseTurnSkill* GetSelectSkill() { return SelectSkill; };

	/// <summary>
	/// データ読み込みインターフェースを設定
	/// </summary>
	void SetLoadDataInterface(ILoadDataInterface* Interface) { LoadDataInterface = Interface; };

	/// <summary>
	/// 戦闘データ読み込みインターフェースを設定
	/// </summary>
	void SetLoadBattleDataInterface(ILoadBattleDataInterface* Interface) { LoadBattleDataInterface = Interface; };

public:
	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnRight() override;

private:
	/// <summary>
	/// 次のウィジットに移動
	/// </summary>
	/// <param name="NextWidget">移動するウィジット</param>
	void NextWidget(UInputWidget* NextWidget);

	/// <summary>
	/// 前のウィジットに移動
	/// </summary>
	/// <return> 移動した = true </return>
	bool BackWidget();

	/// <summary>
	/// コマンドの選択
	/// </summary>
	/// <param name="ActionSelectType">選択するコマンド</param>
	void SelectCommand(EActionSelectType ActionSelectType);

	/// <summary>
	/// スキルの対象選択
	/// </summary>
	/// <param name="Skill">選択するスキル</param>
	void SkillTargetSelect(USkill* Skill);

	/// <summary>
	/// 対象選択ウィジットを通常攻撃対象選択位置に
	/// </summary>
	void SetTargetSelectWidgetPositionForNormalAttack();

	/// <summary>
	/// 対象選択ウィジットをスキル対象選択位置に
	/// </summary>
	void SetTargetSelectWidgetPositionForSkill();

	/// <summary>
	/// ウインドウの更新
	/// </summary>
	void WidgetUpdate();

	/// <summary>
	/// スキルウインドウの更新
	/// </summary>
	void SkillSelectWidgetUpdate();

	/// <summary>
	/// アイテムウインドウの更新
	/// </summary>
	void ItemSelectWidgetUpdate();

private:
	/// <summary>
	/// 開いているウインドウの種類
	/// </summary>
	EActiveWidgetType ActiveWidgetType;

	/// <summary>
	/// 開いているウインドウ
	/// </summary>
	UPROPERTY()
	UInputWidget* ActiveWidget;

	/// <summary>
	/// 開いたウインドウの種類の記録
	/// </summary>
	TArray<EActiveWidgetType> WidgetTypeLog;

	/// <summary>
	/// 開いたウインドウの記録
	/// </summary>
	UPROPERTY()
	TArray<UInputWidget*> WidgetLog;

	/// <summary>
	/// 表示するプレイヤー
	/// </summary>
	UPROPERTY()
	UBattleCharacter* Player;

	/// <summary>
	/// 表示するプレイヤーのステータス
	/// </summary>
	UPROPERTY()
	UBattlePlayerStatus* PlayerStatus;

	/// <summary>
	/// 選んだスキル
	/// </summary>
	UPROPERTY()
	UUseTurnSkill* SelectSkill;

	/// <summary>
	/// 行動を選ぶウインドウ
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* CommandActionSelectWidget;

	/// <summary>
	/// スキルを選ぶウインドウ
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* SkillSelectWidget;

	/// <summary>
	/// アイテムを選ぶウインドウ
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ItemSelectWidget;

	/// <summary>
	/// スキル説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* SkillDescriptionWidget;

	/// <summary>
	/// アイテム説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* ItemDescriptionWidget;

	/// <summary>
	/// アイテム個数ウィジット
	/// </summary>
	UPROPERTY()
	UValueWidget* ItemCountWidget;

	/// <summary>
	/// スキル消費MPウィジット
	/// </summary>
	UPROPERTY()
	UValueWidget* SkillMPWidget;

	/// <summary>
	/// 対象を選ぶウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* TargetSelectWidget;

	/// <summary>
	/// データ読み込みインターフェース
	/// </summary>
	ILoadDataInterface* LoadDataInterface;

	/// <summary>
	/// 戦闘データ読み込みインターフェース
	/// </summary>
	ILoadBattleDataInterface* LoadBattleDataInterface;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
