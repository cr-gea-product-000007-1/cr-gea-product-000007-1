﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommandIndexs.generated.h"


UENUM(BlueprintType)
enum class EActionSelectType : uint8
{
    NormalAttack = 0,
    Skill = 1,
    Item = 2,
    Guard = 3,

    Begin = 0,
    End = 3,
};