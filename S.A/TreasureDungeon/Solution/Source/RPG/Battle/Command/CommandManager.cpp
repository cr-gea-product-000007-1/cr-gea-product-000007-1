﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CommandManager.h"
#include "../../PathConstant.h"

const TArray<FString> CommandActionText = { TEXT("攻撃"), TEXT("スキル"), TEXT("道具")};
const FVector2D CommandWindowStartPosition = { 100.0f, 700.0f };

/// <summary>
/// ウィジットを生成
/// </summary>
void UCommandManager::WidgetCreate()
{
	FString Path;
	Path = Path::OneLineSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> SelectWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(SelectWidgetClass))
	{
		//行動選択
		CommandActionSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		CommandActionSelectWidget->AddToViewport();
		CommandActionSelectWidget->SetCanvasPannelRenderTranslation(CommandWindowStartPosition);
		CommandActionSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 3.0 });
		CommandActionSelectWidget->SetText(CommandActionText);
		CommandActionSelectWidget->Hide();
		//対象選択
		TargetSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		TargetSelectWidget->AddToViewport();
		TargetSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 4, 8.0 });
		TargetSelectWidget->SetText(CommandActionText);
		TargetSelectWidget->Hide();
	}
	Path = Path::TwoLineSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> TwoLineSelectWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(TwoLineSelectWidgetClass))
	{
		//スキル選択
		SkillSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), TwoLineSelectWidgetClass);
		SkillSelectWidget->AddToViewport();
		SkillSelectWidget->SetCanvasPannelRenderTranslation(CommandWindowStartPosition);
		SkillSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 4, 8.0 });
		SkillSelectWidget->SetText(CommandActionText);
		SkillSelectWidget->Hide();
		//アイテム選択
		ItemSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), TwoLineSelectWidgetClass);
		ItemSelectWidget->AddToViewport();
		ItemSelectWidget->SetCanvasPannelRenderTranslation(CommandWindowStartPosition);
		ItemSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 4, 8.0 });
		ItemSelectWidget->SetText(CommandActionText);
		ItemSelectWidget->Hide();
	}
	Path = Path::DescriptionWidgetPath;
	TSubclassOf<UDescriptionWidget> DescriptionWidgetClass = TSoftClassPtr<UDescriptionWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(DescriptionWidgetClass))
	{
		//スキル説明文
		SkillDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		SkillDescriptionWidget->AddToViewport();
		SkillDescriptionWidget->SetCanvasPannelRenderTranslation(SkillSelectWidget->GetUpperRightCorner());
		SkillDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 8.0f });
		SkillDescriptionWidget->Hide();
		SkillSelectWidget->AddSubWidget(SkillDescriptionWidget);
		//アイテム説明文
		ItemDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		ItemDescriptionWidget->AddToViewport();
		ItemDescriptionWidget->SetCanvasPannelRenderTranslation(ItemSelectWidget->GetUpperRightCorner());
		ItemDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 8.0f });
		ItemDescriptionWidget->Hide();
		ItemSelectWidget->AddSubWidget(ItemDescriptionWidget);
	};
	Path = Path::ValueWidgetPath;
	TSubclassOf<UValueWidget> ValueWidgetClass = TSoftClassPtr<UValueWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(ValueWidgetClass))
	{
		//アイテム個数
		ItemCountWidget = CreateWidget<UValueWidget>(GetWorld(), ValueWidgetClass);
		ItemCountWidget->AddToViewport();
		ItemCountWidget->SetRenderTranslation(ItemDescriptionWidget->GetLowerLeftCorner());
		ItemCountWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 8.0f });
		ItemCountWidget->SetItemUnit();
		ItemCountWidget->Hide();
		ItemSelectWidget->AddSubWidget(ItemCountWidget);
		//スキル消費MP
		SkillMPWidget = CreateWidget<UValueWidget>(GetWorld(), ValueWidgetClass);
		SkillMPWidget->AddToViewport();
		SkillMPWidget->SetRenderTranslation(SkillDescriptionWidget->GetLowerLeftCorner());
		SkillMPWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 8.0f });
		SkillMPWidget->SetMPUnit();
		SkillMPWidget->Hide();
		SkillSelectWidget->AddSubWidget(SkillMPWidget);
	};

	SoundPlayer = NewObject<USoundPlayer>();
}

/// <summary>
/// コマンド入力を始める
/// </summary>
void UCommandManager::CommandSelectStart()
{
	CommandActionSelectWidget->Open();
	ActiveWidgetType = EActiveWidgetType::CommandSelect;
	ActiveWidget = CommandActionSelectWidget;
	WidgetLog.Empty();
	WidgetTypeLog.Empty();

	SelectSkill = NewObject<UUseTurnSkill>();
	SelectSkill->SetSkillUser(Player);
}

/// <summary>
/// ウィジットを全て閉じる
/// </summary>
void UCommandManager::AllClose()
{
	ActiveWidgetType = EActiveWidgetType::None;
	ActiveWidget = nullptr;
	WidgetLog.Empty();
	WidgetTypeLog.Empty();

	if (CommandActionSelectWidget->IsOpen())
		CommandActionSelectWidget->Close();

	if (SkillSelectWidget->IsOpen())
		SkillSelectWidget->Close();

	if (ItemSelectWidget->IsOpen())
		ItemSelectWidget->Close();

	if (TargetSelectWidget->IsOpen())
		TargetSelectWidget->Close();
}

/// <summary>
/// 表示するプレイヤーのステータスを設定する
/// </summary>
/// <param name="Status">プレイヤーのステータス</param>
void UCommandManager::SetPlayerStatus(UBattlePlayerStatus* Status)
{
	PlayerStatus = Status;
	SkillSelectWidget->SetSkills(PlayerStatus->GetSkills(), ESelectViewType::NameOnly);
};

/// <summary>
/// 所持アイテムを設定する
/// </summary>
void UCommandManager::SetHaveItem()
{
	ItemSelectWidget->SetHaveItems(LoadDataInterface->GetHaveItems(this), ESelectViewType::NameAndCount);
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UCommandManager::OnDecide()
{
	if (!IsValid(ActiveWidget)) return;
	ActiveWidget->OnDecide();

	switch (ActiveWidgetType)
	{
	case UCommandManager::EActiveWidgetType::CommandSelect:
	{
		EActionSelectType ActionSelectType = (EActionSelectType)CommandActionSelectWidget->GetSelectIndex();
		SelectCommand(ActionSelectType);
		break;
	}
	case UCommandManager::EActiveWidgetType::SkillSelect:
	{
		USkill* Skill = PlayerStatus->GetSkills()[SkillSelectWidget->GetSelectIndex()];
		SelectSkill->SetUseSkill(Skill);
		SelectSkill->SetUseItem(nullptr);
		SkillTargetSelect(Skill);
		SetTargetSelectWidgetPositionForSkill();
		SkillDescriptionWidget->Close();
		SkillMPWidget->Close();
		SoundPlayer->PlayDecisionSound(this);
		SkillSelectWidget->StopArrow();
		break;
	}
	case UCommandManager::EActiveWidgetType::ItemSelect:
	{
		UBaseItem* BaseItem = LoadDataInterface->GetHaveItems(this)[ItemSelectWidget->GetSelectIndex()]->GetItem();
		UItem* Item = Cast<UItem>(BaseItem);
		USkill* Skill = LoadDataInterface->GetSkill(this, Item->GetSkillID());
		SelectSkill->SetUseSkill(Skill);
		SelectSkill->SetUseItem(Item);
		SkillTargetSelect(Skill);
		SetTargetSelectWidgetPositionForSkill();
		ItemDescriptionWidget->Close();
		ItemCountWidget->Close();
		SoundPlayer->PlayDecisionSound(this);
		ItemSelectWidget->StopArrow();
		break;
	}
	case UCommandManager::EActiveWidgetType::PlayerTargetSelect:
		SelectSkill->SetSkillTarget(LoadBattleDataInterface->GetPlayerCharacters()[TargetSelectWidget->GetSelectIndex()]);
		SoundPlayer->PlayDecisionSound(this);
		AllClose();
		ActiveWidgetType = EActiveWidgetType::None;
		TargetSelectWidget->StopArrow();
		break;

	case UCommandManager::EActiveWidgetType::EnemyTargetSelect:
		SelectSkill->SetSkillTarget(LoadBattleDataInterface->GetLivingEnemyCharacters()[TargetSelectWidget->GetSelectIndex()]);
		SoundPlayer->PlayDecisionSound(this);
		AllClose();
		ActiveWidgetType = EActiveWidgetType::None;
		TargetSelectWidget->StopArrow();
		break;
	}
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void UCommandManager::OnCancel()
{
	if (BackWidget())
	{
		SoundPlayer->PlayCancelSound(this);
	}
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void UCommandManager::OnUp()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnUp())
		SoundPlayer->PlayCursorMoveSound(this);
	WidgetUpdate();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void UCommandManager::OnDown()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnDown())
		SoundPlayer->PlayCursorMoveSound(this);
	WidgetUpdate();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void UCommandManager::OnLeft()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnLeft())
		SoundPlayer->PlayCursorMoveSound(this);
	WidgetUpdate();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void UCommandManager::OnRight()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnRight())
		SoundPlayer->PlayCursorMoveSound(this);
	WidgetUpdate();
}

/// <summary>
/// 次のウィジットに移動
/// </summary>
/// <param name="NextWidget">移動するウィジット</param>
void UCommandManager::NextWidget(UInputWidget* NextWidget)
{
	if (IsValid(ActiveWidget))
	{
		WidgetLog.Add(ActiveWidget);
		WidgetTypeLog.Add(ActiveWidgetType);
	}
	ActiveWidget = NextWidget;
}

/// <summary>
/// 前のウィジットに移動
/// </summary>
/// <return> 移動した = true </return>
bool UCommandManager::BackWidget()
{
	if (WidgetLog.Num() == 0)
		return false;
	ActiveWidget->Close();

	int Index = WidgetLog.Num() - 1;
	ActiveWidget = WidgetLog[Index];
	WidgetLog.RemoveAt(Index);

	ActiveWidgetType = WidgetTypeLog[Index];
	WidgetTypeLog.RemoveAt(Index);

	ActiveWidget->Open();
	ActiveWidget->ActiveArrow();
	return true;
}

/// <summary>
/// コマンドの選択
/// <param name="ActionSelectType">選択するコマンド</param>
void UCommandManager::SelectCommand(EActionSelectType ActionSelectType)
{
	switch (ActionSelectType)
	{
	case EActionSelectType::NormalAttack:
	case EActionSelectType::Guard: //防御スキルがないため攻撃にする
	{
		USkill* Skill = LoadDataInterface->GetSkill(this, PlayerStatus->GetNormalAttackSkillID());
		SelectSkill->SetUseSkill(Skill);
		SelectSkill->SetUseItem(nullptr);
		SkillTargetSelect(Skill);
		SetTargetSelectWidgetPositionForNormalAttack();
		SoundPlayer->PlayDecisionSound(this);
		CommandActionSelectWidget->StopArrow();
		break;
	}
	case EActionSelectType::Skill:
		if (PlayerStatus->GetSkills().Num() == 0)
		{
			SoundPlayer->PlayCancelSound(this);
			break;
		}
		SkillSelectWidget->Open();
		NextWidget(SkillSelectWidget);
		SkillSelectWidgetUpdate();
		CommandActionSelectWidget->Close();

		ActiveWidgetType = EActiveWidgetType::SkillSelect;
		SoundPlayer->PlayDecisionSound(this);
		CommandActionSelectWidget->StopArrow();
		break;

	case EActionSelectType::Item:
		if (LoadDataInterface->GetHaveItems(this).Num() == 0)
		{
			SoundPlayer->PlayCancelSound(this);
			break;
		}
		ItemSelectWidget->Open();
		NextWidget(ItemSelectWidget);
		ItemSelectWidgetUpdate();
		CommandActionSelectWidget->Close();

		ActiveWidgetType = EActiveWidgetType::ItemSelect;
		SoundPlayer->PlayDecisionSound(this);
		CommandActionSelectWidget->StopArrow();
		break;
	}
}

/// <summary>
/// スキルの対象選択
/// <param name="Skill">選択するスキル</param>
void UCommandManager::SkillTargetSelect(USkill* Skill)
{
	ESkillTarget Target = Skill->GetSkillParameter().SkillTarget;
	switch (Target)
	{
	case ESkillTarget::My:
		SelectSkill->AddSkillTargets(Player);
		AllClose();
		break;

	case ESkillTarget::OneMember:
		TargetSelectWidget->SetCharacter(LoadBattleDataInterface->GetPlayerCharacters());

		TargetSelectWidget->Open();
		NextWidget(TargetSelectWidget);
		ActiveWidgetType = EActiveWidgetType::PlayerTargetSelect;
		break;

	case ESkillTarget::AllMenbers:
		SelectSkill->SetSkillTargets(LoadBattleDataInterface->GetPlayerCharacters());
		AllClose();
		break;

	case ESkillTarget::OneEnemy:
		TargetSelectWidget->SetCharacter(LoadBattleDataInterface->GetLivingEnemyCharacters());

		TargetSelectWidget->Open();
		NextWidget(TargetSelectWidget);
		ActiveWidgetType = EActiveWidgetType::EnemyTargetSelect;
		break;

	case ESkillTarget::AllEnemys:
		SelectSkill->SetSkillTargets(LoadBattleDataInterface->GetLivingEnemyCharacters());
		AllClose();
		break;
	}
}

/// <summary>
/// 対象選択ウィジットを通常攻撃対象選択位置に
/// </summary>
void UCommandManager::SetTargetSelectWidgetPositionForNormalAttack()
{
	TargetSelectWidget->SetCanvasPannelRenderTranslation(CommandActionSelectWidget->GetUpperRightCorner());
}

/// <summary>
/// 対象選択ウィジットをスキル対象選択位置に
/// </summary>
void UCommandManager::SetTargetSelectWidgetPositionForSkill()
{
	TargetSelectWidget->SetCanvasPannelRenderTranslation(SkillSelectWidget->GetUpperRightCorner());
}

/// <summary>
/// ウインドウの更新
/// </summary>
void UCommandManager::WidgetUpdate()
{
	if (ActiveWidgetType == EActiveWidgetType::SkillSelect)
		SkillSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::ItemSelect)
		ItemSelectWidgetUpdate();
}

/// <summary>
/// スキルウインドウの更新
/// </summary>
void UCommandManager::SkillSelectWidgetUpdate()
{
	USkill* Skill = PlayerStatus->GetSkills()[SkillSelectWidget->GetSelectIndex()];
	SkillDescriptionWidget->SetText(Skill->GetSkillParameter().Description);
	SkillMPWidget->SetNum(Skill->GetSkillParameter().Mp);
}

/// <summary>
/// アイテムウインドウの更新
/// </summary>
void UCommandManager::ItemSelectWidgetUpdate()
{
	UHaveItem* HaveItem = LoadDataInterface->GetHaveItems(this)[ItemSelectWidget->GetSelectIndex()];
	ItemDescriptionWidget->SetText(HaveItem->GetItem()->GetDescription());
	ItemCountWidget->SetNum(HaveItem->GetNum());
}