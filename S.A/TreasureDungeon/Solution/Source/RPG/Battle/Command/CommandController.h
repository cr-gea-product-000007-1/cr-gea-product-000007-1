﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "CommandController.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ACommandController : public APlayerController
{
	GENERATED_BODY()

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft();

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight();

public:
	/// <summary>
	/// インターフェースを設定する
	/// </summary>
	void SetInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { InputInterface = Interface; };

private:
	/// <summary>
	/// インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> InputInterface;
};
