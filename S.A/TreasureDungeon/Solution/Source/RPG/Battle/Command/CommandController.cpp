﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CommandController.h"


//SetupInputComponentの継承
void ACommandController::SetupInputComponent()
{
    Super::SetupInputComponent();
    check(InputComponent); //UE4のアサートマクロ

    if (IsValid(InputComponent))
    {
        InputComponent->BindAction("Decide", IE_Pressed, this, &ACommandController::OnDecide);
        InputComponent->BindAction("Cancel", IE_Pressed, this, &ACommandController::OnCancel);
        InputComponent->BindAction("Up", IE_Pressed, this, &ACommandController::OnUp);
        InputComponent->BindAction("Down", IE_Pressed, this, &ACommandController::OnDown);
        InputComponent->BindAction("Left", IE_Pressed, this, &ACommandController::OnLeft);
        InputComponent->BindAction("Right", IE_Pressed, this, &ACommandController::OnRight);
    }
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ACommandController::OnDecide()
{
    if (InputInterface == nullptr) return;

    InputInterface->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void ACommandController::OnCancel()
{
    InputInterface->OnCancel();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void ACommandController::OnUp()
{
    if (InputInterface == nullptr) return;

    InputInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void ACommandController::OnDown()
{
    if (InputInterface == nullptr) return;

    InputInterface->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void ACommandController::OnLeft()
{
    if (InputInterface == nullptr) return;

    InputInterface->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void ACommandController::OnRight()
{
    if (InputInterface == nullptr) return;

    InputInterface->OnRight();
}
