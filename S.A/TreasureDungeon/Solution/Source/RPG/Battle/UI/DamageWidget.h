﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "../../Character/BattleCharacterMesh.h"
#include "DamageWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UDamageWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ダメージを再生
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void PlayDamage(ABattleCharacterMesh* Character, int Damage);

	/// <summary>
	/// 指定した桁を取得
	/// </summary>
	UFUNCTION(BlueprintPure)
	int GetDigit(int Value, int Digit);

	/// <summary>
	/// 桁数を取得
	/// </summary>
	UFUNCTION(BlueprintPure)
	int GetDigitNum(int Value);
};
