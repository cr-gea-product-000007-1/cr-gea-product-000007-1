﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DamageDigitWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UDamageDigitWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 値を設定
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetValue(int Damage);
};
