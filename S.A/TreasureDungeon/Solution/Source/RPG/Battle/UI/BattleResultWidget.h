﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/OpenWidget.h"
#include "../../Character/BattleCharacter.h"
#include "../../Item/BaseItem.h"
#include "BattleResultWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UBattleResultWidget : public UOpenWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 勝利メッセージ表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewWinMessage();

	/// <summary>
	/// 敗北メッセージ表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewLoseMessage();

	/// <summary>
	/// 獲得した経験値とゴールド表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewDropExpandGold(int Exp, int Gold);

	/// <summary>
	/// 獲得したアイテム表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewDropItem(UBaseItem* Item1, UBaseItem* Item2);

	/// <summary>
	/// レベルアップしたプレイヤー表示
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ViewLevelUpPlayer(UBattleCharacter* Player);

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void MoveArrow();

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopArrow();

	/// <summary>
	/// ウィジットのアニメーション中かを返す
	/// </summary>
	/// <returns>ウィジットのアニメーション中 = true</returns>
	bool IsWidgetAnimation() { return bIsWidgetAnimation; };

	/// <summary>
	/// 文字のアニメーション中かを返す
	/// </summary>
	/// <returns>文字のアニメーション中 = true</returns>
	bool IsTextAnimation() { return bIsTextAnimation; };

public:
	/// <summary>
	/// ウィジットのアニメーション中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsWidgetAnimation;

	/// <summary>
	/// 文字のアニメーション中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsTextAnimation;
};
