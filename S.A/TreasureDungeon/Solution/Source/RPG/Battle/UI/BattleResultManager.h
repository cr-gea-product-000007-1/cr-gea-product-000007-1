﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BattleResultWidget.h"
#include "../../Character/BattleCharacter.h"
#include "../../Item/BaseItem.h"
#include "../Interface/BattleCharacterInterface.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "../../Sound/SoundPlayer.h"
#include "BattleResultManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UBattleResultManager : public UObject, public IInputWidgetInterface
{
	GENERATED_BODY()

	enum class EResultPhase
	{
		None,
		Open,
		MessageStart,
		MessageWait,
		Message,
		ResultStart,
		Result,
		LevelUpStart,
		LevelUpWait,
		LevelUp,
		DropStart,
		Drop,
		Finish,

		LoseStart,
		Lose,
	};

public:
	/// <summary>
	/// ウィジットを生成
	/// </summary>
	void WidgetCreate();

	/// <summary>
	/// 更新処理
	/// </summary>
	void Update(float DeltaTime);

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// 勝利リザルト表示開始
	/// </summary>
	void WinOpen();

	/// <summary>
	/// 敗北リザルト表示開始
	/// </summary>
	void LoseOpen();

	/// <summary>
	/// 終了しているかを返す
	/// </summary>
	/// <returns>終了している = true</returns>
	bool IsFinish() { return ResultPhase == EResultPhase::Finish; };

	/// <summary>
	/// 獲得経験値を設定
	/// </summary>
	/// <param name="Exp">獲得経験値</param>
	void SetDropExp(int Exp) { DropExp = Exp; };

	/// <summary>
	/// 獲得ゴールドを設定
	/// </summary>
	/// <param name="Gold">獲得ゴールド</param>
	void SetDropGold(int Gold) { DropGold = Gold; };

	/// <summary>
	/// 獲得アイテムを設定
	/// </summary>
	/// <param name="Items">獲得アイテム</param>
	void SetDropItems(TArray<UBaseItem*> Items) { DropItems = Items; };

	/// <summary>
	/// レベルの上がったプレイヤーを設定
	/// </summary>
	/// <param name="Players">レベルの上がったプレイヤー</param>
	void SetLevelUpPlayers(TArray<UBattleCharacter*> Players) { LevelUpPlayers = Players; };

	/// <summary>
	/// 戦闘キャラクターインターフェース
	/// </summary>
	void SetBattleCharacterInterface(IBattleCharacterInterface* Interface) { BattleCharacterInterface = Interface; };

private:
	/// <summary>
	/// リザルトウインドウ
	/// </summary>
	UPROPERTY()
	UBattleResultWidget* BattleResultWidget;

	/// <summary>
	/// 表示させるリザルト
	/// </summary>
	EResultPhase ResultPhase;

	/// <summary>
	/// タイマー
	/// </summary>
	float Timer;

	/// <summary>
	/// 獲得経験値
	/// </summary>
	int DropExp;

	/// <summary>
	/// 獲得ゴールド
	/// </summary>
	int DropGold;
	
	/// <summary>
	/// 獲得アイテム
	/// </summary>
	UPROPERTY()
	TArray<UBaseItem*> DropItems;

	/// <summary>
	/// レベルの上がったプレイヤー
	/// </summary>
	UPROPERTY()
	TArray<UBattleCharacter*> LevelUpPlayers;

	/// <summary>
	/// 敗北か
	/// </summary>
	bool bIsLose;

	/// <summary>
	/// 戦闘キャラクターインターフェース
	/// </summary>
	IBattleCharacterInterface* BattleCharacterInterface;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
