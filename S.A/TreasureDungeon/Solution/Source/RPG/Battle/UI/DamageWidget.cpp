﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageWidget.h"


/// <summary>
/// 指定した桁を取得
/// </summary>
int UDamageWidget::GetDigit(int Value, int Digit)
{
	if (Digit <= 0) return 0;
	int Ceil = (int)FMath::Pow(10, Digit);
	int Floor = (int)FMath::Pow(10, Digit - 1);
	int Num = (Value % Ceil) / Floor;
	return Num;
}

/// <summary>
/// 桁数を取得
/// </summary>
int UDamageWidget::GetDigitNum(int Value)
{
	if (Value == 0) return 1;
	if (Value < 0) Value *= -1;

	int Digit = 0;
	int Num = 1;
	while (Value >= Num)
	{
		Digit++;
		Num *= 10;
	}
	return Digit;

}
