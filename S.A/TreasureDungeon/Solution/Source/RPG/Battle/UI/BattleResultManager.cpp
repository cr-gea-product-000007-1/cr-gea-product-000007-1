﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleResultManager.h"
#include "../../PathConstant.h"

/// <summary>
/// ウィジットを生成
/// </summary>
void UBattleResultManager::WidgetCreate()
{
	FString Path = Path::BattleResultWidgetPath;
	TSubclassOf<UBattleResultWidget> BattleResultWidgetClass = TSoftClassPtr<UBattleResultWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(BattleResultWidgetClass))
	{
		BattleResultWidget = CreateWidget<UBattleResultWidget>(GetWorld(), BattleResultWidgetClass);
		BattleResultWidget->AddToViewport();
	};

	SoundPlayer = NewObject<USoundPlayer>();
}

/// <summary>
/// 更新処理
/// </summary>
void UBattleResultManager::Update(float DeltaTime)
{
	switch (ResultPhase)
	{
	case EResultPhase::Open:
		if (BattleResultWidget->IsWidgetAnimation()) break;
		if(!bIsLose)
			ResultPhase = EResultPhase::MessageStart;
		else
			ResultPhase = EResultPhase::LoseStart;
		break;

	case EResultPhase::MessageStart:
		BattleResultWidget->ViewWinMessage();
		SoundPlayer->PlayVictorySound(this);
		Timer = 0;
		ResultPhase = EResultPhase::MessageWait;
		break;

	case EResultPhase::MessageWait:
		Timer += DeltaTime;
		if (Timer >= 3.0f)
		{
			BattleResultWidget->MoveArrow();
			ResultPhase = EResultPhase::Message;
		}
		break;

	case EResultPhase::Message:
		break;

	case EResultPhase::ResultStart:
		BattleResultWidget->ViewDropExpandGold(DropExp, DropGold);
		ResultPhase = EResultPhase::Result;
		break;

	case EResultPhase::Result:
		break;

	case EResultPhase::LevelUpStart:
	{
		if (LevelUpPlayers.Num() == 0)
		{
			ResultPhase = EResultPhase::DropStart;
			break;
		}

		UBattleCharacter* LevelUpPlayer = LevelUpPlayers[0];
		LevelUpPlayer->PlayMeshAnimation(ECharacterActionType::Glad);
		BattleCharacterInterface->UpdateStatusBar(Cast<UBattlePlayerStatus>(LevelUpPlayer->GetStatus()));
		SoundPlayer->PlayFanfareSound(this);
		BattleResultWidget->ViewLevelUpPlayer(LevelUpPlayer);
		LevelUpPlayers.Remove(LevelUpPlayer);
		Timer = 0;
		ResultPhase = EResultPhase::LevelUpWait;
		break;
	}
	case EResultPhase::LevelUpWait:
		Timer += DeltaTime;
		if (Timer >= 3.0f)
		{
			BattleResultWidget->MoveArrow();
			ResultPhase = EResultPhase::LevelUp;
		}
		break;

	case EResultPhase::LevelUp:
		break;

	case EResultPhase::DropStart:
	{
		if (DropItems.Num() == 0)
		{
			BattleResultWidget->Close();
			ResultPhase = EResultPhase::Finish;
			break;
		}

		UBaseItem* Item1 = DropItems[0];
		UBaseItem* Item2 = nullptr;
		if (DropItems.Num() >= 2)
			Item2 = DropItems[1];

		BattleResultWidget->ViewDropItem(Item1, Item2);
		DropItems.Remove(Item1);
		DropItems.Remove(Item2);
		ResultPhase = EResultPhase::Drop;
		break;
	}
	case EResultPhase::Drop:
		break;

	case EResultPhase::Finish:
		break;

	case EResultPhase::LoseStart:
		BattleResultWidget->ViewLoseMessage();
		ResultPhase = EResultPhase::Lose;
		break;
	}
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UBattleResultManager::OnDecide()
{
	if (!BattleResultWidget->IsOpen()) return;
	if (BattleResultWidget->IsTextAnimation()) return;

	switch (ResultPhase)
	{
	case EResultPhase::Message:
		if (BattleResultWidget->IsWidgetAnimation()) break;
		ResultPhase = EResultPhase::ResultStart;
		break;

	case EResultPhase::Result:
		if (BattleResultWidget->IsWidgetAnimation()) break;
		BattleResultWidget->ViewDropExpandGold(DropExp, DropGold);
		ResultPhase = EResultPhase::LevelUpStart;
		break;

	case EResultPhase::LevelUp:
		ResultPhase = EResultPhase::LevelUpStart;
		break;

	case EResultPhase::Drop:
		ResultPhase = EResultPhase::DropStart;
		break;

	case EResultPhase::Lose:
		ResultPhase = EResultPhase::Finish;
		break;
	}
}

/// <summary>
/// 勝利リザルト表示開始
/// </summary>
void UBattleResultManager::WinOpen()
{
	ResultPhase = EResultPhase::Open;
	BattleResultWidget->Open();
}

/// <summary>
/// 敗北リザルト表示開始
/// </summary>
void UBattleResultManager::LoseOpen()
{
	ResultPhase = EResultPhase::Open;
	BattleResultWidget->Open();
	bIsLose = true;
}