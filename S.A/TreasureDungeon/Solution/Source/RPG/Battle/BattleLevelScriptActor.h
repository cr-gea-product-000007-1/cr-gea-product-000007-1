﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "BattleManager.h"
#include "Engine/DataTable.h"
#include "BattleLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ABattleLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	// コンストラクタ
	ABattleLevelScriptActor();

private:
	enum BattlePhaseType {
		Initialize,
		Create,
		FadeOut,
		Battle,
		Win,
		Lose,
		FadeIn,
		Finalize,
	};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/// <summary>
	/// ゲームインスタンスのデータをデータテーブルから読み込む
	/// （テスト用）
	/// </summary>
	void LoadDataForGameInstance();

private:
	/// <summary>
	/// 戦闘マネージャー
	/// </summary>
	UPROPERTY()
	UBattleManager* BattleManager;

	/// <summary>
	/// 戦闘の状態
	/// </summary>
	BattlePhaseType BattlePhase;

public:
	/// <summary>
	/// 味方のデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* PlayerDataTable;

	/// <summary>
	/// 敵のデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* EnemyDataTable;

	/// <summary>
	/// スキルデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* SkillDataTable;

	/// <summary>
	/// アイテムデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* ItemDataTable;

	/// <summary>
	/// 武器データテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* WeaponDataTable;

	/// <summary>
	/// 防具データテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* ProtectorDataTable;

	/// <summary>
	/// サウンドデータテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* SoundObjectDataTable;
};
