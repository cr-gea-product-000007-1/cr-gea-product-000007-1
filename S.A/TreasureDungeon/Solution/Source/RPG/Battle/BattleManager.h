﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"

#include "../Character/BattleCharacter.h"
#include "../Character/BattlePlayerStatus.h"
#include "../Character/BattleEnemyStatus.h"
#include "../Skill/UseTurnSkill.h"
#include "../Item/ItemStorage.h"

#include "../System/UI/FadeWidget.h"
#include "../System/UI/UnderMessageWidget.h"
#include "BattleCameraManager.h"
#include "Command/CommandManager.h"
#include "../System/UI/StatusBarManager.h"
#include "UI/BattleResultManager.h"

#include "../RPGGameInstance.h"
#include "../System/Interface/LoadDataInterface.h"
#include "Interface/LoadBattleDataInterface.h"
#include "Interface/BattleCharacterInterface.h"
#include "../System/Interface/SendUnderMessageInterface.h"
#include "../System/Interface/ReceiveUnderMessageInterface.h"
#include "Interface/BattleCameraInterface.h"
#include "../Sound/BGMPlayer.h"

#include "BattleManager.generated.h"



UCLASS()
class RPG_API UBattleManager : public UObject, 
	public ILoadDataInterface, public ILoadBattleDataInterface, public IReceiveUnderMessageInterface, public IBattleCharacterInterface,
	public IBattleCameraInterface
{
	GENERATED_BODY()

private:
	enum class EStartPhase
	{
		Initialize,
		AnimationStart,
		AnimationPlaying,
		Finish,
	};

	enum class ECreatePhase
	{
		PlayerCreate,
		EnemyCreate,
		UICreate,
		CameraCreate,
		Finish,
	};

	enum class EFadeOutPhase
	{
		Start,
		FadeOut,
		Finish,
	};

	enum class ETurnPhase
	{
		TurnStart,
		CommandStart,
		CommandInput,
		CommandFinish,
		EnemyActionSelect,
		ActionSort,
		Action,
		NextAction,
		TrunEnd,
	};

	enum class EWinPhase
	{
		Start,
		ResultOpen,
		Result,
		ResultClose,
		LevelUpStart,
		LevelUp,
		DropStart,
		Drop,
		DropClose,
		Finish,
	};

	enum class ELosePhase
	{
		Start,
		LoseMessageStart,
		LoseMessage,
		Finish,
	};

	enum class EFadeInPhase
	{
		Start,
		FadeIn,
		Finish,
	};

	enum class EActionPhase
	{
		ActionStart,
		ActionEffect,
		ActionFinish,
	};

	enum class EBattleFinishType
	{
		None,
		Win,
		Lose,
	};


public:
	/// <summary>
	/// 開始時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool InitializeEvent();

	/// <summary>
	/// 生成の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool CreateEvent();

	/// <summary>
	/// フェードアウトの処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool FadeOutEvent();

	/// <summary>
	/// 戦闘中の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool BattleEvent();

	/// <summary>
	/// 勝利時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool WinEvent(float DeltaTime);

	/// <summary>
	/// 敗北時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool LoseEvent();

	/// <summary>
	/// フェードインの処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool FadeInEvent();

	/// <summary>
	/// 終了時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool FinalizeEvent();

	/// <summary>
	/// 勝利したかを返す
	/// </summary>
	/// <returns>勝利 = true</returns>
	bool IsWin() { return BattleFinishType == EBattleFinishType::Win; };

	/// <summary>
	/// 敗北したかを返す
	/// </summary>
	/// <returns>敗北 = true</returns>
	bool IsLose() { return BattleFinishType == EBattleFinishType::Lose; };

private:
	/// <summary>
	/// 行動の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool ActionEvent();

	/// <summary>
	/// 戦闘終了判定
	/// </summary>
	void BattleJudge();

	/// <summary>
	/// 味方のステータス一覧取得
	/// </summary>
	/// <returns>味方のステータス一覧</returns>
	TArray<UBattlePlayerStatus*> GetPlayerStatuses();

	/// <summary>
	/// 敵のステータス一覧取得
	/// </summary>
	/// <returns>敵のステータス一覧</returns>
	TArray<UBattleEnemyStatus*> GetEnemyStatuses();

	/// <summary>
	/// 敵の行動の対象選択
	/// </summary>
	TArray<UBattleCharacter*> GetEnemySkillTarget(UBattleCharacter* User, ESkillTarget Target);

	/// <summary>
	/// 通常攻撃のメッセージの表示
	/// </summary>
	/// <param name="User">使用者</param>
	void NormalMessage(UBattleCharacterStatus* UserStatus);

	/// <summary>
	/// スキル使用のメッセージの表示
	/// </summary>
	/// <param name="User">使用者</param>
	/// <param name="UseSkill">スキル</param>
	void SkillMessage(UBattleCharacterStatus* UserStatus, USkill* UseSkill);

	/// <summary>
	/// MP不足のメッセージの表示
	/// </summary>
	void FumbleMessage();

	/// <summary>
	/// 敵からの獲得物設定
	/// </summary>
	void EnemyDrop();

	//------------------------------------------
	// インターフェースイベント
	//------------------------------------------

public:
	/// <summary>
	/// プレイヤーキャラリストを取得
	/// </summary>
	/// <param name="SkillID">プレイヤーキャラリスト</param>
	TArray<UBattleCharacter*> GetPlayerCharacters() override;

	/// <summary>
	/// 敵キャラリストを取得
	/// </summary>
	/// <param name="SkillID">敵キャラリスト</param>
	TArray<UBattleCharacter*> GetEnemyCharacters() override;

	/// <summary>
	/// 生きているプレイヤーキャラリストを取得
	/// </summary>
	TArray<UBattleCharacter*> GetLivingPlayerCharacters() override;

	/// <summary>
	/// 生きている敵キャラリストを取得
	/// </summary>
	TArray<UBattleCharacter*> GetLivingEnemyCharacters() override;

	/// <summary>
	/// メッセージ表示
	/// </summary>
	/// <param name="Message">表示するメッセージ</param>
	void ShowMessage(const FString& Message) override;

	/// <summary>
	/// ステータスバーを更新するイベント
	/// </summary>
	void UpdateStatusBar(UBattlePlayerStatus* Status) override;

	/// <summary>
	/// 後ろからプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	void ViewBackPlayers() override;

	/// <summary>
	/// 前からプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	void ViewFrontPlayers() override;

	/// <summary>
	/// 後ろから敵陣営を写す位置にカメラ移動
	/// </summary>
	void ViewBackEnemys() override;

	/// <summary>
	/// 前から敵陣営を写す位置にカメラ移動
	/// </summary>
	void ViewFrontEnemys() override;

	/// <summary>
	/// 後ろから指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	void ViewBackCharacter(UBattleCharacter* Character) override;

	/// <summary>
	/// 前から指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	void ViewFrontCharacter(UBattleCharacter* Character) override;

	/// <summary>
	/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	void ViewPlayerBackCharacter(UBattleCharacter* Character) override;

	/// <summary>
	/// キャラクターの陣営を後ろから写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	void ViewBackCharacters(UBattleCharacter* Character) override;

	/// <summary>
	/// キャラクターの陣営を前から写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	void ViewFrontCharacters(UBattleCharacter* Character) override;

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	URPGGameInstance* GameInstance;

	/// <summary>
	/// 開始時のフェーズ
	/// </summary>
	EStartPhase StartPhase;

	/// <summary>
	/// 生成のフェーズ
	/// </summary>
	ECreatePhase CreatePhase;

	/// <summary>
	/// フェードアウトのフェーズ
	/// </summary>
	EFadeOutPhase FadeOutPhase;

	/// <summary>
	/// ターン中のフェーズ
	/// </summary>
	ETurnPhase TurnPhase;

	/// <summary>
	/// キャラの行動フェーズ
	/// </summary>
	EActionPhase ActionPhase;

	/// <summary>
	/// 勝利のフェーズ
	/// </summary>
	EWinPhase WinPhase;

	/// <summary>
	/// 敗北のフェーズ
	/// </summary>
	ELosePhase LosePhase;

	/// <summary>
	/// フェードインのフェーズ
	/// </summary>
	EFadeInPhase FadeInPhase;

	/// <summary>
	/// 戦闘終了の種類
	/// </summary>
	EBattleFinishType BattleFinishType;

	/// <summary>
	/// ターン数
	/// </summary>
	int TurnNum;

	/// <summary>
	/// フェード表示
	/// </summary>
	UPROPERTY()
	UFadeWidget* FadeWidget;

	/// <summary>
	/// メッセージ表示
	/// </summary>
	UPROPERTY()
	UUnderMessageWidget* UnderMessageWidget;

	/// カメラマネージャー
	/// </summary>
	UPROPERTY()
	ABattleCameraManager* BattleCameraManager;

	/// <summary>
	/// コマンドマネージャー
	/// </summary>
	UPROPERTY()
	UCommandManager* CommandManager;

	/// <summary>
	/// ステータスUIマネージャー
	/// </summary>
	UPROPERTY()
	UStatusBarManager* StatusBarManager;

	/// <summary>
	/// 戦闘リザルトマネージャー
	/// </summary>
	UPROPERTY()
	UBattleResultManager* BattleResultManager;


	//戦闘中データ

	/// <summary>
	/// 味方一覧
	/// </summary>
	UPROPERTY()
	TArray<UBattleCharacter*> PlayerCharacters;

	/// <summary>
	/// 敵一覧
	/// </summary>
	UPROPERTY()
	TArray<UBattleCharacter*> EnemyCharacters;

	/// <summary>
	/// 行動一覧
	/// </summary>
	UPROPERTY()
	TArray<UUseTurnSkill*> UseTurnSkills;

	/// <summary>
	/// 実行中のスキル
	/// </summary>
	UPROPERTY()
	UUseTurnSkill* ActionSkill;

	/// <summary>
	/// 再生中のエフェクト
	/// </summary>
	UPROPERTY()
	ASkillEffect* PlaytEffect;

	/// <summary>
	/// 敵から入手する経験値
	/// </summary>
	int DropExp;

	/// <summary>
	/// 敵から入手するゴールド
	/// </summary>
	int DropGold;

	/// <summary>
	/// 敵から入手するアイテム
	/// </summary>
	TArray<EItemID> DropItemIds;

	/// <summary>
	/// BGMを再生する
	/// </summary>
	UPROPERTY()
	UBGMPlayer* BGMPlayer;
};
