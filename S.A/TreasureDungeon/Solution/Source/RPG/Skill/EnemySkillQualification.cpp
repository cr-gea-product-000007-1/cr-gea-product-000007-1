﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySkillQualification.h"


/// <summary>
/// このスキルが使えるか
/// </summary>
/// <param name="HpRate">Hpの割合</param>
/// <param name="TurnNum">ターン数</param>
/// <returns>使える = true</returns>
bool FEnemySkillQualification::IsUsing(float HpRate, int TurnNum)
{
	if (UpperHpRate > HpRate && UpperHpRate != -1) return false;
	if (LowerHpRate < HpRate && LowerHpRate != -1) return false;

	if (FrontTurnNum > TurnNum && FrontTurnNum != -1) return false;
	if (BackTurnNum < TurnNum && BackTurnNum != -1) return false;

	return true;
}
