﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Skill.h"
#include "../Character/BattleCharacterStatus.h"
#include "../Character/BattleCharacterMesh.h"


/// <summary>
/// ダメージを計算する
/// </summary>
/// <param name="User"> 使用者 </param>
/// <param name="Target"> 対象者 </param>
/// <return> ダメージ </return>
int USkill::GetDamage(UBattleCharacterStatus* User, UBattleCharacterStatus* Target)
{
	//ダメージを与えるスキルか
	bool bIsDamage = (SkillParameter.ConstPower > 0 || SkillParameter.CorrelationPower > 0);

	//初期威力
	int Damage = SkillParameter.ConstPower;

	//使用者のステータス
	int Status = 0;
	switch (SkillParameter.CorrelationStatus)
	{
	case EStatusType::Hp:
		Status = User->GetFinallyStatus().Hp;
		break;
	case EStatusType::Mp:
		Status = User->GetFinallyStatus().Mp;
		break;
	case EStatusType::Attack:
		Status = User->GetFinallyStatus().Attack;
		break;
	case EStatusType::Vitality:
		Status = User->GetFinallyStatus().Vitality;
		break;
	case EStatusType::Agility:
		Status = User->GetFinallyStatus().Agility;
		break;
	case EStatusType::Intelligence:
		Status = User->GetFinallyStatus().Intelligence;
		break;
	}
	Damage += (int)(Status * SkillParameter.CorrelationPower);

	//対象者の防御力
	if (SkillParameter.DefenceRate > 0)
		Damage -= Target->GetFinallyStatus().Vitality * SkillParameter.DefenceRate;

	//ダメージ乱数
	Damage *= FMath::RandRange(0.95f, 1.05f);

	//ダメージ0ならたまに1
	if (Damage <= 0 && bIsDamage)
		Damage = FMath::RandRange(0, 1);

	//耐性
	Damage *= Target->GetResistRateElement(SkillParameter.Element);

	return Damage;
}

/// <summary>
/// 命中判定を返す
/// </summary>
/// <param name="User"> 使用者 </param>
/// <param name="Target"> 対象者 </param>
/// <return> 命中 = true </return>
bool USkill::IsHitSkill(UBattleCharacterStatus* User, UBattleCharacterStatus* Target)
{
	//スキルの成功率
	if (SkillParameter.HitRate < FMath::RandRange(1, 100)) return false;

	//ステータスの差の成功率
	if (SkillParameter.AvoidRate <= 0.0f) return true;

	//対象者のすばやさが低いなら命中
	if (Target->GetFinallyStatus().Agility < User->GetFinallyStatus().Agility) return true;

	//使用者と対象者のすばやさの差での回避
	float AvoidAgility = (float)Target->GetFinallyStatus().Agility / (float)User->GetFinallyStatus().Agility;
	if ((float)AvoidAgility * SkillParameter.AvoidRate < FMath::RandRange(0.0f, 100.0f)) return false;

	return true;
}

/// <summary>
/// 状態異常の成功判定を返す
/// </summary>
/// <param name="User"> 使用者 </param>
/// <param name="Target"> 対象者 </param>
/// <return> 成功 = true </return>
bool USkill::IsHitState(UBattleCharacterStatus* User, UBattleCharacterStatus* Target)
{
	//スキルの成功率
	if (SkillParameter.StateHitRate < FMath::RandRange(1, 100)) return false;

	//相手の耐性の成功率
	if (Target->GetResistRateState(SkillParameter.State) < FMath::RandRange(1, 100)) return false;
	return true;
}

/// <summary>
/// 使用エフェクト再生
/// </summary>
/// <param name="User"> 使用者 </param>
/// <param name="Targets"> 対象者 </param>
/// <return> 再生したエフェクト </return>
ASkillEffect* USkill::UseEffect(UBattleCharacter* User, TArray<UBattleCharacter*> Targets)
{
	TSubclassOf<ASkillEffect> EffectClass = SkillParameter.Effect;
	ASkillEffect* Effect = User->GetWorld()->SpawnActor<ASkillEffect>(EffectClass);
	if (IsValid(EffectClass))
	{
		Effect = User->GetWorld()->SpawnActor<ASkillEffect>(EffectClass);
		Effect->SetUserMesh(User);
		Effect->SetTargetMeshs(Targets);
		Effect->PlayEffect();
	}
	return Effect;
}