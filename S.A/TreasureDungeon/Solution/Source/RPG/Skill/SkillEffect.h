﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "../Character/BattleCharacter.h"
#include "../Character/CharacterActionType.h"
#include "../Sound/SoundPlayer.h"
#include "../Battle/Interface/BattleCameraInterface.h"
#include "SkillEffect.generated.h"

class UBattleCharacter;

UCLASS()
class RPG_API ASkillEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkillEffect();

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

public:	
	/// <summary>
	/// 再生する
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void PlayEffect();

	/// <summary>
	/// キャラクターのアニメーションの再生
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="CharacterActionType"> アニメーションの種類 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void CharacterPlayAnimation(UBattleCharacter* Character, ECharacterActionType CharacterActionType);

	/// <summary>
	/// 正面を向く
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Speed"> 回転スピード </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void LookForward(UBattleCharacter* Character, float Speed);

	/// <summary>
	/// ターゲットの方向を向く
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Target"> 対象者 </param>
	/// <param name="Speed"> 回転スピード </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void LookTarget(UBattleCharacter* Character, UBattleCharacter* Target, float Speed);


	/// <summary>
	/// 初期位置に移動
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Speed"> 移動スピード </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void GoToFirstLocationForSpeed(UBattleCharacter* Character, float Speed);


	/// <summary>
	/// 初期位置に移動
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Speed"> 移動時間 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void GoToFirstLocationForTime(UBattleCharacter* Character, float Time);

	/// <summary>
	/// ターゲットの方向に移動
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Target"> 対象者 </param>
	/// <param name="Speed"> 移動スピード </param>
	/// <param name="Distance"> 停止距離 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void GoToTargetForSpeed(UBattleCharacter* Character, UBattleCharacter* Target, float Speed, float Distance);

	/// <summary>
	/// ターゲットの方向に移動
	/// </summary>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Target"> 対象者 </param>
	/// <param name="Speed"> 移動時間 </param>
	/// <param name="Distance"> 停止距離 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void GoToTargetForTime(UBattleCharacter* Character, UBattleCharacter* Target, float Time, float Distance);

	/// <summary>
	/// キャラクターにエフェクトの表示
	/// </summary>
	/// <param name="Effect"> 表示エフェクト </param>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Offset"> 相対位置 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void CharacterEffect(TSubclassOf<AActor> Effect, UBattleCharacter* Character, FVector Offset);

	/// <summary>
	/// 対象の中心にエフェクトの表示
	/// </summary>
	/// <param name="Effect"> 表示エフェクト </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void TargetCenterEffect(TSubclassOf<AActor> Effect);

	/// <summary>
	/// エフェクトをターゲットに飛ばす
	/// </summary>
	/// <param name="Effect"> 表示エフェクト </param>
	/// <param name="Character"> キャラクター </param>
	/// <param name="Target"> 対象者 </param>
	/// <param name="ShotTime"> 着弾時間 </param>
	/// <param name="Delay"> 移動遅延 </param>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void BulletEffect(TSubclassOf<AActor> Effect, UBattleCharacter* Character, UBattleCharacter* Target, float ShotTime, float Delay);

	/// <summary>
	/// 対象者全員の中心を取得
	/// </summary>
	/// <returns>対象者全員の中心</returns>
	UFUNCTION(BlueprintPure)
	FVector TargetCenterLocation();

	/// <summary>
	/// 使用者、対象者からプレイヤーを選択する
	/// </summary>
	/// <returns>プレイヤー</returns>
	UFUNCTION(BlueprintPure)
	UBattleCharacter* SelectPlayerSkillUserOrTarget();

	/// <summary>
	/// 後ろからプレイヤーを写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void ViewBackPlayers();

	/// <summary>
	/// 前からプレイヤー陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void ViewFrontPlayers();

	/// <summary>
	/// 後ろから敵を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void ViewBackEnemys();

	/// <summary>
	/// 前から敵陣営を写す位置にカメラ移動
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void ViewFrontEnemys();

	/// <summary>
	/// 後ろから指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void ViewBackCharacter(UBattleCharacter* Character);

	/// <summary>
	/// 前から指定したキャラクターを写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void ViewFrontCharacter(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void ViewPlayerBackCharacter(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターの陣営を後ろから写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void ViewBackCharacters(UBattleCharacter* Character);

	/// <summary>
	/// キャラクターの陣営を前から写す位置にカメラ移動
	/// </summary>
	/// <param name="Character">写すキャラクター</param>
	UFUNCTION(BlueprintCallable)
	void ViewFrontCharacters(UBattleCharacter* Character);

	/// <summary>
	/// サウンドを鳴らす
	/// </summary>
	/// <param name="Obj">鳴らすオブジェクト</param>
	/// <param name="Sound">鳴らすサウンド</param>
	/// <param name="FadeTime">フェードインする時間</param>
	/// <param name="Picth">ピッチ</param>
	UFUNCTION(BlueprintCallable)
	void SoundPlay(UObject* Obj, USoundBase* Sound, float FadeTime, float Picth);

public:
	/// <summary>
	/// 使用者を設定する
	/// </summary>
	/// <param name="User">使用者</param>
	void SetUserMesh(UBattleCharacter* NewUser) { User = NewUser; };

	/// <summary>
	/// 対象者を設定する
	/// </summary>
	/// <param name="Targets">対象者</param>
	void SetTargetMeshs(TArray <UBattleCharacter*> NewTargets) { Targets = NewTargets; };

	/// <summary>
	/// 終了しているかを返す
	/// </summary>
	/// <returns>終了しているか</returns>
	bool IsFinish() { return bIsFinish; };

	/// <summary>
	/// カメラインターフェースを設定する
	/// </summary>
	void SetBattleCameraInterface(IBattleCameraInterface* Interface) { BattleCameraInterface = Interface; };

public:
	/// <summary>
	/// 使用エフェクト
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	TArray<TSubclassOf<ACharacter>> Effects;

	/// <summary>
	/// 使用者
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	UBattleCharacter* User;

	/// <summary>
	/// 対象者
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	TArray<UBattleCharacter*> Targets;

	/// <summary>
	/// 終了しているか
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	bool bIsFinish = false;

	/// <summary>
	/// カメラインターフェース
	/// </summary>
	IBattleCameraInterface* BattleCameraInterface;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
