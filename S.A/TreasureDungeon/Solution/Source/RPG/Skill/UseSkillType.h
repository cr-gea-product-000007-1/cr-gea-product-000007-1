// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UseSkillType.generated.h"


UENUM()
enum class EUseSkillType : uint8
{
	None,
	DefaultUseSkill,
	MagicFumble,
	AbilityFumble,
	Stan,
};