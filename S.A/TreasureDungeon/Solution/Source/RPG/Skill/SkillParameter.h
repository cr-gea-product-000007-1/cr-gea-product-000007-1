﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "SkillID.h"
#include "SkillTarget.h"
#include "../Character/CharacterActionType.h"
#include "../Skill/SkillEffect.h"
#include "../StatusDatabase/Status.h"
#include "../StatusDatabase/Element.h"
#include "../StatusDatabase/State.h"
#include "SkillParameter.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FSkillParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// 名前
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Name;

	/// <summary>
	/// スキルID
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillID SkillID;
	
	/// <summary>
	/// 説明文
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Description;

	/// <summary>
	/// 消費MP
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Mp;

	/// <summary>
	/// 対象者
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillTarget SkillTarget = ESkillTarget::OneEnemy;

	/// <summary>
	/// 使用エフェクト
	/// </summary>
	/// <param name="User"> 使用者 </param>
	/// <param name="Targets"> 対象者 </param>
	UPROPERTY(EditAnywhere)
	TSubclassOf<ASkillEffect> Effect;

	/// <summary>
	/// 基礎威力
	/// </summary>
	UPROPERTY(EditAnywhere)
	int ConstPower;

	/// <summary>
	/// 依存ステータス
	/// </summary>
	UPROPERTY(EditAnywhere)
	EStatusType CorrelationStatus = EStatusType::Attack;

	/// <summary>
	/// 依存割合
	/// </summary>
	UPROPERTY(EditAnywhere)
	float CorrelationPower;

	/// <summary>
	/// 相手の防御割合
	/// </summary>
	UPROPERTY(EditAnywhere)
	float DefenceRate;

	/// <summary>
	/// 命中率
	/// </summary>
	UPROPERTY(EditAnywhere)
	int HitRate = 100;

	/// <summary>
	/// 回避割合
	/// </summary>
	UPROPERTY(EditAnywhere)
	float AvoidRate;

	/// <summary>
	/// 属性
	/// </summary>
	UPROPERTY(EditAnywhere)
	EElementID Element;

	/// <summary>
	/// 状態異常の種類
	/// </summary>
	UPROPERTY(EditAnywhere)
	EStateID State;

	/// <summary>
	/// 状態異常の成功率
	/// </summary>
	UPROPERTY(EditAnywhere)
	int StateHitRate;

	/// <summary>
	/// 呪文か
	/// </summary>
	UPROPERTY(EditAnywhere)
	bool bIsMagic;
};