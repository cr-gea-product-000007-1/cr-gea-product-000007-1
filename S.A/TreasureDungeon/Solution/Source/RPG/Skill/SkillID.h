﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SkillID.generated.h"


UENUM(BlueprintType)
enum class ESkillID : uint8
{
    None,
    NormalAttack,
    Heal1,
    Heal2,
    Heal3,
    AreaHeal1,
    AreaHeal2,
    Fire1,
    Fire2,
    Fire3,
    Ice1,
    Ice2,
    Ice3,
    Wind1,
    Wind2,
    Wind3,
    Potion1,
    Potion2,
    Potion3,

    MagicFumble,
    AbilityFumble,

    Test1,
    Test2,
    Test3,
    Test4,
    Test5,
};
