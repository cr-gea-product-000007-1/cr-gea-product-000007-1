﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SkillTarget.generated.h"


UENUM(BlueprintType)
enum class ESkillTarget : uint8
{
	None,
	My,
	OneMember,
	AllMenbers,
	OneEnemy,
	AllEnemys,
};
