﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SkillParameter.h"
#include "Skill.generated.h"

class UBattleCharacterStatus;
class UBattleCharacterMesh;


UCLASS(BlueprintType)
class RPG_API USkill : public UObject
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// スキルの名前を取得する
	/// </summary>
	/// <return> スキルの名前 </return>
	FString GetSkillName() { return SkillParameter.Name; };

	/// <summary>
	/// ダメージを計算する
	/// </summary>
	/// <param name="User"> 使用者 </param>
	/// <param name="Target"> 対象者 </param>
	/// <return> ダメージ </return>
	int GetDamage(UBattleCharacterStatus* User, UBattleCharacterStatus* Target);

	/// <summary>
	/// 命中判定を返す
	/// </summary>
	/// <param name="User"> 使用者 </param>
	/// <param name="Target"> 対象者 </param>
	/// <return> 命中 = true </return>
	bool IsHitSkill(UBattleCharacterStatus* User, UBattleCharacterStatus* Target);

	/// <summary>
	/// 状態異常の成功判定を返す
	/// </summary>
	/// <param name="User"> 使用者 </param>
	/// <param name="Target"> 対象者 </param>
	/// <return> 成功 = true </return>
	bool IsHitState(UBattleCharacterStatus* User, UBattleCharacterStatus* Target);

	/// <summary>
	/// 使用エフェクト再生
	/// </summary>
	/// <param name="User"> 使用者 </param>
	/// <param name="Targets"> 対象者 </param>
	/// <return> 再生したエフェクト </return>
	ASkillEffect* UseEffect(UBattleCharacter* User, TArray<UBattleCharacter*> Targets);

	/// <summary>
	/// スキルのパラメータを設定する
	/// </summary>
	/// <param name="Parameter">スキルのパラメータ</param>
	void SetSkillParameter(FSkillParameter& Parameter) { SkillParameter = Parameter; };

	/// <summary>
	/// スキルのパラメータを取得する
	/// </summary>
	/// <return> スキルのパラメータ </return>
	FSkillParameter GetSkillParameter() { return SkillParameter; };

private:
	/// <summary>
	/// スキルのパラメータ
	/// </summary>
	UPROPERTY()
	FSkillParameter SkillParameter;
};
