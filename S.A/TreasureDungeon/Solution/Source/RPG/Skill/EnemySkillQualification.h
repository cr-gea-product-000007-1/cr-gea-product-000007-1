﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "SkillID.h"
#include "EnemySkillQualification.generated.h"


USTRUCT(BlueprintType)
struct FEnemySkillQualification : public FTableRowBase
{
	GENERATED_BODY()

public:
	/// <summary>
	/// スキルのIDを取得する
	/// </summary>
	ESkillID GetSkillID() { return SkillID; };

	/// <summary>
	/// このスキルが使えるか
	/// </summary>
	/// <param name="HpRate">Hpの割合</param>
	/// <param name="TurnNum">ターン数</param>
	/// <returns>使える = true</returns>
	bool IsUsing(float HpRate, int TurnNum);

public:
	/// <summary>
	/// スキル
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillID SkillID;

	/// <summary>
	/// HPの割合がこの割合より高いことが条件
	/// </summary>
	UPROPERTY(EditAnywhere)
	float UpperHpRate = -1.0f;

	/// <summary>
	/// HPの割合がこの割合より低いことが条件
	/// </summary>
	UPROPERTY(EditAnywhere)
	float LowerHpRate = -1.0f;

	/// <summary>
	/// ターン数がこの値より早いことが条件
	/// </summary>
	UPROPERTY(EditAnywhere)
	int FrontTurnNum = -1;

	/// <summary>
	/// ターン数がこの値より遅いことが条件
	/// </summary>
	UPROPERTY(EditAnywhere)
	int BackTurnNum = -1;
};
