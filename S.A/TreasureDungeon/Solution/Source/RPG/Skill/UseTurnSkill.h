﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Skill.h"
#include "UseSkillType.h"
#include "../Item/BaseItem.h"
#include "UseTurnSkill.generated.h"

class UBattleCharacter;

UCLASS()
class RPG_API UUseTurnSkill : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 使用スキルを取得
	/// </summary>
	/// <returns>スキル</returns>
	USkill* GetUseSkil() { return UseSkill; };

	/// <summary>
	/// 使用スキルを設定
	/// </summary>
	/// <param name="Skill">スキル</param>
	void SetUseSkill(USkill* Skill) { UseSkill = Skill; };

	/// <summary>
	/// スキルの使用者を取得
	/// </summary>
	/// <returns>スキル使用者</returns>
	UBattleCharacter* GetSkillUser() { return SkillUser; };

	/// <summary>
	/// スキルの使用者を設定
	/// </summary>
	/// <param name="User">スキルの使用者</param>
	void SetSkillUser(UBattleCharacter* User) { SkillUser = User; };

	/// <summary>
	/// 行動順を設定
	/// </summary>
	/// <param name="NewPriority">行動順</param>
	void SetPriority(int NewPriority) { Priority = NewPriority; };

	/// <summary>
	/// 行動順を取得
	/// </summary>
	/// <param name="NewPriority">行動順</param>
	int GetPriority() { return Priority; };

	/// <summary>
	/// スキルの対象者設定
	/// </summary>
	/// <param name="Target">スキルの対象者</param>
	void SetSkillTarget(UBattleCharacter* Target);

	/// <summary>
	/// スキルの対象者設定
	/// </summary>
	/// <param name="Target">スキルの対象者</param>
	void SetSkillTargets(TArray<UBattleCharacter*> Targets) { SkillTargets = Targets; };

	/// <summary>
	/// スキルの対象者追加
	/// </summary>
	/// <param name="Target">スキルの対象者</param>
	void AddSkillTargets(UBattleCharacter* Target);

	/// <summary>
	/// スキル対象者取得
	/// </summary>
	/// <param name="Index">スキル対象者インデックス</param>
	/// <returns>スキル対象者</returns>
	UBattleCharacter* GetSkillTarget(int Index);

	/// <summary>
	/// スキル対象者取得
	/// </summary>
	/// <param name="Index">スキル対象者インデックス</param>
	/// <returns>スキル対象者</returns>
	TArray<UBattleCharacter*> GetAllSkillTargets() { return SkillTargets; };

	/// <summary>
	/// スキルの対象者全削除
	/// </summary>
	void ClearSkillTargets() { SkillTargets.Empty(); };

	/// <summary>
	/// スキルが実行できるか
	/// </summary>
	bool IsUsable();

	/// <summary>
	/// 使用アイテム設定
	/// </summary>
	/// <param name="Item">使用アイテム</param>
	void SetUseItem(UBaseItem* Item) { UseItem = Item; };

	/// <summary>
	/// 使用アイテム取得
	/// </summary>
	/// <param name="Item">使用アイテム</param>
	UBaseItem* GetUseItem() { return UseItem; };

private:
	/// <summary>
	/// 使用スキル
	/// </summary>
	UPROPERTY()
	USkill* UseSkill;

	/// <summary>
	/// スキルの使用者
	/// </summary>
	UPROPERTY()
	UBattleCharacter* SkillUser;

	/// <summary>
	/// スキルの対象者
	/// </summary>
	UPROPERTY()
	TArray<UBattleCharacter*> SkillTargets;

	/// <summary>
	/// 行動順
	/// </summary>
	int Priority;

	/// <summary>
	/// 使用するアイテム
	/// </summary>
	UPROPERTY()
	UBaseItem* UseItem;
};
