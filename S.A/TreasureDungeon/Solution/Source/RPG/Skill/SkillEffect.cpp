﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillEffect.h"
#include "../Character/BattleCharacter.h"

// Sets default values
ASkillEffect::ASkillEffect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASkillEffect::BeginPlay()
{
	Super::BeginPlay();
	
	SoundPlayer = NewObject<USoundPlayer>();
}

/// <summary>
/// 対象者全員の中心を取得
/// </summary>
/// <returns>対象者全員の中心</returns>
FVector ASkillEffect::TargetCenterLocation()
{
	UBattleCharacter* Begin = *Targets.begin();
	FVector Min = Begin->GetEffectLocation();
	FVector Max = Begin->GetEffectLocation();
	for (UBattleCharacter*Target : Targets)
	{
		if (Target == Begin) continue;
		FVector TargetEffectLocation = Target->GetEffectLocation();

		if (Min.X > TargetEffectLocation.X) Min.X = TargetEffectLocation.X;
		if (Min.Y > TargetEffectLocation.Y) Min.Y = TargetEffectLocation.Y;
		if (Min.Z > TargetEffectLocation.Z) Min.Z = TargetEffectLocation.Z;

		if (Max.X < TargetEffectLocation.X) Max.X = TargetEffectLocation.X;
		if (Max.Y < TargetEffectLocation.Y) Max.Y = TargetEffectLocation.Y;
		if (Max.Z < TargetEffectLocation.Z) Max.Z = TargetEffectLocation.Z;
	}
	return (Min + Max) / 2;
}

/// <summary>
/// 使用者、対象者からプレイヤーを選択する
/// </summary>
/// <returns>プレイヤー</returns>
UBattleCharacter* ASkillEffect::SelectPlayerSkillUserOrTarget()
{
	if (User->IsPlayerCharacter())
		return User;
	for (UBattleCharacter* Target : Targets)
	{
		if (Target->IsPlayerCharacter())
			return Target;
	}
	return nullptr;
}

/// <summary>
/// 後ろからプレイヤーを写す位置にカメラ移動
/// </summary>
void ASkillEffect::ViewBackPlayers()
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewBackPlayers();
	}
}

/// <summary>
/// 前からプレイヤー陣営を写す位置にカメラ移動
/// </summary>
void ASkillEffect::ViewFrontPlayers()
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewFrontPlayers();
	}
}

/// <summary>
/// 後ろから敵を写す位置にカメラ移動
/// </summary>
void ASkillEffect::ViewBackEnemys()
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewBackEnemys();
	}
}

/// <summary>
/// 前から敵陣営を写す位置にカメラ移動
/// </summary>
void ASkillEffect::ViewFrontEnemys()
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewFrontEnemys();
	}
}

/// <summary>
/// 後ろから指定したキャラクターを写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
UFUNCTION(BlueprintCallable)
void ASkillEffect::ViewBackCharacter(UBattleCharacter* Character)
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewBackCharacter(Character);
	}
}

/// <summary>
/// 前から指定したキャラクターを写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
UFUNCTION(BlueprintCallable)
void ASkillEffect::ViewFrontCharacter(UBattleCharacter* Character)
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewFrontCharacter(Character);
	}
}

/// <summary>
/// キャラクターをプレイヤーの背面方向で写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
UFUNCTION(BlueprintCallable)
void ASkillEffect::ViewPlayerBackCharacter(UBattleCharacter* Character)
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewPlayerBackCharacter(Character);
	}
}

/// <summary>
/// キャラクターの陣営を後ろから写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void ASkillEffect::ViewBackCharacters(UBattleCharacter* Character)
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewBackCharacters(Character);
	}
}

/// <summary>
/// キャラクターの陣営を前から写す位置にカメラ移動
/// </summary>
/// <param name="Character">写すキャラクター</param>
void ASkillEffect::ViewFrontCharacters(UBattleCharacter* Character)
{
	if (BattleCameraInterface != nullptr)
	{
		BattleCameraInterface->ViewFrontCharacters(Character);
	}
}

/// <summary>
/// サウンドを鳴らす
/// </summary>
/// <param name="Obj">鳴らすオブジェクト</param>
/// <param name="Sound">鳴らすサウンド</param>
/// <param name="FadeTime">フェードインする時間</param>
/// <param name="Picth">ピッチ</param>
void ASkillEffect::SoundPlay(UObject* Obj, USoundBase* Sound, float FadeTime, float Picth)
{
	if (IsValid(SoundPlayer))
	{
		SoundPlayer->PlaySound(Obj, Sound, FadeTime, Picth);
	}
}