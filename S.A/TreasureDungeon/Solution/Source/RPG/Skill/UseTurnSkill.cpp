﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UseTurnSkill.h"
#include "../Character/BattleCharacter.h"



/// <summary>
/// スキルの対象者設定
/// </summary>
/// <param name="Target">スキルの対象者</param>
void UUseTurnSkill::SetSkillTarget(UBattleCharacter* Target) 
{
	TArray<UBattleCharacter*> Targets = { Target };
	SkillTargets = Targets;
};

/// <summary>
/// スキルの対象者追加
/// </summary>
/// <param name="Target">スキルの対象者</param>
void UUseTurnSkill::AddSkillTargets(UBattleCharacter* Target)
{
	if (SkillTargets.Num() == 0)
	{
		SkillTargets.Add(Target);
	}
	else
	{
		UBattleCharacter* Character = (*SkillTargets.FindByKey(Target));
		if (Character != nullptr)
			SkillTargets.Add(Target);
	}
}

/// <summary>
/// スキル対象者取得
/// </summary>
/// <param name="Index">スキル対象者インデックス</param>
/// <returns>スキル対象者</returns>
UBattleCharacter* UUseTurnSkill::GetSkillTarget(int Index)
{
	if (Index < 0) return nullptr;
	if (Index >= SkillTargets.Num()) return nullptr;

	return SkillTargets[Index];
}

/// <summary>
/// スキルが実行できるか
/// </summary>
bool UUseTurnSkill::IsUsable()
{
	if (!IsValid(UseSkill)) return false;
	if (!IsValid(SkillUser)) return false;
	if (SkillTargets.Num() == 0) return false;

	return true;
}