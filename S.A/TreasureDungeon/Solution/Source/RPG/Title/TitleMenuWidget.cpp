﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleMenuWidget.h"


/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UTitleMenuWidget::SetSelectIndex(ETitleMenuType Index)
{
	SelectIndex = Index;
	UpdateArrow();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleMenuWidget::OnUp()
{
	int Index = (int)SelectIndex - 1;
	if (Index < (int)ETitleMenuType::Begin)
		Index = (int)ETitleMenuType::End;
	SelectIndex = (ETitleMenuType)Index;
	UpdateArrow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleMenuWidget::OnDown()
{
	int Index = (int)SelectIndex + 1;
	if (Index > (int)ETitleMenuType::End)
		Index = (int)ETitleMenuType::Begin;
	SelectIndex = (ETitleMenuType)Index;
	UpdateArrow();
	return true;
}