﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "TitleManager.h"
#include "TitleLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ATitleLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	enum ETitlePhaseType
	{
		Initialize,
		Create,
		Title,
		GameStart,
	};


	ATitleLevelScriptActor();

public:
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void BeginPlay() override;

private:
	/// <summary>
	/// フェーズ
	/// </summary>
	ETitlePhaseType TitlePhase;

	/// <summary>
	/// タイトルマネージャー
	/// </summary>
	UPROPERTY()
	UTitleManager* TitleManager;
};
