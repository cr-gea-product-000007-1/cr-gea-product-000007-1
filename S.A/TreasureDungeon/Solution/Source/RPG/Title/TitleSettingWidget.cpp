﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleSettingWidget.h"
#include "Kismet/GameplayStatics.h"


/// <summary>
/// サウンド関連の読み込み
/// </summary>
void UTitleSettingWidget::LoadSound()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	SE_Volume = GameInstance->SE_Volue;
	BGM_Volume = GameInstance->BGM_Volue;
}

/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UTitleSettingWidget::SetSelectIndex(ETitleSettingType Index)
{
	SelectIndex = Index;
	UpdateWindow();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleSettingWidget::OnUp()
{
	int Index = (int)SelectIndex - 1;
	if (Index < (int)ETitleSettingType::SEVolume)
		Index = (int)ETitleSettingType::Close;
	SelectIndex = (ETitleSettingType)Index;
	UpdateWindow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleSettingWidget::OnDown()
{
	int Index = (int)SelectIndex + 1;
	if (Index > (int)ETitleSettingType::Close)
		Index = (int)ETitleSettingType::SEVolume;
	SelectIndex = (ETitleSettingType)Index;
	UpdateWindow();
	return true;
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleSettingWidget::OnLeft()
{
	switch (SelectIndex)
	{
	case ETitleSettingType::SEVolume:
		AddSEVolume(-SlideVolume);
		break;
	case ETitleSettingType::BGMVolume:
		AddBGMVolume(-SlideVolume);
		break;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UTitleSettingWidget::OnRight()
{
	switch (SelectIndex)
	{
	case ETitleSettingType::SEVolume:
		AddSEVolume(SlideVolume);
		break;
	case ETitleSettingType::BGMVolume:
		AddBGMVolume(SlideVolume);
		break;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// SEの音量を加算
/// </summary>
/// <param name="AddValume">加算する音量</param>
void UTitleSettingWidget::AddSEVolume(float AddValume)
{
	if (GameInstance == nullptr) return;

	SE_Volume += AddValume;
	if (SE_Volume < 0.0f)
		SE_Volume = 0.0f;
	if (SE_Volume > 1.0f)
		SE_Volume = 1.0f;
	GameInstance->SE_Volue = SE_Volume;

	TitleInterface->BGMVolumeUpdate();
}

/// <summary>
/// BGMの音量を加算
/// </summary>
/// <param name="AddValume">加算する音量</param>
void UTitleSettingWidget::AddBGMVolume(float AddValume)
{
	if (GameInstance == nullptr) return;

	BGM_Volume += AddValume;
	if (BGM_Volume < 0.0f)
		BGM_Volume = 0.0f;
	if (BGM_Volume > 1.0f)
		BGM_Volume = 1.0f;
	GameInstance->BGM_Volue = BGM_Volume;

	TitleInterface->BGMVolumeUpdate();
}

