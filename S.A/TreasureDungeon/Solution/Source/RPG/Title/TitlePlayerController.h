﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../System/Interface/InputWidgetInterface.h"
#include "TitlePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ATitlePlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel();

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	void OnMenu();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft();

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight();

public:
	/// <summary>
	/// メニュー入力インターフェースを設定する
	/// </summary>
	void SetMenuInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { TitleInputInterface = Interface; };

private:
	/// <summary>
	/// メニュー入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> TitleInputInterface;
};