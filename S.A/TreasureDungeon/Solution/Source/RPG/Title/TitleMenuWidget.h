﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../System/UI/InputWidget.h"
#include "TitleMenuWidget.generated.h"


UENUM(BlueprintType)
enum class ETitleMenuType : uint8
{
	GameStart = 0,
	Setting = 1,
	Credit = 2,
	GameEnd = 3,

	Begin = 0,
	End = 3,
};

UCLASS()
class RPG_API UTitleMenuWidget : public UInputWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	UFUNCTION(BlueprintCallable)
	void SetSelectIndex(ETitleMenuType Index);

	/// <summary>
	/// 選んでいるインデックスを取得
	/// </summary>
	/// <return> 選んでいるインデックス </return>
	ETitleMenuType GetSelectIndex() { return SelectIndex; };

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnDown() override;

	/// <summary>
	/// 矢印の位置を更新
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateArrow();

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ActiveArrow() override;

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void StopArrow() override;

public:
	/// <summary>
	/// 選んでいるインデックス
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	ETitleMenuType SelectIndex;
};