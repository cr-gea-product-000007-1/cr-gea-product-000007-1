﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TitleMenuWidget.h"
#include "TitleSettingWidget.h"
#include "../System/UI/OpenWidget.h"
#include "../System/UI/BaseSelectWidget.h"
#include "../System/UI/FadeWidget.h"
#include "../System/Interface/InputWidgetInterface.h"
#include "TitleInterface.h"
#include "../Sound/SoundPlayer.h"
#include "../Sound/BGMPlayer.h"
#include "TitleManager.generated.h"



UCLASS()
class RPG_API UTitleManager : public UObject, public IInputWidgetInterface, public ITitleInterface
{
	GENERATED_BODY()

	enum class ETitlePhase
	{
		Title,
		GameStart,
		SettingOpen,
		Setting,
		CreditOpen,
		Credit,
	};

	enum class EGameStartPhase
	{
		Start,
		FadeIn,
		Finish,
	};

public:
	/// <summary>
	/// 開始時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool InitializeEvent();

	/// <summary>
	/// 生成の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool CreateEvent();

	/// <summary>
	/// タイトル中の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool TitleEvent();

	/// <summary>
	/// ゲーム開始の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool GameStartEvent();

protected:
	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight() override;

	/// <summary>
	/// BGM音量更新
	/// </summary>
	void BGMVolumeUpdate() override { BGMPlayer->VolumeUpdate(); };

private:
	/// <summary>
	/// タイトルのフェーズ
	/// </summary>
	ETitlePhase TitlePhase;

	/// <summary>
	/// ゲーム開始のフェーズ
	/// </summary>
	EGameStartPhase GameStartPhase;

	/// <summary>
	/// メニューウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* MenuWidget;

	/// <summary>
	/// 設定ウィジット
	/// </summary>
	UPROPERTY()
	UTitleSettingWidget* SettingWidget;

	/// <summary>
	/// クレジットウィジット
	/// </summary>
	UPROPERTY()
	UOpenWidget* CreditWidget;

	/// <summary>
	/// フェード
	/// </summary>
	UPROPERTY()
	UFadeWidget* FadeWidget;

	/// <summary>
	/// BGM
	/// </summary>
	UPROPERTY()
	USoundBase* BGM;

	/// <summary>
	/// BGMを再生する
	/// </summary>
	UPROPERTY()
	UBGMPlayer* BGMPlayer;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
