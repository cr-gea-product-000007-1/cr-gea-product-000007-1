﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleManager.h"
#include "../PathConstant.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TitlePlayerController.h"

const TArray<FString> TitleMenuText = { TEXT("ゲーム開始"), TEXT("設定"), TEXT("クレジット"), TEXT("ゲーム終了") };

/// <summary>
/// 開始時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UTitleManager::InitializeEvent()
{
	FString Path = Path::NormalFadeWidgetPath;
	TSubclassOf<UFadeWidget> FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(FadeWidgetClass))
	{
		FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
		FadeWidget->AddToViewport(10000);
		FadeWidget->PlayFadeOut();
	};

	BGM = Cast<USoundBase>(StaticLoadObject(USoundBase::StaticClass(), nullptr, *Path::TitleBGMPath));
	BGMPlayer = NewObject<UBGMPlayer>();
	BGMPlayer->PlayBGM(this, BGM);
	SoundPlayer = NewObject<USoundPlayer>();
	return true;
}

/// <summary>
/// 生成の処理
/// </summary>
/// <return> 終了 = true </return>
bool UTitleManager::CreateEvent()
{
	FString Path;
	//メニューウィジット生成
	Path = Path::OneLineSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> TitleMenuWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(TitleMenuWidgetClass))
	{
		MenuWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), TitleMenuWidgetClass);
		MenuWidget->AddToViewport();
		MenuWidget->Initialize(FSelectWidgetParameter{ 40, 80, 4, 5.0f });
		MenuWidget->SetCanvasPannelRenderTranslationCenter(FVector2D(960.0f, 800.0f));
		MenuWidget->SetText(TitleMenuText);
		MenuWidget->UpdateWindow();
		MenuWidget->ActiveArrow();
	};
	//設定ウィジット生成
	Path = Path::SettingWidgetPath;
	TSubclassOf<UTitleSettingWidget> SettingWidgetClass = TSoftClassPtr<UTitleSettingWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(SettingWidgetClass))
	{
		SettingWidget = CreateWidget<UTitleSettingWidget>(GetWorld(), SettingWidgetClass);
		SettingWidget->AddToViewport();
		SettingWidget->SetRenderTranslation(FVector2D(0.0f, 200.0f));
		SettingWidget->LoadSound();
		SettingWidget->SetTitleInterface(this);
	};
	//クレジットウィジット生成
	Path = Path::CreditWidgetPath;
	TSubclassOf<UOpenWidget> CreditWidgetClass = TSoftClassPtr<UOpenWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(CreditWidgetClass))
	{
		CreditWidget = CreateWidget<UOpenWidget>(GetWorld(), CreditWidgetClass);
		CreditWidget->AddToViewport();
	};
	//コントローラー設定
	ATitlePlayerController* TitlePlayerController = Cast<ATitlePlayerController>(GetWorld()->GetFirstPlayerController());
	if (IsValid(TitlePlayerController))
	{
		TitlePlayerController->SetMenuInputInterface(this);
	}

	return true;
}

/// <summary>
/// タイトル中の処理
/// </summary>
/// <return> 終了 = true </return>
bool UTitleManager::TitleEvent()
{
	switch (TitlePhase)
	{
	case UTitleManager::ETitlePhase::Title:
		break;

	case UTitleManager::ETitlePhase::GameStart:
		return true;

	case UTitleManager::ETitlePhase::SettingOpen:
		SettingWidget->Open();
		MenuWidget->StopArrow();
		TitlePhase = ETitlePhase::Setting;
		break;

	case UTitleManager::ETitlePhase::CreditOpen:
		CreditWidget->Open();
		MenuWidget->StopArrow();
		TitlePhase = ETitlePhase::Credit;
		break;
	}

	return false;
}

/// <summary>
/// ゲーム開始の処理
/// </summary>
/// <return> 終了 = true </return>
bool UTitleManager::GameStartEvent()
{
	switch (GameStartPhase)
	{
	case EGameStartPhase::Start:
		FadeWidget->PlayFadeIn();
		GameStartPhase = EGameStartPhase::FadeIn;
		break;

	case EGameStartPhase::FadeIn:
		if (!FadeWidget->IsAnimation())
			GameStartPhase = EGameStartPhase::Finish;
		break;

	case EGameStartPhase::Finish:
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("FieldLevel"), true);
		return true;
	}
	return false;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UTitleManager::OnDecide()
{
	switch (TitlePhase)
	{
	case UTitleManager::ETitlePhase::Title:
	{
		ETitleMenuType TitleMenuType = (ETitleMenuType)MenuWidget->GetSelectIndex();
		SoundPlayer->PlayDecisionSound(this);
 		switch (TitleMenuType)
		{
		case ETitleMenuType::GameStart:
			TitlePhase = ETitlePhase::GameStart;
			break;

		case ETitleMenuType::Setting:
			TitlePhase = ETitlePhase::SettingOpen;
			break;

		case ETitleMenuType::Credit:
			TitlePhase = ETitlePhase::CreditOpen;
			break;

		case ETitleMenuType::GameEnd:
			UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
			break;
		}
		break;
	}
	case UTitleManager::ETitlePhase::Setting:
		if (SettingWidget->GetSelectIndex() == ETitleSettingType::Close)
		{
			SettingWidget->Close();
			MenuWidget->ActiveArrow();
			SoundPlayer->PlayDecisionSound(this);
			TitlePhase = ETitlePhase::Title;
		}
		break;

	case UTitleManager::ETitlePhase::Credit:
		CreditWidget->Close();
		MenuWidget->ActiveArrow();
		SoundPlayer->PlayDecisionSound(this);
		TitlePhase = ETitlePhase::Title;
		break;
	}
	MenuWidget->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void UTitleManager::OnCancel()
{
	switch (TitlePhase)
	{
	case UTitleManager::ETitlePhase::Title:
		break;

	case UTitleManager::ETitlePhase::Setting:
		SettingWidget->Close();
		MenuWidget->ActiveArrow();
		SoundPlayer->PlayCancelSound(this);
		TitlePhase = ETitlePhase::Title;
		break;

	case UTitleManager::ETitlePhase::Credit:
		CreditWidget->Close();
		MenuWidget->ActiveArrow();
		SoundPlayer->PlayCancelSound(this);
		TitlePhase = ETitlePhase::Title;
		break;
	}
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UTitleManager::OnUp() 
{
	if (TitlePhase == ETitlePhase::Title)
	{
		MenuWidget->OnUp();
		SoundPlayer->PlayCursorMoveSound(this);
	}
	if (TitlePhase == ETitlePhase::Setting)
	{
		SettingWidget->OnUp();
		SoundPlayer->PlayCursorMoveSound(this);
	}
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UTitleManager::OnDown()
{
	if (TitlePhase == ETitlePhase::Title)
	{
		MenuWidget->OnDown();
		SoundPlayer->PlayCursorMoveSound(this);
	}

	if (TitlePhase == ETitlePhase::Setting)
	{
		SettingWidget->OnDown();
		SoundPlayer->PlayCursorMoveSound(this);
	}
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void UTitleManager::OnLeft()
{
	if (TitlePhase == ETitlePhase::Setting)
	{
		SettingWidget->OnLeft();
		SoundPlayer->PlayCursorMoveSound(this);
	}
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void UTitleManager::OnRight()
{
	if (TitlePhase == ETitlePhase::Setting)
	{
		SettingWidget->OnRight();
		SoundPlayer->PlayCursorMoveSound(this);
	}
}