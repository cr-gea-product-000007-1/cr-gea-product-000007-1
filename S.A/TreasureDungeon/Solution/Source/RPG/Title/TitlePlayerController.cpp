﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitlePlayerController.h"


//SetupInputComponentの継承
void ATitlePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	check(InputComponent); //UE4のアサートマクロ

	if (IsValid(InputComponent))
	{
		InputComponent->BindAction("Decide", IE_Pressed, this, &ATitlePlayerController::OnDecide);
		InputComponent->BindAction("Cancel", IE_Pressed, this, &ATitlePlayerController::OnCancel);
		InputComponent->BindAction("Menu", IE_Pressed, this, &ATitlePlayerController::OnMenu);
		InputComponent->BindAction("Up", IE_Pressed, this, &ATitlePlayerController::OnUp);
		InputComponent->BindAction("Down", IE_Pressed, this, &ATitlePlayerController::OnDown);
		InputComponent->BindAction("Left", IE_Pressed, this, &ATitlePlayerController::OnLeft);
		InputComponent->BindAction("Right", IE_Pressed, this, &ATitlePlayerController::OnRight);
	}
	bShowMouseCursor = true;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnDecide()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnCancel()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnCancel();
}

/// <summary>
/// メニューキーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnMenu()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnMenu();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnUp()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnDown()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnLeft()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void ATitlePlayerController::OnRight()
{
	if (TitleInputInterface == nullptr) return;

	TitleInputInterface->OnRight();
}
