﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleLevelScriptActor.h"

/// <summary>
/// コンストラクタ
/// </summary>
ATitleLevelScriptActor::ATitleLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
///　BeginPlay関数の継承
/// </summary>
void ATitleLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	TitlePhase = ATitleLevelScriptActor::ETitlePhaseType::Initialize;

	TitleManager = NewObject<UTitleManager>(this);
}

/// <summary>
///　Tick関数の継承
/// 毎フレーム呼ばれる
/// </summary>
void ATitleLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	switch (TitlePhase)
	{
	case ETitlePhaseType::Initialize:
	{
		if (TitleManager->InitializeEvent())
			TitlePhase = Create;
		break;
	}

	case ETitlePhaseType::Create:
	{

		if (TitleManager->CreateEvent())
			TitlePhase = Title;
		break;
	}
	case ETitlePhaseType::Title:
	{
		if (TitleManager->TitleEvent())
			TitlePhase = GameStart;
		break;
	}
	case ETitlePhaseType::GameStart:
	{
		TitleManager->GameStartEvent();
		break;
	}
	}
}