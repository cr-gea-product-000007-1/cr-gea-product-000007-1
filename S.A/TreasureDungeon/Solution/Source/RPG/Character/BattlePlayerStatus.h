﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "BattleCharacterStatus.h"
#include "StatusData/PlayerParameter.h"
#include "StatusData/PlayerLevelParameter.h"
#include "../Item/Weapon.h"
#include "../Item/Protector.h"
#include "BattlePlayerStatus.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class RPG_API UBattlePlayerStatus : public UBattleCharacterStatus
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 味方情報を設定する
	/// </summary>
	/// <param name="Parameter">味方情報</param>
	void SetPlayerStatus(FPlayerParameter Parameter, URPGGameInstance* GameInstance);

	/// <summary>
	/// プレイヤーか
	/// </summary>
	/// <return> プレイヤー = true </return>
	virtual bool IsPlayer() { return true; };

	/// <summary>
	/// レベルを取得する
	/// </summary>
	/// <return> レベル </return>
	UFUNCTION(BlueprintPure)
	int GetLevel() { return Level; };

	/// <summary>
	/// 最終的なステータスを取得する
	/// </summary>
	/// <return> 最終的なステータス </return>
	UFUNCTION(BlueprintPure)
	virtual FStatus GetFinallyStatus() override;

	/// <summary>
	/// 経験値を獲得
	/// </summary>
	/// <param name="Exp">獲得経験値</param>
	/// <return> レベルアップ = true </return>
	bool AddExp(int Exp);

	/// <summary>
	/// レベルを上げる
	/// </summary>
	/// <param name="LevelUp">上昇量</param>
	void AddLevel(UObject* Obj, int LevelUp);

	/// <summary>
	/// 通常攻撃のスキル取得
	/// </summary>
	ESkillID GetNormalAttackSkillID();

	/// <summary>
	/// 習得スキル追加
	/// </summary>
	void AddSkill(USkill* Skill) { Skills.Add(Skill); };

	/// <summary>
	/// 習得スキル取得
	/// </summary>
	TArray<USkill*> GetSkills() { return Skills; };

	/// <summary>
	/// 直前のレベルを取得する
	/// </summary>
	/// <return> 直前のレベル </return>
	UFUNCTION(BlueprintPure)
	int GetOldLevel() { return OldLevel; };

	/// <summary>
	/// 直前のレベルのステータスを取得する
	/// </summary>
	/// <return> 直前のレベルのステータス </return>
	UFUNCTION(BlueprintPure)
	FStatus GetOldLevelStatus() { return OldLevelStatus; };

	/// <summary>
	/// 直前で新しく習得したスキルを取得する
	/// </summary>
	/// <return> 直前で新しく習得したスキル </return>
	UFUNCTION(BlueprintPure)
	TArray<USkill*> GetNewSkills() { return NewSkills; };

	/// <summary>
	/// 直前のレベルから上昇したステータスを取得する
	/// </summary>
	/// <return> 直前のレベルから上昇したステータス </return>
	UFUNCTION(BlueprintPure)
	FStatus GetLevelRiseStatus() { return Status::SubtractStatus(Status, OldLevelStatus); };

	/// <summary>
	/// 装備品を取得する
	/// </summary>
	/// <param name="ItemID">装備部位</param>
	/// <return> 装備 </return>
	UFUNCTION(BlueprintPure)
	UEquipment* GetEquipment(EItemType ItemID);

	/// <summary>
	/// 装備品を全て取得する
	/// </summary>
	/// <return> 装備 </return>
	UFUNCTION(BlueprintPure)
	TArray<UEquipment*> GetEquipments();

	/// <summary>
	/// 装備品を装備する
	/// </summary>
	/// <param name="ItemID">装備部位</param>
	/// <param name="NewProtector">装備品</param>
	/// <return> 装備していた装備 </return>
	UEquipment* EquipItem(EItemType ItemID, UEquipment* Equipment);

	/// <summary>
	/// 装備した場合のステータスを取得
	/// </summary>
	/// <param name="ItemID">装備部位</param>
	/// <param name="NewProtector">装備品</param>
	/// <return> 装備したステータス </return>
	UFUNCTION(BlueprintPure)
	FStatus GetEquipStatus(EItemType ItemID, UEquipment* EquipItem);

	/// <summary>
	/// 最終的な属性耐性を取得する
	/// </summary>
	/// <return> 属性耐性 </return>
	float GetResistRateElement(EElementID Element) override;

	/// <summary>
	/// 最終的な状態異常耐性を取得する
	/// </summary>
	/// <return> 状態異常耐性 </return>
	float GetResistRateState(EStateID State) override;

	/// <summary>
	/// 実際に行動するスキル取得
	/// </summary>
	/// <returns>行動パターン</returns>
	EUseSkillType GetActionSkill(USkill* UseSkill) override;

	/// <summary>
	/// 全回復する
	/// </summary>
	void FullHeal();

private:
	/// <summary>
	/// レベル
	/// </summary>
	int Level;

	/// <summary>
	/// 総獲得経験値
	/// </summary>
	int SumExp;

	/// <summary>
	/// 今のレベルの経験値
	/// </summary>
	int NowLevelExp;

	/// <summary>
	/// 次のレベルまでの経験値
	/// </summary>
	int NextExp;
	
	/// <summary>
	/// 習得スキル
	/// </summary>
	UPROPERTY()
	TArray<USkill*> Skills;

	/// <summary>
	/// レベルアップテーブル
	/// </summary>
	UPROPERTY()
	TArray<FPlayerLevelParameter> LevelUpParameterList;

	/// <summary>
	/// 直前のレベル
	/// </summary>
	int OldLevel;

	/// <summary>
	/// 直前のレベルのステータス
	/// </summary>
	FStatus OldLevelStatus;

	/// <summary>
	/// 直前で新しく習得したスキル
	/// </summary>
	UPROPERTY()
	TArray<USkill*> NewSkills;

	/// <summary>
	/// 装備品
	/// </summary>
	UPROPERTY()
	TMap<EItemType, UEquipment*> Equipments;

	/// <summary>
	/// 装備していない場合の装備品
	/// </summary>
	UPROPERTY()
	UEquipment* NoEquipment;
};
