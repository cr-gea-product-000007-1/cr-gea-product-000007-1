﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusData/PlayerID.h"
#include "StatusData/EnemyID.h"

namespace Path
{
	const TMap<EPlayerID, FString> PlayerMeshPaths =
	{
		{ EPlayerID::Player1, "/Game/BluePrint/BattleCharacter/BP_BattlePlayer1Mesh.BP_BattlePlayer1Mesh_C" },
	};

	const TMap<EEnemyID, FString> EnemyMeshPaths =
	{
		{ EEnemyID::Enemy1, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy1Mesh.BP_BattleEnemy1Mesh_C" },
		{ EEnemyID::Enemy2, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy2Mesh.BP_BattleEnemy2Mesh_C" },
		{ EEnemyID::Enemy3, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy3Mesh.BP_BattleEnemy3Mesh_C" },
		{ EEnemyID::Enemy4, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy4Mesh.BP_BattleEnemy4Mesh_C" },
		{ EEnemyID::Enemy5, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy5Mesh.BP_BattleEnemy5Mesh_C" },
		{ EEnemyID::Enemy6, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy6Mesh.BP_BattleEnemy6Mesh_C" },
		{ EEnemyID::Enemy7, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy7Mesh.BP_BattleEnemy7Mesh_C" },
		{ EEnemyID::Enemy8, "/Game/BluePrint/BattleCharacter/BP_BattleEnemy8Mesh.BP_BattleEnemy8Mesh_C" },
		{ EEnemyID::Boss1, "/Game/BluePrint/BattleCharacter/BP_BattleBoss1Mesh.BP_BattleBoss1Mesh_C" }
	};
}