﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CharacterActionType.h"
#include "BattleCharacterMesh.generated.h"



UCLASS()
class RPG_API ABattleCharacterMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABattleCharacterMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="AnimatoinType">再生させるアニメーションの種類</param>
	UFUNCTION(BlueprintCallable)
	void PlayMeshAnimation(ECharacterActionType AnimatoinType);
	
	/// <summary>
	/// ダメージエフェクト再生
	/// </summary>
	/// <param name="Damage">ダメージ</param>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDamageEffect(int Damage);

	/// <summary>
	/// 現在地を初期位置に設定
	/// </summary>
	void FirstLocationSetting();

	/// <summary>
	/// 現在のアニメーションでロックする
	/// </summary>
	void Lock() { bIsLock = true; };

	/// <summary>
	/// アニメーションロックする
	/// </summary>
	void UnLock() { bIsLock = false; };

	/// <summary>
	/// 現在のアニメーションでロック状態の取得
	/// </summary>
	bool IsLock() { return bIsLock; }

	/// <summary>
	/// 演出位置取得
	/// </summary>
	/// <returns>演出位置</returns>
	UFUNCTION(BlueprintPure)
	FVector GetEffectLocation() { return EffectLocation + GetActorLocation(); };

	/// <summary>
	/// 後ろからキャラクターを写すカメラ位置取得
	/// </summary>
	/// <returns>カメラ位置</returns>
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	FTransform GetBackCameraPoint();

	/// 前からキャラクターを写すカメラ位置取得
	/// </summary>
	/// <returns>カメラ位置</returns>
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	FTransform GetFrontCameraPoint();

protected:
	/// <summary>
	/// 待機アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayIdle();

	/// <summary>
	/// 走るアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayRun();

	/// <summary>
	/// ジャンプアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayJump();

	/// <summary>
	/// 攻撃アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayAttack();

	/// <summary>
	/// 魔法アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayMagic();

	/// <summary>
	/// 防御アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayGuard();

	/// <summary>
	/// 道具アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayItem();

	/// <summary>
	/// ダメージアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDamage();

	/// <summary>
	/// ダメージアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayStan();

	/// <summary>
	/// 衝撃アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayShock();

	/// <summary>
	/// 喜びアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayGlad();

	/// <summary>
	/// 死亡アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDie();

public:
	/// <summary>
	/// アニメーションか再生中か
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bPlayAnimation;

	/// <summary>
	/// 初期位置
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	FVector FirstLocation;

	/// <summary>
	/// 再生するダメージの値
	/// </summary>
	TArray<int> DamageEffects;

	/// <summary>
	/// 演出位置
	/// </summary>
	UPROPERTY(EditAnywhere)
	FVector EffectLocation;

	/// <summary>
	/// 現在のアニメーションでロックするか
	/// </summary>
	bool bIsLock;
};
