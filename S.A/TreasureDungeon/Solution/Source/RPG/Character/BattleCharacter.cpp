﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleCharacter.h"
#include "BattlePlayerStatus.h"
#include "BattleEnemyStatus.h"
#include "CharacterMeshPath.h"


/// <summary>
/// メッシュの生成
/// </summary>
/// <returns>生成したメッシュ</returns>
ABattleCharacterMesh* UBattleCharacter::CreateMesh()
{
	FString Path;
	if (IsPlayerCharacter())
	{
		EPlayerID PlayerID = (EPlayerID)BattleCharacterStatus->GetCharacterID();
		if (Path::PlayerMeshPaths.Contains(PlayerID))
		{
			Path = Path::PlayerMeshPaths[PlayerID];
			TSubclassOf<ABattleCharacterMesh> MeshClass = TSoftClassPtr<ABattleCharacterMesh>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(MeshClass))
			{
				BattleCharacterMesh = GetWorld()->SpawnActor<ABattleCharacterMesh>(MeshClass);
			}
		}
	}
	else {
		//EEnemyID EnemyID = (EEnemyID)(BattleCharacterStatus->GetCharacterID() - (int)EPlayerID::LastPlayer);
		EnemyID = (EEnemyID)(BattleCharacterStatus->GetCharacterID() - (int)EPlayerID::LastPlayer);
		if (Path::EnemyMeshPaths.Contains(EnemyID))
		{
			EnemyMeshPaths = Path::EnemyMeshPaths;
			Path = Path::EnemyMeshPaths[EnemyID];
			TSubclassOf<ABattleCharacterMesh> MeshClass = TSoftClassPtr<ABattleCharacterMesh>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(MeshClass))
			{
				BattleCharacterMesh = GetWorld()->SpawnActor<ABattleCharacterMesh>(MeshClass);
				BattleCharacterMesh->SetActorRotation(FRotator(0.0f, 180.0f, 0.0f));
			}
		}
	}
	return BattleCharacterMesh;
}

/// <summary>
/// 更新処理
/// </summary>
void UBattleCharacter::Update()
{

}

/// <summary>
/// アニメーション再生
/// </summary>
/// <param name="AnimatoinType">再生させるアニメーションの種類</param>
void UBattleCharacter::PlayMeshAnimation(ECharacterActionType AnimatoinType)
{
	BattleCharacterMesh->PlayMeshAnimation(AnimatoinType);
}

/// <summary>
/// 待機アニメーション再生
/// </summary>
void UBattleCharacter::PlayIdelAnimation()
{
	if (BattleCharacterStatus->IsDie()) return;
	BattleCharacterMesh->PlayMeshAnimation(ECharacterActionType::Idel);
}

/// <summary>
/// ダメージアニメーション再生
/// </summary>
void UBattleCharacter::PlayDamageAnimation()
{
	if (BattleCharacterStatus->IsDie())
	{
		BattleCharacterMesh->PlayMeshAnimation(ECharacterActionType::Die); 
		BattleCharacterMesh->Lock();
	}
	else
	{
		BattleCharacterMesh->PlayMeshAnimation(ECharacterActionType::Damage);
	}
}

/// <summary>
/// ダメージエフェクト再生
/// </summary>
void UBattleCharacter::PlayDamageEffect()
{
	int Damage = BattleCharacterStatus->RemoveNextDamageLog();
	BattleCharacterMesh->PlayDamageEffect(Damage);
	ShowDamageMessage(Damage);
	if (BattleCharacterStatus->IsDie())
		ShowDieMessage();

	if (BattleCharacterInterface != nullptr)
	{
		UBattlePlayerStatus* PlayerStatus = Cast<UBattlePlayerStatus>(BattleCharacterStatus);
		if(IsValid(PlayerStatus))
			BattleCharacterInterface->UpdateStatusBar(PlayerStatus);
	}
}

/// <summary>
/// 演出位置取得
/// </summary>
/// <returns>演出位置</returns>
FVector UBattleCharacter::GetEffectLocation()
{
	return BattleCharacterMesh->GetEffectLocation();
}

/// <summary>
/// ダメージ時の表示メッセージを表示
/// </summary>
/// <param name="Damage">ダメージ</param>
void UBattleCharacter::ShowDamageMessage(int Damage)
{
	FString Message;
	FString Name = BattleCharacterStatus->GetCharacterName();
	if (Damage == 0)
	{
		Message = Name + TEXT(" は ダメージを受けなかった！");
		ShowMessage(Message);
		return;
	}
	if (Damage > 0)
	{
		if (IsPlayerCharacter())
		{
			Message = Name + TEXT(" は ") + FString::FromInt(Damage) + TEXT(" ダメージ受けた！");
		}
		else
		{
			Message = Name + TEXT(" に ") + FString::FromInt(Damage) + TEXT(" ダメージ与えた！");
		}
	}
	else
	{
		Message = Name + TEXT(" の HP が ") + FString::FromInt(FMath::Abs(Damage)) + TEXT(" 回復した！");
	}
	ShowMessage(Message);
}

/// <summary>
/// 死亡時の表示メッセージを表示
/// </summary>
void UBattleCharacter::ShowDieMessage()
{
	FString Message;
	if (IsPlayerCharacter())
	{
		Message = BattleCharacterStatus->GetCharacterName() + TEXT(" は 死んでしまった！");
	}
	else
	{
		Message = BattleCharacterStatus->GetCharacterName() + TEXT(" を 倒した！");
	}
	ShowMessage(Message);
}