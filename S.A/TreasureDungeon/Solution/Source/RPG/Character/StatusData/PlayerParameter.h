﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "PlayerID.h"
#include "../../Skill/SkillID.h"
#include "../../Item/ItemID.h"
#include "../../StatusDatabase/Status.h"
#include "PlayerParameter.generated.h"



USTRUCT(BlueprintType)
struct FPlayerParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// ID
	/// </summary>
	UPROPERTY(EditAnywhere)
	EPlayerID ID;

	/// <summary>
	/// 名前
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Name;

	/// <summary>
	/// 初期ステータス
	/// </summary>
	UPROPERTY(EditAnywhere)
	FStatus Status;

	/// <summary>
	/// 通常攻撃サウンド
	/// </summary>
	UPROPERTY(EditAnywhere)
	USoundBase* NormalAttackSound;

	/// <summary>
	/// レベルアップテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* LevelUpTable;

	/// <summary>
	/// 初期武器
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Weapon;

	/// <summary>
	/// 初期盾
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Shield;

	/// <summary>
	/// 初期兜
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Helmet;

	/// <summary>
	/// 初期鎧
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Armor;

	/// <summary>
	/// 初期装飾品
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Accessory;

	/// <summary>
	/// 初期スキルID
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<ESkillID> SkillID;
};