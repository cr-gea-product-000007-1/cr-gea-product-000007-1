﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "EnemyID.h"
#include "../../StatusDatabase/Status.h"
#include "../../StatusDatabase/Element.h"
#include "../../StatusDatabase/State.h"
#include "../../Item/ItemID.h"
#include "../../Skill/EnemySkillQualification.h"
#include "EnemyParameter.generated.h"



USTRUCT(BlueprintType)
struct FEnemyParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// ID
	/// </summary>
	UPROPERTY(EditAnywhere)
	EEnemyID ID;

	/// <summary>
	/// 名前
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Name;

	/// <summary>
	/// 初期ステータス
	/// </summary>
	UPROPERTY(EditAnywhere)
	FStatus Status;

	/// <summary>
	/// 使用スキル
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FEnemySkillQualification> UseSkills;

	/// <summary>
	/// 通常攻撃サウンド
	/// </summary>
	UPROPERTY(EditAnywhere)
	USoundBase* NormalAttackSound;

	/// <summary>
	/// 属性耐性
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FElementResist> ElementResist;

	/// <summary>
	/// 状態異常耐性
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FStateResist> StateResist;

	/// <summary>
	/// 獲得経験値
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Exp;
	/// <summary>
	/// 獲得ゴールド
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Gold;

	/// <summary>
	/// ドロップアイテムID
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID DropItemID;

	/// <summary>
	/// ドロップ率
	/// </summary>
	UPROPERTY(EditAnywhere)
	float DropRate;
};