﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerID.generated.h"


UENUM(BlueprintType)
enum class EPlayerID : uint8
{
    None,
    Player1,
    Player2,
    LastPlayer = 10,
};
