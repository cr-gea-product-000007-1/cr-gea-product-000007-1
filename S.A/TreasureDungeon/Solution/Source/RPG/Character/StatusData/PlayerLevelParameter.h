﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "../../StatusDatabase/Status.h"
#include "../../Skill/SkillID.h"
#include "PlayerLevelParameter.generated.h"



USTRUCT(BlueprintType)
struct FPlayerLevelParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// 上昇ステータス
	/// </summary>
	UPROPERTY(EditAnywhere)
	FStatus Status;

	/// <summary>
	/// 習得スキルID
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillID SkillID;

	/// <summary>
	/// 必要経験値
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Exp;
};