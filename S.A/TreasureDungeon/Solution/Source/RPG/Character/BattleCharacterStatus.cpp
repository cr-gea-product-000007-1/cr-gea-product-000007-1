﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleCharacterStatus.h"

/// <summary>
/// 最終的なステータスを取得する
/// </summary>
/// <return> 最終的なステータス </return>
FStatus UBattleCharacterStatus::GetFinallyStatus()
{
	return Status;
}

/// <summary>
/// 最終的な属性耐性を取得する
/// </summary>
/// <return> 属性耐性 </return>
float UBattleCharacterStatus::GetResistRateElement(EElementID Element)
{
	return 1.0f;
}

/// <summary>
/// 最終的な状態異常耐性を取得する
/// </summary>
/// <return> 状態異常耐性 </return>
float UBattleCharacterStatus::GetResistRateState(EStateID State)
{
	return 1.0f;
}

/// <summary>
/// ダメージを与える
/// </summary>
/// <param name="Value"> 与えるダメージ </param>
void UBattleCharacterStatus::GiveDamage(int Value, bool IsLog)
{
	FStatus FinalyStatus = GetFinallyStatus();
	NowHp -= Value;
	if (NowHp <= 0) {
		NowHp = 0;
		bIsDie = true;
	}

	if (NowHp > FinalyStatus.Hp) {
		NowHp = FinalyStatus.Hp;
	}

	if(IsLog)
		DamageLog.Add(Value);
}

/// <summary>
/// MPを消費
/// </summary>
/// <param name="Value"> 消費するMP </param>
void UBattleCharacterStatus::UseMp(int Value)
{
	NowMp -= Value;
	if (NowMp <= 0) {
		NowMp = 0;
	}

	if (NowMp > Status.Mp) {
		NowMp = Status.Mp;
	}
}

/// <summary>
/// 次のダメージの記録を取り出す
/// </summary>
/// <return> 取り出したダメージの記録 </return>
int UBattleCharacterStatus::RemoveNextDamageLog()
{
	if (DamageLog.Num() == 0) return 0;
	int Damage = *DamageLog.begin();
	DamageLog.RemoveAt(0);
	return Damage;
};

/// <summary>
/// 行動終了時のイベント
/// </summary>
void UBattleCharacterStatus::ActionEndEvent()
{
	//状態異常の更新
}