﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleEnemyStatus.h"
#include "StatusData/PlayerID.h"


/// <summary>
/// 敵情報を設定する
/// </summary>
/// <param name="Parameter">敵情報</param>
/// <param name="EnemyTypeNum">同じ敵の出現数</param>
void UBattleEnemyStatus::SetEnemyStatus(FEnemyParameter Parameter, int EnemyTypeNum)
{
	EnemyTypeNum %= 26;
	char Mark = 'A' + EnemyTypeNum;

	CharacterID = (int)Parameter.ID + (int)EPlayerID::LastPlayer;
	CharacterName = Parameter.Name + Mark;
	Status = Parameter.Status;
	NowHp = Status.Hp;
	NowMp = Status.Mp;
	UseSkills = Parameter.UseSkills;
	NormalAttackSound = Parameter.NormalAttackSound;
	ElementResist = Parameter.ElementResist;
	StateResist = Parameter.StateResist;
	Exp = Parameter.Exp;
	Gold = Parameter.Gold;
	DropItemID = Parameter.DropItemID;
	DropRate = Parameter.DropRate;
}

/// <summary>
/// 最終的なステータスを取得する
/// </summary>
/// <return> 最終的なステータス </return>
FStatus UBattleEnemyStatus::GetFinallyStatus()
{
	return Status;
}

/// <summary>
/// このターンに使用するスキルのIDを取得する
/// </summary>
/// <param name="TurnNum">ターン数</param>
/// <return> 使用スキルID </return>
ESkillID UBattleEnemyStatus::GetUseSkillID(int TurnNum)
{
	float HpRate = (float)NowHp / (float)Status.Hp;
	TArray<FEnemySkillQualification*> EnemySkillQualifications;
	for (FEnemySkillQualification& UseSkill : UseSkills)
	{
		if (UseSkill.IsUsing(HpRate, TurnNum))
			EnemySkillQualifications.Add(&UseSkill);
	}

	if (EnemySkillQualifications.Num() == 0)
		return ESkillID::NormalAttack;

	int Index = FMath::Rand() % EnemySkillQualifications.Num();
	return EnemySkillQualifications[Index]->GetSkillID();
}

/// <summary>
/// 最終的な属性耐性を取得する
/// </summary>
/// <return> 属性耐性 </return>
float UBattleEnemyStatus::GetResistRateElement(EElementID Element)
{
	float Result = 1.0f;
	FElementResist* ResultElementResist = ElementResist.FindByPredicate([&Element](const FElementResist& Resist) { return Resist.Element == Element; });
	if (ResultElementResist != nullptr)
		Result = ResultElementResist->Rate;
	return Result;
}

/// <summary>
/// 最終的な状態異常耐性を取得する
/// </summary>
/// <return> 状態異常耐性 </return>
float UBattleEnemyStatus::GetResistRateState(EStateID State)
{
	float Result = 1.0f;
	FStateResist* ResultStateResist = StateResist.FindByPredicate([&State](const FStateResist& Resist) { return Resist.State == State; });
	if (ResultStateResist != nullptr)
		Result = ResultStateResist->Rate;
	return Result;
}

/// <summary>
/// 実際に行動するスキル取得
/// </summary>
/// <returns>行動パターン</returns>
EUseSkillType UBattleEnemyStatus::GetActionSkill(USkill* UseSkill)
{
	FSkillParameter SkillParameter = UseSkill->GetSkillParameter();
	int UseMp = SkillParameter.Mp;
	if (Status.Mp >= UseMp)
		return EUseSkillType::DefaultUseSkill;

	if (SkillParameter.bIsMagic)
		return EUseSkillType::MagicFumble;
	else
		return EUseSkillType::AbilityFumble;
}

/// <summary>
/// 今回ドロップするアイテムのID取得
/// </summary>
/// <returns>ドロップアイテムID</returns>
EItemID UBattleEnemyStatus::GetDropItemID()
{
	if (DropRate <= 0)
		return EItemID::None;

	if (DropRate >= FMath::RandRange(0.0f, 1.0f))
		return DropItemID;
	else
		return EItemID::None;
}