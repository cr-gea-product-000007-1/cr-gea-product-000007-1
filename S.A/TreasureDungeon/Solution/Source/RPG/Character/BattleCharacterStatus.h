﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Skill/Skill.h"
#include "../Skill/UseTurnSkill.h"
#include "../StatusDatabase/Status.h"
#include "../StatusDatabase/Element.h"
#include "../StatusDatabase/State.h"
#include "../System/Interface/LoadDataInterface.h"
#include "BattleCharacterStatus.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class RPG_API UBattleCharacterStatus : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// プレイヤーか
	/// </summary>
	/// <return> プレイヤー = true </return>
	virtual bool IsPlayer() { return true; };

	/// <summary>
	/// キャラクターID返す
	/// </summary>
	/// <return> キャラクターID </return>
	virtual int GetCharacterID() { return CharacterID; };

	/// <summary>
	/// 名前を設定
	/// </summary>
	/// <param name="NewName">名前</param>
	void SetCharacterName(FString NewName) { CharacterName = NewName; };

	/// <summary>
	/// 名前を取得
	/// </summary>
	/// <returns>名前</returns>
	UFUNCTION(BlueprintPure)
	FString GetCharacterName() { return CharacterName; };

	/// <summary>
	/// ステータスを取得する
	/// </summary>
	/// <return> ステータス </return>
	UFUNCTION(BlueprintPure)
	FStatus GetOriginalStatus() { return Status; };

	/// <summary>
	/// 最終的なステータスを取得する
	/// </summary>
	/// <return> 最終的なステータス </return>
	virtual FStatus GetFinallyStatus();

	/// <summary>
	/// 最大HPを取得
	/// </summary>
	/// <return> 最大HP </return>
	UFUNCTION(BlueprintPure)
	int GetMaxHp() { return GetFinallyStatus().Hp; };

	/// <summary>
	/// HPを取得
	/// </summary>
	/// <return> HP </return>
	UFUNCTION(BlueprintPure)
	int GetNowHp() { return NowHp; };

	/// <summary>
	/// 最大MPを取得
	/// </summary>
	/// <return> 最大MP </return>
	UFUNCTION(BlueprintPure)
	int GetMaxMp() { return GetFinallyStatus().Mp; };

	/// <summary>
	/// MPを取得
	/// </summary>
	/// <return> MP </return>
	UFUNCTION(BlueprintPure)
	int GetNowMp() { return NowMp; };

	/// <summary>
	/// 通常攻撃サウンドを取得
	/// </summary>
	/// <return> MP </return>
	UFUNCTION(BlueprintPure)
	USoundBase* GetNormalAttackSound() { return NormalAttackSound; };

	/// <summary>
	/// 最終的な属性耐性を取得する
	/// </summary>
	/// <return> 属性耐性 </return>
	virtual float GetResistRateElement(EElementID Element);

	/// <summary>
	/// 最終的な状態異常耐性を取得する
	/// </summary>
	/// <return> 状態異常耐性 </return>
	virtual float GetResistRateState(EStateID State);

	/// <summary>
	/// ダメージを与える
	/// </summary>
	/// <param name="Value"> 与えるダメージ </param>
	/// <param name="IsLog"> ダメージの記録を取るか </param>
	void GiveDamage(int Value, bool IsLog = true);

	/// <summary>
	/// MPを消費
	/// </summary>
	/// <param name="Value"> 消費するMP </param>
	void UseMp(int Value);

	/// <summary>
	/// 次のダメージの記録を取り出す
	/// </summary>
	/// <return> 取り出したダメージの記録 </return>
	UFUNCTION(BlueprintCallable)
	int RemoveNextDamageLog();

	/// <summary>
	/// ダメージの記録の初期化
	/// </summary>
	void ResetDamageLog() { DamageLog.Empty(); };

	/// <summary>
	/// 死亡状態を取得する
	/// </summary>
	/// <return> 死亡状態 </return>
	bool IsDie() { return bIsDie; };

	/// <summary>
	/// 行動終了時のイベント
	/// </summary>
	void ActionEndEvent();

	/// <summary>
	/// 実際に行動するスキル取得
	/// </summary>
	/// <returns>行動パターン</returns>
	virtual EUseSkillType GetActionSkill(USkill* UseSkill) { return EUseSkillType::DefaultUseSkill; };

	/// <summary>
	/// 状態異常を付与する
	/// </summary>
	//void AddState(UState State);

	/// <summary>
	/// データ読み込みインターフェースを設定
	/// </summary>
	void SetLoadDataInterface(ILoadDataInterface* Interface) { LoadDataInterface = Interface; };

protected:
	/// <summary>
	/// キャラクターID
	/// </summary>
	int CharacterID;

	/// <summary>
	/// 名前
	/// </summary>
	FString CharacterName;

	/// <summary>
	/// 現在のHP
	/// </summary>
	int NowHp;

	/// <summary>
	/// 現在のMP
	/// </summary>
	int NowMp;

	/// <summary>
	/// ステータス
	/// </summary>
	UPROPERTY()
	FStatus Status;

	/// <summary>
	/// 通常攻撃サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* NormalAttackSound;

	/// <summary>
	/// 死亡状態
	/// </summary>
	bool bIsDie;

	/// <summary>
	/// ダメージの記録
	/// </summary>
	TArray<int> DamageLog;

	/// <summary>
	/// 状態異常リスト
	/// </summary>
	//TArray<EStateID> States;

	/// <summary>
	/// データ読みこみインターフェース
	/// </summary>
	ILoadDataInterface* LoadDataInterface;
};
