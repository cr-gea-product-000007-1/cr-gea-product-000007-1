﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BattleCharacterStatus.h"
#include "BattleCharacterMesh.h"
#include "CharacterActionType.h"
#include "StatusData/PlayerID.h"
#include "../Battle/Interface/BattleCharacterInterface.h"
#include "../System/Interface/SendUnderMessageInterface.h"
#include "BattleCharacter.generated.h"

UCLASS(BlueprintType)
class RPG_API UBattleCharacter : public UObject, public ISendUnderMessageInterface
{
	GENERATED_BODY()

private:

	enum ActionPhaseType
	{
		Idel,                //待機中
		ActionStart,         //行動開始
		SkillAnimationStart, //スキルアニメーション開始
		SkillAnimation,      //スキルアニメーション中
		SkillAnimationEnd,   //スキルアニメーション終了
		ActionFinish,        //行動終了
	};

public:
	/// <summary>
	/// プレイヤーキャラクターかを取得
	/// </summary>
	/// <returns>プレイヤー = true</returns>
	UFUNCTION(BlueprintPure)
	bool IsPlayerCharacter() { return BattleCharacterStatus->GetCharacterID() <= (int)EPlayerID::LastPlayer; };

	/// <summary>
	/// ステータスを設定
	/// </summary>
	/// <param name="Status">ステータス</param>
	void SetStatus(UBattleCharacterStatus* Status) { BattleCharacterStatus = Status; };

	/// <summary>
	/// メッシュの生成
	/// </summary>
	/// <returns>生成したメッシュ</returns>
	ABattleCharacterMesh* CreateMesh();

	/// <summary>
	/// 更新処理
	/// </summary>
	virtual void Update();

	/// <summary>
	/// スキルを使用
	/// </summary>
	/// <param name="Skill">スキル</param>
	void SkillUse(USkill* Skill);

	/// <summary>
	/// ステータスを返す
	/// </summary>
	/// <returns>ステータス</returns>
	UFUNCTION(BlueprintPure)
	UBattleCharacterStatus* GetStatus() { return BattleCharacterStatus; };

	/// <summary>
	/// メッシュを返す
	/// </summary>
	/// <returns>メッシュ</returns>
	UFUNCTION(BlueprintPure)
	ABattleCharacterMesh* GetMesh() { return BattleCharacterMesh; };

	/// <summary>
	/// キャラクターのインターフェースを設定する
	/// </summary>
	/// <param name="Interface">キャラクターのインターフェース</param>
	void SetBattleCharacterInterface(IBattleCharacterInterface* Interface) { BattleCharacterInterface = Interface; };

	/// <summary>
	/// アニメーション再生
	/// </summary>
	/// <param name="AnimatoinType">再生させるアニメーションの種類</param>
	UFUNCTION(BlueprintCallable)
	void PlayMeshAnimation(ECharacterActionType AnimatoinType);

	/// <summary>
	/// 待機アニメーション再生
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void PlayIdelAnimation();

	/// <summary>
	/// ダメージアニメーション再生
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void PlayDamageAnimation();

	/// <summary>
	/// ダメージエフェクト再生
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void PlayDamageEffect();

	/// <summary>
	/// 演出位置取得
	/// </summary>
	/// <returns>演出位置</returns>
	UFUNCTION(BlueprintPure)
	FVector GetEffectLocation();

	/// <summary>
	/// ダメージ時の表示メッセージを表示
	/// </summary>
	/// <param name="Damage">ダメージ</param>
	UFUNCTION(BlueprintCallable)
	void ShowDamageMessage(int Damage);

	/// <summary>
	/// 死亡時の表示メッセージを表示
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void ShowDieMessage();

	/// <summary>
	/// 通常攻撃サウンドを取得
	/// </summary>
	/// <return> MP </return>
	UFUNCTION(BlueprintPure)
	USoundBase* GetNormalAttackSound() { return BattleCharacterStatus->GetNormalAttackSound(); };

private:
	/// <summary>
	/// 行動の種類
	/// </summary>
	ECharacterActionType ActionType;

	/// <summary>
	/// 行動のフェーズ
	/// </summary>
	ActionPhaseType ActionPhase;

	/// <summary>
	/// ステータス
	/// </summary>
	UPROPERTY()
	UBattleCharacterStatus* BattleCharacterStatus;

	/// <summary>
	/// メッシュ
	/// </summary>
	UPROPERTY()
	ABattleCharacterMesh* BattleCharacterMesh;

	/// <summary>
	/// キャラクターのインターフェース
	/// </summary>
	IBattleCharacterInterface* BattleCharacterInterface;

	EEnemyID EnemyID;
	TMap<EEnemyID, FString> EnemyMeshPaths;
};
