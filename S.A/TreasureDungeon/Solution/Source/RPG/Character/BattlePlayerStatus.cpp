﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattlePlayerStatus.h"
#include "../Battle/BattleConstant.h"
#include "../RPGGameInstance.h"


/// <summary>
/// 味方情報を設定する
/// </summary>
/// <param name="Parameter">味方情報</param>
void UBattlePlayerStatus::SetPlayerStatus(FPlayerParameter Parameter, URPGGameInstance* GameInstance)
{
	CharacterID = (int)Parameter.ID;
	CharacterName = Parameter.Name;
	Status = Parameter.Status;
	NowHp = Status.Hp;
	NowMp = Status.Mp;
	NormalAttackSound = Parameter.NormalAttackSound;

	//レベルアップ情報読み込み
	Level = 1;
	UDataTable* LevelUpTable = Parameter.LevelUpTable;
	TArray<FName> RowNames = LevelUpTable->GetRowNames();
	for (FName RowName : RowNames)
	{
		FPlayerLevelParameter PlayerLevelUpParameter = *LevelUpTable->FindRow<FPlayerLevelParameter>(RowName, "");
		LevelUpParameterList.Add(PlayerLevelUpParameter);
	}
	NextExp = LevelUpParameterList[Level - 1].Exp;

	Equipments.Add(EItemType::Weapon, nullptr);
	Equipments.Add(EItemType::Shield, nullptr);
	Equipments.Add(EItemType::Helmet, nullptr);
	Equipments.Add(EItemType::Armor, nullptr);
	Equipments.Add(EItemType::Accessory, nullptr);

	NoEquipment = GameInstance->NoEquipment;
}

/// <summary>
/// 最終的なステータスを取得する
/// </summary>
/// <return> 最終的なステータス </return>
FStatus UBattlePlayerStatus::GetFinallyStatus()
{
	FStatus Result = Status;

	for (TPair<EItemType, UEquipment*> Equipment : Equipments)
	{
		if (Equipment.Value == nullptr) continue;
		Result = Status::AddStatus(Result, Equipment.Value->GetStatus());
	}

	return Result;
}

///<summary>
/// 経験値を獲得
/// </summary>
/// <param name="Exp">獲得経験値</param>
/// <return> レベルアップ = true </return>
bool UBattlePlayerStatus::AddExp(int Exp)
{
	if (LoadDataInterface == nullptr)return false;

	OldLevel = Level;
	OldLevelStatus = Status;
	NewSkills.Empty();

	bool LevelUp = false;
	SumExp += Exp;
	NowLevelExp += Exp;
	while (NowLevelExp >= NextExp)
	{
		NowLevelExp -= NextExp;
		LevelUp = true;
		Level++;
		FPlayerLevelParameter NextLevelParameter = LevelUpParameterList[Level - 1];
		Status = Status::AddStatus(Status, NextLevelParameter.Status);
		NowHp += NextLevelParameter.Status.Hp;
		NowMp += NextLevelParameter.Status.Mp;
		if (NextLevelParameter.SkillID != ESkillID::None)
		{
			USkill* Skill = LoadDataInterface->GetSkill(this, NextLevelParameter.SkillID);
			AddSkill(Skill);
			NewSkills.Add(Skill);
		}
		NextExp = NextLevelParameter.Exp;
	}
	return LevelUp;
}

/// <summary>
/// レベルを上げる
/// </summary>
/// <param name="LevelUp">上昇量</param>
void UBattlePlayerStatus::AddLevel(UObject* Obj, int LevelUp)
{
	for (int i = 0; i < LevelUp; i++)
	{
		SumExp += NextExp;
		Level++;
		FPlayerLevelParameter NextLevelParameter = LevelUpParameterList[Level - 1];
		Status = Status::AddStatus(Status, NextLevelParameter.Status);
		NowHp += NextLevelParameter.Status.Hp;
		NowMp += NextLevelParameter.Status.Mp;
		if (NextLevelParameter.SkillID != ESkillID::None)
		{
			USkill* Skill = LoadDataInterface->GetSkill(Obj, NextLevelParameter.SkillID);
			AddSkill(Skill);
		}
		NextExp = NextLevelParameter.Exp;
	}
}

/// <summary>
/// 通常攻撃のスキル取得
/// </summary>
ESkillID UBattlePlayerStatus::GetNormalAttackSkillID()
{
	UWeapon* Weapon = Cast<UWeapon>(Equipments[EItemType::Weapon]);
	if (!IsValid(Weapon))
		return BattleConstant::DefaultNormalAttackSkill;

	return Weapon->GetNormalAttackSkillID();
}

/// <summary>
/// 装備品を取得する
/// </summary>
/// <param name="ItemID">装備部位</param>
/// <return> 装備 </return>
UEquipment* UBattlePlayerStatus::GetEquipment(EItemType ItemID)
{
	if (!Equipments.Contains(ItemID))
		return NoEquipment;

	UEquipment* Equipment = Equipments[ItemID];
	if (IsValid(Equipment))
		return Equipment;
	else
		return NoEquipment;
}

/// <summary>
/// 装備品を全て取得する
/// </summary>
/// <return> 装備 </return>
TArray<UEquipment*> UBattlePlayerStatus::GetEquipments() {

	TArray<UEquipment*> ReturnEquipments;
	for (TPair<EItemType, UEquipment*> Equiment : Equipments)
	{
		if(IsValid(Equiment.Value))
			ReturnEquipments.Add(Equiment.Value);
		else
			ReturnEquipments.Add(NoEquipment);
	}
	return ReturnEquipments;
};

/// <summary>
/// 装備品を装備する
/// </summary>
/// <param name="ItemID">装備部位</param>
/// <param name="NewProtector">装備品</param>
/// <return> 装備していた装備 </return>
UEquipment* UBattlePlayerStatus::EquipItem(EItemType ItemID, UEquipment* Equipment)
{
	EItemType ItemType;
	if(IsValid(Equipment))
		ItemType = Equipment->GetItemType();
	else
		ItemType = ItemID;
	if(ItemType == EItemType::NoEquipmwnt)
		ItemType = ItemID;

	UEquipment* OldEquipment = nullptr;
	if (Equipments.Contains(ItemType))
	{
		OldEquipment = Equipments[ItemType];
		Equipments[ItemType] = Equipment;
	}
	else {
		Equipments[ItemType] = nullptr;
	}
	return OldEquipment;
}

/// <summary>
/// 装備した場合のステータスを取得
/// </summary>
/// <param name="ItemID">装備部位</param>
/// <param name="NewProtector">装備品</param>
/// <return> 装備したステータス </return>
FStatus UBattlePlayerStatus::GetEquipStatus(EItemType ItemID, UEquipment* EquipItem)
{
	FStatus Result = Status;

	for (TPair<EItemType, UEquipment*> Equipment : Equipments)
	{
		if (Equipment.Key == EquipItem->GetItemType())
		{
			Result = Status::AddStatus(Result, EquipItem->GetStatus());
		}
		else
		{
			if (Equipment.Key == ItemID) continue;
			if (!IsValid(Equipment.Value)) continue;
				Result = Status::AddStatus(Result, Equipment.Value->GetStatus());
		}
	}

	return Result;
}

/// <summary>
/// 最終的な属性耐性を取得する
/// </summary>
/// <return> 属性耐性 </return>
float UBattlePlayerStatus::GetResistRateElement(EElementID Element)
{
	float Result = 1.0f;
	for (TPair<EItemType, UEquipment*> Equipment : Equipments)
	{
		UProtector* Protector = Cast<UProtector>(Equipment.Value);
		if (!IsValid(Protector)) continue;
		TArray<FElementResist> ElementResist = Protector->GetElementResist();
		FElementResist* ResultElementResist = ElementResist.FindByPredicate([&Element](const FElementResist& Resist) { return Resist.Element == Element; });
		if (ResultElementResist != nullptr)
			Result *= ResultElementResist->Rate;
	}
	return Result;
}

/// <summary>
/// 最終的な状態異常耐性を取得する
/// </summary>
/// <return> 状態異常耐性 </return>
float UBattlePlayerStatus::GetResistRateState(EStateID State)
{
	float Result = 1.0f;
	for (TPair<EItemType, UEquipment*> Equipment : Equipments)
	{
		UProtector* Protector = Cast<UProtector>(Equipment.Value);
		if (!IsValid(Protector)) continue;
		TArray<FStateResist> StateResist = Protector->GetStateResist();
		FStateResist* ResultStateResist = StateResist.FindByPredicate([&State](const FStateResist& Resist) { return Resist.State == State; });
		if (ResultStateResist != nullptr)
			Result *= ResultStateResist->Rate;
	}
	return Result;
}

/// <summary>
/// 実際に行動するスキル取得
/// </summary>
/// <returns>行動パターン</returns>
EUseSkillType UBattlePlayerStatus::GetActionSkill(USkill* UseSkill)
{
	FSkillParameter SkillParameter = UseSkill->GetSkillParameter();
	int UseMp = SkillParameter.Mp;
	if (Status.Mp >= UseMp)
		return EUseSkillType::DefaultUseSkill;

	if(SkillParameter.bIsMagic)
		return EUseSkillType::MagicFumble;
	else
		return EUseSkillType::AbilityFumble;
}

/// <summary>
/// 全回復する
/// </summary>
void UBattlePlayerStatus::FullHeal()
{
	FStatus FinallyStatus = GetFinallyStatus();
	NowHp = FinallyStatus.Hp;
	NowMp = FinallyStatus.Mp;
}
