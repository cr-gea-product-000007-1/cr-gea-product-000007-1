﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BattleCharacterAnimationBlock.generated.h"


UENUM(BlueprintType)
enum class EBattleCharacterAnimationBlock : uint8
{
    None,
    Idle,
    LookForward,
    LookTarget,
    AttackAction,
    MagicAction,
    Die,
};