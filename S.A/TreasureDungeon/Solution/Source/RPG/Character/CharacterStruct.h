﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CharacterStruct.generated.h"


class UBattleCharacter;
class UBattlePlayerStatus;
class UBattleEnemyStatus;

USTRUCT(BlueprintType)
struct FCharacterStruct
{
	GENERATED_USTRUCT_BODY()

	UBattleCharacter* Character;
};

USTRUCT(BlueprintType)
struct FPlayerStruct
{
	GENERATED_USTRUCT_BODY()

	UBattleCharacter* Character;
	UBattlePlayerStatus* Status;
};

USTRUCT(BlueprintType)
struct FEnemyStruct
{
	GENERATED_USTRUCT_BODY()

	UBattleCharacter* Character;
	UBattleEnemyStatus* Status;
};