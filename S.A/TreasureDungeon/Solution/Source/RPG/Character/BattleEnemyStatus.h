﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BattleCharacterStatus.h"
#include "StatusData/EnemyParameter.h"
#include "../Skill/EnemySkillQualification.h"
#include "../Item/ItemID.h"
#include "BattleEnemyStatus.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UBattleEnemyStatus : public UBattleCharacterStatus
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 敵情報を設定する
	/// </summary>
	/// <param name="Parameter">敵情報</param>
	/// <param name="EnemyTypeNum">同じ敵の出現数</param>
	void SetEnemyStatus(FEnemyParameter Parameter, int EnemyTypeNum);

	/// <summary>
	/// プレイヤーか
	/// </summary>
	/// <return> プレイヤー = true </return>
	virtual bool IsPlayer() { return false; };

	/// <summary>
	/// 最終的なステータスを取得する
	/// </summary>
	/// <return> 最終的なステータス </return>
	virtual FStatus GetFinallyStatus() override;

	/// <summary>
	/// このターンに使用するスキルのIDを取得する
	/// </summary>
	/// <param name="TurnNum">ターン数</param>
	/// <return> 使用スキルID </return>
	ESkillID GetUseSkillID(int TurnNum);

	/// <summary>
	/// 最終的な属性耐性を取得する
	/// </summary>
	/// <return> 属性耐性 </return>
	float GetResistRateElement(EElementID Element) override;

	/// <summary>
	/// 最終的な状態異常耐性を取得する
	/// </summary>
	/// <return> 状態異常耐性 </return>
	float GetResistRateState(EStateID State) override;

	/// <summary>
	/// 実際に行動するスキル取得
	/// </summary>
	/// <returns>行動パターン</returns>
	EUseSkillType GetActionSkill(USkill* UseSkill) override;

	/// <summary>
	/// 獲得経験値取得
	/// </summary>
	/// <returns>獲得経験値取得</returns>
	int GetExp() { return Exp; };

	/// <summary>
	/// 獲得ゴールド取得
	/// </summary>
	/// <returns>獲得ゴールド取得</returns>
	int GetGold() { return Gold; };

	/// <summary>
	/// 今回ドロップするアイテムのID取得
	/// </summary>
	/// <returns>ドロップアイテムID</returns>
	EItemID GetDropItemID();

private:
	/// <summary>
	/// 使用するスキルの条件リスト
	/// </summary>
	UPROPERTY()
	TArray<FEnemySkillQualification> UseSkills;

	/// <summary>
	/// 属性耐性
	/// </summary>
	TArray<FElementResist> ElementResist;

	/// <summary>
	/// 状態異常耐性
	/// </summary>
	TArray<FStateResist> StateResist;

	/// <summary>
	/// 獲得経験値
	/// </summary>
	int Exp;

	/// <summary>
	/// 獲得ゴールド
	/// </summary>
	int Gold;

	/// <summary>
	/// ドロップアイテムのID
	/// </summary>
	EItemID DropItemID;

	/// <summary>
	/// アイテムのドロップ率
	/// </summary>
	float DropRate;
};
