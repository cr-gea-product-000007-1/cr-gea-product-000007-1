﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleCharacterMesh.h"

// Sets default values
ABattleCharacterMesh::ABattleCharacterMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABattleCharacterMesh::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABattleCharacterMesh::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/// <summary>
/// アニメーション再生
/// </summary>
/// <param name="AnimatoinType">再生させるアニメーションの種類</param>
void ABattleCharacterMesh::PlayMeshAnimation(ECharacterActionType AnimatoinType)
{
	if (IsLock()) return;

	switch (AnimatoinType)
	{
	case ECharacterActionType::Idel:
		PlayIdle();
		break;

	case ECharacterActionType::Run:
		PlayRun();
		break;

	case ECharacterActionType::Jump:
		PlayJump();
		break;

	case ECharacterActionType::Attack:
		PlayAttack();
		break;

	case ECharacterActionType::Magic:
		PlayMagic();
		break;

	case ECharacterActionType::Guard:
		PlayGuard();
		break;

	case ECharacterActionType::Item:
		PlayItem();
		break;

	case ECharacterActionType::Damage:
		PlayDamage();
		break;

	case ECharacterActionType::Shock:
		PlayShock();
		break;

	case ECharacterActionType::Stan:
		PlayStan();
		break;

	case ECharacterActionType::Glad:
		PlayGlad();
		break;

	case ECharacterActionType::Die:
		PlayDie();
		break;
	}
}

/// <summary>
/// 現在地を初期位置に設定
/// </summary>
void ABattleCharacterMesh::FirstLocationSetting()
{
	FirstLocation = GetActorLocation();
}