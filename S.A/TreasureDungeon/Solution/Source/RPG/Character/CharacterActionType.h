﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CharacterActionType.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class ECharacterActionType : uint8
{
	None,   //なし
	Idel,   //待機
	Run,    //走る
	Jump,   //ジャンプ
	Attack, //攻撃
	Magic,  //魔法
	Guard,  //防御
	Item,   //道具使用
	Damage, //ダメージ
	Shock,  //衝撃を受ける
	Stan,   //休み
	Sleep,  //眠り
	Glad,   //喜ぶ
	Die,    //死亡
};