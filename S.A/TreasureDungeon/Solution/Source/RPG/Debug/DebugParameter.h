// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Item/ItemID.h"
#include "Engine/DataTable.h"
#include "DebugParameter.generated.h"


UENUM(BlueprintType)
enum class EPlayerStartPoint : uint8
{
	Start,
	CheckPint1,
	Boss,
};


USTRUCT(BlueprintType)
struct FDebugParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// デバッグ機能を有効にするか
	/// </summary>
	UPROPERTY(EditAnywhere)
	bool bIsDebug;

	/// <summary>
	/// 開始位置
	/// </summary>
	UPROPERTY(EditAnywhere)
	EPlayerStartPoint PlayerStartPoint;

	/// <summary>
	/// レベル
	/// </summary>
	UPROPERTY(EditAnywhere)
	int StartLevel;

	/// <summary>
	/// 武器
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Weapon;

	/// <summary>
	/// 盾
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Shield;

	/// <summary>
	/// 兜
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Helmet;

	/// <summary>
	/// 鎧
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Armor;

	/// <summary>
	/// 装飾品
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID Accessory;

	/// <summary>
	/// 所持品
	/// </summary>
	UPROPERTY(EditAnywhere)
	TMap<EItemID,int> HaveItems;

	/// <summary>
	/// 所持ゴールド
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Gold;

	/// <summary>
	/// エンカウントを有効にするか
	/// </summary>
	UPROPERTY(EditAnywhere)
	bool bIsEncount;
};