﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "../System/GameInstanceDataSetter.h"
#include "BootLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ABootLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	enum EPhaseType
	{
		Initialize,
		DataLoad,
		PlayerCreate,
		LevelChange,
	};

	ABootLevelScriptActor();

public:
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void BeginPlay() override;

private:
	/// <summary>
	/// データテーブルをゲームインスタントに読み込む
	/// </summary>
	UPROPERTY()
	UGameInstanceDataSetter* GameInstanceDataSetter;

	/// <summary>
	/// フェーズ
	/// </summary>
	EPhaseType Phase;
};