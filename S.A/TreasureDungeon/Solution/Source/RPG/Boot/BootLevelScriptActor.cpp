﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BootLevelScriptActor.h"
#include "../System/CreateFirstPlayer.h"


/// <summary>
/// コンストラクタ
/// </summary>
ABootLevelScriptActor::ABootLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
///　Tick関数の継承
/// 毎フレーム呼ばれる
/// </summary>
void ABootLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	switch (Phase)
	{
	case EPhaseType::Initialize:
		GameInstanceDataSetter = NewObject<UGameInstanceDataSetter>(this);
		GameInstanceDataSetter->LoadStart();
		Phase = EPhaseType::DataLoad;
		break;

	case EPhaseType::DataLoad:
		GameInstanceDataSetter->LoadDataUpdate();
		if(GameInstanceDataSetter->IsLoadFinish())
			Phase = EPhaseType::PlayerCreate;
		break;

	case EPhaseType::PlayerCreate:
	{
		UCreateFirstPlayer* CreateFirstPlayer = NewObject<UCreateFirstPlayer>(this);
		CreateFirstPlayer->Initialize();
		CreateFirstPlayer->PlayerCreate();

		Phase = EPhaseType::LevelChange;
		break;
	}
	//次のレベルへ移動
	case EPhaseType::LevelChange:
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		break;

	}
}

/// <summary>
///　BeginPlay関数の継承
/// </summary>
void ABootLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	//ウインドサイズ設定
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), "r.SetRes 1280x720w");

	Phase = EPhaseType::Initialize;
}
