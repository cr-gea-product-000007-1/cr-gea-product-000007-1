﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ShopIndexs.generated.h"

UENUM()
enum class EShopActionType : uint8
{
	Buy = 0,
	Sell = 1,
	End = 2,

	Begin = 0,
};

UENUM()
enum class EShopItemType : uint8
{
	Item = 0,
	Equipment = 1,

	Begin = 0,
	End = 1,
};