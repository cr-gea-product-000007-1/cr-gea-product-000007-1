﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ShopItemCountWidget.h"

/// <summary>
/// 最小数
/// </summary>
int MinCount = 1;

/// <summary>
/// 最大数の最大値
/// </summary>
int MaxMaxCount = 99;

/// <summary>
/// 個数を設定
/// </summary>
void UShopItemCountWidget::SetCounter(int NewCounter)
{
	Counter = NewCounter;
	if (Counter < MinCount)
		Counter = MinCount;
	if (MaxCount > 0)
		if (Counter > MaxCount)
			Counter = MaxCount;
	UpdateWindow();
}

/// <summary>
/// 最大数を設定
/// </summary>
void UShopItemCountWidget::SetMaxIndex(int Count)
{
	MaxCount = Count;
	if(MaxCount >= MaxMaxCount)
		MaxCount = MaxMaxCount;
};

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UShopItemCountWidget::OnUp()
{
	SetCounter(Counter + 1);
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UShopItemCountWidget::OnDown()
{
	SetCounter(Counter - 1);
	return true;
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UShopItemCountWidget::OnLeft()
{
	SetCounter(Counter + 10);
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UShopItemCountWidget::OnRight()
{
	SetCounter(Counter - 10);
	return true;
}