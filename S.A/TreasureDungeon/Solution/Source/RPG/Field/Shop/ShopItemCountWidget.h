﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/InputWidget.h"
#include "ShopItemCountWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UShopItemCountWidget : public UInputWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 個数を設定
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void SetCounter(int NewCounter);

	/// <summary>
	/// 個数を取得
	/// </summary>
	int GetCounter() { return Counter; };

	/// <summary>
	/// 最大数を設定
	/// </summary>
	void SetMaxIndex(int Count);

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnRight() override;

public:
	/// <summary>
	/// 選んでいるインデックス
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	int Counter;

private:
	/// <summary>
	/// 最大数
	/// </summary>
	int MaxCount;
};
