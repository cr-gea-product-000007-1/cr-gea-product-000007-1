﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "../../System/UI/BaseSelectWidget.h"
#include "../../System/UI/DescriptionWidget.h"
#include "../../System/UI/ValueWidget.h"
#include "../../System/UI/AllPlayerEquipmentStatusWidget.h"
#include "../UI/GoldWidget.h"
#include "ShopIndexs.h"
#include "ShopItemCountWidget.h"

#include "../Interface/FieldEventInterface.h"
#include "../Interface/FieldManagerStopperInterface.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "../../Sound/SoundPlayer.h"
#include "../../System/Interface/SendUnderMessageInterface.h"
#include "../../System/Interface/LoadDataInterface.h"

#include "ShopManager.generated.h"


UCLASS()
class RPG_API UShopManager : public UObject, public ILoadDataInterface, public IInputWidgetInterface, public ISendUnderMessageInterface
{
	GENERATED_BODY()

	enum class EActiveWidgetType
	{
		None,
		ActionSelect,
		BuyItemSelect,
		BuyCountSelect,
		BuyEquimpentTargetSelect,
		SellItemTypeSelect,
		SellItemSelect,
		SellCountSelect,
		End,
	};

public:
	/// <summary>
	/// ウィジットを生成
	/// </summary>
	void WidgetCreate();

	/// <summary>
	/// ショップを始める
	/// </summary>
	/// <param name="ItemIDs">商品リストのID</param>
	void ShopStart(TArray<EItemID> ItemIDs);

	/// <summary>
	/// ウィジットを全て閉じる
	/// </summary>
	void AllClose();

	/// <summary>
	/// ショップを開いているか
	/// </summary>
	bool IsShop() { return bIsShop; };

	/// <summary>
	/// ショップを開けるか設定する
	/// </summary>
	void SetIsOpen(bool IsOpen) { bIsOpen = IsOpen; };

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnRight() override;

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	void SetFieldEventInterface(IFieldEventInterface* Interface);

	/// <summary>
	/// マネージャー停止インターフェースを設定する
	/// </summary>
	/// <param name="Interface">マネージャー停止インターフェース</param>
	void SetFieldManagerStopperInterface(IFieldManagerStopperInterface* Interface) { FieldManagerStopperInterface = Interface; };

private:
	/// <summary>
	/// 次のウィジットに移動
	/// </summary>
	/// <param name="NextWidget">移動するウィジット</param>
	/// <param name="NewActiveWidgetType">移動するウィジットの種類</param>
	void NextWidget(UInputWidget* NextWidget, EActiveWidgetType NewActiveWidgetType);

	/// <summary>
	/// 前のウィジットに移動
	/// </summary>
	/// <param name="Num">移動回数</param>
	/// <return> 移動した = true </return>
	bool BackWidget(int Num = 1);

	/// <summary>
	/// メニューの選択
	/// </summary>
	/// <param name="MenuSelectType">選択するメニュー</param>
	void SelectCommand(EShopActionType ShopActionType);

	/// <summary>
	/// プレイヤーキャラリストを取得
	/// </summary>
	TArray<UBattlePlayerStatus*> GetPlayerStatuses();

	/// <summary>
	/// 所持アイテムを取得
	/// </summary>
	TArray<UHaveItem*> GetHaveItem();

	/// <summary>
	/// 所持装備を取得
	/// </summary>
	TArray<UHaveItem*> GetHaveEquipment();

	/// <summary>
	/// アイテムがあるか
	/// </summary>
	/// <param name="ItemType">アイテムの種類</param>
	/// <return> アイテムがある = true </return>
	bool ExitItem();

	/// <summary>
	/// 購入ウインドウの更新
	/// </summary>
	void BuyItemSelectWidgetUpdate();

	/// <summary>
	/// 売却ウインドウの更新
	/// </summary>
	void SellItemSelectWidgetUpdate();

	/// <summary>
	/// 選んでいる売却アイテム取得
	/// </summary>
	UHaveItem* GetSellSelectItem();

	/// <summary>
	/// 選んでいる購入アイテムの所持数取得
	/// </summary>
	int GetBuySelectItemHaveNum();

	/// <summary>
	/// 選んでいる購入アイテムの最大購入数取得
	/// </summary>
	int GetBuySelectItemMaxNum();

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	UPROPERTY()
	URPGGameInstance* GameInstance;

	/// <summary>
	/// 開いているウインドウの種類
	/// </summary>
	EActiveWidgetType ActiveWidgetType;

	/// <summary>
	/// 開いているウインドウ
	/// </summary>
	UPROPERTY()
	UInputWidget* ActiveWidget;

	/// <summary>
	/// 開いたウインドウの種類の記録
	/// </summary>
	TArray<EActiveWidgetType> WidgetTypeLog;

	/// <summary>
	/// 開いたウインドウの記録
	/// </summary>
	UPROPERTY()
	TArray<UInputWidget*> WidgetLog;

	/// <summary>
	/// 商品リスト
	/// </summary>
	UPROPERTY()
	TArray<UBaseItem*> BuyItems;

	/// <summary>
	/// ショップを開いているか
	/// </summary>
	bool bIsShop;

	/// <summary>
	/// ショップを開ける状態か
	/// </summary>
	bool bIsOpen = true;

	/// <summary>
	/// 所持金ウィジット
	/// </summary>
	UPROPERTY()
	UGoldWidget* GoldWidget;

	/// <summary>
	/// メニュー選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ActionSelectWidget;

	/// <summary>
	/// アイテムの種類選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ItemTypeSelectWidget;

	/// <summary>
	/// アイテム購入ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* BuyItemWidget;

	/// <summary>
	/// 装備者選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* BuyEquipSelectWidget;

	/// <summary>
	/// アイテム売却ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* SellItemWidget;

	/// <summary>
	/// アイテム個数選択ウィジット
	/// </summary>
	UPROPERTY()
	UShopItemCountWidget* ItemCountWidget;

	/// <summary>
	/// 購入アイテム説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* BuyItemDescriptionWidget;

	/// <summary>
	/// 売却アイテム説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* SellItemDescriptionWidget;

	/// <summary>
	/// 購入アイテム個数ウィジット
	/// </summary>
	UPROPERTY()
	UValueWidget* BuyItemCountWidget;

	/// <summary>
	/// 売却アイテム個数ウィジット
	/// </summary>
	UPROPERTY()
	UValueWidget* SellItemCountWidget;

	/// <summary>
	/// 装備者選択テキスト
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* BuyEquipSelectTextWidget;

	/// <summary>
	/// プレイヤーの装備時ステータス表示ウィジット
	/// </summary>
	UPROPERTY()
	UAllPlayerEquipmentStatusWidget* AllPlayerEquipmentStatusWidget;

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	IFieldEventInterface* FieldEventInterface;

	/// <summary>
	/// マネージャー停止インターフェース
	/// </summary>
	IFieldManagerStopperInterface* FieldManagerStopperInterface;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
