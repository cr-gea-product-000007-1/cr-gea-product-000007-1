﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ShopManager.h"
#include "Kismet/GameplayStatics.h"
#include "../../Item/ItemStorage.h"
#include "../../PathConstant.h"

const float CountWidgetX = 200.0f;
const FVector2D Center(960.0f, 540.0f);
const FVector2D SellItemPosition(0.0f, 100.0f);
const FVector2D ShopWindowStartPosition = (100.0f, 150.0f);

const TArray<FString> ShopMenuText = { TEXT("買う"), TEXT("売る"), TEXT("やめる") };
const TArray<FString> SellItemText = { TEXT("道具"), TEXT("装備") };
const FString BuyEquipSelectText = TEXT("装備者選択");
const FString NotEquipText = TEXT("装備しない");

/// <summary>
/// ウィジットを生成
/// </summary>
void UShopManager::WidgetCreate()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	FString Path;
	//所持金表示
	Path = Path::GoldWidgetPath;
	TSubclassOf<UGoldWidget> GoldWidgetClass = TSoftClassPtr<UGoldWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(GoldWidgetClass))
	{
		GoldWidget = CreateWidget<UGoldWidget>(GetWorld(), GoldWidgetClass);
		GoldWidget->AddToViewport();
		GoldWidget->SetRenderTranslation(FVector2D(650.0f, -450.0f));
	};
	Path = Path::OneLineSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> BaseSelectWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(BaseSelectWidgetClass))
	{
		//項目選択
		ActionSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), BaseSelectWidgetClass);
		ActionSelectWidget->AddToViewport();
		ActionSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 3.0 });
		ActionSelectWidget->SetText(ShopMenuText);
		ActionSelectWidget->Hide();
		//装備者選択
		BuyEquipSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), BaseSelectWidgetClass);
		BuyEquipSelectWidget->AddToViewport();
		BuyEquipSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 5, 6.0f });
		BuyEquipSelectWidget->Hide();
		//アイテムの種類選択
		ItemTypeSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), BaseSelectWidgetClass);
		ItemTypeSelectWidget->AddToViewport();
		ItemTypeSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 2, 3.0 });
		ItemTypeSelectWidget->SetText(SellItemText);
		ItemTypeSelectWidget->Hide();
	};
	Path = Path::OneLinePageSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> SelectPageWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(SelectPageWidgetClass))
	{
		//購入アイテム選択
		BuyItemWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectPageWidgetClass);
		BuyItemWidget->AddToViewport();
		BuyItemWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 8, 10.0 });
		BuyItemWidget->Hide();
		//売却アイテム選択
		SellItemWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectPageWidgetClass);
		SellItemWidget->AddToViewport();
		SellItemWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 8, 13.0 });
		SellItemWidget->Hide();
	}
	Path = Path::DescriptionWidgetPath;
	TSubclassOf<UDescriptionWidget> DescriptionWidgetClass = TSoftClassPtr<UDescriptionWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(DescriptionWidgetClass))
	{
		//購入アイテム説明文
		BuyItemDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		BuyItemDescriptionWidget->AddToViewport();
		BuyItemDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 8.0f });
		BuyItemDescriptionWidget->Hide();
		BuyItemWidget->AddSubWidget(BuyItemDescriptionWidget);
		//売却アイテム説明文
		SellItemDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		SellItemDescriptionWidget->AddToViewport();
		SellItemDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 8.0f });
		SellItemDescriptionWidget->Hide();
		SellItemWidget->AddSubWidget(SellItemDescriptionWidget);
		//装備者選択テキスト
		BuyEquipSelectTextWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		BuyEquipSelectTextWidget->AddToViewport();
		BuyEquipSelectTextWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 7.75f });
		BuyEquipSelectTextWidget->SetText(BuyEquipSelectText);
		BuyEquipSelectTextWidget->Hide();
		BuyEquipSelectWidget->AddSubWidget(BuyEquipSelectTextWidget);
	};
	Path = Path::ValueWidgetPath;
	TSubclassOf<UValueWidget> ValueWidgetClass = TSoftClassPtr<UValueWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(ValueWidgetClass))
	{
		//購入アイテム個数
		BuyItemCountWidget = CreateWidget<UValueWidget>(GetWorld(), ValueWidgetClass);
		BuyItemCountWidget->AddToViewport();
		BuyItemCountWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 8.0f });
		BuyItemCountWidget->SetItemUnit();
		BuyItemCountWidget->Hide();
		BuyItemWidget->AddSubWidget(BuyItemCountWidget);
		//売却アイテム個数
		SellItemCountWidget = CreateWidget<UValueWidget>(GetWorld(), ValueWidgetClass);
		SellItemCountWidget->AddToViewport();
		SellItemCountWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 8.0f });
		SellItemCountWidget->SetItemUnit();
		SellItemCountWidget->Hide();
		SellItemWidget->AddSubWidget(SellItemCountWidget);
	};
	//個数選択
	Path = Path::ShopItemCountWidgetPath;
	TSubclassOf<UShopItemCountWidget> ShopValueWidgetClass = TSoftClassPtr<UShopItemCountWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(ShopValueWidgetClass))
	{
		ItemCountWidget = CreateWidget<UShopItemCountWidget>(GetWorld(), ShopValueWidgetClass);
		ItemCountWidget->AddToViewport();
	};
	//装備ステータス表示
	Path = Path::AllPlayerEquipmentStatusWidgetPath;
	TSubclassOf<UAllPlayerEquipmentStatusWidget> AllPlayerEquipmentStatusWidgetClass = TSoftClassPtr<UAllPlayerEquipmentStatusWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(ShopValueWidgetClass))
	{
		AllPlayerEquipmentStatusWidget = CreateWidget<UAllPlayerEquipmentStatusWidget>(GetWorld(), AllPlayerEquipmentStatusWidgetClass);
		AllPlayerEquipmentStatusWidget->AddToViewport();
		AllPlayerEquipmentStatusWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, GetPlayerStatuses().Num(), 10.0 }, BuyItemDescriptionWidget->GetWindowSize());
		AllPlayerEquipmentStatusWidget->Hide();
		BuyItemWidget->AddSubWidget(AllPlayerEquipmentStatusWidget);
	};

	ActionSelectWidget->SetCanvasPannelRenderTranslation(ShopWindowStartPosition);

	BuyItemWidget->SetCanvasPannelRenderTranslation(ActionSelectWidget->GetUpperRightCorner());
	BuyItemDescriptionWidget->SetCanvasPannelRenderTranslation(BuyItemWidget->GetUpperRightCorner());
	BuyItemCountWidget->SetRenderTranslation(BuyItemDescriptionWidget->GetLowerLeftCorner());
	AllPlayerEquipmentStatusWidget->SetCanvasPannelRenderTranslation(BuyItemDescriptionWidget->GetLowerLeftCorner());
	BuyEquipSelectTextWidget->SetCanvasPannelRenderTranslation(BuyItemDescriptionWidget->GetUpperRightCorner());
	BuyEquipSelectWidget->SetCanvasPannelRenderTranslation(BuyEquipSelectTextWidget->GetLowerLeftCorner());

	ItemTypeSelectWidget->SetCanvasPannelRenderTranslation(ActionSelectWidget->GetUpperRightCorner());
	SellItemWidget->SetCanvasPannelRenderTranslation(ItemTypeSelectWidget->GetUpperRightCorner());
	SellItemDescriptionWidget->SetCanvasPannelRenderTranslation(SellItemWidget->GetUpperRightCorner());
	SellItemCountWidget->SetRenderTranslation(SellItemDescriptionWidget->GetLowerLeftCorner());

	SoundPlayer = NewObject<USoundPlayer>();
}

/// <summary>
/// ショップを始める
/// </summary>
/// <param name="ItemIDs">商品リストのID</param>
void UShopManager::ShopStart(TArray<EItemID> ItemIDs)
{
	if (FieldManagerStopperInterface == nullptr) return;

	GoldWidget->Open();
	GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
	ActionSelectWidget->Open();
	ActiveWidgetType = EActiveWidgetType::ActionSelect;
	ActiveWidget = ActionSelectWidget;
	WidgetLog.Empty();
	WidgetTypeLog.Empty();
	bIsShop = true;

	BuyItems.Empty();
	//ショップ内容を読み込み
	for (EItemID ItemID : ItemIDs)
	{
		UBaseItem* BuyItem = GetBaseItem(this, ItemID);
		if (IsValid(BuyItem))
			BuyItems.Add(BuyItem);
	}
	BuyItemWidget->SetItems(BuyItems,ESelectViewType::NameAndValue);
	AllPlayerEquipmentStatusWidget->SetPlayerStatuses(GameInstance->PlayerStatuses);
	BuyEquipSelectWidget->SetPlayerStatus(GameInstance->PlayerStatuses);
	BuyEquipSelectWidget->AddText(NotEquipText);
}

/// <summary>
/// ウィジットを全て閉じる
/// </summary>
void UShopManager::AllClose()
{
	ActiveWidgetType = EActiveWidgetType::None;
	ActiveWidget = nullptr;
	bIsShop = false;
	WidgetLog.Empty();
	//FieldManagerStopperInterface->SetFieldEventFlag(true);

	if (GoldWidget->IsOpen())
		GoldWidget->Close();

	if (ActionSelectWidget->IsOpen())
		ActionSelectWidget->Close();

	if (BuyItemWidget->IsOpen())
		BuyItemWidget->Close();

	if (SellItemWidget->IsOpen())
		SellItemWidget->Close();

	if (ItemCountWidget->IsOpen())
		ItemCountWidget->Close();

	if (AllPlayerEquipmentStatusWidget->IsOpen())
		AllPlayerEquipmentStatusWidget->Close();
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UShopManager::OnDecide()
{
	if (!IsValid(ActiveWidget)) return;
	ActiveWidget->OnDecide();

	switch (ActiveWidgetType)
	{
	//メニュー選択
	case EActiveWidgetType::ActionSelect:
	{
		EShopActionType ShopActionType = (EShopActionType)ActionSelectWidget->GetSelectIndex();
		SelectCommand(ShopActionType);
		break;
	}
	//-------------------------------------------------------
	// 購入
	//-------------------------------------------------------

	//アイテム選択
	case EActiveWidgetType::BuyItemSelect:
	{
		UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
		switch (BuyItem->GetItemType())
		{
		case EItemType::Item:
		{
			int MaxNum = GetBuySelectItemMaxNum();
			//0こしか買えないなら進まない
			if (MaxNum == 0)
			{
				SoundPlayer->PlayCancelSound(this);
				break;
			}
			SoundPlayer->PlayDecisionSound(this);
			BuyItemWidget->StopArrow();
			NextWidget(ItemCountWidget, EActiveWidgetType::BuyCountSelect);
			ItemCountWidget->Open();
			ItemCountWidget->SetMaxIndex(MaxNum);
			break;
		}
		case EItemType::NoEquipmwnt:
			break;

		//装備全種類
		default:
		{
			int SumValue = BuyItem->GetValue();
			if (SumValue > GameInstance->Gold)
			{
				SoundPlayer->PlayCancelSound(this);
				break;
			}
			SoundPlayer->PlayDecisionSound(this);
			BuyItemWidget->StopArrow();
			NextWidget(BuyEquipSelectWidget, EActiveWidgetType::BuyEquimpentTargetSelect);
			BuyEquipSelectWidget->Open();
			break;
		}
		}
		break;
	}
	//購入数選択
	case EActiveWidgetType::BuyCountSelect:
	{
		UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
		int ItemCount = ItemCountWidget->GetCounter();

		int SumValue = BuyItem->GetValue()* ItemCount;
		if (SumValue <= GameInstance->Gold)
		{
			SoundPlayer->PlayDecisionSound(this);
			GameInstance->Gold -= SumValue;
			GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
			FieldEventInterface->InsertItem(BuyItem->GetItemID(), ItemCount);
			BackWidget(); 
			BuyItemSelectWidgetUpdate();
		}
		else {
			SoundPlayer->PlayCancelSound(this);
		}
		break;
	}
	//装備者選択
	case EActiveWidgetType::BuyEquimpentTargetSelect:
	{
		int Index = BuyEquipSelectWidget->GetSelectIndex();
		UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
		if (Index == GetPlayerStatuses().Num())
		{
			GameInstance->Gold -= BuyItem->GetValue();
			GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
			FieldEventInterface->InsertItem(BuyItem->GetItemID(), 1);
			BackWidget();
			BuyItemSelectWidgetUpdate();
		}
		else {
			UEquipment* Equipment = Cast<UEquipment>(BuyItem);
			if (IsValid(Equipment))
			{
				GameInstance->Gold -= BuyItem->GetValue();
				GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
				UEquipment* OldEquipment = GetPlayerStatuses()[Index]->EquipItem(Equipment->GetItemType(), Equipment);
				if(IsValid(OldEquipment))
					FieldEventInterface->InsertItem(OldEquipment->GetItemID(), 1);
				BackWidget();
				BuyItemSelectWidgetUpdate();
			}
		}
		break;
	}
	//-------------------------------------------------------
	// 売却
	//-------------------------------------------------------

	//アイテム分類選択
	case EActiveWidgetType::SellItemTypeSelect:
	{
		if (!ExitItem())
		{
			SoundPlayer->PlayCancelSound(this);
			break;
		}
		SoundPlayer->PlayDecisionSound(this);
		ItemTypeSelectWidget->StopArrow();

		NextWidget(SellItemWidget, EActiveWidgetType::SellItemSelect);
		SellItemWidget->Open();
		SellItemSelectWidgetUpdate();
		break;
	}
	//アイテム選択
	case EActiveWidgetType::SellItemSelect:
	{
		SoundPlayer->PlayDecisionSound(this);
		SellItemWidget->StopArrow();
		NextWidget(ItemCountWidget, EActiveWidgetType::SellCountSelect);
		ItemCountWidget->Open();
		UHaveItem* SellHaveItem = GetSellSelectItem();
		if (SellHaveItem == nullptr) break;
		ItemCountWidget->SetMaxIndex(SellHaveItem->GetNum());
		break;
	}
	//アイテム数選択
	case EActiveWidgetType::SellCountSelect:
	{
		UHaveItem* SellHaveItem = GetSellSelectItem();
		if (SellHaveItem == nullptr) break;
		UBaseItem* SellItem = SellHaveItem->GetItem();
		int SellItemNum = SellHaveItem->GetNum();
		int ItemCount = ItemCountWidget->GetCounter();

		if (ItemCount <= SellItemNum)
		{
			int SumValue = SellItem->GetSellValue() * ItemCount;
			SoundPlayer->PlayDecisionSound(this);
			GameInstance->Gold += SumValue;
			GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
			FieldEventInterface->LostItem(SellItem->GetItemID(), ItemCount);
			//アイテムが残っていたら1回、ないなら2回戻る
			if (ExitItem())
			{
				BackWidget(1);
				SellItemSelectWidgetUpdate();
			}
			else{
				BackWidget(2);
			}
		}
		else {
			SoundPlayer->PlayCancelSound(this);
		}
		break;
	}
	};
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void UShopManager::OnCancel()
{
	if (BackWidget())
	{
		SoundPlayer->PlayCancelSound(this);
		return;
	}

	if (ActiveWidgetType != EActiveWidgetType::ActionSelect) return;
	SoundPlayer->PlayCancelSound(this);
	AllClose();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void UShopManager::OnUp()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnUp())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::BuyItemSelect)
		BuyItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::SellItemSelect)
		SellItemSelectWidgetUpdate();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void UShopManager::OnDown()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnDown())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::BuyItemSelect)
		BuyItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::SellItemSelect)
		SellItemSelectWidgetUpdate();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void UShopManager::OnLeft()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnLeft())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::BuyItemSelect)
		BuyItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::SellItemSelect)
		SellItemSelectWidgetUpdate();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void UShopManager::OnRight()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnRight())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::BuyItemSelect)
		BuyItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::SellItemSelect)
		SellItemSelectWidgetUpdate();
}

/// <summary>
/// 次のウィジットに移動
/// </summary>
/// <param name="NextWidget">移動するウィジット</param>
/// <param name="NewActiveWidgetType">移動するウィジットの種類</param>
void UShopManager::NextWidget(UInputWidget* NextWidget, EActiveWidgetType NewActiveWidgetType)
{
	if (IsValid(ActiveWidget))
	{
		WidgetLog.Add(ActiveWidget);
		WidgetTypeLog.Add(ActiveWidgetType);
	}
	ActiveWidget = NextWidget;
	ActiveWidgetType = NewActiveWidgetType;
}

/// <summary>
/// 前のウィジットに移動
/// </summary>
/// <param name="Num">移動回数</param>
/// <return> 移動した = true </return>
bool UShopManager::BackWidget(int Num)
{
	for (int i = 0; i < Num; i++)
	{
		if (WidgetLog.Num() == 0)
			return false;
		ActiveWidget->Close();

		int Index = WidgetLog.Num() - 1;
		ActiveWidget = WidgetLog[Index];
		WidgetLog.RemoveAt(Index);

		ActiveWidgetType = WidgetTypeLog[Index];
		WidgetTypeLog.RemoveAt(Index);

		ActiveWidget->ActiveArrow();
	}
	return true;
}

/// <summary>
/// メニューの選択
/// </summary>
/// <param name="MenuSelectType">選択するメニュー</param>
void UShopManager::SelectCommand(EShopActionType ShopActionType)
{
	switch (ShopActionType)
	{
	case EShopActionType::Buy:
	{
		NextWidget(BuyItemWidget, EActiveWidgetType::BuyItemSelect);
		BuyItemWidget->Open();
		BuyItemSelectWidgetUpdate();
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->StopArrow();
		ItemCountWidget->SetRenderTranslation(BuyItemDescriptionWidget->GetUpperRightCorner());
		break;
	}
	case EShopActionType::Sell:
	{
		NextWidget(ItemTypeSelectWidget, EActiveWidgetType::SellItemTypeSelect);
		ItemTypeSelectWidget->Open();
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->StopArrow();
		ItemCountWidget->SetRenderTranslation(SellItemDescriptionWidget->GetUpperRightCorner());
		break;
	}
	case EShopActionType::End:
	{
		SoundPlayer->PlayDecisionSound(this);
		AllClose();
		break;
	}
	}
}

/// <summary>
/// プレイヤーキャラリストを取得
/// </summary>
TArray<UBattlePlayerStatus*> UShopManager::GetPlayerStatuses()
{
	if (GameInstance == nullptr)
		GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	return GameInstance->PlayerStatuses;
}

/// <summary>
/// 所持アイテムを取得
/// </summary>
TArray<UHaveItem*> UShopManager::GetHaveItem()
{
	if (GameInstance == nullptr)
		GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	return GameInstance->ItemStorage->GetInItems();
}

/// <summary>
/// 所持装備を取得
/// </summary>
TArray<UHaveItem*> UShopManager::GetHaveEquipment()
{
	if (GameInstance == nullptr)
		GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	TArray<UHaveItem*>& HaveWeapons = GameInstance->ItemStorage->GetInWeapons();
	TArray<UHaveItem*>& HaveProtectors = GameInstance->ItemStorage->GetInProtectors();
	TArray<UHaveItem*> HaveItems = HaveWeapons;
	HaveItems.Append(HaveProtectors);
	return HaveItems;
}

/// <summary>
/// フィールドイベントインターフェース
/// </summary>
void UShopManager::SetFieldEventInterface(IFieldEventInterface* Interface)
{ 
	FieldEventInterface = Interface;
};

/// <summary>
/// アイテムがあるか
/// </summary>
/// <param name="ItemType">アイテムの種類</param>
/// <return> アイテムがある = true </return>
bool UShopManager::ExitItem()
{
	EShopItemType ItemType = (EShopItemType)ItemTypeSelectWidget->GetSelectIndex();
	switch (ItemType)
	{
	case EShopItemType::Item:
		return GetHaveItem().Num() > 0;

	case EShopItemType::Equipment:
		return GetHaveEquipment().Num() > 0;
	}
	return false;
};

/// <summary>
/// 購入ウインドウの更新
/// </summary>
void UShopManager::BuyItemSelectWidgetUpdate()
{
	BuyItemWidget->UpdateWindow();

	UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
	BuyItemDescriptionWidget->SetText(BuyItem->GetDescription());

	UEquipment* Equipment = Cast<UEquipment>(BuyItem);
	if (IsValid(Equipment))
	{
		AllPlayerEquipmentStatusWidget->SetEquipment(Equipment);
		AllPlayerEquipmentStatusWidget->Open();
		AllPlayerEquipmentStatusWidget->UpdateWindow();
		BuyItemCountWidget->Close();
	}
	else {
		AllPlayerEquipmentStatusWidget->Close();
		AllPlayerEquipmentStatusWidget->SetEquipment(nullptr);
		BuyItemCountWidget->Open();
		BuyItemCountWidget->SetNum(GetBuySelectItemHaveNum());
	}
}

/// <summary>
/// 売却ウインドウの更新
/// </summary>
void UShopManager::SellItemSelectWidgetUpdate()
{
	EShopItemType ItemType = (EShopItemType)ItemTypeSelectWidget->GetSelectIndex();
	switch (ItemType)
	{
	case EShopItemType::Item:
		SellItemWidget->SetHaveItems(GetHaveItem(), ESelectViewType::NameAndCountAndSellValue);
		SellItemWidget->ClampSelectIndex();
		SellItemCountWidget->SetNum(GetSellSelectItem()->GetNum());
		break;

	case EShopItemType::Equipment:
		SellItemWidget->SetHaveItems(GetHaveEquipment(), ESelectViewType::NameAndCountAndSellValue);
		SellItemWidget->ClampSelectIndex();
		SellItemCountWidget->SetNum(GetSellSelectItem()->GetNum());
		break;
	}

	SellItemWidget->UpdateWindow();
	SellItemDescriptionWidget->SetText(GetSellSelectItem()->GetItem()->GetDescription());
}

/// <summary>
/// 選んでいる売却アイテム取得
/// </summary>
UHaveItem* UShopManager::GetSellSelectItem()
{
	UHaveItem* SellHaveItem = nullptr;
	EShopItemType ItemType = (EShopItemType)ItemTypeSelectWidget->GetSelectIndex();
	switch (ItemType)
	{
	case EShopItemType::Item:
		SellHaveItem = GetHaveItem()[SellItemWidget->SelectIndex];
		break;

	case EShopItemType::Equipment:
		SellHaveItem = GetHaveEquipment()[SellItemWidget->SelectIndex];
		break;
	}
	return SellHaveItem;
}

/// <summary>
/// 選んでいる購入アイテムの所持数取得
/// </summary>
int UShopManager::GetBuySelectItemHaveNum()
{
	UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
	TArray<UHaveItem*>HaveItem = GetHaveItem();
	UHaveItem* SelectHaveItem = nullptr;
	if (HaveItem.Num() > 0)
	{
		UHaveItem** SelectHaveItemPtr = HaveItem.FindByPredicate([BuyItem](UHaveItem* HaveItem)
			{return HaveItem->GetItem()->GetItemID() == BuyItem->GetItemID(); });
		if (SelectHaveItemPtr != nullptr)
			SelectHaveItem = *SelectHaveItemPtr;
	}
	int Num = 0;
	if (IsValid(SelectHaveItem))
		Num = SelectHaveItem->GetNum();
	return Num;
}

/// <summary>
/// 選んでいる購入アイテムの最大購入数取得
/// </summary>
int UShopManager::GetBuySelectItemMaxNum()
{
	UBaseItem* BuyItem = BuyItems[BuyItemWidget->GetSelectIndex()];
	int MaxNum = GameInstance->Gold / BuyItem->GetValue();
	return MaxNum;
}