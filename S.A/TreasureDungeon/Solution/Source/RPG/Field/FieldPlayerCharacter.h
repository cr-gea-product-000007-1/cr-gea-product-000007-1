﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FieldPlayerCharacter.generated.h"

UCLASS()
class RPG_API AFieldPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AFieldPlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/// <summary>
	/// 現在地を前フレームの座標に設定
	/// </summary>
	void SetNowLocationInOldLocation();

	/// <summary>
	/// 移動距離を取り出す
	/// </summary>
	/// <returns>移動距離</returns>
	float RemoveMoveDistgance();

	/// <summary>
	/// 移動できるかを設定
	/// </summary>
	/// <param name="IsMove">移動できるか</param>
	void SetIsMove(bool IsMove) { bIsMove = IsMove; };

	/// <summary>
	/// 調べる範囲の中心を取得
	/// </summary>
	/// <returns>調べる範囲の中心</returns>
	UFUNCTION(BlueprintImplementableEvent)
	FVector GetChackAreaCenter();

	/// <summary>
	/// 調べる範囲の半径を取得
	/// </summary>
	/// <returns>調べる範囲の半径</returns>
	float GetChackAreaRadius() { return ChackAreaRadius; };

	/// <summary>
	/// カメラ角度設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetCameraControlRotate(FRotator Rotate);

	/// <summary>
	/// カメラ角度取得
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	FRotator GetCameraControlRotate();

private:
	/// <summary>
	/// 前フレームの座標
	/// </summary>
	FVector OldLocation;

	/// <summary>
	/// 移動距離
	/// </summary>
	float MoveDistance;

public:
	/// <summary>
	/// 移動できるか
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bIsMove;

	/// <summary>
	/// 調べる範囲の半径
	/// </summary>
	UPROPERTY(EditAnywhere)
	float ChackAreaRadius;
};
