﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldPlayerController.h"


//SetupInputComponentの継承
void AFieldPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	check(InputComponent); //UE4のアサートマクロ

	if (IsValid(InputComponent))
	{
		InputComponent->BindAction("Decide", IE_Pressed, this, &AFieldPlayerController::OnDecide);
		InputComponent->BindAction("Cancel", IE_Pressed, this, &AFieldPlayerController::OnCancel);
		InputComponent->BindAction("Menu", IE_Pressed, this, &AFieldPlayerController::OnMenu);
		InputComponent->BindAction("Up", IE_Pressed, this, &AFieldPlayerController::OnUp);
		InputComponent->BindAction("Down", IE_Pressed, this, &AFieldPlayerController::OnDown);
		InputComponent->BindAction("Left", IE_Pressed, this, &AFieldPlayerController::OnLeft);
		InputComponent->BindAction("Right", IE_Pressed, this, &AFieldPlayerController::OnRight);
	}
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnDecide()
{
	if (CommandInputInterface == nullptr) return;
	if (CheckObjectInputInterface == nullptr) return;
	if (ShopInputInterface == nullptr) return;

	CommandInputInterface->OnDecide();
	CheckObjectInputInterface->OnDecide();
	ShopInputInterface->OnDecide();

	if (TutorialInputInterface == nullptr) return;
	TutorialInputInterface->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnCancel()
{
	if (CommandInputInterface == nullptr) return;
	if (CheckObjectInputInterface == nullptr) return;
	if (ShopInputInterface == nullptr) return;

	CommandInputInterface->OnCancel();
	CheckObjectInputInterface->OnCancel();
	ShopInputInterface->OnCancel();
}

/// <summary>
/// メニューキーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnMenu()
{
	if (CommandInputInterface == nullptr) return;
	if (CheckObjectInputInterface == nullptr) return;
	if (ShopInputInterface == nullptr) return;

	CommandInputInterface->OnMenu();
	CheckObjectInputInterface->OnMenu();
	ShopInputInterface->OnMenu();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnUp()
{
	if (CommandInputInterface == nullptr) return;
	if (CheckObjectInputInterface == nullptr) return;
	if (ShopInputInterface == nullptr) return;

	CommandInputInterface->OnUp();
	CheckObjectInputInterface->OnUp();
	ShopInputInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnDown()
{
	if (CommandInputInterface == nullptr) return;

	CommandInputInterface->OnDown();
	CheckObjectInputInterface->OnDown();
	ShopInputInterface->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnLeft()
{
	if (CommandInputInterface == nullptr) return;

	CommandInputInterface->OnLeft();
	CheckObjectInputInterface->OnLeft();
	ShopInputInterface->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void AFieldPlayerController::OnRight()
{
	if (CommandInputInterface == nullptr) return;

	CommandInputInterface->OnRight();
	CheckObjectInputInterface->OnRight();
	ShopInputInterface->OnRight();
}
