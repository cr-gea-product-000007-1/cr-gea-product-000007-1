﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../System/Interface/InputWidgetInterface.h"
#include "FieldPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API AFieldPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel();

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	void OnMenu();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft();

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight();

public:
	/// <summary>
	/// コマンド入力インターフェースを設定する
	/// </summary>
	void SetCommandInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { CommandInputInterface = Interface; };

	/// <summary>
	/// 調べるオブジェクトの入力インターフェースを設定する
	/// </summary>
	void SetCheckObjectInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { CheckObjectInputInterface = Interface; };

	/// <summary>
	/// ショップの入力インターフェースを設定する
	/// </summary>
	void SetShopInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { ShopInputInterface = Interface; };

	/// <summary>
	/// チュートリアルの入力インターフェースを設定する
	/// </summary>
	void SetTutorialInputInterface(const TScriptInterface<IInputWidgetInterface>& Interface) { TutorialInputInterface = Interface; };

private:
	/// <summary>
	/// コマンド入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> CommandInputInterface;

	/// <summary>
	/// 調べるオブジェクトの入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> CheckObjectInputInterface;

	/// <summary>
	/// ショップの入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> ShopInputInterface;

	/// <summary>
	/// チュートリアルの入力インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IInputWidgetInterface> TutorialInputInterface;
};
