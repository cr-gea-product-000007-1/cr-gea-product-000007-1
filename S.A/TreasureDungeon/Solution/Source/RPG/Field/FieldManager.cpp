﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldManager.h"
#include "../PathConstant.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/Actor.h"
#include "FieldPlayerController.h"
#include "../RPGGameInstance.h"

/// <summary>
/// 開始時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UFieldManager::InitializeEvent()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	AFieldPlayerCharacter* Player = Cast<AFieldPlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	if(IsValid(Player))
		FieldPlayerCharacter = Player;

	FString Path = Path::NormalFadeWidgetPath;
	TSubclassOf<UFadeWidget> FadeWidgetClass = TSoftClassPtr<UFadeWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(FadeWidgetClass))
	{
		FadeWidget = CreateWidget<UFadeWidget>(GetWorld(), FadeWidgetClass);
		FadeWidget->AddToViewport();
		FadeWidget->PlayFadeOut();
	};

	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (IsValid(GameInstance))
	{
		if (GameInstance->bInBattle)
		{
			FieldPlayerCharacter->SetActorTransform(GameInstance->BattleTransform);
			FieldPlayerCharacter->SetCameraControlRotate(GameInstance->CameraControlRotate);
		}
	}
	FieldPlayerCharacter->SetNowLocationInOldLocation();

	BGM = Cast<USoundBase>(StaticLoadObject(USoundBase::StaticClass(), nullptr, *Path::FieldBGMPath));
	BGMPlayer = NewObject<UBGMPlayer>();
	BGMPlayer->PlayBGM(this, BGM);
	SoundPlayer = NewObject<USoundPlayer>();
	EncountSE = Cast<USoundBase>(StaticLoadObject(USoundBase::StaticClass(), nullptr, *Path::EncountSoundPath));
	
	return true;
}

/// <summary>
/// 生成の処理
/// </summary>
/// <return> 終了 = true </return>
bool UFieldManager::CreateEvent()
{
	switch (CreatePhase)
	{
	case ECreatePhase::MenuCreate:
		FieldMenuManager = NewObject<UFieldMenuManager>(this);
		FieldMenuManager->WidgetCreate();
		FieldMenuManager->SetReceiveUnderMessageInterface(this);
		FieldMenuManager->SetFieldManagerStopperInterface(this);
		FieldMenuManager->SetFieldEventInterface(this);
		FieldMenuManager->SetFieldPlayerInterface(this);

		//チュートリアル生成
		if (!GameInstance->bTutorialFinish)
		{
			FString Path = Path::TutorialWidgetPath;
			TSubclassOf<UTutorialWidget> TutorialWidgetClass = TSoftClassPtr<UTutorialWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(TutorialWidgetClass))
			{
				TutorialWidget = CreateWidget<UTutorialWidget>(GetWorld(), TutorialWidgetClass);
				TutorialWidget->AddToViewport();
			};
		}
		CreatePhase = ECreatePhase::CheckObjectCreate;
		break;

	case ECreatePhase::CheckObjectCreate:
	{
		CheckObjectManager = NewObject<UCheckObjectManager>(this);
		CheckObjectManager->Initialize();
		CheckObjectManager->SetFieldPlayerCharacter(FieldPlayerCharacter);
		CheckObjectManager->SetFieldEventInterfaceForCheckObjects(this);
		CheckObjectManager->SetFieldManagerStopperInterface(this);
		CreatePhase = ECreatePhase::ShopCreate;
		break;
	}
	case ECreatePhase::ShopCreate:
	{
		ShopManager = NewObject<UShopManager>(this);
		ShopManager->WidgetCreate();
		ShopManager->SetFieldEventInterface(this);
		ShopManager->SetFieldManagerStopperInterface(this);
		CreatePhase = ECreatePhase::PlayerCreate;
		break;
	}
	case ECreatePhase::PlayerCreate:
	{
		EncountManager = NewObject<UEncountManager>(this);
		EncountManager->Initialize();

		AFieldPlayerController* FieldPlayerController = Cast<AFieldPlayerController>(GetWorld()->GetFirstPlayerController());
		if (IsValid(FieldPlayerController))
		{
			FieldPlayerController->SetCommandInputInterface(FieldMenuManager);
			FieldPlayerController->SetCheckObjectInputInterface(CheckObjectManager);
			FieldPlayerController->SetShopInputInterface(ShopManager);
			FieldPlayerController->SetTutorialInputInterface(TutorialWidget);
		}
		CreatePhase = ECreatePhase::UICreate;
		break;
	}

	case ECreatePhase::UICreate:
	{
		FString Path;
		//メッセージ生成
		Path = Path::UnderMessageWidgetPath;
		TSubclassOf<UUnderMessageWidget> UnderMessageWidgetClass = TSoftClassPtr<UUnderMessageWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(UnderMessageWidgetClass))
		{
			UnderMessageWidget = CreateWidget<UUnderMessageWidget>(GetWorld(), UnderMessageWidgetClass);
			UnderMessageWidget->AddToViewport();
		};
		//メッセージボックス生成
		Path = Path::MessageBoxWidgetPath;
		TSubclassOf<UMessageBoxWidget> MessageBoxWidgetClass = TSoftClassPtr<UMessageBoxWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(MessageBoxWidgetClass))
		{
			MessageBoxWidget = CreateWidget<UMessageBoxWidget>(GetWorld(), MessageBoxWidgetClass);
			MessageBoxWidget->AddToViewport();
		};
		//ステータスバー生成
		StatusBarManager = NewObject<UStatusBarManager>(this);
		StatusBarManager->CreateStatusWidgets(GetPlayerStatuses(this));
		StatusBarManager->Hide();

		CreatePhase = ECreatePhase::Finish;
		break;

	}
	case ECreatePhase::Finish:
		return true;
	}

	return false;
}

/// <summary>
/// フィールド中の処理
/// </summary>
/// <return> 終了 = true </return>
bool UFieldManager::FieldEvent()
{
	//チュートリアル
	if (!GameInstance->bTutorialFinish)
	{
		if (TutorialWidget->IsTutorialFinish())
		{
			GameInstance->bTutorialFinish = true;
			SetFieldEventFlag(true);
		}
		else if (!TutorialWidget->IsOpen())
		{
			TutorialWidget->Open();
			SetFieldEventFlag(false);
		}
		return false;
	}

	//移動フラグリセット
	FieldPlayerCharacter->SetIsMove(false);

	//メニュー
	if (FieldMenuManager->IsMenu())
	{
		CheckObjectManager->NullCheckTarget();
		return false;
	}

	//調べる
	CheckObjectManager->CheckTargetUpdate();
	CheckObjectManager->CheckEventUpdate();
	if (CheckObjectManager->IsCheckEvent())
	{
		return false;
	}

	//エンカウント
	if (GameInstance->bIsEncount)
	{
		if (bIsBattle)
			return true;

		float MoveDistance = FieldPlayerCharacter->RemoveMoveDistgance();
		EncountManager->EncountUpdate(MoveDistance);
		if (EncountManager->IsEncount())
		{
			EncountManager->DrawingEncountStart(FieldPlayerCharacter);
			SoundPlayer->PlaySound(this, EncountSE);
			bIsBattle = true;
			BGMPlayer->StopBGM();
		}
	}

	//移動可
	FieldPlayerCharacter->SetIsMove(true);
	return false;
}

/// <summary>
/// 戦闘遷移の処理
/// </summary>
/// <return> 終了 = true </return>
bool UFieldManager::InBattleEvent()
{
	FieldPlayerCharacter->SetIsMove(false);
	switch (InBattlePhase)
	{
	case EInBattlePhase::Start:
		FadeWidget->PlayFadeIn();
		InBattlePhase = EInBattlePhase::FadeIn;
		break;

	case EInBattlePhase::FadeIn:
		if (!FadeWidget->IsAnimation())
			InBattlePhase = EInBattlePhase::Finish;
		break;

	case EInBattlePhase::Finish:
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("BattleLevel"), true);
		return true;
	}
	return false;
}

/// <summary>
/// 終了時の処理
/// </summary>
/// <return> 終了 = true </return>
bool UFieldManager::FinalizeEvent()
{
	return true;
}

/// <summary>
/// エンカウントテーブルを設定
/// </summary>
/// <param name="EncountParameters">エンカウントテーブル</param>
void UFieldManager::SetEncountParameterTable(UDataTable* EncountDataTable)
{
	if (!IsValid(EncountManager))return;
	
	EncountManager->SetEncountTable(EncountDataTable);
}

/// <summary>
/// フィールドイベントの実行フラグを設定
/// </summary>
/// <param name="Flag">実行フラグ</param>
void UFieldManager::SetFieldEventFlag(bool Flag)
{
	CheckObjectManager->SetIsCheck(Flag);
	FieldMenuManager->SetIsOpen(Flag);
	//ShopManager->SetIsOpen(Flag);
}

/// <summary>
/// 自動送りのメッセージ表示
/// </summary>
/// <param name="Message">表示するメッセージ</param>
void UFieldManager::ShowMessage(const FString& Message)
{
	if (!IsValid(UnderMessageWidget)) return;
		UnderMessageWidget->AddMessage(Message);
}

/// <summary>
/// メッセージボックスを開く
/// </summary>
void UFieldManager::OpenMessageBox()
{
	if (!IsValid(MessageBoxWidget)) return;
	MessageBoxWidget->Open();
}

/// <summary>
/// メッセージボックスを閉じる
/// </summary>
void UFieldManager::CloseMessageBox()
{
	if (!IsValid(MessageBoxWidget)) return;
	MessageBoxWidget->Close();
}

/// <summary>
/// メッセージボックスにメッセージを表示させる
/// </summary>
/// <param name="Message">行区切りのメッセージ</param>
void UFieldManager::ShowMessageBox(TArray<FString> Message)
{
	if (!IsValid(MessageBoxWidget)) return;
	MessageBoxWidget->ViewMessage(Message);
}

/// <summary>
/// メッセージボックスにメッセージを表示しているか
/// </summary>
/// <return> 表示中 = true </return>
bool UFieldManager::IsShowMessageBox()
{
	if (!IsValid(MessageBoxWidget)) return false;
	return MessageBoxWidget->IsOpen();
}

/// <summary>
/// メッセージボックスがウィジットのアニメーション中か
/// </summary>
/// <return> アニメーション中 = true </return>
bool UFieldManager::IsWidgetAnimationMessageBox()
{
	if (!IsValid(MessageBoxWidget)) return false;
	return MessageBoxWidget->IsWidgetAnimation();
}

/// <summary>
/// メッセージボックスが文字のアニメーション中か
/// </summary>
/// <returns>文字のアニメーション中 = true</returns>
bool UFieldManager::IsTextAnimationMessageBox()
{
	if (!IsValid(MessageBoxWidget)) return false;
	return MessageBoxWidget->IsTextAnimation();
}

/// <summary>
/// アイテムを手に入れる
/// </summary>
/// <param name="ItemID">アイテムID</param>
void UFieldManager::InsertItem(EItemID ItemID, int Num)
{
	if (GameInstance == nullptr) return;
	UBaseItem* BaseItem = GetBaseItem(this, ItemID);
	GameInstance->ItemStorage->InsertBaseItem(BaseItem, Num);
}

/// <summary>
/// アイテムを失う
/// </summary>
/// <param name="ItemID">アイテムID</param>
void UFieldManager::LostItem(EItemID ItemID, int Num)
{
	if (GameInstance == nullptr) return;
	UBaseItem* BaseItem = GetBaseItem(this, ItemID);
	GameInstance->ItemStorage->LostBaseItem(BaseItem, Num);
}

/// <summary>
/// アイテムの個数取得
/// </summary>
/// <param name="ItemID">アイテムID</param>
/// <returns>個数</returns>
int UFieldManager::GetItemNum(EItemID ItemID)
{
	if (GameInstance == nullptr) return 0;
	UBaseItem* BaseItem = GetBaseItem(this, ItemID);
	return GameInstance->ItemStorage->GetBaseItemNum(BaseItem);
}

/// <summary>
/// 所持金取得
/// </summary>
/// <returns>所持金</returns>
int UFieldManager::GetHaveGold()
{
	if (GameInstance == nullptr) return 0;
	return GameInstance->Gold;
}

/// <summary>
/// エンカウントする
/// </summary>
/// <param name="EncountEnemyIDGroup">エンカウントする敵ID</param>
void UFieldManager::EncountStart(TArray<EEnemyID> EncountEnemyIDGroup)
{
	if (!IsValid(EncountManager)) return;
	EncountManager->EncountStart(FieldPlayerCharacter, EncountEnemyIDGroup);
	SoundPlayer->PlaySound(this, EncountSE);
	bIsBattle = true;
}

/// <summary>
/// ショップを開始する
/// </summary>
/// <param name="ItemIDs">商品リストのID</param>
void UFieldManager::ShopStart(TArray<EItemID> ItemIDs)
{
	ShopManager->ShopStart(ItemIDs);
}

/// <summary>
/// ショップが開いているか
/// </summary>
/// <returns>ショップが開いている = true</returns>
bool UFieldManager::IsShop()
{
	return ShopManager->IsShop();
}

/// <summary>
/// 全回復する
/// </summary>
void UFieldManager::FullHeal()
{
	if (GameInstance == nullptr) return;
	TArray<UBattlePlayerStatus*> PlayerStatuses = GameInstance->PlayerStatuses;
	for (UBattlePlayerStatus* PlayerStatuse : PlayerStatuses)
	{
		PlayerStatuse->FullHeal();
		StatusBarManager->SetStatus(PlayerStatuse);
	}
}

/// <summary>
/// スキルをプレイヤーに与える
/// </summary>
/// <param name="User">使用者</param>
/// <param name="Targets">対象者</param>
/// <param name="Skill">スキル</param>
void UFieldManager::HitSkillPlayer(UBattlePlayerStatus* User, TArray<UBattlePlayerStatus*> Targets, USkill* Skill)
{
	for (UBattlePlayerStatus* Target : Targets)
	{
		if (Target->IsDie()) continue;

		int Damage = 0;
		if (Skill->IsHitSkill(User, Target))
			Damage = Skill->GetDamage(User, Target);

		Target->GiveDamage(Damage, false);
	}
}

/// <summary>
/// ステータスバーの表示
/// </summary>
void UFieldManager::ViewStatusBar()
{
	if (!IsValid(StatusBarManager)) return;
	StatusBarManager->ViewSlide();
}

/// <summary>
/// ステータスバーの非表示
/// </summary>
void UFieldManager::NonViewStatusBar()
{
	if (!IsValid(StatusBarManager)) return;
	StatusBarManager->NonViewSlide();
}

/// <summary>
/// ステータスバーの更新
/// </summary>
void UFieldManager::UpdateStatusBar()
{
	if (!IsValid(StatusBarManager)) return;
	StatusBarManager->SetAllStatus(GetPlayerStatuses(this));
}