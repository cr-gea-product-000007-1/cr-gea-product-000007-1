﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldPlayerCharacter.h"

// Sets default values
AFieldPlayerCharacter::AFieldPlayerCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFieldPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	SetNowLocationInOldLocation();
}

// Called every frame
void AFieldPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector NowLocation = GetActorLocation();
	NowLocation.Z = 0;
	float Distance = FVector::Distance(NowLocation, OldLocation);
	MoveDistance += Distance;
	OldLocation = NowLocation;
}

// Called to bind functionality to input
void AFieldPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/// <summary>
/// 現在地を前フレームの座標に設定
/// </summary>
void AFieldPlayerCharacter::SetNowLocationInOldLocation()
{
	OldLocation = GetActorLocation();
	OldLocation.Z = 0;
}

/// <summary>
/// 移動距離を取り出す
/// </summary>
/// <returns>移動距離</returns>
float AFieldPlayerCharacter::RemoveMoveDistgance()
{
	float ReturnDictance = MoveDistance;
	MoveDistance = 0;
	return ReturnDictance;
}
