﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Engine/DataTable.h"
#include "Kismet/GameplayStatics.h"
#include "FieldManager.h"
#include "FieldLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API AFieldLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	AFieldLevelScriptActor();

	enum class EFieldPhaseType
	{
		Initialize,
		Create,
		EncountSetting,
		Field,
		InBattlle,
		Finalize,
	};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/// <summary>
	/// フィールドマネージャー
	/// </summary>
	UPROPERTY()
	UFieldManager* FieldManager;

	/// <summary>
	/// フィールドの状態
	/// </summary>
	EFieldPhaseType FieldPhase;

	/// <summary>
	/// エンカウントテーブル
	/// </summary>
	UPROPERTY(EditAnywhere)
	UDataTable* EncountDataTable;
};
