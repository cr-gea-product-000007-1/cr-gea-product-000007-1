﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FieldPlayerCharacter.h"
#include "Encount/EncountManager.h"
#include "Menu/FieldMenuManager.h"
#include "Shop/ShopManager.h"
#include "../System/UI/StatusBarManager.h"
#include "../System/UI/FadeWidget.h"
#include "../System/UI/UnderMessageWidget.h"
#include "../System/UI/MessageBoxWidget.h"
#include "UI/TutorialWidget.h"
#include "Object/CheckObjectManager.h"
#include "../RPGGameInstance.h"

#include "../System/Interface/SendUnderMessageInterface.h"
#include "../System/Interface/ReceiveUnderMessageInterface.h"
#include "../System/Interface/LoadDataInterface.h"
#include "Interface/FieldManagerStopperInterface.h"
#include "Interface/FieldPlayerInterface.h"
#include "Interface/FieldEventInterface.h"
#include "../Sound/BGMPlayer.h"
#include "../Sound/SoundPlayer.h"

#include "FieldManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UFieldManager : public UObject, public IReceiveUnderMessageInterface, public ILoadDataInterface, 
	public IFieldManagerStopperInterface, public IFieldPlayerInterface, public IFieldEventInterface
{
	GENERATED_BODY()


	enum class ECreatePhase
	{
		MenuCreate,
		CheckObjectCreate,
		ShopCreate,
		PlayerCreate,
		UICreate,
		Finish,
	};

	enum class EInBattlePhase
	{
		Start,
		FadeIn,
		Finish,
	};

public:
	/// <summary>
	/// 開始時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool InitializeEvent();

	/// <summary>
	/// 生成の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool CreateEvent();

	/// <summary>
	/// フィールド中の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool FieldEvent();

	/// <summary>
	/// 戦闘遷移の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool InBattleEvent();

	/// <summary>
	/// 終了時の処理
	/// </summary>
	/// <return> 終了 = true </return>
	bool FinalizeEvent();

	/// <summary>
	/// エンカウントテーブルを設定
	/// </summary>
	/// <param name="EncountParameters">エンカウントテーブル</param>
	void SetEncountParameterTable(UDataTable* EncountDataTable);

	/// <summary>
	/// 戦闘しているか
	/// </summary>
	/// <return> 戦闘 = true </return>
	bool IsBattle() { return bIsBattle; };

	/// <summary>
	/// フィールドイベントの実行フラグを設定
	/// </summary>
	/// <param name="Flag">実行フラグ</param>
	void SetFieldEventFlag(bool Flag) override;

	/// <summary>
	/// 自動送りのメッセージを表示させる
	/// </summary>
	/// <param name="Message">表示するメッセージ</param>
	void ShowMessage(const FString& Message) override;

	/// <summary>
	/// メッセージボックスを開く
	/// </summary>
	void OpenMessageBox() override;

	/// <summary>
	/// メッセージボックスを閉じる
	/// </summary>
	void CloseMessageBox() override;

	/// <summary>
	/// メッセージボックスにメッセージを表示させる
	/// </summary>
	/// <param name="Message">行区切りのメッセージ</param>
	void ShowMessageBox(TArray<FString> Message) override;

	/// <summary>
	/// メッセージボックスにメッセージを表示しているか
	/// </summary>
	/// <return> 表示中 = true </return>
	bool IsShowMessageBox() override;

	/// <summary>
	/// メッセージボックスがウィジットのアニメーション中か
	/// </summary>
	/// <return> アニメーション中 = true </return>
	bool IsWidgetAnimationMessageBox() override;

	/// <summary>
	/// メッセージボックスが文字のアニメーション中か
	/// </summary>
	/// <returns>文字のアニメーション中 = true</returns>
	bool IsTextAnimationMessageBox() override;

	/// <summary>
	/// アイテムを手に入れる
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	void InsertItem(EItemID ItemID, int Num) override;

	/// <summary>
	/// アイテムを失う
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	void LostItem(EItemID ItemID, int Num) override;

	/// <summary>
	/// アイテムの個数取得
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	/// <returns>個数</returns>
	int GetItemNum(EItemID ItemID) override;

	/// <summary>
	/// 所持金取得
	/// </summary>
	/// <returns>所持金</returns>
	int GetHaveGold() override;

	/// <summary>
	/// エンカウントする
	/// </summary>
	/// <param name="EncountEnemyIDGroup">エンカウントする敵ID</param>
	void EncountStart(TArray<EEnemyID> EncountEnemyIDGroup) override;

	/// <summary>
	/// ショップを開始する
	/// </summary>
	/// <param name="ItemIDs">商品リストのID</param>
	void ShopStart(TArray<EItemID> ItemIDs) override;

	/// <summary>
	/// ショップが開いているか
	/// </summary>
	/// <returns>ショップが開いている = true</returns>
	bool IsShop() override;

	/// <summary>
	/// 全回復する
	/// </summary>
	void FullHeal() override;

	/// <summary>
	/// BGM音量更新
	/// </summary>
	void BGMVolumeUpdate() override { BGMPlayer->VolumeUpdate(); };

	/// <summary>
	/// スキルをプレイヤーに与える
	/// </summary>
	/// <param name="User">使用者</param>
	/// <param name="Targets">対象者</param>
	/// <param name="Skill">スキル</param>
	void HitSkillPlayer(UBattlePlayerStatus* User, TArray<UBattlePlayerStatus*> Targets, USkill* Skill) override;

	/// <summary>
	/// ステータスバーの表示
	/// </summary>
	void ViewStatusBar() override;

	/// <summary>
	/// ステータスバーの非表示
	/// </summary>
	void NonViewStatusBar() override;

	/// <summary>
	/// ステータスバーの更新
	/// </summary>
	void UpdateStatusBar() override;
	
private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	UPROPERTY()
	URPGGameInstance* GameInstance;

	/// <summary>
	/// エンカウントマネージャー
	/// </summary>
	UPROPERTY()
	UEncountManager* EncountManager;

	/// <summary>
	/// メニューマネージャー
	/// </summary>
	UPROPERTY()
	UFieldMenuManager* FieldMenuManager;

	/// <summary>
	/// ショップマネージャー
	/// </summary>
	UPROPERTY()
	UShopManager* ShopManager;

	/// <summary>
	/// 調べるオブジェクトのマネージャー
	/// </summary>
	UPROPERTY()
	UCheckObjectManager* CheckObjectManager;

	/// <summary>
	/// 操作するプレイヤー
	/// </summary>
	UPROPERTY()
	AFieldPlayerCharacter* FieldPlayerCharacter;

	/// <summary>
	/// 生成のフェーズ
	/// </summary>
	ECreatePhase CreatePhase;

	/// <summary>
	/// 戦闘遷移のフェーズ
	/// </summary>
	EInBattlePhase InBattlePhase;

	/// <summary>
	/// 戦闘しているか
	/// </summary>
	bool bIsBattle;

	/// <summary>
	/// フェード
	/// </summary>
	UPROPERTY()
	UFadeWidget* FadeWidget;

	/// <summary>
	/// 自動送りのメッセージ
	/// </summary>
	UPROPERTY()
	UUnderMessageWidget* UnderMessageWidget;

	/// <summary>
	/// メッセージボックス
	/// </summary>
	UPROPERTY()
	UMessageBoxWidget* MessageBoxWidget;

	/// <summary>
	/// ステータスバーマネージャー
	/// </summary>
	UPROPERTY()
	UStatusBarManager* StatusBarManager;

	/// <summary>
	/// チュートリアル
	/// </summary>
	UPROPERTY()
	UTutorialWidget* TutorialWidget;

	/// <summary>
	/// BGM
	/// </summary>
	UPROPERTY()
	USoundBase* BGM;

	/// <summary>
	/// エンカウントSE
	/// </summary>
	UPROPERTY()
	USoundBase* EncountSE;

	/// <summary>
	/// BGMを再生する
	/// </summary>
	UPROPERTY()
	UBGMPlayer* BGMPlayer;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
