﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interface/FieldEventInterface.h"
#include "CheckObject.generated.h"

UCLASS()
class RPG_API ACheckObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	/// <summary>
	/// 矢印表示位置取得する
	/// </summary>
	/// <returns>矢印表示位置</returns>
	UFUNCTION(BlueprintPure)
	FVector GetArrowLocation() { return GetVirtualArrowLocation(); };

	/// <summary>
	/// 矢印表示位置取得する仮想関数
	/// </summary>
	/// <returns>矢印表示位置</returns>
	virtual FVector GetVirtualArrowLocation() { return FVector(); };

	/// <summary>
	/// 調べた時のイベント開始
	/// </summary>
	virtual void CheckEventStart() { };

	/// <summary>
	/// 調べた時のイベントを更新
	/// </summary>
	/// <returns>イベント終了 = true</returns>
	virtual bool CheckEventUpdate() { return false; };

	/// <summary>
	/// 調べる対象になっているか設定
	/// </summary>
	/// <param name="IsCheckTarget">調べる対象になっているか</param>
	void SetChackTarget(bool IsCheckTarget);

	/// <summary>
	/// 調べる対象になっているか取得
	/// </summary>
	/// <returns>調べる対象 = true</returns>
	bool IsChackTarget() { return bIsCheckTarget; };

	/// <summary>
	/// 直前に調べる対象になっているか所得
	/// </summary>
	/// <returns>直前から調べる対象 = true</returns>
	bool IsOldChackTarget() { return bIsOldCheckTarget; };

	/// <summary>
	/// 調べた時のイベント中か取得
	/// </summary>
	/// <returns>調べたイベント中 = true</returns>
	bool IsCheckEvent() { return bIsCheckEvent; };

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	void SetFieldEventInterface(IFieldEventInterface* Interface) { FieldEventInterface = Interface; };

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	virtual void OnDecide() {};

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	virtual void OnCancel() {};

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	virtual void OnMenu() {};

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	virtual void OnUp() {};

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	virtual void OnDown() {};

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	virtual void OnLeft() {};

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	virtual void OnRight() {};

protected:
	/// <summary>
	/// 調べる対象になっているか
	/// </summary>
	bool bIsCheckTarget;

	/// <summary>
	/// 調べる対象になっているか
	/// </summary>
	bool bIsOldCheckTarget;

	/// <summary>
	/// 調べた時のイベント中か
	/// </summary>
	bool bIsCheckEvent;

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	IFieldEventInterface* FieldEventInterface;
};
