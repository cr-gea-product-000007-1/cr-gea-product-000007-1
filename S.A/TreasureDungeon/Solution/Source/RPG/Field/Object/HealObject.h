﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CheckObject.h"
#include "../../Sound/SoundPlayer.h"
#include "HealObject.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API AHealObject : public ACheckObject
{
	GENERATED_BODY()

	enum class EHealPhaseTyep
	{
		None,
		StartMessageStart,
		StartMessageInput,
		Heal,
		Finish,
	};

public:
	void BeginPlay() override;

	/// <summary>
	/// 矢印表示位置取得する仮想関数
	/// </summary>
	/// <returns>矢印表示位置</returns>
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	FVector GetVirtualArrowLocation() override;

	/// <summary>
	/// 調べた時のイベント開始
	/// </summary>
	void CheckEventStart() override;

	/// <summary>
	/// 調べた時のイベントを更新
	/// </summary>
	/// <returns>イベント終了 = true</returns>
	bool CheckEventUpdate() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

private:
	/// <summary>
	/// イベントのフェーズ
	/// </summary>
	EHealPhaseTyep HealPhaseTyep;

	/// <summary>
	/// 開始時表示するメッセージ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FString> StartMessage;

	/// <summary>
	/// 回復するSE
	/// </summary>
	UPROPERTY(EditAnywhere)
	USoundBase* HealSound;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};
