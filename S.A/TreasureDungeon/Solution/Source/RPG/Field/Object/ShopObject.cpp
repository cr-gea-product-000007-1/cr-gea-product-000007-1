﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ShopObject.h"


/// <summary>
/// 調べた時のイベント開始
/// </summary>
void AShopObject::CheckEventStart()
{
	if (FieldEventInterface == nullptr) return;

	bIsCheckEvent = true;
	ShopPhaseTyep = EShopPhaseTyep::StartMessageStart;
	FieldEventInterface->OpenMessageBox();
}

/// <summary>
/// 調べた時のイベントを更新
/// </summary>
/// <returns>イベント終了 = true</returns>
bool AShopObject::CheckEventUpdate()
{
	if (FieldEventInterface == nullptr) return false;
	switch (ShopPhaseTyep)
	{
	case EShopPhaseTyep::StartMessageStart:
		if (!FieldEventInterface->IsWidgetAnimationMessageBox())
		{
			FieldEventInterface->ShowMessageBox(StartMessage);
			ShopPhaseTyep = EShopPhaseTyep::StartMessageInput;
		}
		break;

	case EShopPhaseTyep::StartMessageInput:
		if (!FieldEventInterface->IsShowMessageBox())
		{
			ShopPhaseTyep = EShopPhaseTyep::ShopStart;
		}
		break;

	case EShopPhaseTyep::ShopStart:
		FieldEventInterface->ShopStart(ShopItemID);
		ShopPhaseTyep = EShopPhaseTyep::Shop;
		break;

	case EShopPhaseTyep::Shop:
		if (!FieldEventInterface->IsShop())
		{
			ShopPhaseTyep = EShopPhaseTyep::EndMessageOpen;
		}
		break;

	//やめるを押すのと同時にメッセージ送り判定されないように1fフェーズで遅らせる
	case EShopPhaseTyep::EndMessageOpen:
		FieldEventInterface->OpenMessageBox();
		ShopPhaseTyep = EShopPhaseTyep::EndMessageStart;
		break;

	case EShopPhaseTyep::EndMessageStart:
		if (!FieldEventInterface->IsWidgetAnimationMessageBox())
		{
			FieldEventInterface->ShowMessageBox(EndMessage);
			ShopPhaseTyep = EShopPhaseTyep::EndMessageInput;
		}
		break;

	case EShopPhaseTyep::EndMessageInput:
		if (!FieldEventInterface->IsShowMessageBox())
		{
			ShopPhaseTyep = EShopPhaseTyep::Finish;
			bIsCheckEvent = false;
			return true;
		}
		break;
	}
	return false;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void AShopObject::OnDecide()
{
	if (FieldEventInterface == nullptr) return;

	if (ShopPhaseTyep == EShopPhaseTyep::StartMessageInput || 
		ShopPhaseTyep == EShopPhaseTyep::EndMessageInput)
		FieldEventInterface->CloseMessageBox();
}