﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealObject.h"


void AHealObject::BeginPlay()
{
	Super::BeginPlay();
	SoundPlayer = NewObject<USoundPlayer>();
}


/// <summary>
/// 調べた時のイベント開始
/// </summary>
void AHealObject::CheckEventStart()
{
	if (FieldEventInterface == nullptr) return;

	bIsCheckEvent = true;
	HealPhaseTyep = EHealPhaseTyep::StartMessageStart;
	FieldEventInterface->OpenMessageBox();
}

/// <summary>
/// 調べた時のイベントを更新
/// </summary>
/// <returns>イベント終了 = true</returns>
bool AHealObject::CheckEventUpdate()
{
	if (FieldEventInterface == nullptr) return false;
	switch (HealPhaseTyep)
	{
	case EHealPhaseTyep::StartMessageStart:
		if (!FieldEventInterface->IsWidgetAnimationMessageBox())
		{
			SoundPlayer->PlaySound(this, HealSound);
			FieldEventInterface->ShowMessageBox(StartMessage);
			HealPhaseTyep = EHealPhaseTyep::StartMessageInput;
		}
		break;

	case EHealPhaseTyep::StartMessageInput:
		if (!FieldEventInterface->IsShowMessageBox())
		{
			HealPhaseTyep = EHealPhaseTyep::Heal;
		}
		break;

	case EHealPhaseTyep::Heal:
		FieldEventInterface->FullHeal();
		HealPhaseTyep = EHealPhaseTyep::Finish;
		bIsCheckEvent = false;
		return true;
	}
	return false;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void AHealObject::OnDecide()
{
	if (FieldEventInterface == nullptr) return;

	if (HealPhaseTyep == EHealPhaseTyep::StartMessageInput)
		FieldEventInterface->CloseMessageBox();
}