﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckObject.h"

// Sets default values
ACheckObject::ACheckObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACheckObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACheckObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/// <summary>
/// 調べる対象になっているか設定
/// </summary>
/// <param name="IsCheckTarget">調べる対象になっているか</param>
void ACheckObject::SetChackTarget(bool IsCheckTarget) 
{
	bIsOldCheckTarget = bIsCheckTarget;
	bIsCheckTarget = IsCheckTarget; 
};
