﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CheckObject.h"
#include "BossEncounterObject.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API ABossEncounterObject : public ACheckObject
{
	GENERATED_BODY()

	enum class EBossEncountPhaseTyep
	{
		None,
		MessageStart,
		MessageInput,
		Battle,
	};

public:
	/// <summary>
	/// 矢印表示位置取得する仮想関数
	/// </summary>
	/// <returns>矢印表示位置</returns>
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	FVector GetVirtualArrowLocation() override;

	/// <summary>
	/// 調べた時のイベント開始
	/// </summary>
	void CheckEventStart() override;

	/// <summary>
	/// 調べた時のイベントを更新
	/// </summary>
	/// <returns>イベント終了 = true</returns>
	bool CheckEventUpdate() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

private:
	/// <summary>
	/// イベントのフェーズ
	/// </summary>
	EBossEncountPhaseTyep BossEncountPhaseTyep;

	/// <summary>
	/// 表示するメッセージ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FString> Message;

	/// <summary>
	/// エンカウントする敵のIDグループ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<EEnemyID> EncountEnemyGroupID;
};
