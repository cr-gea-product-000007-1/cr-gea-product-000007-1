﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../FieldPlayerCharacter.h"
#include "CheckObject.h"
#include "../UI/CheckTargetWidget.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "../Interface/FieldEventInterface.h"
#include "../Interface/FieldManagerStopperInterface.h"
#include "CheckObjectManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UCheckObjectManager : public UObject, public IInputWidgetInterface
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// 調べるプレイヤー
	/// </summary>
	void SetFieldPlayerCharacter(AFieldPlayerCharacter* NewFieldPlayerCharacter) { FieldPlayerCharacter = NewFieldPlayerCharacter; };

	/// <summary>
	/// 調べることができるオブジェクト全てにフィールドイベントインターフェースを設定
	/// </summary>
	void SetFieldEventInterfaceForCheckObjects(IFieldEventInterface* Interface);

	/// <summary>
	/// 調べることができるオブジェクトの更新
	/// </summary>
	void CheckTargetUpdate();

	/// <summary>
	/// 調べられなくする
	/// </summary>
	void NullCheckTarget();

	/// <summary>
	/// 調べたオブジェクトのイベント更新
	/// </summary>
	void CheckEventUpdate();

	/// <summary>
	/// 調べられる状態か設定する
	/// </summary>
	void SetIsCheck(bool IsCheck) { bIsCheck = IsCheck; };

	/// <summary>
	/// 調べたオブジェクトのイベントの実行中かを返す
	/// </summary>
	/// <returns>エンカウント = true</returns>
	bool IsCheckEvent();

	/// <summary>
	/// 調べたオブジェクトのイベントを開始
	/// </summary>
	/// <param name="Player">エンカウントするプレイヤー</param>
	void CheckEventStart();

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel() override;

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	void OnMenu() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnRight() override;

	/// <summary>
	/// マネージャー停止インターフェースを設定する
	/// </summary>
	/// <param name="Interface">マネージャー停止インターフェース</param>
	void SetFieldManagerStopperInterface(IFieldManagerStopperInterface* Interface) { FieldManagerStopperInterface = Interface; };

private:
	/// <summary>
	/// 調べるプレイヤー
	/// </summary>
	UPROPERTY()
	AFieldPlayerCharacter* FieldPlayerCharacter;

	/// <summary>
	/// 調べられるオブジェクト一覧
	/// </summary>
	UPROPERTY()
	TArray<ACheckObject*> CheckObjects;

	/// <summary>
	/// 調べているオブジェクト
	/// </summary>
	UPROPERTY()
	ACheckObject* CheckTargetObject;

	/// <summary>
	/// 調べる対象表示
	/// </summary>
	UPROPERTY()
	UCheckTargetWidget* CheckTargetWidget;

	/// <summary>
	/// 決定キーを押したか
	/// </summary>
	bool bOnDicide;

	/// <summary>
	///調べられる状態か
	/// </summary>
	bool bIsCheck = true;

	/// <summary>
	/// マネージャー停止インターフェース
	/// </summary>
	IFieldManagerStopperInterface* FieldManagerStopperInterface;
};
