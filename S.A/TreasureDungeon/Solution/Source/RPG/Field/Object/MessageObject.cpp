﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MessageObject.h"


/// <summary>
/// 調べた時のイベント開始
/// </summary>
void AMessageObject::CheckEventStart()
{
	if (FieldEventInterface == nullptr) return;

	bIsCheckEvent = true;
	BossEncountPhaseTyep = EMessageObjectPhaseTyep::MessageStart;
	FieldEventInterface->OpenMessageBox();
}

/// <summary>
/// 調べた時のイベントを更新
/// </summary>
/// <returns>イベント終了 = true</returns>
bool AMessageObject::CheckEventUpdate()
{
	if (FieldEventInterface == nullptr) return false;
	switch (BossEncountPhaseTyep)
	{
	case EMessageObjectPhaseTyep::MessageStart:
		if (!FieldEventInterface->IsWidgetAnimationMessageBox())
		{
			FieldEventInterface->ShowMessageBox(Message);
			BossEncountPhaseTyep = EMessageObjectPhaseTyep::MessageInput;
		}
		break;
	case EMessageObjectPhaseTyep::MessageInput:
		if (!FieldEventInterface->IsShowMessageBox())
		{
			bIsCheckEvent = false;
			return true;
		}
		break;
	}
	return false;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void AMessageObject::OnDecide()
{
	if (BossEncountPhaseTyep != EMessageObjectPhaseTyep::MessageInput) return;
	if (FieldEventInterface == nullptr) return;
	FieldEventInterface->CloseMessageBox();
}