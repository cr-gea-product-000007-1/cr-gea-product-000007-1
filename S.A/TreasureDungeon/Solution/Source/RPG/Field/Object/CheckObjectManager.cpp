﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckObjectManager.h"
#include "Kismet/GameplayStatics.h"
#include "../../PathConstant.h"


/// <summary>
/// 初期化
/// </summary>
void UCheckObjectManager::Initialize()
{
	//調べる対象取得
	TArray<AActor*> FindActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACheckObject::StaticClass(), FindActors);
	for (AActor* FindActor : FindActors)
	{
		ACheckObject* FindCheckObject = Cast<ACheckObject>(FindActor);
		if (IsValid(FindCheckObject))
			CheckObjects.Add(FindCheckObject);
	}
	//調べる対象の矢印作成
	FString Path = Path::ChectTargetWidgetPath;
	TSubclassOf<UCheckTargetWidget> CheckTargetWidgetClass = TSoftClassPtr<UCheckTargetWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(CheckTargetWidgetClass))
	{
		CheckTargetWidget = CreateWidget<UCheckTargetWidget>(GetWorld(), CheckTargetWidgetClass);
		CheckTargetWidget->AddToViewport(-1);
	};
}

/// <summary>
/// 調べることができるオブジェクト全てにフィールドイベントインターフェースを設定
/// </summary>
void UCheckObjectManager::SetFieldEventInterfaceForCheckObjects(IFieldEventInterface* Interface)
{
	for (ACheckObject* CheckObject : CheckObjects)
		CheckObject->SetFieldEventInterface(Interface);
}

/// <summary>
/// 調べることができるオブジェクトの更新
/// </summary>
void UCheckObjectManager::CheckTargetUpdate()
{
	if (CheckObjects.Num() == 0) return;
	//イベント実行中なら更新しない
	if (IsValid(CheckTargetObject))
		if (CheckTargetObject->IsCheckEvent()) return;

	ACheckObject* CheckTarget = nullptr;
	FVector ChackAreaCenter = FieldPlayerCharacter->GetChackAreaCenter();
	float ChackAreaRadius = FieldPlayerCharacter->GetChackAreaRadius();
	float MaxPowDistance = FMath::Pow(ChackAreaRadius, 2);
	for (ACheckObject* CheckObject : CheckObjects)
	{
		float TargetDistance = FVector::DistSquared(ChackAreaCenter, CheckObject->GetActorLocation());
		if (MaxPowDistance > TargetDistance)
		{
			CheckTarget = CheckObject;
			MaxPowDistance = TargetDistance;
		}
	}

	//nullptrでも入れる
	CheckTargetWidget->SetChactTarget(CheckTarget);
	CheckTargetObject = CheckTarget;
}

/// <summary>
/// 調べられなくする
/// </summary>
void UCheckObjectManager::NullCheckTarget()
{
	ACheckObject* CheckTarget = nullptr;
	CheckTargetWidget->SetChactTarget(CheckTarget);
	CheckTargetObject = CheckTarget;
}

/// <summary>
/// 調べたオブジェクトのイベント更新
/// </summary>
void UCheckObjectManager::CheckEventUpdate()
{
	if (!IsValid(CheckTargetObject)) {
		bOnDicide = false;
		return;
	}

	if (CheckTargetObject->IsCheckEvent())
	{
		if(CheckTargetObject->CheckEventUpdate())
			FieldManagerStopperInterface->SetFieldEventFlag(true);
	}
	else {
		if (bOnDicide)
		{
			bOnDicide = false;
			CheckEventStart();
		}
	}
}

/// <summary>
/// 調べたオブジェクトのイベントの実行中かを返す
/// </summary>
/// <returns>エンカウント = true</returns>
bool UCheckObjectManager::IsCheckEvent()
{
	if (!IsValid(CheckTargetObject)) return false;

	return CheckTargetObject->IsCheckEvent();
}

/// <summary>
/// 調べたオブジェクトのイベントを開始
/// </summary>
/// <param name="Player">エンカウントするプレイヤー</param>
void UCheckObjectManager::CheckEventStart()
{
	if (!bIsCheck) return;
	if (!IsValid(CheckTargetObject)) return;
	if (FieldManagerStopperInterface == nullptr) return;

	FieldManagerStopperInterface->SetFieldEventFlag(false);
	CheckTargetObject->CheckEventStart();
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UCheckObjectManager::OnDecide()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) {
		bOnDicide = true;
		return;
	}

	CheckTargetObject->OnDecide();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void UCheckObjectManager::OnCancel()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnCancel();
}

/// <summary>
/// メニューキーを押したときのイベント
/// </summary>
void UCheckObjectManager::OnMenu()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnMenu();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UCheckObjectManager::OnUp()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UCheckObjectManager::OnDown()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UCheckObjectManager::OnLeft()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
void UCheckObjectManager::OnRight()
{
	if (!IsValid(CheckTargetObject)) return;
	if (!CheckTargetObject->IsCheckEvent()) return;

	CheckTargetObject->OnRight();
}
