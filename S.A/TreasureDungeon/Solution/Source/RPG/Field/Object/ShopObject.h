﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CheckObject.h"
#include "ShopObject.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API AShopObject : public ACheckObject
{
	GENERATED_BODY()

		enum class EShopPhaseTyep
	{
		None,
		StartMessageStart,
		StartMessageInput,
		ShopStart,
		Shop,
		EndMessageOpen,
		EndMessageStart,
		EndMessageInput,
		Finish,
	};

public:
	/// <summary>
	/// 矢印表示位置取得する仮想関数
	/// </summary>
	/// <returns>矢印表示位置</returns>
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	FVector GetVirtualArrowLocation() override;

	/// <summary>
	/// 調べた時のイベント開始
	/// </summary>
	void CheckEventStart() override;

	/// <summary>
	/// 調べた時のイベントを更新
	/// </summary>
	/// <returns>イベント終了 = true</returns>
	bool CheckEventUpdate() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

private:
	/// <summary>
	/// イベントのフェーズ
	/// </summary>
	EShopPhaseTyep ShopPhaseTyep;

	/// <summary>
	/// 開始時表示するメッセージ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FString> StartMessage;

	/// <summary>
	/// 終了時表示するメッセージ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FString> EndMessage;

	/// <summary>
	/// 販売アイテムのIDグループ
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<EItemID> ShopItemID;
};
