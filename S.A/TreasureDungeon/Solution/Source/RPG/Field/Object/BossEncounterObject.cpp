﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BossEncounterObject.h"
#include "../../RPGGameInstance.h"
#include "Kismet/GameplayStatics.h"


/// <summary>
/// 調べた時のイベント開始
/// </summary>
void ABossEncounterObject::CheckEventStart()
{
	if (FieldEventInterface == nullptr) return;

	bIsCheckEvent = true;
	BossEncountPhaseTyep = EBossEncountPhaseTyep::MessageStart;
	FieldEventInterface->OpenMessageBox();
}

/// <summary>
/// 調べた時のイベントを更新
/// </summary>
/// <returns>イベント終了 = true</returns>
bool ABossEncounterObject::CheckEventUpdate()
{
	if (FieldEventInterface == nullptr) return false;
	switch (BossEncountPhaseTyep)
	{
	case EBossEncountPhaseTyep::MessageStart:
		if (!FieldEventInterface->IsWidgetAnimationMessageBox())
		{
			FieldEventInterface->ShowMessageBox(Message);
			BossEncountPhaseTyep = EBossEncountPhaseTyep::MessageInput;
		}
		break;
	case EBossEncountPhaseTyep::MessageInput:
		if (!FieldEventInterface->IsShowMessageBox())
		{
			BossEncountPhaseTyep = EBossEncountPhaseTyep::Battle;
		}
		break;
	case EBossEncountPhaseTyep::Battle:
		FieldEventInterface->EncountStart(EncountEnemyGroupID);
		bIsCheckEvent = false;
		//この戦闘を勝てばゲームクリア
		URPGGameInstance* GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		GameInstance->bGameClear = true;
		return true;
	}
	return false;
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ABossEncounterObject::OnDecide()
{
	if (BossEncountPhaseTyep != EBossEncountPhaseTyep::MessageInput) return;
	if (FieldEventInterface == nullptr) return;
	FieldEventInterface->CloseMessageBox();
}