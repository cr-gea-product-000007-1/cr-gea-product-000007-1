﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../FieldPlayerCharacter.h"
#include "EncountArea.h"
#include "EncountParameter.h"
#include "EncountManager.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UEncountManager : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// エンカウントテーブルを設定
	/// </summary>
	/// <param name="EncountDataTable">エンカウントテーブル</param>
	void SetEncountTable(UDataTable* EncountDataTable);
	
	/// <summary>
	/// エンカウント更新
	/// </summary>
	/// <param name="MoveDistance">移動距離</param>
	void EncountUpdate(float MoveDistance);

	/// <summary>
	/// エンカウントしたかを取得
	/// </summary>
	/// <returns>エンカウント = true</returns>
	bool IsEncount() { return EncountStartDistance <= 0; };

	/// <summary>
	/// ランダムエンカウントする
	/// </summary>
	/// <param name="Player">エンカウントするプレイヤー</param>
	void DrawingEncountStart(AFieldPlayerCharacter* Player);

	/// <summary>
	/// エンカウントする
	/// </summary>
	/// <param name="Player">エンカウントするプレイヤー</param>
	/// <param name="EncountEnemyIDGroup">エンカウントする敵ID</param>
	void EncountStart(AFieldPlayerCharacter* Player, TArray<EEnemyID> EncountEnemyIDGroup);
	
private:
	/// <summary>
	/// エンカウントテーブル
	/// </summary>
	TArray<FEncountParameter> EncountParameterList;

	/// <summary>
	/// エンカウントする範囲
	/// </summary>
	UPROPERTY()
	TArray<AEncountArea*> EncountAreas;

	/// <summary>
	/// 戦闘開始までの距離
	/// </summary>
	float EncountStartDistance;
};
