﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "../../Character/StatusData/EnemyID.h"
#include "EncountParameter.generated.h"



USTRUCT(BlueprintType)
struct FEncountGroup
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// 出現する敵グループのID
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<EEnemyID> EnemyIDGroup;
};

USTRUCT(BlueprintType)
struct FEncountParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// エンカウントID
	/// </summary>
	UPROPERTY(EditAnywhere)
	int EncountID;

	/// <summary>
	/// 出現する敵グループ一覧
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FEncountGroup> EncountGroupList;
};