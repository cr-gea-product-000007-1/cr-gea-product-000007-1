﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "EncountManager.h"
#include "Kismet/GameplayStatics.h"
#include "../../RPGGameInstance.h"

const float MinEncount = 800;
const float MaxEncount = 3000;

/// <summary>
/// 初期化
/// </summary>
void UEncountManager::Initialize()
{
	EncountStartDistance = FMath::RandRange(MinEncount, MaxEncount);

	TArray<AActor*> FindActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEncountArea::StaticClass(), FindActors);
	for (AActor* FindActor : FindActors)
	{
		AEncountArea* FindEncountArea = Cast<AEncountArea>(FindActor);
		if (IsValid(FindEncountArea))
			EncountAreas.Add(FindEncountArea);
	}
}

/// <summary>
/// エンカウントテーブルを設定
/// </summary>
/// <param name="EncountDataTable">エンカウントテーブル</param>
void UEncountManager::SetEncountTable(UDataTable* EncountDataTable)
{
	TArray<FName> RowNames = EncountDataTable->GetRowNames();
	for (FName RowName : RowNames)
	{
		FEncountParameter* EncountParameter = EncountDataTable->FindRow<FEncountParameter>(RowName, "");
		EncountParameterList.Add(*EncountParameter);
	}
}

/// <summary>
/// エンカウント更新
/// </summary>
/// <param name="MoveDistance">移動距離</param>
void UEncountManager::EncountUpdate(float MoveDistance)
{
	if (EncountAreas.Num() == 0) return;

	for (AEncountArea* EncountArea : EncountAreas)
	{
		if (EncountArea->InPlayer())
		{
			EncountStartDistance -= MoveDistance * EncountArea->GetEncountRate();
			return;
		}
	}
}

/// <summary>
/// ランダムエンカウントする
/// </summary>
/// <param name="Player">エンカウントするプレイヤー</param>
void UEncountManager::DrawingEncountStart(AFieldPlayerCharacter* Player)
{
	for (AEncountArea* EncountArea : EncountAreas)
	{
		if (EncountArea->InPlayer())
		{
			int EncountID = EncountArea->GetEncountID();
			FEncountParameter EncountParameter = *EncountParameterList.FindByPredicate([EncountID](FEncountParameter Parameter) { return Parameter.EncountID == EncountID; });

			TArray<FEncountGroup>& EncountGroupList = EncountParameter.EncountGroupList;
			FEncountGroup EncountGroup = EncountGroupList[FMath::Rand() % EncountGroupList.Num()];
			TArray<EEnemyID> EncountEnemyIDGroup;
			for (EEnemyID EnemyID : EncountGroup.EnemyIDGroup)
			{
				EncountEnemyIDGroup.Add(EnemyID);
			}
			EncountStart(Player, EncountEnemyIDGroup);
			return;
		}
	}
}

/// <summary>
/// エンカウントする
/// </summary>
/// <param name="EncountEnemyIDGroup">エンカウントする敵ID</param>
void UEncountManager::EncountStart(AFieldPlayerCharacter* Player, TArray<EEnemyID> EncountEnemyIDGroup)
{
	URPGGameInstance* GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	GameInstance->bInBattle = true;
	GameInstance->EncountEnemyIDGroup = EncountEnemyIDGroup;
	GameInstance->BattleTransform = Player->GetTransform();
	GameInstance->CameraControlRotate = Player->GetCameraControlRotate();
}
