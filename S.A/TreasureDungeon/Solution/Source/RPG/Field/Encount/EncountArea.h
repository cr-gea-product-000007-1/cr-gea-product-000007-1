﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EncountArea.generated.h"

UCLASS()
class RPG_API AEncountArea : public AActor
{
	GENERATED_BODY()

public:
	/// <summary>
	/// エンカウント候補のID
	/// </summary>
	/// <returns>エンカウント候補のID</returns>
	int GetEncountID() { return EncountID; };

	/// <summary>
	/// エンカウント率を取得
	/// </summary>
	/// <returns>エンカウント率</returns>
	float GetEncountRate() { return EncountRate; };

	/// <summary>
	/// プレイヤーが範囲内に入っているか取得
	/// </summary>
	/// <returns>範囲内に入っている = true</returns>
	bool InPlayer() { return bInPlayer; };

public:
	/// <summary>
	/// エンカウント候補のID
	/// </summary>
	UPROPERTY(EditAnywhere)
	int EncountID;

	/// <summary>
	/// エンカウント率
	/// </summary>
	UPROPERTY(EditAnywhere)
	float EncountRate;

	/// <summary>
	/// プレイヤーが範囲内に入っているか
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	bool bInPlayer;
};
