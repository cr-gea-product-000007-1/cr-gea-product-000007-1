﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/InputWidget.h"
#include "../../RPGGameInstance.h"
#include "../Interface/FieldEventInterface.h"
#include "MenuSettingWidget.generated.h"


UENUM(BlueprintType)
enum class EMenuSettingType : uint8
{
	SEVolume = 0,
	BGMVolume = 1,
	GameContinue = 2,
	GameEnd = 3,
};

UCLASS()
class RPG_API UMenuSettingWidget : public UInputWidget
{
	GENERATED_BODY()

	//音量の増減値
	const float SlideVolume = 0.1f;

public:
	/// <summary>
	/// サウンド関連の読み込み
	/// </summary>
	void LoadSound();

	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	/// <param name="Index">インデックス</param>
	UFUNCTION(BlueprintCallable)
	void SetSelectIndex(EMenuSettingType Index);

	/// <summary>
	/// 選んでいるインデックスを取得
	/// </summary>
	/// <return> 選んでいるインデックス </return>
	EMenuSettingType GetSelectIndex() { return SelectIndex; };

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnRight() override;

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ActiveArrow() override;

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void StopArrow() override;

	/// <summary>
	/// フィールドイベントインターフェースを設定
	/// </summary>
	void SetFieldEventInterface(IFieldEventInterface* Interface) { FieldEventInterface = Interface; };

private:
	/// <summary>
	/// SEの音量を加算
	/// </summary>
	/// <param name="AddValume">加算する音量</param>
	void AddSEVolume(float AddValume);

	/// <summary>
	/// BGMの音量を加算
	/// </summary>
	/// <param name="AddValume">加算する音量</param>
	void AddBGMVolume(float AddValume);

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	UPROPERTY()
	URPGGameInstance* GameInstance;

public:
	/// <summary>
	/// 選んでいるインデックス
	/// </summary>
	UPROPERTY(BlueprintReadOnly)
	EMenuSettingType SelectIndex;

	/// <summary>
	/// SEの音量
	/// </summary>
	UPROPERTY(BlueprintReadOnly)
	float SE_Volume;

	/// <summary>
	/// BGMの音量
	/// </summary>
	UPROPERTY(BlueprintReadOnly)
	float BGM_Volume;

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	IFieldEventInterface* FieldEventInterface;
};
