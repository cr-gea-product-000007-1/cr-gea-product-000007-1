﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSettingWidget.h"
#include "Kismet/GameplayStatics.h"


/// <summary>
/// サウンド関連の読み込み
/// </summary>
void UMenuSettingWidget::LoadSound()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	SE_Volume = GameInstance->SE_Volue;
	BGM_Volume = GameInstance->BGM_Volue;
}

/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UMenuSettingWidget::SetSelectIndex(EMenuSettingType Index)
{
	SelectIndex = Index;
	UpdateWindow();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UMenuSettingWidget::OnUp()
{
	int Index = (int)SelectIndex - 1;
	//ゲームを続けるは横移動なので上に移動
	if (Index >= (int)EMenuSettingType::GameContinue)
		Index = (int)EMenuSettingType::BGMVolume;

	if (Index < (int)EMenuSettingType::SEVolume)
		Index = (int)EMenuSettingType::GameContinue;
	SelectIndex = (EMenuSettingType)Index;
	UpdateWindow();
	return true;
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UMenuSettingWidget::OnDown()
{
	int Index = (int)SelectIndex + 1;
	if (Index > (int)EMenuSettingType::GameContinue)
		Index = (int)EMenuSettingType::SEVolume;
	SelectIndex = (EMenuSettingType)Index;
	UpdateWindow();
	return true;
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UMenuSettingWidget::OnLeft()
{
	switch (SelectIndex)
	{
	case EMenuSettingType::SEVolume:
		AddSEVolume(-SlideVolume);
		break;
	case EMenuSettingType::BGMVolume:
		AddBGMVolume(-SlideVolume);
		break;
	case EMenuSettingType::GameContinue:
		SelectIndex = EMenuSettingType::GameEnd;
		break;
	case EMenuSettingType::GameEnd:
		SelectIndex = EMenuSettingType::GameContinue;
		break;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
/// <return> イベント実行した = true </return>
bool UMenuSettingWidget::OnRight()
{
	switch (SelectIndex)
	{
	case EMenuSettingType::SEVolume:
		AddSEVolume(SlideVolume);
		break;
	case EMenuSettingType::BGMVolume:
		AddBGMVolume(SlideVolume);
		break;
	case EMenuSettingType::GameContinue:
		SelectIndex = EMenuSettingType::GameEnd;
		break;
	case EMenuSettingType::GameEnd:
		SelectIndex = EMenuSettingType::GameContinue;
		break;
	}
	UpdateWindow();
	return true;
}

/// <summary>
/// SEの音量を加算
/// </summary>
/// <param name="AddValume">加算する音量</param>
void UMenuSettingWidget::AddSEVolume(float AddValume)
{
	if (GameInstance == nullptr) return;

	SE_Volume += AddValume;
	if (SE_Volume < 0.0f)
		SE_Volume = 0.0f;
	if (SE_Volume > 1.0f)
		SE_Volume = 1.0f;
	GameInstance->SE_Volue = SE_Volume;

	FieldEventInterface->BGMVolumeUpdate();
}

/// <summary>
/// BGMの音量を加算
/// </summary>
/// <param name="AddValume">加算する音量</param>
void UMenuSettingWidget::AddBGMVolume(float AddValume)
{
	if (GameInstance == nullptr) return;

	BGM_Volume += AddValume;
	if (BGM_Volume < 0.0f)
		BGM_Volume = 0.0f;
	if (BGM_Volume > 1.0f)
		BGM_Volume = 1.0f;
	GameInstance->BGM_Volue = BGM_Volume;

	FieldEventInterface->BGMVolumeUpdate();
}