﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuGameEndWidget.h"


/// <summary>
/// 選んでいるインデックスを設定
/// </summary>
void UMenuGameEndWidget::SetSelectIndex(EMenuGameEndType Index)
{
	SelectIndex = Index;
	UpdateArrow();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
bool UMenuGameEndWidget::OnLeft()
{
	if (SelectIndex == EMenuGameEndType::Continue)
		SelectIndex = EMenuGameEndType::End;
	else
		SelectIndex = EMenuGameEndType::Continue;

	UpdateArrow();
	return true;
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
bool UMenuGameEndWidget::OnRight()
{
	if (SelectIndex == EMenuGameEndType::Continue)
		SelectIndex = EMenuGameEndType::End;
	else
		SelectIndex = EMenuGameEndType::Continue;

	UpdateArrow();
	return true;
}