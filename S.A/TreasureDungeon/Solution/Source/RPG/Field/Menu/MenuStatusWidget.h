﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/InputWidget.h"
#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/Equipment.h"
#include "MenuStatusWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UMenuStatusWidget : public UInputWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// プレイヤーのステータスを設定する
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerStatus(UBattlePlayerStatus* PlayerStatus);
};
