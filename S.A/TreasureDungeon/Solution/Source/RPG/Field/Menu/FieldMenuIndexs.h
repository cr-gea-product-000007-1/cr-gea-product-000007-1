﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FieldMenuIndexs.generated.h"



UENUM(BlueprintType)
enum class EMenuSelectType : uint8
{
	Item = 0,
	Equipment = 1,
	Status = 2,
	Setting = 3,

	Begin = 0,
	End = 3,
};

UENUM(BlueprintType)
enum class EMenuEquipmentPartsType : uint8
{
	Weapon = 0,
	Shield = 1,
	Helmet = 2,
	Armor = 3,
	Accessory = 4,

	Begin = 0,
	End = 4,
};

UENUM()
enum class EMenuItemUseType : uint8
{
	Use,
	Trush,
};