﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/InputWidget.h"
#include "MenuGameEndWidget.generated.h"


UENUM()
enum class EMenuGameEndType : uint8
{
	End,
	Continue,
};

UCLASS()
class RPG_API UMenuGameEndWidget : public UInputWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 選んでいるインデックスを設定
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void SetSelectIndex(EMenuGameEndType Index);

	/// <summary>
	/// 選んでいるインデックスを取得
	/// </summary>
	EMenuGameEndType GetSelectIndex() { return SelectIndex; };

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	bool OnRight() override;

	/// <summary>
	/// 矢印の位置を更新
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateArrow();

	/// <summary>
	/// 矢印を動かす
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void ActiveArrow() override;

	/// <summary>
	/// 矢印を止める
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void StopArrow() override;

public:
	/// <summary>
	/// 選んでいるインデックス
	/// </summary>
	UPROPERTY(BlueprintReadonly)
	EMenuGameEndType SelectIndex;

	/// <summary>
	/// インデックスの最大値
	/// </summary>
	UPROPERTY(BlueprintReadwrite)
	int MaxIndex;
};
