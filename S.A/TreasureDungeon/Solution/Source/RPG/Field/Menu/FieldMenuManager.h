﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "../../Character/BattlePlayerStatus.h"
#include "../../Item/Item.h"

#include "FieldMenuIndexs.h"
#include "MenuStatusWidget.h"
#include "MenuSettingWidget.h"
#include "MenuGameEndWidget.h"
#include "../../System/UI/BaseSelectWidget.h"
#include "../../System/UI/DescriptionWidget.h"
#include "../../System/UI/ValueWidget.h"
#include "../../System/UI/OnePlayerEquipmentStatusWidget.h"
#include "../../System/UI/PlayerScreenView.h"

#include "../../System/UI/InputWidget.h"
#include "../UI/GoldWidget.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "../../Sound/SoundPlayer.h"
#include "../../System/Interface/SendUnderMessageInterface.h"
#include "../../System/Interface/LoadDataInterface.h"
#include "../Interface/FieldEventInterface.h"
#include "../Interface/FieldPlayerInterface.h"
#include "../Interface/FieldManagerStopperInterface.h"
#include "../../RPGGameInstance.h"

#include "FieldMenuManager.generated.h"


UCLASS()
class RPG_API UFieldMenuManager : public UObject, public ILoadDataInterface, public IInputWidgetInterface, public ISendUnderMessageInterface
{
	GENERATED_BODY()

	enum class EActiveWidgetType
	{
		None,
		MenuSelect,
		ItemSelect,
		ItemUse,
		ItemTargetSelect,
		EquipmentTargetSelect,
		EquipmentPartsSelect,
		EquipmentSelect,
		StatusTargetSelect,
		StatusView,
		Setting,
		GameEnd,
	};

public:
	/// <summary>
	/// ウィジットを生成
	/// </summary>
	void WidgetCreate();

	/// <summary>
	/// コマンド入力を始める
	/// </summary>
	void CommandSelectStart();

	/// <summary>
	/// ウィジットを全て閉じる
	/// </summary>
	void AllClose();

	/// <summary>
	/// メニューを開いているか
	/// </summary>
	bool IsMenu() { return bIsMenu; };

	/// <summary>
	/// メニューを開けるか設定する
	/// </summary>
	void SetIsOpen(bool IsOpen) { bIsOpen = IsOpen; };

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// キャンセルキーを押したときのイベント
	/// </summary>
	void OnCancel() override;

	/// <summary>
	/// メニューキーを押したときのイベント
	/// </summary>
	void OnMenu() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	/// <return> イベント実行した = true </return>
	void OnRight() override;

	/// <summary>
	/// フィールドイベントインターフェースを設定
	/// </summary>
	void SetFieldEventInterface(IFieldEventInterface* Interface);

	/// <summary>
	/// プレイヤーインターフェースを設定する
	/// </summary>
	/// <param name="Interface">プレイヤーインターフェース</param>
	void SetFieldPlayerInterface(IFieldPlayerInterface* Interface) { FieldPlayerInterface = Interface; };

	/// <summary>
	/// マネージャー停止インターフェースを設定する
	/// </summary>
	/// <param name="Interface">マネージャー停止インターフェース</param>
	void SetFieldManagerStopperInterface(IFieldManagerStopperInterface* Interface) { FieldManagerStopperInterface = Interface; };

private:
	/// <summary>
	/// 次のウィジットに移動
	/// </summary>
	/// <param name="NextWidget">移動するウィジット</param>
	/// <param name="NewActiveWidgetType">移動するウィジットの種類</param>
	void NextWidget(UInputWidget* NextWidget, EActiveWidgetType NewActiveWidgetType);

	/// <summary>
	/// 前のウィジットに移動
	/// </summary>
	/// <param name="Num">移動回数</param>
	/// <return> 移動した = true </return>
	bool BackWidget(int Num = 1);

	/// <summary>
	/// メニューの選択
	/// </summary>
	/// <param name="MenuSelectType">選択するメニュー</param>
	void SelectCommand(EMenuSelectType MenuSelectType);

	/// <summary>
	/// アイテム選択時の位置にウィジットを移動
	/// </summary>
	void SetItemWidgetLocation();

	/// <summary>
	/// 装備選択時の位置にウィジットを移動
	/// </summary>
	void SetEquipmentWidgetLocation();

	/// <summary>
	/// ステータス選択時の位置にウィジットを移動
	/// </summary>
	void SetStatusWidgetLocation();

	/// <summary>
	/// ゲーム設定時の位置にウィジットを移動
	/// </summary>
	void SetSettingWidgetLocation();

	/// <summary>
	/// プレイヤーキャラリストを取得
	/// </summary>
	TArray<UBattlePlayerStatus*> GetPlayerStatuses();

	/// <summary>
	/// アイテムがあるか
	/// </summary>
	/// <return> アイテムがある = true </return>
	bool ExitItem() { return GetHaveItems(this).Num() > 0; };

	/// <summary>
	/// アイテム使用
	/// </summary>
	/// <param name="Item">アイテム</param>
	void UseItem(UItem* Item);

	/// <summary>
	/// 装備候補取得
	/// </summary>
	/// <param name="EquipmentType">装備部位</param>
	TArray<FHaveEquipment> GetEquipmentCandidate(EItemType EquipmentType);

	/// <summary>
	/// アイテム選択ウインドウの更新
	/// </summary>
	void ItemSelectWidgetUpdate();

	/// <summary>
	/// 装備選択ウインドウの更新
	/// </summary>
	void EquipmentSelectWidgetUpdate();

private:
	/// <summary>
	/// ゲームインスタンス
	/// </summary>
	UPROPERTY()
	URPGGameInstance* GameInstance;

	/// <summary>
	/// 開いているウインドウの種類
	/// </summary>
	EActiveWidgetType ActiveWidgetType;

	/// <summary>
	/// 開いているウインドウ
	/// </summary>
	UPROPERTY()
	UInputWidget* ActiveWidget;

	/// <summary>
	/// 開いたウインドウの種類の記録
	/// </summary>
	TArray<EActiveWidgetType> WidgetTypeLog;

	/// <summary>
	/// 開いたウインドウの記録
	/// </summary>
	UPROPERTY()
	TArray<UInputWidget*> WidgetLog;

	/// <summary>
	/// メニューを開いているか
	/// </summary>
	bool bIsMenu;

	/// <summary>
	/// メニューを開ける状態か
	/// </summary>
	bool bIsOpen = true;

	/// <summary>
	/// メニュー選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ActionSelectWidget;

	/// <summary>
	/// 所持金ウィジット
	/// </summary>
	UPROPERTY()
	UGoldWidget* GoldWidget;

	/// <summary>
	/// 対象ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* TargetSelectWidget;

	/// <summary>
	/// アイテム説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* ItemDescriptionWidget;

	/// <summary>
	/// アイテム個数ウィジット
	/// </summary>
	UPROPERTY()
	UValueWidget* ItemCountWidget;

	/// <summary>
	/// 装備説明文ウィジット
	/// </summary>
	UPROPERTY()
	UDescriptionWidget* EquipmentDescriptionWidget;

	/// <summary>
	/// 装備ステータス表示ウィジット
	/// </summary>
	UPROPERTY()
	UOnePlayerEquipmentStatusWidget* OnePlayerEquipmentStatusWidget;

	/// <summary>
	/// アイテム選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ItemSelectWidget;

	/// <summary>
	/// アイテム使用ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* ItemUseWidget;

	/// <summary>
	/// 装備部位選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* EquipmentPartsSelectWidget;

	/// <summary>
	/// 装備選択ウィジット
	/// </summary>
	UPROPERTY()
	UBaseSelectWidget* EquipmentSelectWidget;

	/// <summary>
	/// ステータスウィジット
	/// </summary>
	UPROPERTY()
	UMenuStatusWidget* MenuStatusWidget;

	/// <summary>
	/// プレイヤー表示ウィジット
	/// </summary>
	UPROPERTY()
	APlayerScreenView* PlayerScreenView;

	/// <summary>
	/// ゲーム設定ウィジット
	/// </summary>
	UPROPERTY()
	UMenuSettingWidget* SettingWidget;

	/// <summary>
	/// ゲーム終了ウィジット
	/// </summary>
	UPROPERTY()
	UMenuGameEndWidget* GameEndWidget;

	/// <summary>
	/// フィールドイベントインターフェース
	/// </summary>
	IFieldEventInterface* FieldEventInterface;

	/// <summary>
	/// プレイヤーインターフェース
	/// </summary>
	IFieldPlayerInterface* FieldPlayerInterface;

	/// <summary>
	/// マネージャー停止インターフェース
	/// </summary>
	IFieldManagerStopperInterface* FieldManagerStopperInterface;

	/// <summary>
	/// サウンドを再生する
	/// </summary>
	UPROPERTY()
	USoundPlayer* SoundPlayer;
};