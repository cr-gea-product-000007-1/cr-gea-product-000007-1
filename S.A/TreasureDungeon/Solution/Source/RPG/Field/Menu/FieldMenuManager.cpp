﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldMenuManager.h"
#include "Kismet/GameplayStatics.h"
#include "../../Item/ItemStorage.h"
#include "../../PathConstant.h"
#include "Kismet/KismetSystemLibrary.h"

const TArray<FString> FieldMenuText = { TEXT("アイテム"), TEXT("装備"), TEXT("ステータス"), TEXT("設定") };
const TArray<FString> FieldItemUseText = { TEXT("使う"), TEXT("やめる") };
const FVector2D MenuWindowStartPosition = (100.0f, 150.0f);

/// <summary>
/// ウィジットを生成
/// </summary>
void UFieldMenuManager::WidgetCreate()
{
	GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	FString Path;
	Path = Path::OneLineSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> SelectWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(SelectWidgetClass))
	{
		//項目選択
		ActionSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		ActionSelectWidget->AddToViewport();
		ActionSelectWidget->SetCanvasPannelRenderTranslation(MenuWindowStartPosition);
		ActionSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 4, 5.0 });
		ActionSelectWidget->SetText(FieldMenuText);
		ActionSelectWidget->Hide();
		//対象選択
		TargetSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		TargetSelectWidget->AddToViewport();
		TargetSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 4, 6.0 });
		TargetSelectWidget->SetPlayerStatus(GetPlayerStatuses());
		TargetSelectWidget->Hide();
		//アイテム使用
		ItemUseWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		ItemUseWidget->AddToViewport();
		ItemUseWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 2, 3.0 });
		ItemUseWidget->SetText(FieldItemUseText);
		ItemUseWidget->Hide();
		//装備部位選択
		EquipmentPartsSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectWidgetClass);
		EquipmentPartsSelectWidget->AddToViewport();
		EquipmentPartsSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 5, 5.0 });
		EquipmentPartsSelectWidget->Hide();
	};
	Path = Path::OneLinePageSelectWidgetPath;
	TSubclassOf<UBaseSelectWidget> SelectPageWidgetClass = TSoftClassPtr<UBaseSelectWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(SelectPageWidgetClass))
	{
		//アイテム選択
		ItemSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectPageWidgetClass);
		ItemSelectWidget->AddToViewport();
		ItemSelectWidget->SetCanvasPannelRenderTranslation(MenuWindowStartPosition);
		ItemSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 8, 9.0 });
		ItemSelectWidget->Hide();
		//装備選択
		EquipmentSelectWidget = CreateWidget<UBaseSelectWidget>(GetWorld(), SelectPageWidgetClass);
		EquipmentSelectWidget->AddToViewport();
		EquipmentSelectWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 8, 9.0 });
		EquipmentSelectWidget->Hide();
	}
	Path = Path::DescriptionWidgetPath;
	TSubclassOf<UDescriptionWidget> DescriptionWidgetClass = TSoftClassPtr<UDescriptionWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(DescriptionWidgetClass))
	{
		//アイテム説明文
		ItemDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		ItemDescriptionWidget->AddToViewport();
		ItemDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 10.0 });
		ItemDescriptionWidget->Hide();
		ItemSelectWidget->AddSubWidget(ItemDescriptionWidget);
		//装備説明文
		EquipmentDescriptionWidget = CreateWidget<UDescriptionWidget>(GetWorld(), DescriptionWidgetClass);
		EquipmentDescriptionWidget->AddToViewport();
		EquipmentDescriptionWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 3, 10.0 });
		EquipmentDescriptionWidget->Hide();
		EquipmentSelectWidget->AddSubWidget(EquipmentDescriptionWidget);
	};
	//所持金表示
	Path = Path::GoldWidgetPath;
	TSubclassOf<UGoldWidget> GoldWidgetClass = TSoftClassPtr<UGoldWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(GoldWidgetClass))
	{
		GoldWidget = CreateWidget<UGoldWidget>(GetWorld(), GoldWidgetClass);
		GoldWidget->AddToViewport();
		GoldWidget->SetRenderTranslation(FVector2D(650.0f, -450.0f));
	};

	Path = Path::ValueWidgetPath;
	TSubclassOf<UValueWidget> ValueWidgetClass = TSoftClassPtr<UValueWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(ValueWidgetClass))
	{
		//アイテム個数
		ItemCountWidget = CreateWidget<UValueWidget>(GetWorld(), ValueWidgetClass);
		ItemCountWidget->AddToViewport();
		ItemCountWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, SelectTextSpace, 1, 10.0f });
		ItemCountWidget->SetItemUnit();
		ItemCountWidget->Hide();
		ItemSelectWidget->AddSubWidget(ItemCountWidget);
	};
	//装備ステータス
	Path = Path::OnePlayerEquipmentStatusWidgetPath;
	TSubclassOf<UOnePlayerEquipmentStatusWidget> OnePlayerEquipmentStatusWidgetClass = TSoftClassPtr<UOnePlayerEquipmentStatusWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(OnePlayerEquipmentStatusWidgetClass))
	{
		OnePlayerEquipmentStatusWidget = CreateWidget<UOnePlayerEquipmentStatusWidget>(GetWorld(), OnePlayerEquipmentStatusWidgetClass);
		OnePlayerEquipmentStatusWidget->AddToViewport();
		OnePlayerEquipmentStatusWidget->Initialize(FSelectWidgetParameter{ SelectTextFontSize, 50 }, EquipmentDescriptionWidget->GetWindowSize());
		OnePlayerEquipmentStatusWidget->Hide();
		EquipmentSelectWidget->AddSubWidget(OnePlayerEquipmentStatusWidget);
	};
	//ステータス
	Path = Path::MenuStatusWidgetPath;
	TSubclassOf<UMenuStatusWidget> MenuStatusWidgetClass = TSoftClassPtr<UMenuStatusWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(MenuStatusWidgetClass))
	{
		MenuStatusWidget = CreateWidget<UMenuStatusWidget>(GetWorld(), MenuStatusWidgetClass);
		MenuStatusWidget->AddToViewport();
	};
	//プレイヤー表示
	Path = Path::PlayerScreenViewPath;
	TSubclassOf<APlayerScreenView> PlayerScreenViewClass = TSoftClassPtr<APlayerScreenView>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(PlayerScreenViewClass))
	{
		PlayerScreenView = GetWorld()->SpawnActor<APlayerScreenView>(PlayerScreenViewClass);
		TArray<UBattlePlayerStatus*> PlayerStatuses = GetPlayerStatuses();
		TArray<UBattleCharacterStatus*> CharacterStatuses;
		for (UBattlePlayerStatus* PlayerStatus : PlayerStatuses)
			CharacterStatuses.Add(PlayerStatus);
		PlayerScreenView->CreatePlayer(CharacterStatuses);
	};
	//ゲーム設定
	Path = Path::MenuSettingWidgetPath;
	TSubclassOf<UMenuSettingWidget> MenuSettingWidgetClass = TSoftClassPtr<UMenuSettingWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(MenuSettingWidgetClass))
	{
		SettingWidget = CreateWidget<UMenuSettingWidget>(GetWorld(), MenuSettingWidgetClass);
		SettingWidget->AddToViewport();
		SettingWidget->LoadSound();
	};
	//ゲーム終了
	Path = Path::MenuGameEndWidgetPath;
	TSubclassOf<UMenuGameEndWidget> MenuGameEndWidgetClass = TSoftClassPtr<UMenuGameEndWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(MenuGameEndWidgetClass))
	{
		GameEndWidget = CreateWidget<UMenuGameEndWidget>(GetWorld(), MenuGameEndWidgetClass);
		GameEndWidget->AddToViewport();
	};

	SoundPlayer = NewObject<USoundPlayer>();
}

/// <summary>
/// コマンド入力を始める
/// </summary>
void UFieldMenuManager::CommandSelectStart()
{
	if (!bIsOpen) return;
	if (FieldEventInterface == nullptr) return;
	if (FieldManagerStopperInterface == nullptr) return;

	FieldManagerStopperInterface->SetFieldEventFlag(false);
	ActionSelectWidget->Open();
	GoldWidget->Open();
	GoldWidget->SetHaveGold(FieldEventInterface->GetHaveGold());
	FieldPlayerInterface->ViewStatusBar();
	ActiveWidgetType = EActiveWidgetType::MenuSelect;
	ActiveWidget = ActionSelectWidget;
	bIsMenu = true;
	WidgetLog.Empty();
	WidgetTypeLog.Empty();
}

/// <summary>
/// ウィジットを全て閉じる
/// </summary>
void UFieldMenuManager::AllClose()
{
	ActiveWidgetType = EActiveWidgetType::None;
	ActiveWidget = nullptr;
	bIsMenu = false;
	FieldPlayerInterface->NonViewStatusBar();
	WidgetLog.Empty();
	WidgetTypeLog.Empty();
	FieldManagerStopperInterface->SetFieldEventFlag(true);

	if (ActionSelectWidget->IsOpen())
		ActionSelectWidget->Hide();

	if (GoldWidget->IsOpen())
		GoldWidget->Close();

	if (TargetSelectWidget->IsOpen())
		TargetSelectWidget->Hide();

	if (ItemSelectWidget->IsOpen())
		ItemSelectWidget->Hide();

	if (ItemUseWidget->IsOpen())
		ItemUseWidget->Hide();
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnDecide()
{
	if (!IsValid(ActiveWidget)) return;
	ActiveWidget->OnDecide();

	switch (ActiveWidgetType)
	{
	case EActiveWidgetType::MenuSelect:
	{
		EMenuSelectType MenuSelectType = (EMenuSelectType)ActionSelectWidget->GetSelectIndex();
		SelectCommand(MenuSelectType);
		break;
	}
	case EActiveWidgetType::ItemSelect:
	{
		UBaseItem* BaseItem = GetHaveItems(this)[ItemSelectWidget->GetSelectIndex()]->GetItem();
		UItem* Item = Cast<UItem>(BaseItem);
		if (Item->GetItemParameter().ItemUseType == EItemUseType::NotUse)
		{
			SoundPlayer->PlayCancelSound(this);
			break;
		}
		USkill* Skill = GetSkill(this, Item->GetSkillID());
		FSkillParameter SkillParameter = Skill->GetSkillParameter();
		if (SkillParameter.SkillTarget != ESkillTarget::OneMember && SkillParameter.SkillTarget != ESkillTarget::AllMenbers)
			break;
		SoundPlayer->PlayDecisionSound(this);
		NextWidget(ItemUseWidget, EActiveWidgetType::ItemUse);
		break;
	}
	case EActiveWidgetType::ItemUse:
	{
		EMenuItemUseType ItenUseType = (EMenuItemUseType)ItemUseWidget->GetSelectIndex();
		if (ItenUseType == EMenuItemUseType::Use)
		{
			UBaseItem* BaseItem = GetHaveItems(this)[ItemSelectWidget->GetSelectIndex()]->GetItem();
			UItem* Item = Cast<UItem>(BaseItem);
			USkill* Skill = GetSkill(this, Item->GetSkillID());
			FSkillParameter SkillParameter = Skill->GetSkillParameter();
			if (SkillParameter.SkillTarget == ESkillTarget::OneMember)
			{
				SoundPlayer->PlayDecisionSound(this);
				NextWidget(TargetSelectWidget, EActiveWidgetType::ItemTargetSelect);
			}

			if (SkillParameter.SkillTarget == ESkillTarget::AllMenbers)
			{
				SoundPlayer->PlayDecisionSound(this);
				TArray<UBattlePlayerStatus*> Players = GetPlayerStatuses();
				FieldPlayerInterface->HitSkillPlayer(*Players.begin(),Players,Skill);
				FieldPlayerInterface->UpdateStatusBar();
				UseItem(Item);
				//アイテムが残っていたら1回、ないなら2回戻る
				BackWidget(ExitItem() ? 1 : 2);
				ItemSelectWidgetUpdate();
			}
		}
		else {
			BackWidget();
		}
		break;
	}
	case EActiveWidgetType::ItemTargetSelect:
	{
		SoundPlayer->PlayDecisionSound(this);
		UBattlePlayerStatus* Player = GetPlayerStatuses()[TargetSelectWidget->GetSelectIndex()];
		UBaseItem* BaseItem = GetHaveItems(this)[ItemSelectWidget->GetSelectIndex()]->GetItem();
		UItem* Item = Cast<UItem>(BaseItem);
		USkill* Skill = GetSkill(this, Item->GetSkillID());
		FSkillParameter SkillParameter = Skill->GetSkillParameter();
		FieldPlayerInterface->HitSkillPlayer(Player, TArray<UBattlePlayerStatus*>{Player}, Skill);
		FieldPlayerInterface->UpdateStatusBar();
		UseItem(Item);
		//アイテムが残っていたら2回、ないなら3回戻る
		if (ExitItem())
		{
			BackWidget(2);
			ItemSelectWidgetUpdate();
		}
		else {
			BackWidget(3);
		}
		break;
	}
	case EActiveWidgetType::EquipmentTargetSelect:
	{
		SoundPlayer->PlayDecisionSound(this);
		UBattlePlayerStatus* PlayerStatus = GetPlayerStatuses()[TargetSelectWidget->GetSelectIndex()];
		EquipmentPartsSelectWidget->SetEquipments(PlayerStatus->GetEquipments(),ESelectViewType::NameOnly);
		NextWidget(EquipmentPartsSelectWidget, EActiveWidgetType::EquipmentPartsSelect);
		OnePlayerEquipmentStatusWidget->SetPlayerStatus(PlayerStatus);
		break;
	}
	case EActiveWidgetType::EquipmentPartsSelect:
	{
		SoundPlayer->PlayDecisionSound(this);
		EItemType EquipmentType = (EItemType)(EquipmentPartsSelectWidget->GetSelectIndex() + (int)EItemType::Weapon); 
		NextWidget(EquipmentSelectWidget, EActiveWidgetType::EquipmentSelect);
		EquipmentSelectWidgetUpdate();
		OnePlayerEquipmentStatusWidget->SetEquipmentType(EquipmentType);
		EquipmentSelectWidgetUpdate();
		break;
	}
	case EActiveWidgetType::EquipmentSelect:
	{
		UBattlePlayerStatus* PlayerStatus = GetPlayerStatuses()[TargetSelectWidget->GetSelectIndex()];
		EItemType EquipmentType = (EItemType)(EquipmentPartsSelectWidget->GetSelectIndex() + (int)EItemType::Weapon);
		UEquipment* NewEquipment = GetEquipmentCandidate(EquipmentType)[EquipmentSelectWidget->GetSelectIndex()].Equipment;
		UEquipment* OldEquipment = PlayerStatus->EquipItem(EquipmentType, NewEquipment);

		UItemStorage* ItemStrage = GameInstance->ItemStorage;
		if(IsValid(NewEquipment))
			if (NewEquipment->GetItemType() != EItemType::NoEquipmwnt)
		ItemStrage->LostBaseItem(NewEquipment, 1);

		if (IsValid(OldEquipment))
			if (OldEquipment->GetItemType() != EItemType::NoEquipmwnt)
				ItemStrage->InsertBaseItem(OldEquipment, 1);

		SoundPlayer->PlayDecisionSound(this);
		BackWidget();
		EquipmentPartsSelectWidget->SetEquipments(PlayerStatus->GetEquipments(), ESelectViewType::NameOnly); 
		EquipmentSelectWidgetUpdate();
		FieldPlayerInterface->UpdateStatusBar();
		break;
	}
	case EActiveWidgetType::StatusTargetSelect:
	{
		SoundPlayer->PlayDecisionSound(this);
		UBattlePlayerStatus* PlayerStatus = GetPlayerStatuses()[TargetSelectWidget->GetSelectIndex()];
		MenuStatusWidget->SetPlayerStatus(PlayerStatus);
		NextWidget(MenuStatusWidget, EActiveWidgetType::StatusView);
		MenuStatusWidget->Open();
		break;
	}
	case EActiveWidgetType::Setting:
	{
		EMenuSettingType SettingType = SettingWidget->GetSelectIndex();

		switch (SettingType)
		{
		case EMenuSettingType::SEVolume:
			break;
		case EMenuSettingType::BGMVolume:
			break;
		case EMenuSettingType::GameContinue:
			SoundPlayer->PlayDecisionSound(this);
			BackWidget();
			break;
		case EMenuSettingType::GameEnd:
			SoundPlayer->PlayDecisionSound(this);
			NextWidget(GameEndWidget, EActiveWidgetType::GameEnd);
			break;
		}

		break;
	}
	case EActiveWidgetType::GameEnd:
	{
		EMenuGameEndType SettingType = GameEndWidget->GetSelectIndex();
		if (SettingType == EMenuGameEndType::Continue)
			BackWidget();
		else
			UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
		break;
	}
	}
}

/// <summary>
/// メニューキーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnMenu()
{
	if (!bIsOpen) return;
	if (ActiveWidgetType != EActiveWidgetType::None) return;
	if (IsValid(ActiveWidget)) return;

	SoundPlayer->PlayDecisionSound(this);
	CommandSelectStart();
}

/// <summary>
/// キャンセルキーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnCancel()
{
	if (BackWidget())
	{
		SoundPlayer->PlayCancelSound(this);
		return;
	}

	if (ActiveWidgetType != EActiveWidgetType::MenuSelect) return;
	SoundPlayer->PlayCancelSound(this);
	AllClose();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnUp()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnUp())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::ItemSelect)
		ItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::EquipmentSelect)
		EquipmentSelectWidgetUpdate();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnDown()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnDown())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::ItemSelect)
		ItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::EquipmentSelect)
		EquipmentSelectWidgetUpdate();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnLeft()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnLeft())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::ItemSelect)
		ItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::EquipmentSelect)
		EquipmentSelectWidgetUpdate();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void UFieldMenuManager::OnRight()
{
	if (!IsValid(ActiveWidget)) return;
	if (ActiveWidget->OnRight())
		SoundPlayer->PlayCursorMoveSound(this);

	if (ActiveWidgetType == EActiveWidgetType::ItemSelect)
		ItemSelectWidgetUpdate();

	if (ActiveWidgetType == EActiveWidgetType::EquipmentSelect)
		EquipmentSelectWidgetUpdate();
}

/// <summary>
/// 次のウィジットに移動
/// </summary>
/// <param name="NextWidget">移動するウィジット</param>
/// <param name="NewActiveWidgetType">移動するウィジットの種類</param>
void UFieldMenuManager::NextWidget(UInputWidget* NextWidget, EActiveWidgetType NewActiveWidgetType)
{
	if (IsValid(ActiveWidget))
	{
		WidgetLog.Add(ActiveWidget);
		WidgetTypeLog.Add(ActiveWidgetType);
		ActiveWidget->StopArrow();
	}
	ActiveWidget = NextWidget;
	ActiveWidgetType = NewActiveWidgetType;
	ActiveWidget->Open();
}

/// <summary>
/// 前のウィジットに移動
/// </summary>
/// <param name="Num">移動回数</param>
/// <return> 移動した = true </return>
bool UFieldMenuManager::BackWidget(int Num)
{
	for (int i = 0; i < Num; i++)
	{
		if (WidgetLog.Num() == 0)
			return false;
		ActiveWidget->Close();

		int Index = WidgetLog.Num() - 1;
		ActiveWidget = WidgetLog[Index];
		WidgetLog.RemoveAt(Index);

		ActiveWidgetType = WidgetTypeLog[Index];
		WidgetTypeLog.RemoveAt(Index);

		ActiveWidget->Open();
		ActiveWidget->ActiveArrow();
	}
	return true;
}

/// <summary>
/// メニューの選択
/// </summary>
/// <param name="MenuSelectType">選択するメニュー</param>
void UFieldMenuManager::SelectCommand(EMenuSelectType MenuSelectType)
{
	switch (MenuSelectType)
	{
	case EMenuSelectType::Item:
	{
		if (!ExitItem())
		{
			SoundPlayer->PlayCancelSound(this);
			return;
		}
		SetItemWidgetLocation();
		NextWidget(ItemSelectWidget, EActiveWidgetType::ItemSelect);
		ItemSelectWidgetUpdate();
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->Close();
		break;
	}
	case EMenuSelectType::Equipment:
	{
		SetEquipmentWidgetLocation();
		NextWidget(TargetSelectWidget, EActiveWidgetType::EquipmentTargetSelect);
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->Close();
		break;
	}
	case EMenuSelectType::Status:
	{
		SetStatusWidgetLocation();
		NextWidget(TargetSelectWidget, EActiveWidgetType::StatusTargetSelect);
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->Close();
		break;
	}
	case EMenuSelectType::Setting:
	{
		SetSettingWidgetLocation();
		NextWidget(SettingWidget, EActiveWidgetType::Setting);
		SettingWidget->Open();
		SoundPlayer->PlayDecisionSound(this);
		ActionSelectWidget->Close();
		break;
	}
	}
}

/// <summary>
/// アイテム選択時の位置にウィジットを移動
/// </summary>
void UFieldMenuManager::SetItemWidgetLocation()
{
	ItemSelectWidget->SetCanvasPannelRenderTranslation(MenuWindowStartPosition);
	ItemDescriptionWidget->SetCanvasPannelRenderTranslation(ItemSelectWidget->GetUpperRightCorner());
	ItemCountWidget->SetRenderTranslation(ItemDescriptionWidget->GetLowerLeftCorner());
	ItemUseWidget->SetCanvasPannelRenderTranslation(ItemDescriptionWidget->GetUpperRightCorner());
	TargetSelectWidget->SetCanvasPannelRenderTranslation(ItemUseWidget->GetUpperRightCorner());
}

/// <summary>
/// 装備選択時の位置にウィジットを移動
/// </summary>
void UFieldMenuManager::SetEquipmentWidgetLocation()
{
	TargetSelectWidget->SetCanvasPannelRenderTranslation(MenuWindowStartPosition);
	EquipmentPartsSelectWidget->SetCanvasPannelRenderTranslation(TargetSelectWidget->GetUpperRightCorner());
	EquipmentSelectWidget->SetCanvasPannelRenderTranslation(EquipmentPartsSelectWidget->GetUpperRightCorner());
	EquipmentDescriptionWidget->SetCanvasPannelRenderTranslation(EquipmentSelectWidget->GetUpperRightCorner());
	OnePlayerEquipmentStatusWidget->SetRenderTranslation(EquipmentDescriptionWidget->GetLowerLeftCorner());
}

/// <summary>
/// ステータス選択時の位置にウィジットを移動
/// </summary>
void UFieldMenuManager::SetStatusWidgetLocation()
{
	TargetSelectWidget->SetCanvasPannelRenderTranslation(MenuWindowStartPosition);
	MenuStatusWidget->SetRenderTranslation(FVector2D(0.0f, 0.0f));
}

/// <summary>
/// ゲーム設定時の位置にウィジットを移動
/// </summary>
void UFieldMenuManager::SetSettingWidgetLocation()
{
	SettingWidget->SetRenderTranslation(FVector2D(0.0f, 0.0f));
	GameEndWidget->SetRenderTranslation(FVector2D(0.0f, 0.0f));
}

/// <summary>
/// プレイヤーキャラリストを取得
/// </summary>
TArray<UBattlePlayerStatus*> UFieldMenuManager::GetPlayerStatuses()
{
	if(GameInstance == nullptr)
		GameInstance = Cast<URPGGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	return GameInstance->PlayerStatuses;
}

/// <summary>
/// アイテム使用
/// </summary>
/// <param name="Item">アイテム</param>
void UFieldMenuManager::UseItem(UItem* Item)
{
	ShowMessage(Item->GetItemName() + TEXT(" を使った !"));
	if(GameInstance->ItemStorage->LostItem(Item, 1))
		ItemSelectWidget->SetHaveItems(GetHaveItems(this), ESelectViewType::NameAndCount);
}

/// <summary>
/// 装備候補取得
/// </summary>
/// <param name="EquipmentType">装備部位</param>
TArray<FHaveEquipment> UFieldMenuManager::GetEquipmentCandidate(EItemType EquipmentType)
{
	TArray<FHaveEquipment> EquipmentCandidates;
	TArray<UHaveItem*> HaveItems;
	if (EquipmentType == EItemType::Weapon)
		HaveItems = GetHaveWeapons(this);
	else
		HaveItems = GetHaveProtectors(this);

	//同じ部位の装備を変換して抜き出す
	for (UHaveItem* HaveItem : HaveItems)
	{
		UBaseItem* BaseItem = HaveItem->GetItem();
		if (BaseItem->GetItemType() == EquipmentType)
		{
			FHaveEquipment EquipmentCandidate;
			UEquipment* Equipment = Cast<UEquipment>(BaseItem);
			if(!IsValid(Equipment)) continue;
			EquipmentCandidate.Equipment = Equipment;
			EquipmentCandidate.Num = HaveItem->GetNum();
			EquipmentCandidates.Add(EquipmentCandidate);
		}
	}
	//装備しない場合に選ぶ項目
	FHaveEquipment NoEquipmentCandidate;
	NoEquipmentCandidate.Equipment = GameInstance->NoEquipment;
	NoEquipmentCandidate.Num = 0;
	EquipmentCandidates.Add(NoEquipmentCandidate);
	
	return EquipmentCandidates;
}

/// <summary>
/// アイテム選択ウインドウの更新
/// </summary>
void UFieldMenuManager::ItemSelectWidgetUpdate()
{
	ItemSelectWidget->SetHaveItems(GetHaveItems(this), ESelectViewType::NameAndCount);
	ItemSelectWidget->ClampSelectIndex();
	ItemSelectWidget->UpdateWindow();
	TArray<UHaveItem*> HaveItems = GetHaveItems(this);
	int Index = ItemSelectWidget->GetSelectIndex();
	if (HaveItems.Num() >= Index)
	{
		UBaseItem* BaseItem = HaveItems[Index]->GetItem();
		ItemDescriptionWidget->SetText(BaseItem->GetDescription()); 
		ItemCountWidget->SetNum(HaveItems[Index]->GetNum());
	}
}

/// <summary>
/// 装備選択ウインドウの更新
/// </summary>
void UFieldMenuManager::EquipmentSelectWidgetUpdate()
{
	EItemType EquipmentType = (EItemType)(EquipmentPartsSelectWidget->GetSelectIndex() + (int)EItemType::Weapon);
	EquipmentSelectWidget->SetHaveEquipments(GetEquipmentCandidate(EquipmentType), ESelectViewType::NameAndCount);
	EquipmentSelectWidget->ClampSelectIndex();
	EquipmentSelectWidget->UpdateWindow(); 
	TArray<FHaveEquipment> EquipmentCandidate = GetEquipmentCandidate(EquipmentType);
	int Index = EquipmentSelectWidget->GetSelectIndex();
	if (EquipmentCandidate.Num() >= Index)
	{
		UEquipment* Equipment = EquipmentCandidate[Index].Equipment;
		EquipmentDescriptionWidget->SetText(Equipment->GetDescription());
		OnePlayerEquipmentStatusWidget->SetEquipment(Equipment);
		OnePlayerEquipmentStatusWidget->UpdateWindow();
	}
}


/// <summary>
/// フィールドイベントインターフェースを設定
/// </summary>
void UFieldMenuManager::SetFieldEventInterface(IFieldEventInterface* Interface)
{
	FieldEventInterface = Interface;

	if (IsValid(SettingWidget))
		SettingWidget->SetFieldEventInterface(Interface);
}