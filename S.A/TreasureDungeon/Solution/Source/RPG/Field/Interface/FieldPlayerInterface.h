﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FieldPlayerInterface.generated.h"

class UBattlePlayerStatus;
class USkill;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFieldPlayerInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IFieldPlayerInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// スキルをプレイヤーに与える
	/// </summary>
	/// <param name="User">使用者</param>
	/// <param name="Targets">対象者</param>
	/// <param name="Skill">スキル</param>
	virtual void HitSkillPlayer(UBattlePlayerStatus* User, TArray<UBattlePlayerStatus*> Targets, USkill* Skill) = 0;

	/// <summary>
	/// ステータスバーの表示
	/// </summary>
	virtual void ViewStatusBar() = 0;

	/// <summary>
	/// ステータスバーの非表示
	/// </summary>
	virtual void NonViewStatusBar() = 0;

	/// <summary>
	/// ステータスバーの更新
	/// </summary>
	virtual void UpdateStatusBar() = 0;
};
