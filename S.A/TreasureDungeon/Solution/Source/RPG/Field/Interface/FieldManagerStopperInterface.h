﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FieldManagerStopperInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFieldManagerStopperInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IFieldManagerStopperInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// フィールドイベントの実行フラグを設定
	/// </summary>
	/// <param name="Flag">実行フラグ</param>
	virtual void SetFieldEventFlag(bool Flag) = 0;
};
