﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../../Character/StatusData/EnemyID.h"
#include "../../Item/ItemID.h"
#include "FieldEventInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFieldEventInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RPG_API IFieldEventInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// メッセージボックスを開く
	/// </summary>
	virtual void OpenMessageBox() = 0;

	/// <summary>
	/// メッセージボックスを閉じる
	/// </summary>
	virtual void CloseMessageBox() = 0;

	/// <summary>
	/// メッセージボックスにメッセージを表示させる
	/// </summary>
	/// <param name="Message">行区切りのメッセージ</param>
	virtual void ShowMessageBox(TArray<FString>Message) = 0;

	/// <summary>
	/// メッセージボックスにメッセージを表示しているか
	/// </summary>
	/// <return> 表示中 = true </return>
	virtual bool IsShowMessageBox() = 0;

	/// <summary>
	/// メッセージボックスがウィジットのアニメーション中か
	/// </summary>
	/// <return> アニメーション中 = true </return>
	virtual bool IsWidgetAnimationMessageBox() = 0;

	/// <summary>
	/// メッセージボックスが文字のアニメーション中か
	/// </summary>
	/// <returns>文字のアニメーション中 = true</returns>
	virtual bool IsTextAnimationMessageBox() = 0;

	/// <summary>
	/// アイテムを手に入れる
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	virtual void InsertItem(EItemID ItemID, int Num) = 0;

	/// <summary>
	/// アイテムを失う
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	virtual void LostItem(EItemID ItemID, int Num) = 0;

	/// <summary>
	/// アイテムの個数取得
	/// </summary>
	/// <param name="ItemID">アイテムID</param>
	/// <returns>個数</returns>
	virtual int GetItemNum(EItemID ItemID) = 0;

	/// <summary>
	/// 所持金取得
	/// </summary>
	/// <returns>所持金</returns>
	virtual int GetHaveGold() = 0;

	/// <summary>
	/// エンカウントする
	/// </summary>
	/// <param name="EncountEnemyIDGroup">エンカウントする敵ID</param>
	virtual void EncountStart(TArray<EEnemyID> EncountEnemyIDGroup) = 0;

	/// <summary>
	/// ショップを開始する
	/// </summary>
	/// <param name="ItemIDs">商品リストのID</param>
	virtual void ShopStart(TArray<EItemID> ItemIDs) = 0;

	/// <summary>
	/// ショップが開いているか
	/// </summary>
	/// <returns>ショップが開いている = true</returns>
	virtual bool IsShop() = 0;

	/// <summary>
	/// 全回復する
	/// </summary>
	virtual void FullHeal() = 0;

	/// <summary>
	/// BGM音量更新
	/// </summary>
	virtual void BGMVolumeUpdate() = 0;
};
