﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FieldLevelScriptActor.h"


// コンストラクタ
AFieldLevelScriptActor::AFieldLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFieldLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	FieldPhase = AFieldLevelScriptActor::EFieldPhaseType::Initialize;

	FieldManager = NewObject<UFieldManager>(this);
}

void AFieldLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(FieldManager))
	{
		UE_LOG(LogTemp, Display, TEXT("FieldManager Error"));
		return;
	}
	//フェーズごとの処理
	switch (FieldPhase)
	{
	case EFieldPhaseType::Initialize:
		if (FieldManager->InitializeEvent())
			FieldPhase = EFieldPhaseType::Create;
		break;

	case EFieldPhaseType::Create:
		if (FieldManager->CreateEvent())
			FieldPhase = EFieldPhaseType::EncountSetting;
		break;

	case EFieldPhaseType::EncountSetting:
		FieldManager->SetEncountParameterTable(EncountDataTable);
		FieldPhase = EFieldPhaseType::Field;
		break;

	case EFieldPhaseType::Field:
		if (FieldManager->FieldEvent())
		{
			if(FieldManager->IsBattle())
				FieldPhase = EFieldPhaseType::InBattlle;
			else
				FieldPhase = EFieldPhaseType::Finalize;
		}
		break;

	case EFieldPhaseType::InBattlle:
		FieldManager->InBattleEvent();
		break;

	case EFieldPhaseType::Finalize:
		FieldManager->FinalizeEvent();
		break;
	}
}