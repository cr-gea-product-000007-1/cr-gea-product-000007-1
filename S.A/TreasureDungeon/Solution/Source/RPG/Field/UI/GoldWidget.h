// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "../../System/UI/OpenWidget.h"
#include "../../System/UI/WindowParameter.h"
#include "GoldWidget.generated.h"

/**
 *
 */
UCLASS()
class RPG_API UGoldWidget : public UOpenWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void Initialize(FSelectWidgetParameter SelectWidgetParameter);

	/// <summary>
	/// 所持金設定
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void SetHaveGold(int Gold);
};
