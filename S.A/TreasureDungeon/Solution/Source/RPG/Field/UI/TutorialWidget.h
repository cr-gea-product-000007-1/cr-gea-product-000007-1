﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../System/UI/OpenWidget.h"
#include "../../System/Interface/InputWidgetInterface.h"
#include "TutorialWidget.generated.h"


UENUM(BlueprintType)
enum class ETutorialPhaseType : uint8
{
	None,
	Page1,
	Page2,
	Finish,
};

UCLASS()
class RPG_API UTutorialWidget : public UOpenWidget, public IInputWidgetInterface
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 次のチュートリアルに進む
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void NextTutorial();

	/// <summary>
	/// チュートリアルが終了しているか返す
	/// </summary>
	/// <returns>チュートリアルが終了しているか</returns>
	bool IsTutorialFinish() { return bIsTutorialFinish; };

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override { NextTutorial(); };

public:
	/// <summary>
	/// チュートリアルの状態
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	ETutorialPhaseType TutorialPhaseType;

	/// <summary>
	/// チュートリアルが終了しているか
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	bool bIsTutorialFinish = false;
};
