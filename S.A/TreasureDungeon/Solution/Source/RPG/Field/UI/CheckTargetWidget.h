﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "../Object/CheckObject.h"
#include "CheckTargetWidget.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UCheckTargetWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 調べる対象を設定
	/// </summary>
	void SetChactTarget(ACheckObject* Target) { CheckTarget = Target; };

public:
	/// <summary>
	/// 調べる対象
	/// </summary>
	UPROPERTY(BlueprintReadOnly)
	ACheckObject* CheckTarget;
};
