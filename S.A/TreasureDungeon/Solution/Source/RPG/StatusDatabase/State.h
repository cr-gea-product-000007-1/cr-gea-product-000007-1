﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "State.generated.h"


UENUM(BlueprintType)
enum class EStateID : uint8
{
    None,
    Poison,
};


USTRUCT(BlueprintType)
struct FStateResist
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	EStateID State;

	UPROPERTY(EditAnywhere)
	float Rate = 1.0f;
};