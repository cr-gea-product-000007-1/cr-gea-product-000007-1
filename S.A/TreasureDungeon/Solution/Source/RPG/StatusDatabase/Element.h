﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Element.generated.h"


UENUM(BlueprintType)
enum class EElementID : uint8
{
	None,
	Unattributed,
	Fire,
	Ice,
	Wind,
};



USTRUCT(BlueprintType)
struct FElementResist
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	EElementID Element;

	UPROPERTY(EditAnywhere)
	float Rate = 1.0f;
};