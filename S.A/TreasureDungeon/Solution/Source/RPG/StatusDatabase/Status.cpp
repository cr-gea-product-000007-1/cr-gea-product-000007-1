﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Status.h"


namespace Status
{
	FStatus AddStatus(FStatus Status1, FStatus Status2)
	{
		FStatus Result = Status1;

		Result.Hp += Status2.Hp;
		Result.Mp += Status2.Mp;
		Result.Attack += Status2.Attack;
		Result.Vitality += Status2.Vitality;
		Result.Agility += Status2.Agility;
		Result.Intelligence += Status2.Intelligence;

		return Result;
	};

	FStatus SubtractStatus(FStatus Status1, FStatus Status2)
	{
		FStatus Result = Status1;

		Result.Hp -= Status2.Hp;
		Result.Mp -= Status2.Mp;
		Result.Attack -= Status2.Attack;
		Result.Vitality -= Status2.Vitality;
		Result.Agility -= Status2.Agility;
		Result.Intelligence -= Status2.Intelligence;

		return Result;
	};
}

int UStatusBlueprintFunctionLibrary::GetStatusParameter(FStatus Status, EStatusType StatusType)
{
	switch (StatusType)
	{
	case EStatusType::Hp:
		return Status.Hp;

	case EStatusType::Mp:
		return Status.Mp;

	case EStatusType::Attack:
		return Status.Attack;

	case EStatusType::Vitality:
		return Status.Vitality;

	case EStatusType::Agility:
		return Status.Agility;

	case EStatusType::Intelligence:
		return Status.Intelligence;
	}
	return 0;
}

/// <summary>
/// ステータス項目名を取得
/// </summary>
FString UStatusBlueprintFunctionLibrary::GetStatusParameterName(EStatusType StatusType)
{
	switch (StatusType)
	{
	case EStatusType::Hp:
		return FString(TEXT("HP"));

	case EStatusType::Mp:
		return FString(TEXT("MP"));

	case EStatusType::Attack:
		return FString(TEXT("攻撃力"));

	case EStatusType::Vitality:
		return FString(TEXT("守備力"));

	case EStatusType::Agility:
		return FString(TEXT("素早さ"));

	case EStatusType::Intelligence:
		return FString(TEXT("賢さ"));
	}
	return FString();
}