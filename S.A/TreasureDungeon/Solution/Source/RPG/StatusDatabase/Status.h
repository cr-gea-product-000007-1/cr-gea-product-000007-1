﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Status.generated.h"


USTRUCT(Blueprintable)
struct FStatus
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Hp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Mp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Attack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Vitality;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Agility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Intelligence;
};


UENUM(BlueprintType)
enum class EStatusType : uint8
{
	Hp,
	Mp,
	Attack,
	Vitality,
	Agility,
	Intelligence,
};

namespace Status
{
	/// <summary>
	/// ステータス同士を足す
	/// </summary>
	UFUNCTION(BlueprintPure)
		FStatus AddStatus(FStatus Status1, FStatus Status2);

	/// <summary>
	/// ステータスからステータスを引く
	/// </summary>
	UFUNCTION(BlueprintPure)
		FStatus SubtractStatus(FStatus Status1, FStatus Status2);
}

UCLASS()
class UStatusBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ステータスから指定したパラメータを取得
	/// </summary>
	UFUNCTION(BlueprintPure, Category = "Status")
	static int GetStatusParameter(FStatus Status, EStatusType StatusType);

	/// <summary>
	/// ステータス項目名を取得
	/// </summary>
	UFUNCTION(BlueprintPure, Category = "Status")
	static FString GetStatusParameterName(EStatusType StatusType);
};