﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EquipmentParameter.h"
#include "../StatusDatabase/Element.h"
#include "../StatusDatabase/State.h"
#include "ProtectorParameter.generated.h"

USTRUCT(BlueprintType)
struct FProtectorParameter : public FEquipmentParameter
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// 属性耐性
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FElementResist> ElementResist;

	/// <summary>
	/// 状態異常耐性
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<FStateResist> StateResist;
};