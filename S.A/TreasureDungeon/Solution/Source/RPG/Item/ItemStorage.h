﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HaveItem.h"
#include "Item.h"
#include "Weapon.h"
#include "Protector.h"
#include "ItemStorage.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UItemStorage : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 所持アイテム取得
	/// </summary>
	/// <retunrn>所持アイテム</retunrn>
	TArray<UHaveItem*>& GetInItems() { return InItems; };

	/// <summary>
	/// 所持武器取得
	/// </summary>
	/// <retunrn>所持武器</retunrn>
	TArray<UHaveItem*>& GetInWeapons() { return InWeapons; };

	/// <summary>
	/// 所持防具取得
	/// </summary>
	/// <retunrn>所持防具</retunrn>
	TArray<UHaveItem*>& GetInProtectors() { return InProtectors; };

	/// <summary>
	/// アイテムを基底クラスで入れる
	/// </summary>
	/// <param name="BaseItem">入れるアイテム</param>
	/// <param name="Num">個数</param>
	/// <retunrn>新しいアイテム = true</retunrn>
	bool InsertBaseItem(UBaseItem* BaseItem, int Num);

	/// <summary>
	/// アイテムを入れる
	/// </summary>
	/// <param name="Item">入れるアイテム</param>
	/// <param name="Num">個数</param>
	/// <retunrn>新しいアイテム = true</retunrn>
	bool InsertItem(UItem* Item, int Num);

	/// <summary>
	/// 武器を入れる
	/// </summary>
	/// <param name="Weapon">入れる武器</param>
	/// <param name="Num">個数</param>
	/// <retunrn>新しいアイテム = true</retunrn>
	bool InsertWeapon(UWeapon* Weapon, int Num);

	/// <summary>
	/// 防具を入れる
	/// </summary>
	/// <param name="Protector">入れる防具</param>
	/// <param name="Num">個数</param>
	/// <retunrn>新しいアイテム = true</retunrn>
	bool InsertProtector(UProtector* Protector, int Num);

	/// <summary>
	/// アイテムを基底クラスで失う
	/// </summary>
	/// <param name="BaseItem">失うアイテム</param>
	/// <param name="Num">個数</param>
	/// <retunrn>個数0 = true</retunrn>
	bool LostBaseItem(UBaseItem* BaseItem, int Num);

	/// <summary>
	/// アイテムを失う
	/// </summary>
	/// <param name="Item">失うアイテム</param>
	/// <param name="Num">個数</param>
	/// <retunrn>個数0 = true</retunrn>
	bool LostItem(UItem* Item, int Num);

	/// <summary>
	/// 武器を失う
	/// </summary>
	/// <param name="Weapon">失う武器</param>
	/// <param name="Num">個数</param>
	/// <retunrn>個数0 = true</retunrn>
	bool LostWeapon(UWeapon* Weapon, int Num);

	/// <summary>
	/// 防具を失う
	/// </summary>
	/// <param name="Protector">失う防具</param>
	/// <param name="Num">個数</param>
	/// <retunrn>個数0 = true</retunrn>
	bool LostProtector(UProtector* Protector, int Num);

	/// <summary>
	/// アイテムを基底クラスで個数取得
	/// </summary>
	/// <param name="BaseItem">アイテム</param>
	/// <retunrn>個数</retunrn>
	int GetBaseItemNum(UBaseItem* BaseItem);

	/// <summary>
	/// アイテムの個数取得
	/// </summary>
	/// <param name="Item">アイテム</param>
	/// <retunrn>個数0 = true</retunrn>
	int GetItemNum(UItem* Item);

	/// <summary>
	/// 武器の個数取得
	/// </summary>
	/// <param name="Weapon">武器</param>
	/// <retunrn>個数0 = true</retunrn>
	int GetWeaponNum(UWeapon* Weapon);

	/// <summary>
	/// 防具の個数取得
	/// </summary>
	/// <param name="Protector">防具</param>
	/// <retunrn>個数0 = true</retunrn>
	int GetProtectorNum(UProtector* Protector);

private:
	/// <summary>
	/// 所持アイテム
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<UHaveItem*> InItems;

	/// <summary>
	/// 所持武器
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<UHaveItem*> InWeapons;

	/// <summary>
	/// 所持防具
	/// </summary>
	UPROPERTY(EditAnywhere)
	TArray<UHaveItem*> InProtectors;
	
};
