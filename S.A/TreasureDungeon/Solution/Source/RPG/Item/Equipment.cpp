﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment.h"


/// <summary>
/// ステータスの上昇量取得
/// </summary>
/// <returns>ステータスの上昇量</returns>
FStatus UEquipment::GetStatus()
{
	FStatus Result;

	Result.Hp = EquipmentParameter.Hp;
	Result.Mp = EquipmentParameter.Mp;
	Result.Attack = EquipmentParameter.Attack;
	Result.Vitality = EquipmentParameter.Vitality;
	Result.Agility = EquipmentParameter.Agility;
	Result.Intelligence = EquipmentParameter.Intelligence;

	return Result;
}

/// <summary>
/// メインの上昇ステータスの種類取得
/// </summary>
/// <returns>ステータスの上昇量</returns>
EStatusType UEquipment::GetMainStatusType()
{
	//武器は攻撃力
	if (EquipmentParameter.ItemType == EItemType::Weapon)
		return EStatusType::Attack;
	//防具は守備力
	if (EquipmentParameter.ItemType != EItemType::Accessory)
		return EStatusType::Vitality;

	//装飾品は一番高いステータス
	TMap<EStatusType, int>AccessoryAllStatus;
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Hp, EquipmentParameter.Hp));
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Mp, EquipmentParameter.Mp));
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Attack, EquipmentParameter.Attack));
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Vitality, EquipmentParameter.Vitality));
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Agility, EquipmentParameter.Agility));
	AccessoryAllStatus.Add(TTuple<EStatusType, int>(EStatusType::Intelligence, EquipmentParameter.Intelligence));
	//全て0なら守備力にする
	TTuple<EStatusType, int> Max = TTuple<EStatusType, int>(EStatusType::Vitality, 0);
	for(TTuple<EStatusType, int> AccessoryStatus: AccessoryAllStatus)
	{
		if (AccessoryStatus.Value > Max.Value)
			Max = AccessoryStatus;
	}
	return Max.Key;
}

/// <summary>
/// 装備品かを取得
/// </summary>
/// <returns>装備品=true</returns>
bool UEquipment::IsEquipment()
{
	if (EquipmentParameter.ItemType == EItemType::Item)
		return false;
	if (EquipmentParameter.ItemType == EItemType::NoEquipmwnt)
		return false;

	return true;
};