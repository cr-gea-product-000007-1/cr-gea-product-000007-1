﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Item.h"
#include "Equipment.h"
#include "HaveItem.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UHaveItem : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// アイテムを設定する
	/// </summary>
	/// <param name="BaseItem">アイテム</param>
	void SetItem(UBaseItem* BaseItem) { Item = BaseItem; };

	/// <summary>
	/// アイテムを取得する
	/// </summary>
	/// <returns>アイテム</returns>
	UFUNCTION(BlueprintPure)
	UBaseItem* GetItem() { return Item; };

	/// <summary>
	/// 個数を設定する
	/// </summary>
	/// <param name="ItemNum">個数</param>
	void SetNum(int ItemNum) { Num = ItemNum; };

	/// <summary>
	/// 個数を追加する
	/// </summary>
	/// <param name="ItemNum">個数</param>
	void AddItem(int ItemNum);

	/// <summary>
	/// 個数を追加する
	/// </summary>
	/// <param name="ItemNum">個数</param>
	void LostItem(int ItemNum);

	/// <summary>
	/// 個数を取得する
	/// </summary>
	/// <returns>個数</returns>
	UFUNCTION(BlueprintPure)
	int GetNum() { return Num ; };

	/// <summary>
	/// アイテムが残っているか
	/// </summary>
	/// <param name="ItemNum">個数</param>
	bool IsExsit() { return Num > 0; };

	/// <summary>
	/// アイテムを消費する
	/// </summary>
	void Use() { Num--; };

private:
	/// <summary>
	/// アイテム
	/// </summary>
	UPROPERTY()
	UBaseItem* Item;

	/// <summary>
	/// 個数
	/// </summary>
	int Num;
};


USTRUCT(BlueprintType)
struct FHaveEquipment
{
	GENERATED_BODY()

	/// <summary>
	/// 装備
	/// </summary>
	UPROPERTY()
	UEquipment* Equipment;

	/// <summary>
	/// 個数
	/// </summary>
	int Num;
};