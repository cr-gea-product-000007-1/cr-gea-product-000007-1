﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseItemParameter.h"
#include "../Skill/SkillID.h"
#include "../StatusDatabase/Status.h"
#include "ItemParameter.generated.h"


USTRUCT(BlueprintType)
struct FItemParameter : public FBaseItemParameter
{
	GENERATED_USTRUCT_BODY()

public:
	/// <summary>
	/// 使用時に消耗するか
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemUseType ItemUseType;

	/// <summary>
	/// 使用スキル
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillID UseSkillId;
};