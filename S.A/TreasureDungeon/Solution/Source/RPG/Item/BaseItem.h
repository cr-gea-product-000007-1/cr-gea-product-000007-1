﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ItemParameter.h"
#include "ItemId.h"
#include "BaseItem.generated.h"


UCLASS(BlueprintType)
class RPG_API UBaseItem : public UObject
{
	GENERATED_BODY()
	
public:	
	/// <summary>
	/// 名前取得
	/// </summary>
	/// <returns>名前</returns>
	UFUNCTION(BlueprintPure)
	virtual FString GetItemName() { return FString(); };

	/// <summary>
	/// アイテムID取得
	/// </summary>
	/// <returns>アイテムID</returns>
	UFUNCTION(BlueprintPure)
	virtual EItemID GetItemID() { return EItemID(); };

	/// <summary>
	/// 説明文取得
	/// </summary>
	/// <returns>説明文</returns>
	virtual FString GetDescription() { return FString(); };

	/// <summary>
	/// アイテムの種類取得
	/// </summary>
	/// <returns>アイテムの種類</returns>
	UFUNCTION(BlueprintPure)
	virtual EItemType GetItemType() { return EItemType(); };

	/// <summary>
	/// 値段取得
	/// </summary>
	/// <returns>値段</returns>
	UFUNCTION(BlueprintPure)
	virtual int GetValue() { return 0; };

	/// <summary>
	/// 売値取得
	/// </summary>
	/// <returns>売値</returns>
	UFUNCTION(BlueprintPure)
	int GetSellValue();
};
