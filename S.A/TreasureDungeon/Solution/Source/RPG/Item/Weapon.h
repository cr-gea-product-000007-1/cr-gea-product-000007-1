﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment.h"
#include "WeaponParameter.h"
#include "Weapon.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UWeapon : public UEquipment
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 通常攻撃スキルID取得
	/// </summary>
	/// <returns>通常攻撃スキルID</returns>
	ESkillID GetNormalAttackSkillID() { return NormalAttrackSkillID; };

	/// <summary>
	/// 武器のパラメータを設定
	/// </summary>
	/// <param name="Parameter">武器のパラメータ</param>
	void SetWeaponParameter(FWeaponParameter Parameter);

private:
	/// <summary>
	/// 通常攻撃で発動するスキルのID
	/// </summary>
	ESkillID NormalAttrackSkillID;
};
