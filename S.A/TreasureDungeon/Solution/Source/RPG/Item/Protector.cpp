﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Protector.h"


/// <summary>
/// 防具のパラメータを設定
/// </summary>
/// <param name="Parameter">防具のパラメータ</param>
void UProtector::SetProtectorParameter(FProtectorParameter Parameter)
{
	EquipmentParameter = Parameter;
	ElementResist = Parameter.ElementResist;
	StateResist = Parameter.StateResist;
}
