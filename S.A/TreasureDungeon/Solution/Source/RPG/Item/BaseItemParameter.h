﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "ItemType.h"
#include "ItemId.h"
#include "BaseItemParameter.generated.h"


USTRUCT(BlueprintType)
struct FBaseItemParameter : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// 名前
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Name;

	/// <summary>
	/// アイテムID
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemID ItemID;

	/// <summary>
	/// 説明文
	/// </summary>
	UPROPERTY(EditAnywhere)
	FString Description;

	/// <summary>
	/// アイテムの種類
	/// </summary>
	UPROPERTY(EditAnywhere)
	EItemType ItemType;

	/// <summary>
	/// 値段
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Value;
};