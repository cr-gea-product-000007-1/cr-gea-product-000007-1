﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Skill/SkillID.h"
#include "EquipmentParameter.h"
#include "WeaponParameter.generated.h"

USTRUCT(BlueprintType)
struct FWeaponParameter : public FEquipmentParameter
{
	GENERATED_USTRUCT_BODY()


	/// <summary>
	/// 通常攻撃で発動するスキルのID
	/// </summary>
	UPROPERTY(EditAnywhere)
	ESkillID NormalAttrackSkillID;
};