﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemStorage.h"


/// <summary>
/// アイテムを基底クラスで入れる
/// </summary>
/// <param name="BaseItem">入れるアイテム</param>
/// <param name="Num">個数</param>
/// <retunrn>新しいアイテム = true</retunrn>
bool UItemStorage::InsertBaseItem(UBaseItem* BaseItem, int Num)
{
	UItem* Item = Cast<UItem>(BaseItem);
	if (IsValid(Item))
		return InsertItem(Item, Num);

	UWeapon* Weapon = Cast<UWeapon>(BaseItem);
	if (IsValid(Weapon))
		return InsertWeapon(Weapon, Num);

	UProtector* Protector = Cast<UProtector>(BaseItem);
	if (IsValid(Protector))
		return InsertProtector(Protector, Num);

	return false;
}

/// <summary>
/// アイテムを入れる
/// </summary>
/// <param name="Item">入れるアイテム</param>
/// <param name="Num">個数</param>
/// <retunrn>新しいアイテム = true</retunrn>
bool UItemStorage::InsertItem(UItem* Item, int Num)
{
	TArray<UHaveItem*> FindItems = InItems.FilterByPredicate([Item](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Item;
	});

	if (FindItems.Num() > 0)
	{
		UHaveItem* HaveItem = *FindItems.begin();
		HaveItem->AddItem(Num);
		return false;
	}
	else {
		UHaveItem* HaveItem = NewObject<UHaveItem>();
		HaveItem->SetItem(Item);
		HaveItem->SetNum(Num);
		InItems.Add(HaveItem);
		return true;
	}
}

/// <summary>
/// 武器を入れる
/// </summary>
/// <param name="Weapon">入れる武器</param>
/// <param name="Num">個数</param>
/// <retunrn>新しいアイテム = true</retunrn>
bool UItemStorage::InsertWeapon(UWeapon* Weapon, int Num)
{
	TArray<UHaveItem*> FindWeapons = InWeapons.FilterByPredicate([Weapon](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Weapon;
	});

	if (FindWeapons.Num() > 0)
	{
		UHaveItem* HaveItem = *FindWeapons.begin();
		HaveItem->AddItem(Num);
		return false;
	}
	else {
		UHaveItem* HaveItem = NewObject<UHaveItem>();
		HaveItem->SetItem(Weapon);
		HaveItem->SetNum(Num);
		InWeapons.Add(HaveItem);
		return true;
	}
}

/// <summary>
/// 防具を入れる
/// </summary>
/// <param name="Protector">入れる防具</param>
/// <param name="Num">個数</param>
/// <retunrn>新しいアイテム = true</retunrn>
bool UItemStorage::InsertProtector(UProtector* Protector, int Num)
{
	TArray<UHaveItem*> FindProtectors = InProtectors.FilterByPredicate([Protector](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Protector;
	});

	if (FindProtectors.Num() > 0)
	{
		UHaveItem* HaveItem = *FindProtectors.begin();
		HaveItem->AddItem(Num);
		return false;
	}
	else {
		UHaveItem* HaveItem = NewObject<UHaveItem>();
		HaveItem->SetItem(Protector);
		HaveItem->SetNum(Num);
		InProtectors.Add(HaveItem);
		return true;
	}
}

/// <summary>
/// アイテムを基底クラスで失う
/// </summary>
/// <param name="BaseItem">失うアイテム</param>
/// <param name="Num">個数</param>
/// <retunrn>個数0 = true</retunrn>
bool UItemStorage::LostBaseItem(UBaseItem* BaseItem, int Num)
{
	UItem* Item = Cast<UItem>(BaseItem);
	if (IsValid(Item))
		return LostItem(Item, Num);

	UWeapon* Weapon = Cast<UWeapon>(BaseItem);
	if (IsValid(Weapon))
		return LostWeapon(Weapon, Num);

	UProtector* Protector = Cast<UProtector>(BaseItem);
	if (IsValid(Protector))
		return LostProtector(Protector, Num);

	return false;
}

/// <summary>
/// アイテムを失う
/// </summary>
/// <param name="Item">失うアイテム</param>
/// <param name="Num">個数</param>
/// <retunrn>個数0 = true</retunrn>
bool UItemStorage::LostItem(UItem* Item, int Num)
{
	TArray<UHaveItem*> FindItems = InItems.FilterByPredicate([Item](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Item;
		});

	if (FindItems.Num() > 0)
	{
		UHaveItem* HaveItem = *FindItems.begin();
		HaveItem->LostItem(Num);
		if (!HaveItem->IsExsit())
		{
			InItems.Remove(HaveItem);
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

/// <summary>
/// 武器を失う
/// </summary>
/// <param name="Weapon">失う武器</param>
/// <param name="Num">個数</param>
/// <retunrn>個数0 = true</retunrn>
bool UItemStorage::LostWeapon(UWeapon* Weapon, int Num)
{
	TArray<UHaveItem*> FindWeapons = InWeapons.FilterByPredicate([Weapon](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Weapon;
		});

	if (FindWeapons.Num() > 0)
	{
		UHaveItem* HaveItem = *FindWeapons.begin();
		HaveItem->LostItem(Num);
		if (!HaveItem->IsExsit())
		{
			InWeapons.Remove(HaveItem);
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

/// <summary>
/// 防具を失う
/// </summary>
/// <param name="Protector">失う防具</param>
/// <param name="Num">個数</param>
/// <retunrn>個数0 = true</retunrn>
bool UItemStorage::LostProtector(UProtector* Protector, int Num)
{
	TArray<UHaveItem*> FindProtectors = InProtectors.FilterByPredicate([Protector](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Protector;
	});

	if (FindProtectors.Num() > 0)
	{
		UHaveItem* HaveItem = *FindProtectors.begin();
		HaveItem->LostItem(Num);
		if (!HaveItem->IsExsit())
		{
			InProtectors.Remove(HaveItem);
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

/// <summary>
/// アイテムを基底クラスで個数取得
/// </summary>
/// <param name="BaseItem">アイテム</param>
/// <retunrn>個数</retunrn>
int UItemStorage::GetBaseItemNum(UBaseItem* BaseItem)
{
	UItem* Item = Cast<UItem>(BaseItem);
	if (IsValid(Item))
		return GetItemNum(Item);

	UWeapon* Weapon = Cast<UWeapon>(BaseItem);
	if (IsValid(Weapon))
		return GetWeaponNum(Weapon);

	UProtector* Protector = Cast<UProtector>(BaseItem);
	if (IsValid(Protector))
		return GetProtectorNum(Protector);

	return 0;
}

/// <summary>
/// アイテムの個数取得
/// </summary>
/// <param name="Item">アイテム</param>
/// <retunrn>個数0 = true</retunrn>
int UItemStorage::GetItemNum(UItem* Item)
{
	TArray<UHaveItem*> FindItems = InItems.FilterByPredicate([Item](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Item;
		});

	if (FindItems.Num() > 0)
	{
		UHaveItem* HaveItem = *FindItems.begin();
		return HaveItem->GetNum();
	}
	return 0;
}

/// <summary>
/// 武器の個数取得
/// </summary>
/// <param name="Weapon">武器</param>
/// <retunrn>個数0 = true</retunrn>
int UItemStorage::GetWeaponNum(UWeapon* Weapon)
{
	TArray<UHaveItem*> FindWeapons = InWeapons.FilterByPredicate([Weapon](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Weapon;
		});

	if (FindWeapons.Num() > 0)
	{
		UHaveItem* HaveItem = *FindWeapons.begin();
		return HaveItem->GetNum();
	}
	return 0;
}

/// <summary>
/// 防具の個数取得
/// </summary>
/// <param name="Protector">防具</param>
/// <retunrn>個数0 = true</retunrn>
int UItemStorage::GetProtectorNum(UProtector* Protector)
{
	TArray<UHaveItem*> FindProtectors = InProtectors.FilterByPredicate([Protector](UHaveItem* HaveItem) {
		return HaveItem->GetItem() == Protector;
		});

	if (FindProtectors.Num() > 0)
	{
		UHaveItem* HaveItem = *FindProtectors.begin();
		return HaveItem->GetNum();
	}
	return 0;
}