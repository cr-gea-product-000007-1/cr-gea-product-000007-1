﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ItemType.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
    Item,        //道具
    Weapon,      //武器
    Shield,      //盾
    Helmet,      //兜
    Armor,       //鎧
    Accessory,   //装飾品
    NoEquipmwnt, //装備を外す
};

UENUM(BlueprintType)
enum class EItemUseType : uint8
{
    NotUse,        //使用できない
    Consumable,    //消費する
    NotConsumable, //消費しない
};