﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseItem.h"
#include "Item.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UItem : public UBaseItem
{
	GENERATED_BODY()


public:
	/// <summary>
	/// 名前取得
	/// </summary>
	/// <returns>名前</returns>
	FString GetItemName() override { return ItemParameter.Name; };

	/// <summary>
	/// アイテムID取得
	/// </summary>
	/// <returns>アイテムID</returns>
	EItemID GetItemID() override { return ItemParameter.ItemID; };

	/// <summary>
	/// 説明文取得
	/// </summary>
	/// <returns>説明文</returns>
	FString GetDescription() override { return ItemParameter.Description; };

	/// <summary>
	/// アイテムの種類取得
	/// </summary>
	/// <returns>アイテムの種類</returns>
	EItemType GetItemType() override { return ItemParameter.ItemType; };

	/// <summary>
	/// 値段取得
	/// </summary>
	/// <returns>値段</returns>
	int GetValue() override { return ItemParameter.Value; };

	/// <summary>
	/// 使用時に消耗するか取得
	/// </summary>
	EItemUseType GetUseType() { return ItemParameter.ItemUseType; };

	/// <summary>
	/// 使用スキル取得
	/// </summary>
	ESkillID GetSkillID() { return ItemParameter.UseSkillId; };

	/// <summary>
	/// アイテムのパラメータを設定
	/// </summary>
	/// <param name="Parameter">アイテムのパラメータ</param>
	void SetItemParameter(FItemParameter Parameter) { ItemParameter = Parameter; };

	/// <summary>
	/// アイテムのパラメータ取得
	/// </summary>
	/// <returns>アイテムのパラメータ</returns>
	FItemParameter GetItemParameter() { return ItemParameter; };


private:
	/// <summary>
	/// アイテムのパラメータ
	/// </summary>
	FItemParameter ItemParameter;
	
};
