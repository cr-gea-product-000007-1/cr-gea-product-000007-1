﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseItem.h"
#include "EquipmentParameter.h"
#include "Equipment.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class RPG_API UEquipment : public UBaseItem
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 名前取得
	/// </summary>
	/// <returns>名前</returns>
	FString GetItemName() { return EquipmentParameter.Name; };

	/// <summary>
	/// アイテムID取得
	/// </summary>
	/// <returns>アイテムID</returns>
	EItemID GetItemID() { return EquipmentParameter.ItemID; };

	/// <summary>
	/// 説明文取得
	/// </summary>
	/// <returns>説明文</returns>
	FString GetDescription() { return EquipmentParameter.Description; };

	/// <summary>
	/// アイテムの種類取得
	/// </summary>
	/// <returns>アイテムの種類</returns>
	EItemType GetItemType() { return EquipmentParameter.ItemType; };

	/// <summary>
	/// 装備品かを取得
	/// </summary>
	/// <returns>装備品=true</returns>
	UFUNCTION(BlueprintPure)
	bool IsEquipment();

	/// <summary>
	/// 値段取得
	/// </summary>
	/// <returns>値段</returns>
	int GetValue() { return EquipmentParameter.Value; };

	/// <summary>
	/// HP上昇量取得
	/// </summary>
	/// <returns>HP上昇量</returns>
	int GetHp() { return EquipmentParameter.Hp; };

	/// <summary>
	/// MP上昇量取得
	/// </summary>
	/// <returns>MP上昇量</returns>
	int GetMp() { return EquipmentParameter.Mp; };

	/// <summary>
	/// 攻撃力上昇量取得
	/// </summary>
	/// <returns>攻撃力上昇量</returns>
	int GetAttack() { return EquipmentParameter.Attack; };

	/// <summary>
	/// 守備力上昇量取得
	/// </summary>
	/// <returns>守備力上昇量</returns>
	int GetVitality() { return EquipmentParameter.Vitality; };

	/// <summary>
	/// 素早さ上昇量取得
	/// </summary>
	/// <returns>素早さ上昇量</returns>
	int GetAgility() { return EquipmentParameter.Agility; };

	/// <summary>
	/// 賢さ上昇量取得
	/// </summary>
	/// <returns>賢さ上昇量</returns>
	int GetIntelligence() { return EquipmentParameter.Intelligence; };

	/// <summary>
	/// ステータスの上昇量取得
	/// </summary>
	/// <returns>ステータスの上昇量</returns>
	UFUNCTION(BlueprintPure)
	FStatus GetStatus();

	/// <summary>
	/// メインの上昇ステータスの種類取得
	/// </summary>
	/// <returns>ステータスの上昇量</returns>
	UFUNCTION(BlueprintPure)
	EStatusType GetMainStatusType();

	/// <summary>
	/// 装備のパラメータを設定
	/// </summary>
	/// <param name="Parameter">装備のパラメータ</param>
	void SetEquipmentParameter(FEquipmentParameter Parameter) { EquipmentParameter = Parameter; };

	/// <summary>
	/// 装備のパラメータ取得
	/// </summary>
	/// <returns>装備のパラメータ</returns>
	FEquipmentParameter GetEquipmentParameter() { return EquipmentParameter; };

protected:
	/// <summary>
	/// 装備のパラメータ
	/// </summary>
	FEquipmentParameter EquipmentParameter;
};
