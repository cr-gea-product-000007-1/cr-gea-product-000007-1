﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseItemParameter.h"
#include "EquipmentParameter.generated.h"

USTRUCT(BlueprintType)
struct FEquipmentParameter : public FBaseItemParameter
{
	GENERATED_USTRUCT_BODY()

	/// <summary>
	/// Hp上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Hp;

	/// <summary>
	/// Mp上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Mp;

	/// <summary>
	/// 攻撃力上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Attack;

	/// <summary>
	/// 守備力上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Vitality;

	/// <summary>
	/// 素早さ上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Agility;

	/// <summary>
	/// 賢さ上昇量
	/// </summary>
	UPROPERTY(EditAnywhere)
	int Intelligence;
};