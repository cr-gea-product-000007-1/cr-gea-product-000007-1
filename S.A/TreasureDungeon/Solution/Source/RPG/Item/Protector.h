﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment.h"
#include "ProtectorParameter.h"
#include "Protector.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UProtector : public UEquipment
{
	GENERATED_BODY()


public:
	/// <summary>
	/// 属性耐性取得
	/// </summary>
	/// <returns>属性耐性</returns>
	virtual TArray<FElementResist> GetElementResist()  { return ElementResist; };

	/// <summary>
	/// 状態異常耐性取得
	/// </summary>
	/// <returns>状態異常耐性</returns>
	virtual TArray<FStateResist> GetStateResist() { return StateResist; };

	/// <summary>
	/// 防具のパラメータを設定
	/// </summary>
	/// <param name="Parameter">防具のパラメータ</param>
	void SetProtectorParameter(FProtectorParameter Parameter);

private:
	/// <summary>
	/// 属性耐性
	/// </summary>
	TArray<FElementResist> ElementResist;

	/// <summary>
	/// 状態異常耐性
	/// </summary>
	TArray<FStateResist> StateResist;
};
