﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HaveItem.h"

const int ItemMaxNum = 99;


/// <summary>
/// 個数を追加する
/// </summary>
/// <param name="ItemNum">個数</param>
void UHaveItem::AddItem(int ItemNum) 
{
	Num += ItemNum;
	if (Num > ItemMaxNum)
		Num = ItemMaxNum;
};

/// <summary>
/// 個数を追加する
/// </summary>
/// <param name="ItemNum">個数</param>
void UHaveItem::LostItem(int ItemNum)
{
	Num -= ItemNum;
	if (Num < 0)
		Num = 0;
}