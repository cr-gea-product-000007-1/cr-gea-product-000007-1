﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseItem.h"



/// <summary>
/// 売値取得
/// </summary>
/// <returns>売値</returns>
int UBaseItem::GetSellValue()
{
	int Value = GetValue();
	if (Value > 0)
	{
		Value /= 4;
		if (Value == 0)
			Value = 1;
	}
	return Value;
}