﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ItemID.generated.h"


UENUM(BlueprintType)
enum class EItemID : uint8
{
    None,
    Potion1,
    Potion2,
    Potion3,
    Sword1,
    Sword2,
    Sword3,
    Sword4,
    Sword5,
    Shield1,
    Shield2,
    Shield3,
    Shield4,
    Shield5,
    Helmet1,
    Helmet2,
    Helmet3,
    Helmet4,
    Helmet5,
    Armor1,
    Armor2,
    Armor3,
    Armor4,
    Armor5,
    Accessory1,
    Accessory2,
    Accessory3,
    Accessory4,
    Accessory5,
    NoEquipment,
};
