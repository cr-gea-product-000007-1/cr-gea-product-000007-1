﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"


/// <summary>
/// 武器のパラメータを設定
/// </summary>
/// <param name="Parameter">武器のパラメータ</param>
void UWeapon::SetWeaponParameter(FWeaponParameter Parameter)
{
	EquipmentParameter = Parameter;
	NormalAttrackSkillID = Parameter.NormalAttrackSkillID;
}