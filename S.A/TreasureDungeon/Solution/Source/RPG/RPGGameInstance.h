﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Character/StatusData/PlayerParameter.h"
#include "Character/StatusData/EnemyParameter.h"
#include "Debug/DebugParameter.h"
#include "Kismet/GameplayStatics.h"
#include "RPGGameInstance.generated.h"

struct FPlayerParameter;
struct FEnemyParameter;
class USkill;
class UItem;
class UWeapon;
class UProtector;
class UEquipment;

class UBattlePlayerStatus;
class UItemStorage;


//enum EEnemyID;

/**
 * 
 */
UCLASS()
class RPG_API URPGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	//最初に読み込むデータ

	/// <summary>
	/// プレイヤーの本データ
	/// </summary>
	UPROPERTY()
	TArray<FPlayerParameter> RootPlayerData;

	/// <summary>
	/// 敵の本データ
	/// </summary>
	UPROPERTY()
	TArray<FEnemyParameter> RootEnemyData;

	/// <summary>
	/// スキルの本データ
	/// </summary>
	UPROPERTY()
	TArray<USkill*> RootSkillData;

	/// <summary>
	/// アイテムの本データ
	/// </summary>
	UPROPERTY()
	TArray<UItem*> RootItemData;

	/// <summary>
	/// 武器の本データ
	/// </summary>
	UPROPERTY()
	TArray<UWeapon*> RootWeaponData;

	/// <summary>
	/// 防具の本データ
	/// </summary>
	UPROPERTY()
	TArray<UProtector*> RootProtectorData;

	/// <summary>
	/// 装備しない場合に使われる代替えデータ
	/// </summary>
	UPROPERTY()
	UEquipment* NoEquipment;

	/// <summary>
	/// 決定サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* DecisionSound;

	/// <summary>
	/// キャンセルサウンド
	/// </summary>
	UPROPERTY()
	USoundBase* CancelSound;

	/// <summary>
	/// カーソル移動サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* CursorMoveSound;

	/// <summary>
	/// 勝利サウンド
	/// </summary>
	UPROPERTY()
	USoundBase* VictorySound;

	/// <summary>
	/// ファンファーレサウンド
	/// </summary>
	UPROPERTY()
	USoundBase* FanfareSound;

	/// <summary>
	/// 通常戦闘BGM
	/// </summary>
	UPROPERTY()
	USoundBase* NormalBattleBGM;

	/// <summary>
	/// BGM音量
	/// </summary>
	float BGM_Volue;

	/// <summary>
	/// SE音量
	/// </summary>
	float SE_Volue;


	//ゲーム中のデータ

	/// <summary>
	/// ゲーム中のプレイヤーの状態
	/// </summary>
	UPROPERTY()
	TArray<UBattlePlayerStatus*> PlayerStatuses;

	/// <summary>
	/// 所持しているアイテム
	/// </summary>
	UPROPERTY()
	UItemStorage* ItemStorage;

	/// <summary>
	/// 所持金
	/// </summary>
	int Gold;

	/// <summary>
	/// 戦闘敗北フラグ
	/// </summary>
	bool bBattleLose;

	/// <summary>
	/// ゲームクリアフラグ
	/// </summary>
	bool bGameClear;

	/// <summary>
	/// チュートリアルフラグ
	/// </summary>
	bool bTutorialFinish;


	//戦闘に渡すデータ

	/// <summary>
	/// 現在戦闘中か
	/// </summary>
	bool bInBattle;

	/// <summary>
	/// エンカウントした敵グループの敵ID
	/// </summary>
	TArray<EEnemyID> EncountEnemyIDGroup;

	/// <summary>
	/// エンカウントしたプレイヤー位置
	/// </summary>
	FTransform BattleTransform;

	/// <summary>
	/// エンカウントしたカメラ向き
	/// </summary>
	FRotator CameraControlRotate;

	/// <summary>
	/// 始めてゲームに入ったか
	/// </summary>
	bool bIsFirst = false;

	//デバッグ用データ

	/// <summary>
	/// 開始位置
	/// </summary>
	EPlayerStartPoint PlayerStartPoint = EPlayerStartPoint::Start;

	/// <summary>
	/// エンカウントするか
	/// </summary>
	bool bIsEncount = true;
};
