#include "InGameStartUI.h"
#include "System/Pass.h"


/// <summary>
/// コンストラクタ
/// </summary>
InGameStartUI::InGameStartUI() 
  : start_ui_(nullptr)
  , now_phase_(UiPhase::kInitialize) {

}

/// <summary>
/// デストラクタ
/// </summary>
InGameStartUI::~InGameStartUI() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void InGameStartUI::Update(float frame_second) {
	if (now_phase_ != UiPhase::kInitialize) return;

	start_ui_ = new SingleGraphic();

	start_ui_->LoadGraphicHandle(pass::kInGameStartUIImagePass);
	start_ui_->SetPosition(TwoDimensional(kUIX, kUIY));
	start_ui_->SetLayer(Layer::kInGameUI);

	now_phase_ = UiPhase::kRender;
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameStartUI::Render(Layer layer) {
	if (layer != Layer::kInGameUI) return;
	if (now_phase_ != UiPhase::kRender) return;

	start_ui_->Render(layer);
}