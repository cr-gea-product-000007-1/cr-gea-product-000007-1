#include "InGamePauseUI.h"
#include "System/Pass.h"


/// <summary>
/// コンストラクタ
/// </summary>
InGamePauseUI::InGamePauseUI()
	: pause_ui_(nullptr)
	, now_phase_(UiPhase::kInitialize) {

}

/// <summary>
/// デストラクタ
/// </summary>
InGamePauseUI::~InGamePauseUI() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void InGamePauseUI::Update(float frame_second) {
	if (now_phase_ != UiPhase::kInitialize) return;

	pause_ui_ = new SingleGraphic();

	pause_ui_->LoadGraphicHandle(pass::kInGamePauseUIImagePass);
	pause_ui_->SetPosition(TwoDimensional(kUIX, kUIY));
	pause_ui_->SetLayer(Layer::kInGameUI);

	now_phase_ = UiPhase::kRender;
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGamePauseUI::Render(Layer layer) {
	if (layer != Layer::kInGameUI) return;
	if (now_phase_ != UiPhase::kRender) return;

	pause_ui_->Render(layer);
}