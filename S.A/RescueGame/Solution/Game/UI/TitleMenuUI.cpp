#include "TitleMenuUI.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
TitleMenuUI::TitleMenuUI()
  : phase_type_(PhaseType::kInitialize)
  , menu_()
  , cursor_() {

}

/// <summary>
/// デストラクタ
/// </summary>
TitleMenuUI::~TitleMenuUI() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void TitleMenuUI::Update(float frame_second) {
  switch (phase_type_) {
  case TitleMenuUI::PhaseType::kInitialize:
    menu_ = new SingleGraphic();
    cursor_ = new SingleGraphic();

    menu_->LoadGraphicHandle(pass::kTitleMenuImagePass);
    menu_->SetPosition(TwoDimensional(kMenuX, kMenuY));
    menu_->SetLayer(Layer::kTitleMenu);

    cursor_->LoadGraphicHandle(pass::kTitleCursorImagePass);
    cursor_->SetPosition(TwoDimensional(kCursorX, kCursorY));
    cursor_->SetLayer(Layer::kTitleMenu);

    phase_type_ = PhaseType::kProcessing;
    break;

  case TitleMenuUI::PhaseType::kProcessing: {
    break;
  }
  case TitleMenuUI::PhaseType::kFinalize:
    if (menu_ != nullptr) {
      delete menu_;
      menu_ = nullptr;
    }
    if (cursor_ != nullptr) {
      delete cursor_;
      cursor_ = nullptr;
    }
    break;

  default:
    break;
  }
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleMenuUI::Render(Layer layer) {
  if (menu_ != nullptr) {
    menu_->Render(layer);
  }

  if (cursor_ != nullptr) {
    cursor_->Render(layer);
  }
}

/// <summary>
/// 決定入力された時のイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleMenuUI::OnPushDecide() {

}

/// <summary>
/// カーソルの指す位置を設定する
/// </summary>
/// <param>カーソルの指す位置</param>
/// <returns>なし</returns>
void TitleMenuUI::SetCursorIndex(int index) {
  cursor_->SetPosition(TwoDimensional(kCursorX, kCursorY + index * kCursorMoveY));
}