#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"
#include "Game/Graphic/MultiGraphic.h"
#include "Game/Graphic/AnimationGraphic.h"

class InGameUpperUI : public Task {

private:
	/// <summary>
	/// UIのX座標
	/// </summary>
	const int kUIX = 0;

	/// <summary>
	/// UIのY座標
	/// </summary>
	const int kUIY = 0;



	/// <summary>
	/// タイマーの桁数
	/// </summary>
	const int kTimeDigit = 3;

	/// <summary>
	/// 救助者の桁数
	/// </summary>
	const int kRescuerDigit = 2;



	/// <summary>
	/// タイマーのX位置
	/// </summary>
	const int kTimePosX = 44;

	/// <summary>
	/// タイマーのY位置
	/// </summary>
	const int kTimePosY = 46;

	/// <summary>
	/// 救助中の救助者数のX位置
	/// </summary>
	const int kRescueRescuerPosX = 456;

	/// <summary>
	/// 救助中の救助者数のY位置
	/// </summary>
	const int kRescueRescuerPosY = 16;

	/// <summary>
	/// 助けた救助者数のX位置
	/// </summary>
	const int kCompleteRescuerPosX = 820;

	/// <summary>
	/// 助けた救助者数のY位置
	/// </summary>
	const int kCompleteRescuerPosY = 0;

	/// <summary>
	/// 全ての救助者数のX位置
	/// </summary>
	const int kAllRescuerPosX = 894;

	/// <summary>
	/// 全ての救助者数のY位置
	/// </summary>
	const int kAllRescuerPosY = 76;

	/// <summary>
	/// 救助者アイコンのX位置
	/// </summary>
	const int kRescuerIconPosX = 368;

	/// <summary>
	/// 救助者アイコンのY位置
	/// </summary>
	const int kRescuerIconPosY = 12;

	/// <summary>
	/// 救助者アイコンのコマ間隔
	/// </summary>
	const float kRescuerIconAnimationSpeed = 0.3f;



	/// <summary>
	/// 数字の総枚数
	/// </summary>
	const int kNumberImageAllNum = 10;

	/// <summary>
	/// 数字のX枚数
	/// </summary>
	const int kNumberImageXNum = 5;

	/// <summary>
	/// 数字のY枚数
	/// </summary>
	const int kNumberImageYNum = 2;

	/// <summary>
	/// 大きい数字のXサイズ
	/// </summary>
	const int kBigNumberSizeX = 56;

	/// <summary>
	/// 大きい数字のYサイズ
	/// </summary>
	const int kBigNumberSizeY = 84;

	/// <summary>
	/// 中くらい数字のXサイズ
	/// </summary>
	const int kMiddleNumberSizeX = 46;

	/// <summary>
	/// 中くらい数字のYサイズ
	/// </summary>
	const int kMiddleNumberSizeY = 66;

	/// <summary>
	/// 小さい数字のXサイズ
	/// </summary>
	const int kSmallNumberSizeX = 36;

	/// <summary>
	/// 小さい数字のYサイズ
	/// </summary>
	const int kSmallNumberSizeY = 48;


	/// <summary>
	/// 救助者アイコン総枚数
	/// </summary>
	const int kRescuerIconAllNum = 5;

	/// <summary>
	/// 救助者アイコンX枚数
	/// </summary>
	const int kRescuerIconXNum = 5;

	/// <summary>
	/// 救助者アイコンY枚数
	/// </summary>
	const int kRescuerIconYNum = 1;

	/// <summary>
	/// 救助者アイコンXサイズ
	/// </summary>
	const int kRescuerIconXSize = 74;

	/// <summary>
	/// 救助者アイコンYサイズ
	/// </summary>
	const int kRescuerIconYSize = 120;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	InGameUpperUI();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~InGameUpperUI();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// グラフィックを破棄する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void DeleteGraphic();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 残り時間を設定する
	/// </summary>
	/// <param>残り時間</param>
	/// <returns>なし</returns>
	void SetTime(int);

	/// <summary>
	/// 全ての救助者数を設定する
	/// </summary>
	/// <param>全ての救助者数</param>
	/// <returns>なし</returns>
	void SetAllRescuerNum(int);

	/// <summary>
	/// 救助中の救助者数を設定する
	/// </summary>
	/// <param>救助中の救助者数</param>
	/// <returns>なし</returns>
	void SetRescueRescuerNum(int);

	/// <summary>
	/// 助けた救助者数を設定する
	/// </summary>
	/// <param>助けた救助者数</param>
	/// <returns>なし</returns>
	void SetCompleteRescuerNum(int);

private:
	/// <summary>
	/// 上部のUI
	/// </summary>
	SingleGraphic upper_ui_;

	/// <summary>
	/// 残り時間
	/// </summary>
	int timer_;

	/// <summary>
	/// 残り時間のグラフィック
	/// </summary>
	std::vector<MultiGraphic*> timer_graphic_;

	/// <summary>
	/// 全ての救助者数
	/// </summary>
	int all_rescuer_num_;

	/// <summary>
	/// 全ての救助者のグラフィック
	/// </summary>
	std::vector<MultiGraphic*> all_rescuer_graphic_;

	/// <summary>
	/// 救助中の救助者数
	/// </summary>
	int rescue_rescuer_num_;

	/// <summary>
	/// 救助中の救助者のグラフィック
	/// </summary>
	std::vector<MultiGraphic*> rescue_rescuer_graphic_;

	/// <summary>
	/// 助けた救助者数
	/// </summary>
	int complete_rescuer_num_;

	/// <summary>
	/// 助けた救助者
	/// </summary>
	std::vector<MultiGraphic*> complete_rescuer_graphic_;

	/// <summary>
	/// 救助者のアイコン
	/// </summary>
	AnimationGraphic* rescuer_icon_;
};