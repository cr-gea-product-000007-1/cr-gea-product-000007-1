#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"

class InGameBottomUI : public Task {

private:
	/// <summary>
	/// UIのX座標
	/// </summary>
	const int kUIX = 0;

	/// <summary>
	/// UIのY座標
	/// </summary>
	const int kUIY = 672;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	InGameBottomUI();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~InGameBottomUI();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

private:
	/// <summary>
	/// 下部のUI
	/// </summary>
	SingleGraphic bottom_ui_;
};