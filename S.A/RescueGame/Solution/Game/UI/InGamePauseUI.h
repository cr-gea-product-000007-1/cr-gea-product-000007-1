#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"

class InGamePauseUI : public Task {

private:
	enum class UiPhase {
		kInitialize,       //初期化
		kRender,           //表示
		kRenderFinish,     //表示終了
	};

	/// <summary>
	/// UIのX座標
	/// </summary>
	const int kUIX = 364;

	/// <summary>
	/// UIのY座標
	/// </summary>
	const int kUIY = 298;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	InGamePauseUI();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~InGamePauseUI();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 表示を終了する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finish() { now_phase_ = UiPhase::kRenderFinish; }

private:
	/// <summary>
	/// 開始時のUI
	/// </summary>
	SingleGraphic* pause_ui_;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	UiPhase now_phase_;
};