#include "TitleBackground.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
TitleBackground::TitleBackground()
  : phase_type_(PhaseType::kInitialize)
  , background_()
  , helicopter_()
  , time_(0) {

}

/// <summary>
/// デストラクタ
/// </summary>
TitleBackground::~TitleBackground() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleBackground::Initialize() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void TitleBackground::Update(float frame_second) {
  switch (phase_type_) {
  case TitleBackground::PhaseType::kInitialize:
    background_ = new SingleGraphic();
    helicopter_ = new SingleGraphic();

    background_->LoadGraphicHandle(pass::kTitleBackgroundImagePass);
    background_->SetPosition(TwoDimensional(kBackgroundX, kBackgroundY));
    background_->SetLayer(Layer::kBackground);

    helicopter_->LoadGraphicHandle(pass::kTitleHelicopterImagePass);
    helicopter_->SetPosition(TwoDimensional(kHelicopterX, kHelicopterY));
    helicopter_->SetLayer(Layer::kBackground);

    phase_type_ = PhaseType::kProcessing;
    break;

  case TitleBackground::PhaseType::kProcessing: {
    if (!IsProcessing()) return;

    time_ += frame_second;
    float x = sin(time_ * kHelicopterSpeedX) * kHelicopterMoveX + kHelicopterX;
    float y = cos(time_ * kHelicopterSpeedY) * kHelicopterMoveY + kHelicopterY;
    helicopter_->SetPosition(TwoDimensional(x, y));
    break;
  }
  case TitleBackground::PhaseType::kFinalize:
    if (background_ != nullptr) {
      delete background_;
      background_ = nullptr;
    }
    if (helicopter_ != nullptr) {
      delete helicopter_;
      helicopter_ = nullptr;
    }
    break;

  default:
    break;
  }
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleBackground::Render(Layer layer) {
  if (background_ != nullptr) {
    background_->Render(layer);
  }

  if (helicopter_ != nullptr) {
    helicopter_->Render(layer);
  }
}
