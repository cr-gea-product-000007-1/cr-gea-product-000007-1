#include "InGameBottomUI.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
InGameBottomUI::InGameBottomUI() 
  : bottom_ui_() {

}

/// <summary>
/// デストラクタ
/// </summary>
InGameBottomUI::~InGameBottomUI() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameBottomUI::Initialize() {
  bottom_ui_.LoadGraphicHandle(pass::kInGameBottomUIImagePass);
  bottom_ui_.SetPosition(TwoDimensional(kUIX, kUIY));
  bottom_ui_.SetLayer(Layer::kInGameUI);
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameBottomUI::Render(Layer layer) {
  bottom_ui_.Render(layer);
}
