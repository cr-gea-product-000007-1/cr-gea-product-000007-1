#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"
#include "Game/Graphic/MultiGraphic.h"

class TitleCredit : public Task {

private:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kInitialize,      //初期化
		kProcessing,      //処理
		kFinalize,        //終了
	};

	/// <summary>
	/// イニシャル
	/// </summary>
	const char* kInitialText = "s.a";

	/// <summary>
	/// イニシャルの文字数
	/// </summary>
	const int kInitialTextNum = 3;

	/// <summary>
	/// アイコンの回転スピード
	/// </summary>
	const float kIconRotateSpeed = 0.1f;


	/// <summary>
	/// ウインドウのX座標
	/// </summary>
	const int kWindowX = 162;

	/// <summary>
	/// ウインドウのY座標
	/// </summary>
	const int kWindowY = 74;

	/// <summary>
	/// 文字のX座標
	/// </summary>
	const int kTextX = 188;

	/// <summary>
	/// 文字のY座標
	/// </summary>
	const int kTextY = 112;

	/// <summary>
	/// イニシャルのX座標
	/// </summary>
	const int kInitialX = 558;

	/// <summary>
	/// イニシャルのY座標
	/// </summary>
	const int kInitialY = 186;

	/// <summary>
	/// アイコンのX座標
	/// </summary>
	const int kIconX = 730;

	/// <summary>
	/// アイコンのY座標
	/// </summary>
	const int kIconY = 548;

	/// <summary>
	/// アルファベット文字グラフィックの総枚数
	/// </summary>
	const int kAlphabetAllNum = 27;

	/// <summary>
	/// アルファベット文字グラフィックのX枚数
	/// </summary>
	const int kAlphabetXNum = 9;

	/// <summary>
	/// アルファベット文字グラフィックのY枚数
	/// </summary>
	const int kAlphabetYNum = 3;

	/// <summary>
	/// アルファベット文字グラフィックのXサイズ
	/// </summary>
	const int kAlphabetXSize = 48;

	/// <summary>
	/// アルファベット文字グラフィックのYサイズ
	/// </summary>
	const int kAlphabetYSize = 48;

	/// <summary>
	/// アルファベット文字グラフィックのピリオドのインデックス
	/// </summary>
	const int kAlphabetPeriodIndex = 26;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	TitleCredit();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~TitleCredit();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// ウインドウ
	/// </summary>
	SingleGraphic* window_;

	/// <summary>
	/// 文字
	/// </summary>
	SingleGraphic* text_;

	/// <summary>
	/// イニシャル
	/// </summary>
	std::vector<MultiGraphic*> initial_text_;

	/// <summary>
	/// エンターのアイコン
	/// </summary>
	SingleGraphic* icon_;

	/// <summary>
	/// 経過時間
	/// </summary>
	float time_;
};