#include "System/Task.h"
#include "System/ResultStruct.h"
#include "Game/Graphic/SingleGraphic.h"
#include "Game/Graphic/MultiGraphic.h"

class ResultEvaluate : public Task {

private:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kLoad,            //読み込み
		kInitialize,      //初期化
		kWrite,           //書き込み
		kProcessing,      //処理
		kFinalize,        //終了
	};

	/// <summary>
	/// 時間の桁数
	/// </summary>
	const int kTimeDigit = 3;

	/// <summary>
	/// 救助者数の桁数
	/// </summary>
	const int kRescuerDigit = 2;


	/// <summary>
	/// ミッション結果のX座標
	/// </summary>
	const int kMissionResultX = 168;

	/// <summary>
	/// ミッション結果のY座標
	/// </summary>
	const int kMissionResultY = 172;

	/// <summary>
	/// ランクのX座標
	/// </summary>
	const int kRankX = 554;

	/// <summary>
	/// ランクのY座標
	/// </summary>
	const int kRankY = 354;

	/// <summary>
	/// 時間のX座標
	/// </summary>
	const int kTimeX = 670;

	/// <summary>
	/// 時間のY座標
	/// </summary>
	const int kTimeY = 426;

	/// <summary>
	/// 救助人数のX座標
	/// </summary>
	const int kRescuerNumX = 696;

	/// <summary>
	/// 救助人数のY座標
	/// </summary>
	const int kRescuerNumY = 498;

	/// <summary>
	/// 未救助人数のX座標
	/// </summary>
	const int kNotRescuerNumX = 696;

	/// <summary>
	/// 未救助人数のY座標
	/// </summary>
	const int kNotRescuerNumY = 566;



	/// <summary>
	/// ランクのnew recordのX座標
	/// </summary>
	const int kRankNewRecordX = 540;

	/// <summary>
	/// ランクのnew recordのY座標
	/// </summary>
	const int kRankNewRecordY = 332;

	/// <summary>
	/// 時間のnew recordのX座標
	/// </summary>
	const int kTimeNewRecordX = 584;

	/// <summary>
	/// 時間のnew recordのY座標
	/// </summary>
	const int kTimeNewRecordY = 406;

	/// <summary>
	/// 救助人数のnew recordのX座標
	/// </summary>
	const int kRescuerNumNewRecordX = 584;

	/// <summary>
	/// 救助人数のnew recordのY座標
	/// </summary>
	const int kRescuerNumNewRecordY = 476;

	/// <summary>
	/// 未救助人数のnew recordのX座標
	/// </summary>
	const int kNotRescuerNumNewRecordX = 584;

	/// <summary>
	/// 未救助人数のnew recordのY座標
	/// </summary>
	const int kNotRescuerNumNewRecordY = 546;





	/// <summary>
	/// 数字の総枚数
	/// </summary>
	const int kNumberImageAllNum = 10;

	/// <summary>
	/// 数字のX枚数
	/// </summary>
	const int kNumberImageXNum = 5;

	/// <summary>
	/// 数字のY枚数
	/// </summary>
	const int kNumberImageYNum = 2;

	/// <summary>
	/// 数字のXサイズ
	/// </summary>
	const int kNumberXSize = 26;

	/// <summary>
	/// 数字のYサイズ
	/// </summary>
	const int kNumberYSize = 26;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	ResultEvaluate();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ResultEvaluate();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

private:
	/// <summary>
	/// ミッション結果のUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateMissionResultUI();

	/// <summary>
	/// 救助ランクのUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateRankUI();

	/// <summary>
	/// 時間のUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateTimeUI();

	/// <summary>
	/// 救助者数のUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateCompleteRescuerNumUI();

	/// <summary>
	/// 未救助者数のUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateNotRescuerNumUI();

	/// <summary>
	/// NewRecordのUI生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateNewRecordUI();

	/// <summary>
	/// ハイスコアの更新
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void UpdateHighScore();

	/// <summary>
	/// グラフィックの破棄
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void DeleteGraphic();

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// ミッション結果
	/// </summary>
	SingleGraphic* mission_result_;

	/// <summary>
	/// ランク
	/// </summary>
	SingleGraphic* rank_;

	/// <summary>
	/// 時間
	/// </summary>
	std::vector<MultiGraphic*> time_;

	/// <summary>
	/// 救助人数
	/// </summary>
	std::vector<MultiGraphic*> complete_rescuer_num_;

	/// <summary>
	/// 未救助人数
	/// </summary>
	std::vector<MultiGraphic*> not_rescuer_num_;


	/// <summary>
	/// ランクのnew record
	/// </summary>
	SingleGraphic* rank_new_record_;

	/// <summary>
	/// 時間のnew record
	/// </summary>
	SingleGraphic* time_new_record_;

	/// <summary>
	/// 救助人数のnew record
	/// </summary>
	SingleGraphic* rescuer_num_new_record_;

	/// <summary>
	/// 未救助人数のnew record
	/// </summary>
	SingleGraphic* not_rescuer_num_new_record_;

	/// <summary>
	/// 今回のスコア
	/// </summary>
	ResultStruct result_;

	/// <summary>
	/// ハイスコア
	/// </summary>
	ResultStruct high_score_;
};