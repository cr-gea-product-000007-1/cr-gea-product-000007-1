#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"

class TitleMenuUI : public Task {

private:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kInitialize,      //初期化
		kProcessing,      //処理
		kFinalize,        //終了
	};

	/// <summary>
	/// メニューのX座標
	/// </summary>
	const int kMenuX = 516;

	/// <summary>
	/// メニューのY座標
	/// </summary>
	const int kMenuY = 600;

	/// <summary>
	/// カーソルのX座標
	/// </summary>
	const int kCursorX = 398;

	/// <summary>
	/// カーソルのY座標
	/// </summary>
	const int kCursorY = 598;

	/// <summary>
	/// カーソルのY座標の移動間隔
	/// </summary>
	const int kCursorMoveY = 56;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	TitleMenuUI();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~TitleMenuUI();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

	/// <summary>
	/// 決定入力された時のイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDecide();

	/// <summary>
	/// カーソルの指す位置を設定する
	/// </summary>
	/// <param>カーソルの指す位置</param>
	/// <returns>なし</returns>
	void SetCursorIndex(int);

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// メニュー
	/// </summary>
	SingleGraphic* menu_;

	/// <summary>
	/// カーソル
	/// </summary>
	SingleGraphic* cursor_;
};