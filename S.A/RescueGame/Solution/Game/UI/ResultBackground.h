#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"

class ResultBackground : public Task {

private:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kInitialize,      //初期化
		kProcessing,      //処理
		kFinalize,        //終了
	};

	/// <summary>
	/// タイトルのX座標
	/// </summary>
	const int kTitleX = 0;

	/// <summary>
	/// タイトルのY座標
	/// </summary>
	const int kTileY = 26;

	/// <summary>
	/// ウインドウのX座標
	/// </summary>
	const int kWindowX = 120;

	/// <summary>
	/// ウインドウのY座標
	/// </summary>
	const int kWindowY = 140;

	/// <summary>
	/// キー案内のX座標
	/// </summary>
	const int kInfoX = 408;

	/// <summary>
	/// キー案内のY座標
	/// </summary>
	const int kInfoY = 680;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	ResultBackground();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ResultBackground();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// タイトル
	/// </summary>
	SingleGraphic* title_;

	/// <summary>
	/// ウインドウ
	/// </summary>
	SingleGraphic* window_;

	/// <summary>
	/// キー案内
	/// </summary>
	SingleGraphic* info_;

	/// <summary>
	/// 経過時間
	/// </summary>
	float time_;
};