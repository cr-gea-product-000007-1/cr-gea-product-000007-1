#include "InGameUpperUI.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
InGameUpperUI::InGameUpperUI()
  : upper_ui_() 
  , timer_()
  , timer_graphic_()
  , all_rescuer_num_()
  , all_rescuer_graphic_()
  , rescue_rescuer_num_()
  , rescue_rescuer_graphic_()
  , complete_rescuer_num_()
  , complete_rescuer_graphic_() {
}

/// <summary>
/// デストラクタ
/// </summary>
InGameUpperUI::~InGameUpperUI() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameUpperUI::Initialize() {
  upper_ui_.LoadGraphicHandle(pass::kInGameUpperUIImagePass);
  upper_ui_.SetPosition(TwoDimensional(kUIX, kUIY));
  upper_ui_.SetLayer(Layer::kInGameUI);

  for (int i = 0; i < kTimeDigit; i++) {
    MultiGraphic* graphic = new MultiGraphic();

    graphic->LoadDivideGraphicHandle(pass::kBigNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kBigNumberSizeX, kBigNumberSizeY);
    graphic->SetPosition(TwoDimensional(kTimePosX + i * kBigNumberSizeX, kTimePosY));
    graphic->SetLayer(Layer::kInGameUI);

    timer_graphic_.push_back(graphic);
  }

  for (int i = 0; i < kRescuerDigit; i++) {
    MultiGraphic* graphic = new MultiGraphic();

    graphic->LoadDivideGraphicHandle(pass::kBigNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kBigNumberSizeX, kBigNumberSizeY);
    graphic->SetPosition(TwoDimensional(kRescueRescuerPosX + i * kBigNumberSizeX, kRescueRescuerPosY));
    graphic->SetLayer(Layer::kInGameUI);

    rescue_rescuer_graphic_.push_back(graphic);
  }

  for (int i = 0; i < kRescuerDigit; i++) {
    MultiGraphic* graphic = new MultiGraphic();

    graphic->LoadDivideGraphicHandle(pass::kMiddleNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kMiddleNumberSizeX, kMiddleNumberSizeY);
    graphic->SetPosition(TwoDimensional(kCompleteRescuerPosX + i * kMiddleNumberSizeX, kCompleteRescuerPosY));
    graphic->SetLayer(Layer::kInGameUI);

    complete_rescuer_graphic_.push_back(graphic);
  }

  for (int i = 0; i < kRescuerDigit; i++) {
    MultiGraphic* graphic = new MultiGraphic();

    graphic->LoadDivideGraphicHandle(pass::kMiddleNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kMiddleNumberSizeX, kMiddleNumberSizeY);
    graphic->SetPosition(TwoDimensional(kAllRescuerPosX + i * kMiddleNumberSizeX, kAllRescuerPosY));
    graphic->SetLayer(Layer::kInGameUI);

    all_rescuer_graphic_.push_back(graphic);
  }

  rescuer_icon_ = new AnimationGraphic();
  rescuer_icon_->LoadDivideGraphicHandle(pass::kRescuerIconImagePass, kRescuerIconAllNum, kRescuerIconXNum, kRescuerIconYNum, kRescuerIconXSize, kRescuerIconYSize);
  
  rescuer_icon_->SetPosition(TwoDimensional(kRescuerIconPosX, kRescuerIconPosY));
  rescuer_icon_->SetLayer(Layer::kNone);

  rescuer_icon_->SetAnimationRate(kRescuerIconAnimationSpeed);
  rescuer_icon_->Loop();
  rescuer_icon_->Play();
}

/// <summary>
/// グラフィックを破棄する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameUpperUI::DeleteGraphic() {

  for (MultiGraphic* graphic : timer_graphic_) {
    if (graphic == nullptr) continue;
    delete graphic;
    graphic = nullptr;
  }

  for (MultiGraphic* graphic : all_rescuer_graphic_) {
    if (graphic == nullptr) continue;
    delete graphic;
    graphic = nullptr;
  }

  for (MultiGraphic* graphic : rescue_rescuer_graphic_) {
    if (graphic == nullptr) continue;
    delete graphic;
    graphic = nullptr;
  }

  for (MultiGraphic* graphic : complete_rescuer_graphic_) {
    if (graphic == nullptr) continue;
    delete graphic;
    graphic = nullptr;
  }

  if (rescuer_icon_ != nullptr) {
    delete rescuer_icon_;
    rescuer_icon_ = nullptr;
  }
}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void InGameUpperUI::Update(float frame_second) {
  rescuer_icon_->UpdateAnimation(frame_second);
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameUpperUI::Render(Layer layer) {
  if (layer != Layer::kInGameUI) return;

  upper_ui_.Render(layer);

  for (MultiGraphic* graphic : timer_graphic_) {
    graphic->Render(layer);
  }

  for (MultiGraphic* graphic : all_rescuer_graphic_) {
    graphic->Render(layer);
  }

  for (MultiGraphic* graphic : rescue_rescuer_graphic_) {
    graphic->Render(layer);
  }

  for (MultiGraphic* graphic : complete_rescuer_graphic_) {
    graphic->Render(layer);
  }

  rescuer_icon_->Render(layer);
}

/// <summary>
/// 残り時間を設定する
/// </summary>
/// <param>残り時間</param>
/// <returns>なし</returns>
void InGameUpperUI::SetTime(int time) {
  timer_ = time;

  int tmp = time;
  for (int i = kTimeDigit - kOne; i >= kBigin; i--) {
    int num = tmp % kDigit;

    tmp -= num;
    tmp /= kDigit;

    timer_graphic_[i]->SetGraphicIndex(num);
  }
}

/// <summary>
/// 全ての救助者数を設定する
/// </summary>
/// <param>全ての救助者数</param>
/// <returns>なし</returns>
void InGameUpperUI::SetAllRescuerNum(int num) { 
  all_rescuer_num_ = num;

  int tmp = num;
  for (int i = kRescuerDigit - kOne; i >= kBigin; i--) {
    int num = tmp % kDigit;

    tmp -= num;
    tmp /= kDigit;

    all_rescuer_graphic_[i]->SetGraphicIndex(num);
  }
}

/// <summary>
/// 救助中の救助者数を設定する
/// </summary>
/// <param>救助中の救助者数</param>
/// <returns>なし</returns>
void InGameUpperUI::SetRescueRescuerNum(int num) {
  rescue_rescuer_num_ = num;

  int tmp = num;
  for (int i = kRescuerDigit - kOne; i >= kBigin; i--) {
    int num = tmp % kDigit;

    tmp -= num;
    tmp /= kDigit;

    rescue_rescuer_graphic_[i]->SetGraphicIndex(num);
  }

  if (num > kZero) {
    rescuer_icon_->SetLayer(Layer::kInGameUI);
  }else{
    rescuer_icon_->SetLayer(Layer::kNone);
  }
}

/// <summary>
/// 助けた救助者数を設定する
/// </summary>
/// <param>助けた救助者数</param>
/// <returns>なし</returns>
void InGameUpperUI::SetCompleteRescuerNum(int num) {
  complete_rescuer_num_ = num;

  int tmp = num;
  for (int i = kRescuerDigit - kOne; i >= kBigin; i--) {
    int num = tmp % kDigit;

    tmp -= num;
    tmp /= kDigit;

    complete_rescuer_graphic_[i]->SetGraphicIndex(num);
  }
}