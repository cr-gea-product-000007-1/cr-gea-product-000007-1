#include "System/Task.h"
#include "Game/Graphic/SingleGraphic.h"

class TitleBackground : public Task {

private:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kInitialize,      //初期化
		kProcessing,      //処理
		kFinalize,        //終了
	};

	/// <summary>
	/// 処理の中断フェーズの種類
	/// </summary>
	enum class SuspendedPhaseType {
		kProcessing,           //処理
		kProcessingSuspended,  //中断
	};

	/// <summary>
	/// 背景のX座標
	/// </summary>
	const int kBackgroundX = 124;

	/// <summary>
	/// 背景のY座標
	/// </summary>
	const int kBackgroundY = 0;

	/// <summary>
	/// ヘリコプターのX座標
	/// </summary>
	const int kHelicopterX = 282;

	/// <summary>
	/// ヘリコプターのY座標
	/// </summary>
	const int kHelicopterY = 260;

	/// <summary>
	/// ヘリコプターのX移動幅
	/// </summary>
	const float kHelicopterMoveX = 16.0f;

	/// <summary>
	/// ヘリコプターのY移動幅
	/// </summary>
	const float kHelicopterMoveY = 12.0f;

	/// <summary>
	/// ヘリコプターのX移動スピード
	/// </summary>
	const float kHelicopterSpeedX = 1.2f;

	/// <summary>
	/// ヘリコプターのY移動スピード
	/// </summary>
	const float kHelicopterSpeedY = 0.8f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	TitleBackground();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~TitleBackground();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

	/// <summary>
	/// 処理中にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Processing() { suspended_phase_type_ = SuspendedPhaseType::kProcessing; }

	/// <summary>
	/// 処理を中断する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void ProcessingSuspended() { suspended_phase_type_ = SuspendedPhaseType::kProcessingSuspended; }

private:
	/// <summary>
	/// 処理を継続中か
	/// </summary>
	/// <param>なし</param>
	/// <returns>処理を継続中 = true</returns>
	bool IsProcessing() { return suspended_phase_type_ == SuspendedPhaseType::kProcessing; }

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// 処理の中断フェーズの種類
	/// </summary>
	SuspendedPhaseType suspended_phase_type_;

	/// <summary>
	/// 背景
	/// </summary>
	SingleGraphic* background_;

	/// <summary>
	/// ヘリコプター
	/// </summary>
	SingleGraphic* helicopter_;

	/// <summary>
	/// 経過時間
	/// </summary>
	float time_;
};