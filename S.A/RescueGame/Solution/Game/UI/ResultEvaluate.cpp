#include "ResultEvaluate.h"
#include "System/Pass.h"
#include "System/GameInfo.h"
#include "System/RescueRankFactory.h"
#include "System/HighScoreLoader.h"


/// <summary>
/// コンストラクタ
/// </summary>
ResultEvaluate::ResultEvaluate()
  : phase_type_(PhaseType::kLoad)
  , mission_result_(nullptr)
  , rank_(nullptr)
  , time_()
  , complete_rescuer_num_()
  , rank_new_record_(nullptr)
  , time_new_record_(nullptr)
  , rescuer_num_new_record_()
  , not_rescuer_num_new_record_(nullptr)
  , result_()
  , high_score_() {

}

/// <summary>
/// デストラクタ
/// </summary>
ResultEvaluate::~ResultEvaluate() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::Initialize() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void ResultEvaluate::Update(float frame_second) {
  switch (phase_type_) {
  case ResultEvaluate::PhaseType::kLoad: {

    GameInfo* game_info = GameInfo::GetInstance();
    if (game_info == nullptr) break;
    result_ = game_info->GetResult();

    HighScoreLoader high_score_loader;
    high_score_loader.LoadHighScore(high_score_);

    phase_type_ = PhaseType::kInitialize;
    break;
  }
  case ResultEvaluate::PhaseType::kInitialize: {
    CreateMissionResultUI();
    CreateRankUI();
    CreateTimeUI();
    CreateCompleteRescuerNumUI();
    CreateNotRescuerNumUI();
    CreateNewRecordUI();

    phase_type_ = PhaseType::kWrite;
    break;
  }
  case ResultEvaluate::PhaseType::kWrite: {
    UpdateHighScore();

    HighScoreLoader high_score_loader;
    high_score_loader.SaveHighScore(high_score_);

    phase_type_ = PhaseType::kProcessing;
    break;
  }
  case ResultEvaluate::PhaseType::kProcessing:
    break;

  case ResultEvaluate::PhaseType::kFinalize:
    DeleteGraphic();
    break;

  default:
    break;
  }
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::Render(Layer layer) {
  if (layer != Layer::kResult) return;

  if (mission_result_ != nullptr) {
    mission_result_->Render(layer);
  }

  if (rank_ != nullptr) {
    rank_->Render(layer);
  }

  for (MultiGraphic* time_num : time_) {
    if (time_num == nullptr) continue;

    time_num->Render(layer);
  }

  for (MultiGraphic* rescuer_num : complete_rescuer_num_) {
    if (rescuer_num == nullptr) continue;

    rescuer_num->Render(layer);
  }

  for (MultiGraphic* rescuer_num : not_rescuer_num_) {
    if (rescuer_num == nullptr) continue;

    rescuer_num->Render(layer);
  }

  if (rank_new_record_ != nullptr) {
    rank_new_record_->Render(layer);
  }

  if (time_new_record_ != nullptr) {
    time_new_record_->Render(layer);
  }

  if (rescuer_num_new_record_ != nullptr) {
    rescuer_num_new_record_->Render(layer);
  }

  if (not_rescuer_num_new_record_ != nullptr) {
    not_rescuer_num_new_record_->Render(layer);
  }
}

/// <summary>
/// ミッション結果のUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::CreateMissionResultUI(){
  mission_result_ = new SingleGraphic();

  if (result_.time_ > kZero) {
    mission_result_->LoadGraphicHandle(pass::kResultMissionClearImagePass);
  }
  else {
    mission_result_->LoadGraphicHandle(pass::kResultMissionNotClearImagePass);
  }
  mission_result_->SetPosition(TwoDimensional(kMissionResultX, kMissionResultY));
  mission_result_->SetLayer(Layer::kResult);
}

/// <summary>
/// 救助ランクのUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::CreateRankUI(){
  rank_ = new SingleGraphic();

  switch (result_.rank_) {
  case RescueRank::kDiamondRank:
    rank_->LoadGraphicHandle(pass::kResultDiamondRankImagePass);
    break;

  case RescueRank::kPlatinumRank:
    rank_->LoadGraphicHandle(pass::kResultPlatinumRankImagePass);
    break;

  case RescueRank::kGoldRank:
    rank_->LoadGraphicHandle(pass::kResultGoldRankImagePass);
    break;

  case RescueRank::kSilverRank:
    rank_->LoadGraphicHandle(pass::kResultSilverRankImagePass);
    break;

  case RescueRank::kBronzeRank:
    rank_->LoadGraphicHandle(pass::kResultBronzeRankImagePass);
    break;

  case RescueRank::kNoneRank:
    rank_->LoadGraphicHandle(pass::kResultOutRankImagePass);
    break;

  default:
    break;
  }

  rank_->SetPosition(TwoDimensional(kRankX, kRankY));
  rank_->SetLayer(Layer::kResult);
}

/// <summary>
/// 時間のUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::CreateTimeUI(){

  bool begin = true;
  int tmp = result_.time_;

  for (int i = 0; i < kTimeDigit; i++) {
    int digit = (int)pow(kDigit, kTimeDigit - kOne - i);
    int num = tmp / digit;
    tmp -= num * digit;

    //最後の桁以外で先頭の数字がなく今の桁が0なら表示しない
    if (begin && num == kZero) {
      if (i < kTimeDigit - kOne) {
        continue;
      }
    }
    begin = false;

    MultiGraphic* num_graphic = new MultiGraphic();

    num_graphic->LoadDivideGraphicHandle(pass::kResultNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kNumberXSize, kNumberYSize);
    num_graphic->SetPosition(TwoDimensional(kTimeX + i * kNumberXSize, kTimeY));
    num_graphic->SetGraphicIndex(num);
    num_graphic->SetLayer(Layer::kResult);

    time_.push_back(num_graphic);
  }
}

/// <summary>
/// 救助者数のUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate:: CreateCompleteRescuerNumUI() {

  bool begin = true;
  int tmp = result_.complete_rescuer_num_;

  for (int i = 0; i < kRescuerDigit; i++) {
    int digit = (int)pow(kDigit, kRescuerDigit - kOne - i);
    int num = tmp / digit;
    tmp -= num * digit;

    //最後の桁以外で先頭の数字がなく今の桁が0なら表示しない
    if (begin && num == kZero) {
      if (i < kRescuerDigit - kOne) {
        continue;
      }
    }
    begin = false;

    MultiGraphic* num_graphic = new MultiGraphic();

    num_graphic->LoadDivideGraphicHandle(pass::kResultNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kNumberXSize, kNumberYSize);
    num_graphic->SetPosition(TwoDimensional(kRescuerNumX + i * kNumberXSize, kRescuerNumY));
    num_graphic->SetGraphicIndex(num);
    num_graphic->SetLayer(Layer::kResult);

    complete_rescuer_num_.push_back(num_graphic);
  }
}

/// <summary>
/// 未救助者数のUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::CreateNotRescuerNumUI(){

  bool begin = true;
  int tmp = result_.not_rescuer_num_;
  
  for (int i = 0; i < kRescuerDigit; i++) {
    int digit = (int)pow(kDigit, kRescuerDigit - kOne - i);
    int num = tmp / digit;
    tmp -= num * digit;

    //最後の桁以外で先頭の数字がなく今の桁が0なら表示しない
    if (begin && num == kZero) {
      if (i < kRescuerDigit - kOne) {
        continue;
      }
    }
    begin = false;

    MultiGraphic* num_graphic = new MultiGraphic();

    num_graphic->LoadDivideGraphicHandle(pass::kResultNumberImagePass, kNumberImageAllNum, kNumberImageXNum, kNumberImageYNum, kNumberXSize, kNumberYSize);
    num_graphic->SetPosition(TwoDimensional(kNotRescuerNumX + i * kNumberXSize, kNotRescuerNumY));
    num_graphic->SetGraphicIndex(num);
    num_graphic->SetLayer(Layer::kResult);

    complete_rescuer_num_.push_back(num_graphic);
  }
}

/// <summary>
/// NewRecordのUI生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::CreateNewRecordUI(){
  if (result_.rank_ < high_score_.rank_) {
    rank_new_record_ = new SingleGraphic();
    rank_new_record_->LoadGraphicHandle(pass::kResultNewRecordImagePass);
    rank_new_record_->SetPosition(TwoDimensional(kRankNewRecordX, kRankNewRecordY));
    rank_new_record_->SetLayer(Layer::kResult);
  }

  if (result_.time_ < high_score_.time_) {
    time_new_record_ = new SingleGraphic();
    time_new_record_->LoadGraphicHandle(pass::kResultNewRecordImagePass);
    time_new_record_->SetPosition(TwoDimensional(kTimeNewRecordX, kTimeNewRecordY));
    time_new_record_->SetLayer(Layer::kResult);
  }

  if (result_.complete_rescuer_num_ > high_score_.complete_rescuer_num_) {
    rescuer_num_new_record_ = new SingleGraphic();
    rescuer_num_new_record_->LoadGraphicHandle(pass::kResultNewRecordImagePass);
    rescuer_num_new_record_->SetPosition(TwoDimensional(kRescuerNumNewRecordX, kRescuerNumNewRecordY));
    rescuer_num_new_record_->SetLayer(Layer::kResult);
  }

  if (result_.not_rescuer_num_ < high_score_.not_rescuer_num_) {
    not_rescuer_num_new_record_ = new SingleGraphic();
    not_rescuer_num_new_record_->LoadGraphicHandle(pass::kResultNewRecordImagePass);
    not_rescuer_num_new_record_->SetPosition(TwoDimensional(kNotRescuerNumNewRecordX, kNotRescuerNumNewRecordY));
    not_rescuer_num_new_record_->SetLayer(Layer::kResult);
  }
}

/// <summary>
/// ハイスコアの更新
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::UpdateHighScore() {
  if (result_.rank_ < high_score_.rank_) {
    high_score_.rank_ = result_.rank_;
  }

  if (result_.time_ < high_score_.time_) {
    high_score_.time_ = result_.time_;
  }

  if (result_.complete_rescuer_num_ > high_score_.complete_rescuer_num_) {
    high_score_.complete_rescuer_num_ = result_.complete_rescuer_num_;
  }

  if (result_.not_rescuer_num_ < high_score_.not_rescuer_num_) {
    high_score_.not_rescuer_num_ = result_.not_rescuer_num_;
  }
}

/// <summary>
/// グラフィックの破棄
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultEvaluate::DeleteGraphic() {
  if (mission_result_ != nullptr) {
    delete mission_result_;
    mission_result_ = nullptr;
  }

  if (rank_ != nullptr) {
    delete rank_;
    rank_ = nullptr;
  }

  for (MultiGraphic* time_num : time_) {
    if (time_num == nullptr) continue;

    delete time_num;
    time_num = nullptr;
  }

  for (MultiGraphic* rescuer_num : complete_rescuer_num_) {
    if (rescuer_num == nullptr) continue;

    delete rescuer_num;
    rescuer_num = nullptr;
  }

  for (MultiGraphic* rescuer_num : not_rescuer_num_) {
    if (rescuer_num == nullptr) continue;

    delete rescuer_num;
    rescuer_num = nullptr;
  }

  if (rank_new_record_ != nullptr) {
    delete rank_new_record_;
    rank_new_record_ = nullptr;
  }

  if (time_new_record_ != nullptr) {
    delete time_new_record_;
    time_new_record_ = nullptr;
  }

  if (rescuer_num_new_record_ != nullptr) {
    delete rescuer_num_new_record_;
    rescuer_num_new_record_ = nullptr;
  }

  if (not_rescuer_num_new_record_ != nullptr) {
    delete not_rescuer_num_new_record_;
    not_rescuer_num_new_record_ = nullptr;
  }
}