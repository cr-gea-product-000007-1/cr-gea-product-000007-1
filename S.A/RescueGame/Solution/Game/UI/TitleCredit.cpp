#include "TitleCredit.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
TitleCredit::TitleCredit()
  : phase_type_(PhaseType::kInitialize)
  , window_(nullptr)
  , text_(nullptr)
  , initial_text_()
  , icon_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
TitleCredit::~TitleCredit() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleCredit::Initialize() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void TitleCredit::Update(float frame_second) {
  switch (phase_type_) {
  case TitleCredit::PhaseType::kInitialize:
    window_ = new SingleGraphic();
    text_ = new SingleGraphic();
    icon_ = new SingleGraphic();

    window_->LoadGraphicHandle(pass::kTitleCreditWindowImagePass);
    window_->SetPosition(TwoDimensional(kWindowX, kWindowY));
    window_->SetLayer(Layer::kTitleCredit);

    text_->LoadGraphicHandle(pass::kTitleCreditTextImagePass);
    text_->SetPosition(TwoDimensional(kTextX, kTextY));
    text_->SetLayer(Layer::kTitleCredit);

    icon_->LoadGraphicHandle(pass::kTitleCreditIconImagePass);
    icon_->SetPosition(TwoDimensional(kIconX, kIconY));
    icon_->SetLayer(Layer::kTitleCredit);

    for (int i = 0; i < kInitialTextNum; i++) {
      MultiGraphic* initial = new MultiGraphic();
      initial->LoadDivideGraphicHandle(pass::kAlphabetImagePass, kAlphabetAllNum, kAlphabetXNum, kAlphabetYNum, kAlphabetXSize, kAlphabetYSize);
      initial->SetPosition(TwoDimensional(kInitialX + i * kAlphabetXSize, kInitialY));
      initial->SetLayer(Layer::kTitleCredit);

      int index = 0;
      char initial_char = kInitialText[i];
      if (initial_char >= kChar_a && initial_char <= kChar_z) {
        index = initial_char - kChar_a;
      }
      else {
        index = kAlphabetPeriodIndex;
      }

      initial->SetGraphicIndex(index);

      initial_text_.push_back(initial);
    }

    phase_type_ = PhaseType::kProcessing;
    break;

  case TitleCredit::PhaseType::kProcessing: {
    icon_->AddRotate(kIconRotateSpeed);
    break;
  }
  case TitleCredit::PhaseType::kFinalize:
    if (window_ != nullptr) {
      delete window_;
      window_ = nullptr;
    }
    if (text_ != nullptr) {
      delete text_;
      text_ = nullptr;
    }

    if (icon_ != nullptr) {
      delete icon_;
      icon_ = nullptr;
    }

    for (MultiGraphic* initial : initial_text_) {
      if (initial == nullptr) continue;

      delete initial;
      initial = nullptr;
    }
    break;

  default:
    break;
  }
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleCredit::Render(Layer layer) {
  if (window_ != nullptr) {
    window_->Render(layer);
  }

  if (text_ != nullptr) {
    text_->Render(layer);
  }

  if (icon_ != nullptr) {
    icon_->Render(layer);
  }

  for (MultiGraphic* initial : initial_text_) {
    if (initial == nullptr) continue;

    initial->Render(layer);
  }
}
