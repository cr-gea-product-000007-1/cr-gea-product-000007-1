#include "InGameFinishUI.h"
#include "System/Pass.h"


/// <summary>
/// コンストラクタ
/// </summary>
InGameFinishUI::InGameFinishUI()
	: finish_ui_(nullptr)
  , accumulation_time_(0.0f)
	, now_phase_(UiPhase::kInitialize) {

}

/// <summary>
/// デストラクタ
/// </summary>
InGameFinishUI::~InGameFinishUI() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void InGameFinishUI::Update(float frame_second) {

	switch (now_phase_) {
	case UiPhase::kInitialize:
		finish_ui_ = new AnimationGraphic();

		finish_ui_->LoadDivideGraphicHandle(pass::kInGameFinishUIImagePass, kUIAllNum, kUIXNum, kUIYNum, kUIXSize, kUIYSize);
		
		finish_ui_->SetPosition(TwoDimensional(kUIX, kUIY));
		finish_ui_->SetAnimationRate(kUIAnimationSpeed);
		finish_ui_->SetLayer(Layer::kInGameUI);

		finish_ui_->EndContinue();
		finish_ui_->Play();

		now_phase_ = UiPhase::kRender;
		break;

	case UiPhase::kRender:
		finish_ui_->UpdateAnimation(frame_second);

		accumulation_time_ += frame_second;

		if (accumulation_time_ >= kRenderTime) {
			now_phase_ = UiPhase::kRenderFinish;
		}
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameFinishUI::Render(Layer layer) {
	if (layer != Layer::kInGameUI) return;
	if (now_phase_ != UiPhase::kRender) return;

	finish_ui_->Render(layer);
}