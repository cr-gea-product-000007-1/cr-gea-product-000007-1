#include "ResultBackground.h"
#include "System/Pass.h"

/// <summary>
/// コンストラクタ
/// </summary>
ResultBackground::ResultBackground()
  : phase_type_(PhaseType::kInitialize)
  , title_()
  , window_()
  , time_(0) {

}

/// <summary>
/// デストラクタ
/// </summary>
ResultBackground::~ResultBackground() {

}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultBackground::Initialize() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void ResultBackground::Update(float frame_second) {
  switch (phase_type_) {
  case ResultBackground::PhaseType::kInitialize:
    title_ = new SingleGraphic();
    window_ = new SingleGraphic();
    info_ = new SingleGraphic();

    title_->LoadGraphicHandle(pass::kResultTitleImagePass);
    title_->SetPosition(TwoDimensional(kTitleX, kTileY));
    title_->SetLayer(Layer::kBackground);

    window_->LoadGraphicHandle(pass::kResultWindowImagePass);
    window_->SetPosition(TwoDimensional(kWindowX, kWindowY));
    window_->SetLayer(Layer::kBackground);

    info_->LoadGraphicHandle(pass::kResultKeyInfoImagePass);
    info_->SetPosition(TwoDimensional(kInfoX, kInfoY));
    info_->SetLayer(Layer::kBackground);

    phase_type_ = PhaseType::kProcessing;
    break;

  case ResultBackground::PhaseType::kProcessing: {
    break;
  }
  case ResultBackground::PhaseType::kFinalize:
    if (title_ != nullptr) {
      delete title_;
      title_ = nullptr;
    }
    if (window_ != nullptr) {
      delete window_;
      window_ = nullptr;
    }
    if (info_ != nullptr) {
      delete info_;
      info_ = nullptr;
    }
    break;

  default:
    break;
  }
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultBackground::Render(Layer layer) {
  if (title_ != nullptr) {
    title_->Render(layer);
  }

  if (window_ != nullptr) {
    window_->Render(layer);
  }

  if (info_ != nullptr) {
    info_->Render(layer);
  }
}
