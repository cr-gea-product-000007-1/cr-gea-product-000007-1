#include "System/Task.h"
#include "Game/Graphic/AnimationGraphic.h"

class InGameFinishUI : public Task {

private:
	enum class UiPhase {
		kInitialize,       //初期化
		kRender,           //表示
		kRenderFinish,     //表示終了
	};

	/// <summary>
	/// UIのX座標
	/// </summary>
	const int kUIX = 364;

	/// <summary>
	/// UIのY座標
	/// </summary>
	const int kUIY = 298;

	/// <summary>
	/// UIの表示時間
	/// </summary>
	const float kRenderTime = 1.5f;

	/// <summary>
	/// UIの総枚数
	/// </summary>
	const int kUIAllNum = 5;

	/// <summary>
	/// UIのX枚数
	/// </summary>
	const int kUIXNum = 1;

	/// <summary>
	/// UIのY枚数
	/// </summary>
	const int kUIYNum = 5;

	/// <summary>
	/// UIのXサイズ
	/// </summary>
	const int kUIXSize = 364;

	/// <summary>
	/// UIのYサイズ
	/// </summary>
	const int kUIYSize = 60;

	/// <summary>
	/// UIのコマ間隔
	/// </summary>
	const float kUIAnimationSpeed = 0.15f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	InGameFinishUI();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~InGameFinishUI();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 表示が終了したかを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>表示が終了したか</returns>
	bool IsFinish() { return now_phase_ == UiPhase::kRenderFinish; }

private:
	/// <summary>
	/// 終了時のUI
	/// </summary>
	AnimationGraphic* finish_ui_;

	/// <summary>
	/// 蓄積時間
	/// </summary>
	float accumulation_time_;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	UiPhase now_phase_;
};