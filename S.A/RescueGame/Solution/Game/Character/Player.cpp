#include "Player.h"
#include "DxLib.h"
#include "Shelter.h"

/// <summary>
/// コンストラクタ
/// </summary>
Player::Player() 
  : Character(CharacterType::kPlayer)
  , status_(Status::kGrounded)
  , rescuer_event_interface_(nullptr)
  , player_trigger_event_interface_(nullptr)
  , rescuer_list_() 
  , flying_height_(0)
  , rescue_time_(0.0f) 
  , propeller_graphic_(nullptr)
  , explosion_graphic_(nullptr)
  , resporn_time_(0.0f)
  , resporn_position_() {
}

/// <summary>
/// デストラクタ
/// </summary>
Player::~Player() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Player::Update(float frame_second) {
	switch (phase_type_)	{
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kPlayerImagePass);


		propeller_graphic_ = new SingleGraphic();
		propeller_graphic_->LoadGraphicHandle(pass::kPropellerImagePass);

		explosion_graphic_ = new AnimationGraphic();
		explosion_graphic_->LoadDivideGraphicHandle(pass::kExplosionImagePass,
			kExplosionNum, kExplosionXNum, kExplosionYNum, kTileSize, kTileSize);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kPlayer);

		propeller_graphic_->SetCenterPivotRate();
		propeller_graphic_->SetLayer(Layer::kPlayer);

		explosion_graphic_->SetCenterPivotRate();
		explosion_graphic_->SetLayer(Layer::kPlayer);
		explosion_graphic_->SetAnimationRate(kExplosionSpeed);

		SetDirection(Direction::kDown);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

		if (!IsProcessing()) return;
		
		UpdateRescue(frame_second);
		UpdateFiying();
		UpdateGraphic(frame_second);
		UpdateTrigger();
		UpdateResporn(frame_second);
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void Player::Render(Layer layer) {
	if (!IsRender()) return;
	if (graphic_ == nullptr) return;

	if (status_ == Status::kExplosionStart) return;

	if (status_ == Status::kExplosion) {
		explosion_graphic_->Render(layer);
		return;
	}

	graphic_->Render(layer);
	propeller_graphic_->Render(layer);
}

/// <summary>
/// 接地しているか
/// </summary>
/// <param>なし</param>
/// <returns>接地している = true</returns>
bool Player::IsGrounded() {
	if (status_ == Status::kGrounded) return true;
	if (status_ == Status::kRescue) return true;

	return false;
}

/// <summary>
/// 救助する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Player::Rescue() { 
	status_ = Status::kRescue; 
	rescue_time_ = kZeroFloat;
}

/// <summary>
/// 救助者を救助する
/// </summary>
/// <param>救助する救助者</param>
/// <returns>なし</returns>
void Player::RescueRescuer(Rescuer* rescuer) {
	rescuer_list_.push_back(rescuer);
}

/// <summary>
/// 救助者を引き渡す
/// </summary>
/// <param>なし</param>
/// <returns>引き渡す救助者</returns>
Rescuer* Player::ExtraditeRescuer() {
	if (rescuer_list_.empty()) return nullptr;

	auto rescuer_it = rescuer_list_.begin();
	Rescuer* rescuer = *rescuer_it;

	rescuer_list_.erase(rescuer_it);

	return rescuer;
}

/// <summary>
/// 救助者を救助中か返す
/// </summary>
/// <param>なし</param>
/// <returns>救助中 = true</returns>
bool Player::InRescuer() {
	return !rescuer_list_.empty();
}

/// <summary>
/// 救助状態の更新
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Player::UpdateRescue(float frame_second) {
	switch (status_) {

	case Player::Status::kGrounded:
	  rescuer_event_interface_->RescueShelter(this);
	  rescuer_event_interface_->ExtraditeHeliport(this);
		break;

	case Player::Status::kRescue:
		rescue_time_ += frame_second;
		if (rescue_time_ >= kRescueCompleteTime) {
			status_ = Status::kGrounded;
		}
		break;

	default:
		break;
	}
}

/// <summary>
/// 飛行状態更新
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Player::UpdateFiying() {
	switch (status_) {
	case Player::Status::kTakeoff:
		flying_height_ += kFlyingSpeed;

		if (flying_height_ > kMaxFlyingHeight) {
			flying_height_ = kMaxFlyingHeight;
			status_ = Status::kFiying;
		}
		break;

	case Player::Status::kLanding:
		flying_height_ -= kFlyingSpeed;

		if (flying_height_ < kZeroFloat) {
			flying_height_ = kZeroFloat;
			status_ = Status::kGrounded;
		}
		break;

	default:
		break;
	}

}

/// <summary>
/// グラフィック更新
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Player::UpdateGraphic(float frame_second) {
	GameInfo* game_info = GameInfo::GetInstance();
	if (game_info == nullptr) return;

	TwoDimensional upper_left_position = game_info->GetMapUpperLeft();
	TwoDimensional screen_pos = field_position_ * kTileSize;
	screen_pos = screen_pos + upper_left_position;

	graphic_->SetPosition(screen_pos);
	propeller_graphic_->SetPosition(screen_pos);

	float size = kZeroFloat;

	switch (status_) {
	case Player::Status::kFiying:
		size = kMaxScale;
		break;

	case Player::Status::kGrounded:
	case Player::Status::kRescue:
	case Player::Status::kExtradite:
		size = kMinScale;
		break;

	case Player::Status::kTakeoff:
	case Player::Status::kLanding:
		size = (kMaxScale - kMinScale) * flying_height_ + kMinScale;
		break;

	case Player::Status::kExplosionStart:
		explosion_graphic_->Play();
		explosion_graphic_->SetPosition(screen_pos);
		status_ = Status::kExplosion;
		break;

	case Player::Status::kExplosion:
		explosion_graphic_->UpdateAnimation(frame_second);

		if (explosion_graphic_->IsFinish()) {
			rescue_time_ = kZeroFloat;
			status_ = Status::kResporn;
		}
		break;

	default:
		break;
	}
	graphic_->SetSize(size);
	propeller_graphic_->SetSize(size);

	float rotate = (float)GetDirection() / (float)Direction::kEnd * kPi * kTwo;
	graphic_->SetRotate(rotate);

	propeller_graphic_->AddRotate(flying_height_ * kPropellerRotate);
}

/// <summary>
/// 当たり判定更新
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Player::UpdateTrigger() {
	if (player_trigger_event_interface_ == nullptr) return;
	if (status_ == Status::kExplosionStart) return;
	if (status_ == Status::kExplosion) return;
	if (status_ == Status::kResporn) return;

	if (player_trigger_event_interface_->TriggerPlayerEvent(this)) {
		rescuer_event_interface_->FlightRescuers(this);

		status_ = Status::kExplosionStart;
	}
}

/// <summary>
/// 復活の更新
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Player::UpdateResporn(float frame_second) {
	if (status_ != Status::kResporn) return;

	rescue_time_ += frame_second;

	if (rescue_time_ >= kRespornCompleteTime) {
		SetPosition(resporn_position_);
		SetDirection(Direction::kDown);

		flying_height_ = kZeroFloat;
		status_ = Status::kGrounded;
	}
}