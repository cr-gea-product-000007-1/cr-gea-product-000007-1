#pragma once
#include "System/EnemyId.h"
class EnemyBase;
class EnemyBulletBase;

class IShotEnemyEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IShotEnemyEvent() { }

	/// <summary>
	/// 敵の弾を発射するイベント
	/// </summary>
	/// <param>敵のポインタ</param>
  /// <param>弾の種類</param>
	/// <returns>なし</returns>
	virtual void ShotEnemyEvent(EnemyBase*, EnemyBulletId) = 0;
};