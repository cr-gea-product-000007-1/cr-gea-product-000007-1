#include "EnemyA.h"

/// <summary>
/// コンストラクタ
/// </summary>
EnemyA::EnemyA()
	: EnemyBase(EnemyType::kEnemyA)
	, think_phase_(ThinkPhaseType::kStartMove)
	, start_move_direction_(Direction::kUp)
	, move_direction_()
	, idel_time_(0)
	, engine_() {
	std::random_device seed_gen;
	engine_ = std::mt19937(seed_gen());

}

/// <summary>
/// デストラクタ
/// </summary>
EnemyA::~EnemyA() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void EnemyA::Update(float frame_second) {

	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kEnemyAImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kCharacter);

		SettingSleepGraphic(frame_second);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

	  if (!IsProcessing()) return;

		if (IsSleep()) {
			UpdateSleep(frame_second);
		}
		else {
			UpdateMove(frame_second);
		}
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// 移動行動の更新
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void EnemyA::UpdateMove(float frame_second) {
	switch (think_phase_)	{
	case EnemyA::ThinkPhaseType::kStartMove:
		move_direction_ = TwoDimensional(start_move_direction_, GetMoveSpeed());

		think_phase_ = ThinkPhaseType::kMove;
		break;

	case EnemyA::ThinkPhaseType::kMove: {
		MoveResult move_result = character_controller_->Move(move_direction_, MoveType::kRestrictMove);

		if (move_result != MoveResult::kFreeMove) {
			think_phase_ = ThinkPhaseType::kRevarsMove;
		}
		break;
	}
	case EnemyA::ThinkPhaseType::kRevarsMove:
		move_direction_ = move_direction_ * kReverse;

		//最小時間〜最大時間の間の乱数（小数第2位まで）を設定
		idel_time_ = (engine_() % (int)((kMaxIdelTime - kMinIdelTime) * kRandomFractional)) / kRandomFractional + kMinIdelTime;

		shot_enemy_event_interface_->ShotEnemyEvent(this, EnemyBulletId::kEnemyBulletA);

		think_phase_ = ThinkPhaseType::kIdel;
		break;

	case EnemyA::ThinkPhaseType::kIdel:
		idel_time_ -= frame_second;

		if (idel_time_ <= kZeroFloat) {
			think_phase_ = ThinkPhaseType::kMove;
		}
		break;

	default:
		break;
	}
}