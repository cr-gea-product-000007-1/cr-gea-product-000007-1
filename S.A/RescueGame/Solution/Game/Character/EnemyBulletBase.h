#pragma once
#include "Character.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"
#include "CharacterController.h"
#include "ITriggerEnemyBulletEvent.h"
#include "System/MoveType.h"


class EnemyBulletBase : public Character {

protected:
	/// <summary>
	/// 弾の状態
	/// </summary>
	enum class Status {
		kStartMove,  //移動始め
		kMove,       //移動中
		kHit,        //命中
		kDelete,     //弾を消す
		kFinalize,   //終了処理
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyBulletBase();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~EnemyBulletBase();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	virtual void Update(float frame_second);

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void Render(Layer);

	/// <summary>
	/// キャラクターコントローラーを設定する
	/// </summary>
	/// <param>キャラクターコントローラー</param>
	/// <returns>なし</returns>
	void SetCharacterController(CharacterController* controller) { character_controller_ = controller; }

	/// <summary>
	/// キャラクターのコントローラーを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>キャラクターコントローラー</returns>
	CharacterController* GetCharacterController() { return character_controller_; }

	/// <summary>
	/// 敵の弾の当たり判定のイベントインターフェースを設定
	/// </summary>
	/// <param>敵の弾の当たり判定のイベントインターフェース</param>
	/// <returns>なし</returns>
	void SetITriggerEnemyBulletEvent(ITriggerEnemyBulletEvent* event_interface) { enemy_bullet_event_interface_ = event_interface; }

	/// <summary>
	/// 敵の弾の当たり判定のイベントインターフェースを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>敵の弾の当たり判定のイベントインターフェース</returns>
	ITriggerEnemyBulletEvent* GetITriggerEnemyBulletEvent() { return 	enemy_bullet_event_interface_; }

	/// <summary>
	/// 命中状態にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Hit() { status_ = Status::kHit; }

	/// <summary>
	/// 移動方向を設定する
	/// </summary>
	/// <param>移動方向</param>
	/// <returns>なし</returns>
	void SetMoveDirection(TwoDimensional dir) { move_direction_ = dir; }

private:
	/// <summary>
	/// 移動の更新処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void UpdateMove();

protected:

	/// <summary>
	/// キャラクターコントローラー
	/// </summary>
	CharacterController* character_controller_;

	/// <summary>
	/// 敵の弾の当たり判定のイベントインターフェース
	/// </summary>
	ITriggerEnemyBulletEvent* enemy_bullet_event_interface_;

	/// <summary>
	/// 弾の状態
	/// </summary>
	Status status_;

	/// <summary>
	/// 移動する方向
	/// </summary>
	TwoDimensional move_direction_;

};