#pragma once
#include "Character.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"
#include "CharacterController.h"
#include "ITriggerPlayerBulletEvent.h"
#include "System/MoveType.h"


class PlayerBullet : public Character {

private:
  /// <summary>
  /// 弾の状態
  /// </summary>
	enum class Status {
		kStartMove,  //移動始め
		kMove,       //移動中
		kHit,        //命中
		kDelete,     //弾を消す
		kFinalize,   //終了処理
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	PlayerBullet();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~PlayerBullet();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// キャラクターコントローラーを設定する
	/// </summary>
	/// <param>キャラクターコントローラー</param>
	/// <returns>なし</returns>
	void SetCharacterController(CharacterController* controller) { character_controller_ = controller; }

	/// <summary>
	/// キャラクターのコントローラーを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>キャラクターコントローラー</returns>
	CharacterController* GetCharacterController() { return character_controller_; }

	/// <summary>
	/// 救助者のイベントインターフェースを設定
	/// </summary>
	/// <param>救助者のイベントインターフェース</param>
	/// <returns>なし</returns>
	void SetITriggerPlayerBulletEvent(ITriggerPlayerBulletEvent* event_interface) {	player_bullet_event_interface_ = event_interface; }

	/// <summary>
	/// 救当たり判定のイベントインターフェースを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>当たり判定のイベントインターフェース</returns>
	ITriggerPlayerBulletEvent* GetITriggerPlayerBulletEvent() { return 	player_bullet_event_interface_;	; }

	/// <summary>
	/// 命中状態にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Hit() { status_ = Status::kHit; }

private:
	/// <summary>
	/// 移動の更新処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void UpdateMove();

private:

	/// <summary>
	/// キャラクターコントローラー
	/// </summary>
	CharacterController* character_controller_;

	/// <summary>
	/// 当たり判定のイベントインターフェース
	/// </summary>
	ITriggerPlayerBulletEvent* player_bullet_event_interface_;

	/// <summary>
	/// 弾の状態
	/// </summary>
	Status status_;

	/// <summary>
	/// 移動する方向
	/// </summary>
	TwoDimensional move_direction_;

};