#pragma once
#include "Character.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"
#include "CharacterController.h"
#include "IShotEnemyEvent.h"
#include "System/EnemyId.h"
#include "Game/Graphic/AnimationGraphic.h"


class EnemyBase : public Character {

protected:
	/// <summary>
	/// 敵の状態
	/// </summary>
	enum class Status {
		kFine,      //元気
		kSleep,     //睡眠
	};

	/// <summary>
	/// 敵の種類
	/// </summary>
	enum class EnemyType {
		kNone,        //なし
		kEnemyA,      //敵A
	};

	/// <summary>
	/// 睡眠時間
	/// </summary>
	const float kSleepMaxTime = 4.0f;

	/// <summary>
	/// 睡眠アニメーションのコマスピード
	/// </summary>
	const float kSleepAnimationSpeed = 0.4f;

	/// <summary>
	/// 睡眠グラフィックの総枚数
	/// </summary>
	const int kSleepAllNum = 13;

	/// <summary>
	/// 睡眠グラフィックのX枚数
	/// </summary>
	const int kSleepXNum = 7;

	/// <summary>
	/// 睡眠グラフィックのY枚数
	/// </summary>
	const int kSleepYNum = 2;

	/// <summary>
	/// 睡眠グラフィックのXサイズ
	/// </summary>
	const int kSleepXSize = 32;

	/// <summary>
	/// 睡眠グラフィックのYサイズ
	/// </summary>
	const int kSleepYSize = 64;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyBase();

	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyBase(EnemyType);

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~EnemyBase();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	virtual void Update(float frame_second);

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void Render(Layer);

	/// <summary>
	/// キャラクターコントローラーを設定する
	/// </summary>
	/// <param>キャラクターコントローラー</param>
	/// <returns>なし</returns>
	void SetCharacterController(CharacterController* controller) { character_controller_ = controller; }


	/// <summary>
	/// 弾を発射するインターフェースを設定する
	/// </summary>
	/// <param>弾を発射するインターフェース</param>
	/// <returns>なし</returns>
	void SetIShotEnemyEvent(IShotEnemyEvent* event_interface_) { shot_enemy_event_interface_ = event_interface_; }

	/// <summary>
	/// キャラクターのコントローラーを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>キャラクターコントローラー</returns>
	CharacterController* GetCharacterController() { return character_controller_; }

	/// <summary>
	/// 睡眠状態になる
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし/returns>
	void Sleep();

protected:
	/// <summary>
	/// 睡眠状態か返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>睡眠状態 = true/returns>
	bool IsSleep() { return status_ == Status::kSleep; }

	/// <summary>
	/// 睡眠状態を更新する
	/// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし/returns>
	void UpdateSleep(float frame_second);

	/// <summary>
	/// 睡眠グラフィックの初期設定
	/// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし/returns>
	void SettingSleepGraphic(float frame_second);

protected:
	/// <summary>
	/// 敵の種類
	/// </summary>
	EnemyType enemy_type_;

	/// <summary>
	/// キャラクターコントローラー
	/// </summary>
	CharacterController* character_controller_;

	/// <summary>
	/// 弾を発射するインターフェース
	/// </summary>
	IShotEnemyEvent* shot_enemy_event_interface_;

	/// <summary>
	/// 敵の状態
	/// </summary>
	Status status_;

	/// <summary>
	/// 睡眠時間
	/// </summary>
	float sleep_time_;

	/// <summary>
	/// 睡眠グラフィック
	/// </summary>
	AnimationGraphic* sleep_graphic_;

};