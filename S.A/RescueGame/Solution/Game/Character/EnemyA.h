#pragma once
#include "EnemyBase.h"
#include <random>

/// <summary>
/// ランダムに移動する敵
/// </summary>
class EnemyA : public EnemyBase {

private:
	/// <summary>
	/// 最大待機時間
	/// </summary>
	const float kMaxIdelTime = 2.0f;

	/// <summary>
	/// 最小待機時間
	/// </summary>
	const float kMinIdelTime = 1.0f;

	/// <summary>
	/// 乱数を100分の1の小数まで表現する
	/// </summary>
	const float kRandomFractional = 100.0f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyA();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~EnemyA();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// 開始時の移動方向を設定する
	/// </summary>
	/// <param>開始時の移動方向</param>
	/// <returns>なし</returns>
	void SetStartDirection(Direction dir) { start_move_direction_ = dir; }

private:
	/// <summary>
	/// 移動行動の更新
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void UpdateMove(float frame_second);

private:
	/// <summary>
	/// 思考フェーズ
	/// </summary>
	enum class ThinkPhaseType {
		kNone,       //なし
		kStartMove,  //初期移動方向生成
		kMove,       //移動する
		kRevarsMove, //移動を反転する
		kIdel,       //次の移動まで待機
	};

	/// <summary>
	/// 思考フェーズ
	/// </summary>
	ThinkPhaseType think_phase_;

	/// <summary>
	/// 最初に移動する方向
	/// </summary>
	Direction start_move_direction_;

	/// <summary>
	/// 移動する方向
	/// </summary>
	TwoDimensional move_direction_;

	/// <summary>
	/// 待機時間
	/// </summary>
	float idel_time_;

	/// <summary>
	/// 乱数生成
	/// </summary>
	std::mt19937 engine_;
};

