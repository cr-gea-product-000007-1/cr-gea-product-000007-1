#pragma once
#include "Rescuer.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"

class Heliport : public Character {
private:

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Heliport();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Heliport();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 避難する
	/// </summary>
	/// <param>避難する救助者</param>
	/// <returns>なし</returns>
	void Evacuate(Rescuer*);

private:
	/// <summary>
	/// 中にいる救助者
	/// </summary>
	std::vector<Rescuer*> rescuers_;

};