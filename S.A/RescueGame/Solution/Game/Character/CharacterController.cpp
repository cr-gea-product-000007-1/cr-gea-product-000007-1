#include "CharacterController.h"

/// <summary>
/// コンストラクタ
/// </summary>
CharacterController::CharacterController()
	: character_controller_event_interface_(nullptr)
	, character_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
CharacterController::~CharacterController() {

}

/// <summary>
/// 移動する
/// </summary>
/// <param>移動方向</param>
/// <returns> 移動の結果</returns>
MoveResult CharacterController::Move(TwoDimensional move_pos, MoveType move_type) {
	if (character_controller_event_interface_ == nullptr) return MoveResult::kNotMove;
	MoveResult move_result = character_controller_event_interface_->MoveCharacter(character_, move_pos, move_type);
	return move_result;
}