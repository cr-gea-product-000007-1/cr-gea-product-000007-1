#pragma once

class ITriggerPlayerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ITriggerPlayerEvent() { }

	/// <summary>
	/// プレイヤーの当たり判定イベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerPlayerEvent(Character*) = 0;
};