#include "Rescuer.h"
#include <cmath>
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
Rescuer::Rescuer()
	: Character(CharacterType::kNone)
  , character_controller_(nullptr)
	, rescuer_event_interface_(nullptr)
  , rescuer_status_(RescuerStatus::kEvacuate)
  , flight_time_(0.0f)
  , flight_num_(0) {
}

/// <summary>
/// デストラクタ
/// </summary>
Rescuer::~Rescuer() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Rescuer::Update(float frame_second) {
	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kRescuerImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kCharacter);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

		if (!IsProcessing()) return;

		UpdateMove(frame_second);
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void Rescuer::Render(Layer layer) {
	if (!IsRender()) return;

	if (rescuer_status_ == RescuerStatus::kEvacuate) return;
	if (rescuer_status_ == RescuerStatus::kFlightStart) return;
	if (rescuer_status_ == RescuerStatus::kRescue) return;
	if (rescuer_status_ == RescuerStatus::kRescueComplete) return;

	if (graphic_ == nullptr) return;

	graphic_->Render(layer);
}

/// <summary>
/// 移動の更新処理
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Rescuer::UpdateMove(float frame_second) {
	if (character_controller_ == nullptr) return;
	if (rescuer_event_interface_ == nullptr) return;

	switch (rescuer_status_) {
	case RescuerStatus::kEvacuate:
		break;

	case RescuerStatus::kBoarding: {
		TwoDimensional direction = rescuer_event_interface_->GetPlayerDirection(this);
		TwoDimensional move_pos = direction.GetNormalized();
		move_pos = move_pos * GetMoveSpeed();

		character_controller_->Move(move_pos);

		rescuer_event_interface_->RescuePlayer(this);
		break;
	}
	case RescuerStatus::kRescue:
		break;

	case RescuerStatus::kFlightStart: {
		SetFlightDirection();

		flight_time_ = kZeroFloat;
		rescuer_status_ = RescuerStatus::kFlight;
		break;
	}
	case RescuerStatus::kFlight: {
		MoveResult result = character_controller_->Move(flight_direction);

		if (result != MoveResult::kFreeMove) {
			SetFlightDirection();
		}

		flight_time_ += frame_second;
		if (flight_time_ <= kInvincibleTime) break;

		rescuer_event_interface_->CallForHelpPlayer(this);

		if (rescuer_event_interface_->TriggerRescuerEvent(this)) {
			rescuer_event_interface_->RescuerDieEvent(this);
		}
		break;
	}
	case RescuerStatus::kRescueComplete:
		break;

	default:
		break;
	}
}

/// <summary>
/// 逃走方向設定
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Rescuer::SetFlightDirection() {
	//タスクIDを用いることで方向が被らなくなる
	flight_num_++;
	float seed = (float)GetTaskId() + (float)flight_num_;
	flight_direction = TwoDimensional((float)sin(seed), (float)cos(seed));
	flight_direction = flight_direction * GetMoveSpeed();
}