#pragma once
#include "System/TwoDimensional.h"
class Rescuer;

/// <summary>
/// 救助者のイベントインターフェース
/// </summary>
class IRescuerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IRescuerEvent() { }

	/// <summary>
	/// プレイヤーに救助されに行くイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void CallForHelpPlayer(Rescuer*) = 0;

	/// <summary>
	/// プレイヤーに救助されるイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void RescuePlayer(Rescuer*) = 0;

	/// <summary>
	/// プレイヤーの方向を取得する
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>プレイヤーの方向</returns>
	virtual TwoDimensional GetPlayerDirection(Rescuer*) = 0;

	/// <summary>
	/// 救助者の当たり判定イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerRescuerEvent(Rescuer*) = 0;

	/// <summary>
	/// 救助者の死亡イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void RescuerDieEvent(Rescuer*) = 0;
};