#include "EnemyBulletBase.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
EnemyBulletBase::EnemyBulletBase()
	: Character(CharacterType::kEnemyBullet)
	, character_controller_(nullptr)
	, enemy_bullet_event_interface_(nullptr)
	, status_(Status::kStartMove) {
}

/// <summary>
/// デストラクタ
/// </summary>
EnemyBulletBase::~EnemyBulletBase() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void EnemyBulletBase::Update(float frame_second) {

	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kEnemyBulletBaseImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kCharacter);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

	  if (!IsProcessing()) return;

		UpdateMove();
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void EnemyBulletBase::Render(Layer layer) {
	if (!IsRender()) return;
	if (status_ != Status::kMove) return;
	if (graphic_ == nullptr) return;

	graphic_->Render(layer);
}

/// <summary>
/// 移動の更新処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void EnemyBulletBase::UpdateMove() {

}