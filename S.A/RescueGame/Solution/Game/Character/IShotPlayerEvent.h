#pragma once
class Player;
class PlayerBullet;

class IShotPlayerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IShotPlayerEvent() { }

	/// <summary>
	/// プレイヤーの弾を発射するイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	virtual void ShotPlayerEvent(Player*) = 0;
};