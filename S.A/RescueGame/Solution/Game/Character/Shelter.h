#pragma once
#include "Rescuer.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"

class Shelter : public Character {
private:
	/// <summary>
	/// 避難所の状態
	/// </summary>
	enum class ShelterType {
		kInRescuer,     //救助者が避難中
		kNoneRescuer,   //救助者がいない
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Shelter();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Shelter();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
/// <param>描画するレイヤー</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 救助者が避難する
	/// </summary>
	/// <param>避難する救助者</param>
	/// <returns>なし</returns>
	void Evacuate(Rescuer*);

	/// <summary>
	/// 救助できるか返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>救助できる = true</returns>
	bool IsRescue() { return shelter_type_ == ShelterType::kInRescuer; }

	/// <summary>
	/// 救助者を救助する
	/// </summary>
	/// <param>なし</param>
	/// <returns>救助される救助者</returns>
	Rescuer* Rescue();

private:
	/// <summary>
	/// 避難所の状態
	/// </summary>
	ShelterType shelter_type_;

	/// <summary>
	/// 中にいる救助者
	/// </summary>
	std::vector<Rescuer*> rescuers_;

};