#pragma once
#include "Character.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"
#include "CharacterController.h"
#include "IRescuerEvent.h"
//#include "System/MoveType.h"

/// <summary>
/// 救助者の状態
/// </summary>
enum class RescuerStatus {
	kEvacuate,           //避難する
	kBoarding,           //乗り込む
	kRescue,             //救助中
	kFlightStart,        //逃走開始
	kFlight,             //逃走中
	kRescueComplete,     //救助完了
	kDie,                //死亡
};

class Rescuer : public Character {
private:
	/// <summary>
	/// 逃走開始無敵時間
	/// </summary>
	const float kInvincibleTime = 1.0f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Rescuer();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Rescuer();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// キャラクターコントローラーを設定する
	/// </summary>
	/// <param>キャラクターコントローラー</param>
	/// <returns>なし</returns>
	void SetCharacterController(CharacterController* controller) { character_controller_ = controller; }

	/// <summary>
	/// キャラクターのコントローラーを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>キャラクターコントローラー</returns>
	CharacterController* GetCharacterController() { return character_controller_; }

	/// <summary>
	/// 救助者のイベントインターフェースを設定
	/// </summary>
	/// <param>救助者のイベントインターフェース</param>
	/// <returns>なし</returns>
	void SetIRescuerEvent(IRescuerEvent* event_interface) { rescuer_event_interface_ = event_interface; }

	/// <summary>
	/// 救助者のイベントインターフェースを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>救助者のイベントインターフェース</returns>
	IRescuerEvent* GetIRescuerEvent() { return rescuer_event_interface_; }

	/// <summary>
	/// 救助者の状態を設定する
	/// </summary>
	/// <param>救助者の状態</param>
	/// <returns>なし</returns>
	void SetRescuerStatus(RescuerStatus status) { rescuer_status_ = status; }

private:
	/// <summary>
	/// 移動の更新処理
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void UpdateMove(float frame_second);

	/// <summary>
	/// 逃走方向設定
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetFlightDirection();

private:

	/// <summary>
	/// キャラクターコントローラー
	/// </summary>
	CharacterController* character_controller_;

	/// <summary>
	/// 救助者のイベントインターフェース
	/// </summary>
	IRescuerEvent* rescuer_event_interface_;

	/// <summary>
	/// 救助者の状態
	/// </summary>
	RescuerStatus rescuer_status_;

	/// <summary>
	/// 逃走する方向
	/// </summary>
	TwoDimensional flight_direction;

	/// <summary>
	/// 逃走時間
	/// </summary>
	float flight_time_;

	/// <summary>
	/// 逃走回数
	/// </summary>
	int flight_num_;
};