#pragma once
#include "System/MoveType.h"
class Character;
class TwoDimensional;

/// <summary>
/// キャラクターを移動するインターフェース
/// </summary>
class ICharacterControllerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ICharacterControllerEvent() { }

	/// <summary>
	/// キャラクターを移動させるイベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <param>移動方向</param>
	/// <returns>移動の結果</returns>
	virtual MoveResult MoveCharacter(Character*, TwoDimensional, MoveType = MoveType::kFreeMove) = 0;
};

