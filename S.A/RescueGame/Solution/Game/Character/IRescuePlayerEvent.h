#pragma once
class Character;
class Player;
class Shelter;
class Heliport;

/// <summary>
/// プレイヤーが救助者を扱うインターフェース
/// </summary>
class IRescuePlayerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IRescuePlayerEvent() { }

	/// <summary>
	/// 接触したヘリポートに救助者を引き渡す
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	virtual void ExtraditeHeliport(Player*) = 0;

	/// <summary>
	/// 接触した避難所から救助者を救助する
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	virtual void RescueShelter(Player*) = 0;

	/// <summary>
	/// 救助中の救助者が散らばるイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	virtual void FlightRescuers(Player*) = 0;

};