#pragma once
#include "EnemyBulletBase.h"


class EnemyBulletA : public EnemyBulletBase {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyBulletA();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~EnemyBulletA();

protected:
	/// <summary>
	/// 移動の更新処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void UpdateMove() override;
};