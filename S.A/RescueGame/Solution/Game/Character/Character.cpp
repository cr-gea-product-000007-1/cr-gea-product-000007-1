#include "Character.h"
#include "DxLib.h"
#include "System/DefaultConstant.h"
#include "System/GameInfo.h"
#include <cmath>

/// <summary>
/// コンストラクタ
/// </summary>
Character::Character()
	: phase_type_(PhaseType::kInitialize)
	, character_event_interface_(nullptr)
	, character_type_(CharacterType::kNone)
	, field_position_()
	, graphic_(nullptr)
	, moving_(false)
	, move_speed_(0)
	, direction_(Direction::kRight)
  , radius_(0) {

}

/// <summary>
/// コンストラクタ
/// </summary>
Character::Character(CharacterType character_type)
	: phase_type_(PhaseType::kInitialize)
	, character_event_interface_(nullptr)
	, character_type_(character_type)
	, field_position_()
	, graphic_(nullptr)
	, moving_(false)
	, move_speed_(0)
	, direction_(Direction::kRight)
	, radius_(0) {

}

/// <summary>
/// デストラクタ
/// </summary>
Character::~Character() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Character::Update(float frame_second) {
	GameInfo* game_info = GameInfo::GetInstance();
	if (game_info == nullptr) return;

	TwoDimensional upper_left_position = game_info->GetMapUpperLeft();
	TwoDimensional screen_pos = field_position_ * kTileSize;
	screen_pos = screen_pos + upper_left_position;

	graphic_->SetPosition(screen_pos);
}