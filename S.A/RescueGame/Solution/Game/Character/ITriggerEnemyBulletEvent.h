#pragma once
class EnemyBulletBase;

class ITriggerEnemyBulletEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ITriggerEnemyBulletEvent() { }

	/// <summary>
	/// 敵の弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerEnemyBulletEvent(EnemyBulletBase*) = 0;

	/// <summary>
	/// 敵の弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void HitEnemyBulletEvent(EnemyBulletBase*) = 0;

	/// <summary>
	/// 敵の弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void DeleteEnemyBulletEvent(EnemyBulletBase*) = 0;
};