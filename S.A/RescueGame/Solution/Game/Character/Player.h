#pragma once
#include "Character.h"
#include "Rescuer.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "Game/Graphic/SingleGraphic.h"
#include "Game/Graphic/AnimationGraphic.h"
#include "IRescuePlayerEvent.h"
#include "ITriggerPlayerEvent.h"

class Player : public Character {
private:
  /// <summary>
  /// プレイヤーの状態
  /// </summary>
  enum class Status {
    kTakeoff,         // 離陸
    kFiying,          // 飛行
    kLanding,         // 着陸
    kGrounded,        // 接地
    kRescue,          // 救助中
    kExtradite,       // 引き渡し
    kExplosionStart,  // 爆破開始
    kExplosion,       // 爆破中
    kResporn,         // 復活
  };

  /// <summary>
  /// 最大飛行時の高さ
  /// </summary>
  const float kMaxFlyingHeight = 1.0f;

  /// <summary>
  /// 飛行の上昇、下降スピード
  /// </summary>
  const float kFlyingSpeed = 0.05f;

  /// <summary>
  /// 最大時の大きさ
  const float kMaxScale = 1.0f;

  /// <summary>
  /// 最小時の大きさ
  const float kMinScale = 0.5f;

  /// <summary>
  /// 救助に必要な時間
  /// </summary>
  const float kRescueCompleteTime = 0.4f;

  /// <summary>
  /// プロペラの回転速度
  /// </summary>
  const float kPropellerRotate = 0.3f;

  /// <summary>
  /// 爆破のアニメ速度
  /// </summary>
  const float kExplosionSpeed = 0.15f;

  /// <summary>
  /// 爆破のアニメの総毎数
  /// </summary>
  const int kExplosionNum = 10;

  /// <summary>
  /// 爆破のアニメのX毎数
  /// </summary>
  const int kExplosionXNum = 5;

  /// <summary>
  /// 爆破のアニメのY毎数
  /// </summary>
  const int kExplosionYNum = 2;

  /// <summary>
  /// 復活までの時間
  /// </summary>
  const float kRespornCompleteTime = 2.0f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Player();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Player();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

  /// <summary>
  /// １フレームの描画処理を実行する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Render(Layer) override;

  /// <summary>
  /// 飛んでいるか
  /// </summary>
  /// <param>なし</param>
  /// <returns>飛んでいる = true</returns>
  bool IsFlying() { return status_ == Status::kFiying; }

  /// <summary>
  /// 接地しているか
  /// </summary>
  /// <param>なし</param>
  /// <returns>接地している = true</returns>
  bool IsGrounded();

  /// <summary>
  /// 離陸する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Takeoff() { status_ = Status::kTakeoff; }

  /// <summary>
  /// 着陸する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Landing() { status_ = Status::kLanding; }

  /// <summary>
  /// 救助する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Rescue();

  /// <summary>
  /// 救助者を救助するイベントインターフェースを設定する
  /// </summary>
  /// <param>救助者を救助するイベントインターフェース</param>
  /// <returns>なし</returns>
  void SetIRescuePlayerEvent(IRescuePlayerEvent* event_interface) { rescuer_event_interface_ = event_interface; }

  /// <summary>
  /// 当たり判定のイベントインターフェースを設定する
  /// </summary>
  /// <param>当たり判定のイベントインターフェース</param>
  /// <returns>なし</returns>
  void SetITriggerPlayerEvent(ITriggerPlayerEvent* event_interface) { player_trigger_event_interface_ = event_interface; }

  /// <summary>
  /// 復活位置の設定
  /// </summary>
  /// <param>復活位置</param>
  /// <returns>なし</returns>
  void SetRespornPosition(TwoDimensional pos) { resporn_position_ = pos; }

  /// <summary>
  /// 救助者を救助する
  /// </summary>
  /// <param>救助する救助者</param>
  /// <returns>なし</returns>
  void RescueRescuer(Rescuer*);

  /// <summary>
  /// 救助者を引き渡す
  /// </summary>
  /// <param>なし</param>
  /// <returns>引き渡す救助者</returns>
  Rescuer* ExtraditeRescuer();

  /// <summary>
  /// 救助者を救助中か返す
  /// </summary>
  /// <param>なし</param>
  /// <returns>救助中 = true</returns>
  bool InRescuer();

private:
  /// <summary>
  /// 救助状態の更新
  /// </summary>/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdateRescue(float frame_second);

  /// <summary>
  /// 飛行状態更新
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateFiying();

  /// <summary>
  /// グラフィック更新
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdateGraphic(float frame_second);

  /// <summary>
  /// 当たり判定更新
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateTrigger();

  /// <summary>
  /// 復活の更新
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdateResporn(float frame_second);

private:
  /// <summary>
  /// プレイヤーの状態
  /// </summary>
  Status status_;

  /// <summary>
  /// 救助者を救助するイベントインターフェース
  /// </summary>
  IRescuePlayerEvent* rescuer_event_interface_;

  /// <summary>
  /// 当たり判定のイベントインターフェース
  /// </summary>
  ITriggerPlayerEvent* player_trigger_event_interface_;

	/// <summary>
	/// 救助中の救助者
	/// </summary>
	std::vector<Rescuer*> rescuer_list_;

  /// <summary>
  /// 飛んでいる高さ
  /// </summary>
  float flying_height_;

  /// <summary>
  /// 救助している時間
  /// </summary>
  float rescue_time_;

  /// <summary>
  /// プロペラの描画
  /// </summary>
  SingleGraphic* propeller_graphic_;

  /// <summary>
  /// 爆破の描画
  /// </summary>
  AnimationGraphic* explosion_graphic_;

  /// <summary>
  /// 復活までの時間
  /// </summary>
  float resporn_time_;

  /// <summary>
  /// 復活位置
  /// </summary>
  TwoDimensional resporn_position_;

};

