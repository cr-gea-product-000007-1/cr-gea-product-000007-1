#pragma once
#include <unordered_map>
#include "Player.h"
#include "System/Task.h"
#include "System/DefaultConstant.h"
#include "ICharacterControllerEvent.h"
#include "IShotPlayerEvent.h"
#include "System/ControllerButton.h"
#include "System/Direction.h"
#include "System/KeyInput.h"

class PlayerController : public Task {

public:
  /// <summary>
  /// コンストラクタ
  /// </summary>
  PlayerController();

  /// <summary>
  /// デストラクタ
  /// </summary>
  virtual ~PlayerController();

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Initialize();

  /// <summary>
  /// キャラクターコントローラーインターフェースを設定する
  /// </summary>
  /// <param>キャラクターコントローラーインターフェース</param>
  /// <returns>なし</returns>
  void SetICharacterControllerEvent(ICharacterControllerEvent* event_interface) { character_controller_event_interface_ = event_interface; }

  /// <summary>
  /// プレイヤーの弾を発射するイベントインターフェースを設定する
  /// </summary>
  /// <param>プレイヤーの弾を発射するイベントインターフェース</param>
  /// <returns>なし</returns>
  void SetIShotPlayerEvent(IShotPlayerEvent* event_interface) { player_shot_event_interface_ = event_interface; }

  /// <summary>
  /// 操作するプレイヤーを設定する
  /// </summary>
  /// <param>操作するプレイヤー</param>
  /// <returns>なし</returns>
  void SetPlayer(Player* player) { player_ = player; }

  /// <summary>
  /// １フレームの処理を実行する
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void Update(float frame_second) override;

private:
  /// <summary>
  /// 2つのキーが同時入力されているか
  /// </summary>
  /// <param>なし</param>
  /// <returns>入力されている = true</returns>
  bool CheckSimultaneousInput(ControllerButton input1, ControllerButton input2);

  /// <summary>
  /// プレイヤーの移動
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdatePlayerMove(float frame_second);

  /// <summary>
  /// プレイヤーの飛行
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdatePlayerFiy(float frame_second);

  /// <summary>
  /// プレイヤーのショット
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void UpdatePlayerShot(float frame_second);

private:
  /// <summary>
  /// キャラクターコントローラーインターフェース
  /// </summary>
  ICharacterControllerEvent* character_controller_event_interface_;

  /// <summary>
  /// プレイヤーの弾を発射するイベントインターフェース
  /// </summary>
  IShotPlayerEvent* player_shot_event_interface_;

  /// <summary>
  /// 操作するプレイヤー
  /// </summary>
  Player* player_;

  /// <summary>
  /// 上キーの入力
  /// </summary>
  ControllerButton up_key_input_;

  /// <summary>
  /// 下キーの入力
  /// </summary>
  ControllerButton down_key_input_;

  /// <summary>
  /// 左キーの入力
  /// </summary>
  ControllerButton left_key_input_;

  /// <summary>
  /// 右キーの入力
  /// </summary>
  ControllerButton right_key_input_;

  /// <summary>
  /// 飛行キーの入力
  /// </summary>
  ControllerButton fly_key_input_;

  /// <summary>
  /// ショットキーの入力
  /// </summary>
  ControllerButton shot_key_input_;
};

