#include "Heliport.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
Heliport::Heliport()
	: Character(CharacterType::kHeliport)
	, rescuers_() {
}

/// <summary>
/// デストラクタ
/// </summary>
Heliport::~Heliport() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Heliport::Update(float frame_second) {
	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kHeliportImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kBuild);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

		if (!IsProcessing()) return;
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void Heliport::Render(Layer layer) {
	if (!IsRender()) return;
	if (graphic_ == nullptr) return;

	graphic_->Render(layer);
}

/// <summary>
/// 避難する
/// </summary>
/// <param>避難する救助者</param>
/// <returns>なし</returns>
void Heliport::Evacuate(Rescuer* rescuer) {
	rescuers_.push_back(rescuer);

	rescuer->SetPosition(field_position_);
	rescuer->SetRescuerStatus(RescuerStatus::kRescueComplete);
}
