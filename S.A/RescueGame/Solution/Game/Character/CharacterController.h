#pragma once
#include "ICharacterControllerEvent.h"
#include "System/TwoDimensional.h"


class CharacterController {

public:
  /// <summary>
  /// コンストラクタ
  /// </summary>
  CharacterController();

  /// <summary>
  /// デストラクタ
  /// </summary>
  ~CharacterController();

  /// <summary>
  /// キャラクターを移動させるイベントインターフェースを設定する
  /// </summary>
  /// <param>キャラクターを移動させるイベントインターフェース</param>
  /// <returns>なし</returns>
  void SetICharacterControllerEvent(ICharacterControllerEvent* event_interface) { character_controller_event_interface_ = event_interface; }

  /// <summary>
  /// キャラクターを設定する
  /// </summary>
  /// <param>キャラクター</param>
  /// <returns>なし</returns>
  void SetCharacter(Character* character) { character_ = character; }

  /// <summary>
  /// キャラクターを移動させる
  /// </summary>
  /// <param>移動方向</param>
  /// <returns> 移動の結果</returns>
  MoveResult Move(TwoDimensional move_pos, MoveType = MoveType::kFreeMove);

private:
  /// <summary>
  /// キャラクターを移動させるイベントインターフェース
  /// </summary>
  ICharacterControllerEvent* character_controller_event_interface_;

  /// <summary>
  /// キャラクター
  /// </summary>
  Character* character_;
};

