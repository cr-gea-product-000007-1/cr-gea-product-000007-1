#include "PlayerBullet.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
PlayerBullet::PlayerBullet()
	: Character(CharacterType::kNone)
	, character_controller_(nullptr)
	, player_bullet_event_interface_(nullptr)
	, status_(Status::kStartMove) {
}

/// <summary>
/// デストラクタ
/// </summary>
PlayerBullet::~PlayerBullet() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void PlayerBullet::Update(float frame_second) {
	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kPlayerBulletImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kCharacter);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

    if (!IsProcessing()) return;
		
		UpdateMove();
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void PlayerBullet::Render(Layer layer) {
	if (!IsRender()) return;
	if (status_ != Status::kMove) return;
	if (graphic_ == nullptr) return;

	graphic_->Render(layer);
}

/// <summary>
/// 移動の更新処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void PlayerBullet::UpdateMove() {
	switch (status_) {
	case Status::kStartMove:
		move_direction_ = TwoDimensional(GetDirection(), GetMoveSpeed());

		status_ = Status::kMove;
		break;

	case Status::kMove: {
		MoveResult move_result = character_controller_->Move(move_direction_, MoveType::kRestrictMove);

		if (move_result != MoveResult::kFreeMove) {
			status_ = Status::kHit;
		  break;
		}
		if (player_bullet_event_interface_->TriggerPlayerBulletEvent(this)){
			status_ = Status::kHit;
		}
		break;
	}
	case Status::kHit:
		player_bullet_event_interface_->HitPlayerBulletEvent(this);

		status_ = Status::kDelete;
		break;

	case Status::kDelete:
		//タスクマネージャーから降ろされるのを待つ
		if (!IsRelease()) return;

		player_bullet_event_interface_->DeletePlayerBulletEvent(this);

		status_ = Status::kFinalize;
		break;

	case Status::kFinalize:
		break;

	default:
		break;
	}
}