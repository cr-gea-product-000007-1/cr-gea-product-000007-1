#include "Shelter.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
Shelter::Shelter()
	: Character(CharacterType::kShelter) 
	, shelter_type_(ShelterType::kNoneRescuer)
  , rescuers_() {
}

/// <summary>
/// デストラクタ
/// </summary>
Shelter::~Shelter() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Shelter::Update(float frame_second) {
	switch (phase_type_) {
	case PhaseType::kInitialize:
		graphic_ = new SingleGraphic();
		graphic_->LoadGraphicHandle(pass::kShelterImagePass);

		graphic_->SetCenterPivotRate();
		graphic_->SetLayer(Layer::kBuild);

		phase_type_ = PhaseType::kProcessing;
		break;
	case PhaseType::kProcessing: {
		Character::Update(frame_second);

		if (!IsProcessing()) return;
		break;
	}

	case PhaseType::kFinalize:
		if (graphic_ == nullptr) break;

		delete graphic_;
		graphic_ = nullptr;
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void Shelter::Render(Layer layer) {
	if (!IsRender()) return;
	if (graphic_ == nullptr) return;

	graphic_->Render(layer);
}

/// <summary>
/// 避難する
/// </summary>
/// <param>避難する救助者</param>
/// <returns>なし</returns>
void Shelter::Evacuate(Rescuer* rescuer) {
	rescuers_.push_back(rescuer);

	shelter_type_ = ShelterType::kInRescuer;

	rescuer->SetPosition(field_position_);
	rescuer->SetRescuerStatus(RescuerStatus::kEvacuate);
}

/// <summary>
/// 救助する
/// </summary>
/// <param>なし</param>
/// <returns>救助される救助者</returns>
Rescuer* Shelter::Rescue() {
	if (rescuers_.empty()) return nullptr;
	if (shelter_type_ != ShelterType::kInRescuer) return nullptr;

	auto out_rescuer_it = rescuers_.begin();
	Rescuer* out_rescuer = *out_rescuer_it;

	rescuers_.erase(out_rescuer_it);

	if (rescuers_.empty()) {
		shelter_type_ = ShelterType::kNoneRescuer;
	}

	out_rescuer->SetRescuerStatus(RescuerStatus::kBoarding);

	return out_rescuer;
}