#pragma once
#include "System/EnemyId.h"

class Character;

/// <summary>
/// キャラクターを生成するインターフェース
/// </summary>
class ICharacterCreateEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ICharacterCreateEvent() { }

	/// <summary>
	/// 避難所を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	virtual Shelter* CreateShelter() = 0;

	/// <summary>
	/// 救助者を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	virtual Rescuer* CreateRescuer() = 0;

	/// <summary>
	/// 敵を生成する
	/// </summary>
	/// <param>生成する敵のID</param>
	/// <returns>生成したキャラクター</returns>
	virtual EnemyBase* CreateEnemy(EnemyId) = 0;
};

