#pragma once
class PlayerBullet;

class ITriggerPlayerBulletEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ITriggerPlayerBulletEvent() { }

	/// <summary>
	/// プレイヤーの弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerPlayerBulletEvent(PlayerBullet*) = 0;

	/// <summary>
	/// プレイヤーの弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void HitPlayerBulletEvent(PlayerBullet*) = 0;

	/// <summary>
	/// プレイヤーの弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void DeletePlayerBulletEvent(PlayerBullet*) = 0;
};