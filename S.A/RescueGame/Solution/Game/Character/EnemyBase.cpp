#include "EnemyBase.h"

/// <summary>
/// コンストラクタ
/// </summary>
EnemyBase::EnemyBase()
  : Character(CharacterType::kEnemy)
  , enemy_type_(EnemyType::kNone)
  , character_controller_(nullptr)
  , shot_enemy_event_interface_(nullptr)
  , status_(Status::kFine)
  , sleep_time_(0.0f) 
  , sleep_graphic_(nullptr) {

}

/// <summary>
/// コンストラクタ
/// </summary>
EnemyBase::EnemyBase(EnemyType enemy_type)
  : enemy_type_(enemy_type)
  , character_controller_(nullptr)
  , shot_enemy_event_interface_(nullptr)
  , status_(Status::kFine)
  , sleep_time_(0.0f)
  , sleep_graphic_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
EnemyBase::~EnemyBase() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void EnemyBase::Update(float frame_second) {

}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void EnemyBase::Render(Layer layer) {
  if (!IsRender()) return;

  if (graphic_ == nullptr) return;

  graphic_->Render(layer);

  if (status_ == Status::kFine) return;
  sleep_graphic_->Render(layer);
}

/// <summary>
/// 睡眠状態になる
/// </summary>
/// <param>なし</param>
/// <returns>なし/returns>
void EnemyBase::Sleep() {
  sleep_time_ = kZeroFloat;

  if (status_ == Status::kSleep) return;

  status_ = Status::kSleep;

  sleep_graphic_->SetPosition(graphic_->GetPosition());

  sleep_graphic_->Loop();
  sleep_graphic_->Play();
}

/// <summary>
/// 睡眠状態を更新する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし/returns>
void EnemyBase::UpdateSleep(float frame_second) {
  if (status_ != Status::kSleep) return;
  if (sleep_graphic_ == nullptr) return;

  sleep_graphic_->UpdateAnimation(frame_second);

  if (sleep_time_ >= kSleepMaxTime) {
    sleep_graphic_->Finish();
  }
  else {
    sleep_time_ += frame_second;
  }

  if (sleep_graphic_->IsFinish()) {
    status_ = Status::kFine;
  }
}

/// <summary>
/// 睡眠グラフィックの初期設定
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし/returns>
void EnemyBase::SettingSleepGraphic(float frame_second) {
  sleep_graphic_ = new AnimationGraphic();

  sleep_graphic_->LoadDivideGraphicHandle(pass::kSleepImagePass, kSleepAllNum, kSleepXNum, kSleepYNum, kSleepXSize, kSleepYSize);

  sleep_graphic_->SetPivotRate(TwoDimensional(kHalf, (float)kOne));
  sleep_graphic_->SetAnimationRate(kSleepAnimationSpeed);

  sleep_graphic_->SetLayer(Layer::kCharacter);
}