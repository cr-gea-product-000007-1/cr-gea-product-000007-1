#include "EnemyBulletA.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
EnemyBulletA::EnemyBulletA()
	: EnemyBulletBase() {
}

/// <summary>
/// デストラクタ
/// </summary>
EnemyBulletA::~EnemyBulletA() {

}

/// <summary>
/// 移動の更新処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void EnemyBulletA::UpdateMove() {
	switch (status_) {
	case Status::kStartMove:
		if (move_direction_.Empty()) {
			move_direction_ = TwoDimensional(GetDirection(), GetMoveSpeed());
		}

		status_ = Status::kMove;
		break;

	case Status::kMove: {
		MoveResult move_result = character_controller_->Move(move_direction_, MoveType::kRestrictMove);

		if (move_result != MoveResult::kFreeMove) {
			status_ = Status::kHit;
		}
		break;
	}
	case Status::kHit:
		enemy_bullet_event_interface_->HitEnemyBulletEvent(this);

		status_ = Status::kDelete;
		break;

	case Status::kDelete:
		//タスクマネージャーから降ろされるのを待つ
		if (!IsRelease()) return;

		enemy_bullet_event_interface_->DeleteEnemyBulletEvent(this);

		status_ = Status::kFinalize;
		break;

	case Status::kFinalize:
		break;

	default:
		break;
	}
}