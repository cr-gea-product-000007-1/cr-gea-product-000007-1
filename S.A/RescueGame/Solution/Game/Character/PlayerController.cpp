#include "PlayerController.h"


/// <summary>
/// コンストラクタ
/// </summary>
PlayerController::PlayerController()
	: character_controller_event_interface_(nullptr)
	, player_shot_event_interface_(nullptr)
  , player_(nullptr)
  , up_key_input_()
  , down_key_input_()
  , left_key_input_()
  , right_key_input_()
  , fly_key_input_() 
  , shot_key_input_() {

}

/// <summary>
/// デストラクタ
/// </summary>
PlayerController::~PlayerController() {

}

/// <summary>
/// 初期設定
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void PlayerController::Initialize() {
	up_key_input_.SetKeyCode(kUpKey);
	down_key_input_.SetKeyCode(kDownKey);
	left_key_input_.SetKeyCode(kLeftKey);
	right_key_input_.SetKeyCode(kRightKey);

	fly_key_input_.SetKeyCode(kFlyKey);

	shot_key_input_.SetKeyCode(kShotKey);
}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void PlayerController::Update(float frame_second) {
	UpdatePlayerMove(frame_second);
	UpdatePlayerFiy(frame_second);
	UpdatePlayerShot(frame_second);
}

/// <summary>
/// 2つのキーが同時入力されているか
/// </summary>
/// <param>なし</param>
/// <returns>入力されている = true</returns>
bool PlayerController::CheckSimultaneousInput(ControllerButton input1, ControllerButton input2) {
	//両方同時入力を受け付ける状態か
	bool simultaneous_input = input1.GetSimultaneousInput() && input2.GetSimultaneousInput();
	
	//両方が同時入力猶予状態の場合は例外にする
	bool not_simultaneous_input = input1.IsReleaseDelay() && input2.IsReleaseDelay();

	return simultaneous_input && !not_simultaneous_input;
}

/// <summary>
/// プレイヤーの移動
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void PlayerController::UpdatePlayerMove(float frame_second) {
	up_key_input_.InputUpdate(frame_second);
	down_key_input_.InputUpdate(frame_second);
	left_key_input_.InputUpdate(frame_second);
	right_key_input_.InputUpdate(frame_second);

	if (player_ == nullptr) return;
	if (!player_->IsFlying()) return;

	TwoDimensional move;

	if (CheckSimultaneousInput(up_key_input_, left_key_input_)) {
		move = TwoDimensional(Direction::kUpperLeft, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kUpperLeft);
		return;
	}

	if (CheckSimultaneousInput(up_key_input_, right_key_input_)) {
		move = TwoDimensional(Direction::kUpperRight, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kUpperRight);
		return;
	}

	if (CheckSimultaneousInput(down_key_input_, left_key_input_)) {
		move = TwoDimensional(Direction::kBottomLeft, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kBottomLeft);
		return;
	}

	if (CheckSimultaneousInput(down_key_input_, right_key_input_)) {
		move = TwoDimensional(Direction::kBottomRight, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kBottomRight);
		return;
	}

	if (up_key_input_.GetInput()) {
		move = TwoDimensional(Direction::kUp, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kUp);
		return;
	}

	if (down_key_input_.GetInput()) {
		move = TwoDimensional(Direction::kDown, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kDown);
		return;
	}

	if (left_key_input_.GetInput()) {
		move = TwoDimensional(Direction::kLeft, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kLeft);
		return;
	}

	if (right_key_input_.GetInput()) {
		move = TwoDimensional(Direction::kRight, player_->GetMoveSpeed());
		character_controller_event_interface_->MoveCharacter(player_, move);

		player_->SetDirection(Direction::kRight);
		return;
	}
}

/// <summary>
/// プレイヤーの飛行
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void PlayerController::UpdatePlayerFiy(float frame_second) {
	fly_key_input_.InputUpdate(frame_second);

	if (player_ == nullptr) return;
	if (!fly_key_input_.IsPressed()) return;

	if (player_->IsFlying()) {
		player_->Landing();
	}

	if (player_->IsGrounded()) {
		player_->Takeoff();
	}
}

/// <summary>
/// プレイヤーのショット
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void PlayerController::UpdatePlayerShot(float frame_second) {
	shot_key_input_.InputUpdate(frame_second);

	if (player_ == nullptr) return;
	if (player_shot_event_interface_ == nullptr) return;

	if (!shot_key_input_.IsPressed()) return;
	player_shot_event_interface_->ShotPlayerEvent(player_);
}