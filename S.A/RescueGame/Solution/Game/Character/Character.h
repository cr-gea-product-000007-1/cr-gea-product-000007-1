#pragma once
#include "System/Task.h"
#include "System/Pass.h"
#include "System/Direction.h"
#include "System/DefaultConstant.h"
#include "Game/Character/ICharacterControllerEvent.h"
#include "System/GameInfo.h"
#include "Game/Graphic/BaseGraphic.h"

class Character : public Task {
public:
	/// <summary>
	/// キャラクターの種類
	/// </summary>
	enum class CharacterType {
		kNone,
		kPlayer,
		kPlayerBullet,
		kEnemy,
		kEnemyBullet,
		kShelter,
		kHeliport,
	};

	/// <summary>
	/// コンストラクタ
	/// </summary>
	Character();

	/// <summary>
	/// コンストラクタ
	/// </summary>
	Character(CharacterType);

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Character();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	virtual void Update(float frame_second);

	/// <summary>
	/// キャラクターイベントインターフェースを設定
	/// </summary>
	/// <param>キャラクターイベントインターフェース</param>
	/// <returns>なし</returns>
	void SetCharacterEventInterface(ICharacterControllerEvent* event_interface) { character_event_interface_ = event_interface; }

	/// <summary>
	/// フィールドの座標を取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>フィールドの座標</returns>
	TwoDimensional GetPosition() { return field_position_; }

	/// <summary>
	/// フィールドの座標を設定
	/// </summary>
	/// <param>フィールドの座標</param>
	/// <returns>なし</returns>
	void SetPosition(TwoDimensional pos) { field_position_ = pos; }

	/// <summary>
	/// 移動中にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Moving() { moving_ = true; }

	/// <summary>
	/// 移動中か
	/// </summary>
	/// <param>なし</param>
	/// <returns>移動中 = true</returns>
	bool IsMoving() { return moving_; }

	/// <summary>
	/// 移動スピードを取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>移動スピード</returns>
	float GetMoveSpeed() { return move_speed_; }

	/// <summary>
	/// 移動スピードを設定
	/// </summary>
	/// <param>移動スピード</param>
	/// <returns>なし</returns>
	void SetMoveSpeed(float speed) { move_speed_ = speed; }

	/// <summary>
	/// 向きを設定
	/// </summary>
	/// <param>向き</param>
	/// <returns>なし</returns>
	void SetDirection(Direction direction) { direction_ = direction; }

	/// <summary>
	/// 向きを取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>向き</returns>
	Direction GetDirection() { return direction_; }

	/// <summary>
	/// 当たり判定の半径を設定
	/// </summary>
	/// <param>向き</param>
	/// <returns>なし</returns>
	void SetRadius(float radius) { radius_ = radius; }

	/// <summary>
	/// 当たり判定の半径を取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>向き</returns>
	float GetRadius() { return radius_; }

	/// <summary>
	/// キャラクターの種類を設定
	/// </summary>
	/// <param>キャラクターの種類</param>
	/// <returns>なし</returns>
	void SetCharacterType(CharacterType character_type) { character_type_ = character_type; }

	/// <summary>
	/// キャラクターの種類を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>キャラクターの種類</returns>
	CharacterType GetCharacterType() { return character_type_; }

	/// <summary>
	/// 終了処理にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void FinalizePhase() { phase_type_ = PhaseType::kFinalize; }

	/// <summary>
	/// 処理中にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Processing() { suspended_phase_type_ = SuspendedPhaseType::kProcessing; }

	/// <summary>
	/// 処理を中断する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void ProcessingSuspended() { suspended_phase_type_ = SuspendedPhaseType::kProcessingSuspended; }

protected:
	/// <summary>
	/// 処理を継続中か
	/// </summary>
	/// <param>なし</param>
	/// <returns>処理を継続中 = true</returns>
	bool IsProcessing() { return suspended_phase_type_ == SuspendedPhaseType::kProcessing; }

	/// <summary>
	/// 描画する処理中か
	/// </summary>
	/// <param>なし</param>
	/// <returns>描画する処理中 = true</returns>
	bool IsRender() { return phase_type_ == PhaseType::kProcessing; }

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kInitialize,           //初期化
		kProcessing,           //処理
		kFinalize,             //終了
	};

	/// <summary>
	/// 処理の中断フェーズの種類
	/// </summary>
	enum class SuspendedPhaseType {
		kProcessing,           //処理
		kProcessingSuspended,  //中断
	};

	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// 処理の中断フェーズの種類
	/// </summary>
	SuspendedPhaseType suspended_phase_type_;

	/// <summary>
	/// 表示するグラフィック
	/// </summary>
	BaseGraphic* graphic_;

	/// <summary>
	/// フィールド座標
	/// </summary>
	TwoDimensional field_position_;

private:
	/// <summary>
	/// キャラクターの種類
	/// </summary>
	CharacterType character_type_;

	/// <summary>
	/// キャラクターイベントインターフェース
	/// </summary>
	ICharacterControllerEvent* character_event_interface_;

	/// <summary>
	/// 移動中か
	/// </summary>
	bool moving_;

	/// <summary>
	/// 移動スピード
	/// </summary>
	float move_speed_;

	/// <summary>
	/// 向き
	/// </summary>
	Direction direction_;

	/// <summary>
	/// 当たり判定の半径
	/// </summary>
	float radius_;
};