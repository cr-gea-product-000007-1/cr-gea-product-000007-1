#pragma once
#include "System/Task.h"
#include <vector>
#include "System/GameConstant.h"
#include "Game/Character/Character.h"
#include "Game/Character/Player.h"
#include "Game/Character/Shelter.h"
#include "Game/Character/Heliport.h"
#include "Game/Character/EnemyBase.h"
#include "System/Direction.h"
#include "Character/Character.h"
#include "Character/ICharacterCreateEvent.h"
#include "System/GameInfo.h"
#include "Game/Graphic/MultiGraphic.h"
#include <unordered_map>

typedef std::pair<int, TwoDimensional> CharacterInfo;

/// <summary>
/// マップ
/// </summary>
class Field : public Task {

private:
	/// <summary>
	/// マップのタイル1マスごとの種類
	/// </summary>
	enum class TileType {
		kRoad = 0,        //道
		kWall = 1,        //壁
		kPlayerStart = 2, //プレイヤーのスタート位置
		kEnemyStart = 3,  //敵Aのスタート位置
		kShelter = 10,    //避難所の位置

		kNone = 100,      //なし
		kError = -1,      //読込エラー
	};

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kFieldInitialize,         //フィールドの初期化
		kCharacterInitialize,     //キャラクターの初期化
		kShelterInitialize,       //避難所の初期化
		kEnemyInitialize,         //敵の初期化
		kProcessing,              //処理
		kFinalize,                //終了
		kFinalizeEnd,             //終了済み
	};

	/// <summary>
	/// csvでの識別子とMapTypeの対応表
	/// </summary>
	std::unordered_map<char, const TileType> kTileDataIds = {
		{ 'r', TileType::kRoad},
		{ 'w', TileType::kWall},
		{ 'p', TileType::kPlayerStart},
		{ 'e', TileType::kEnemyStart},
		{ 's', TileType::kShelter},
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Field();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~Field();

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// フィールドのデータを読み込む
	/// </summary>
	/// <param>なし</param>
	/// <returns>読込成功 = true</returns>
	bool LoadFieldData();

	/// <summary>
	/// プレイヤーの登録
	/// </summary>
	/// <param>キャラクター</param>
	/// <returns>なし</returns>
	void RegistrationPlayer(Player* player) { player_ = player; }

	/// <summary>
	/// ヘリポートの登録
	/// </summary>
	/// <param>ヘリポート</param>
	/// <returns>なし</returns>
	void RegistrationHeliport(Heliport* heliport) { heliport_ = heliport; }

  /// <summary>
  /// キャラクターの移動
  /// </summary>
  /// <param>移動するキャラクター</param>
  /// <param>移動量</param>
  /// <returns>移動の種類</returns>
	MoveResult MoveCharacter(Character*, TwoDimensional, MoveType = MoveType::kFreeMove);

	/// <summary>
	/// 初期化が完了したか返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>初期化が完了 = true</returns>
	bool IsInitializeEnd() { return initialize_end_; }

	/// <summary>
	/// キャラクターインターフェースを設定
	/// </summary>
	/// <param>キャラクターインターフェース</param>
	/// <returns>なし</returns>
	void SetICharacterCreateEvent(ICharacterCreateEvent* event_interface) { character_create_event_interface_ = event_interface; }

private:
	/// <summary>
	/// フィールドの初期化処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool FieldInitialize(float frame_second);

	/// <summary>
	/// プレイヤーの初期化処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool PlayerInitialize(float frame_second);

	/// <summary>
	/// 避難所の初期化処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool ShelterInitialize(float frame_second);

	/// <summary>
	/// 敵の初期化処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool EnemyInitialize(float frame_second);

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateProcessing(float frame_second);

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateFinalize(float frame_second);

	/// <summary>
	/// 現在のフェーズを変更する
	/// </summary>
	/// <param>フェーズの種類</param>
	/// <returns>なし</returns>
	void SetPhaseType(PhaseType phase_type) { now_phase = phase_type; }

	/// <summary>
	/// 指定した座標がフィールド内か
	/// </summary>
	/// <param>指定するタイル座標</param>
	/// <returns>フィールド内 = true</returns>
	bool InField(TwoDimensional pos);

	/// <summary>
	/// 指定した座標にキャラクターが通れるかを返す
	/// </summary>
  /// <param>判定するキャラクター</param>
	/// <param>指定するタイル座標</param>
	/// <returns>通れる = true</returns>
	bool CheckCharacterIsPassable(Character* character, TwoDimensional pos);

	/// <summary>
	/// 指定した座標のタイルが通れるかを返す
	/// </summary>
	/// <param>指定するタイル座標</param>
	/// <returns>通れる = true</returns>
	bool CheckTileIsPassable(TwoDimensional pos);

	/// <summary>
	/// 読み込んだ文字列のMapTypeを識別する文字を取得する
	/// </summary>
	/// <param>フェーズの種類</param>
	/// <returns>なし</returns>
	char GetMapTileIdentifier(std::string);

	/// <summary>
	/// 読み込んだ文字列の識別子を取り除いた数字を取得
	/// </summary>
	/// <param>フェーズの種類</param>
	/// <returns>なし</returns>
	int GetMapTileNumber(std::string);

	/// <summary>
	/// 避難所を生成する
	/// </summary>
	/// <param>避難所の情報</param>
	/// <returns>なし</returns>
	void CreateShelter(CharacterInfo);

private:
	/// <summary>
	/// キャラクターインターフェース
	/// </summary>
	ICharacterCreateEvent* character_create_event_interface_;

	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	PhaseType now_phase;

	/// <summary>
	/// 初期化処理が終了したか
	/// </summary>
	bool initialize_end_;

	/// <summary>
	/// タイルマップに使うグラフィックハンドル
	/// </summary>
	MultiGraphic tile_graphic_;

	/// <summary>
	/// フィールドデータ
	/// </summary>
	std::vector<std::vector<TileType>> field_data_;

	/// <summary>
	/// フィールドの大きさ
	/// </summary>
	TwoDimensional field_size_;

	/// <summary>
	/// プレイヤー
	/// </summary>
	Player* player_;

	/// <summary>
	/// 読み込んだ避難所の人数と位置
	/// </summary>
	std::vector<CharacterInfo> load_shelter_info_list_;

	/// <summary>
	/// ヘリポート
	/// </summary>
	Heliport* heliport_;

	/// <summary>
	/// 読み込んだ敵の種類と位置
	/// </summary>
	std::vector<CharacterInfo> load_enemy_info_list_;
};

