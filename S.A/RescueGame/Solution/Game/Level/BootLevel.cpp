#include "BootLevel.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
BootLevel::BootLevel() {

}

/// <summary>
/// デストラクタ
/// </summary>
BootLevel::~BootLevel() {

}

/// <summary>
/// 初期化処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool BootLevel::UpdateInitialize(float frame_second) {
	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool BootLevel::UpdateProcessing(float frame_second) {
	standby_time_ += frame_second;
	if (standby_time_ >= kOneSeconds) {
		SwitchLevel(LevelId::kTitleLevel);
	  return true;
	}
	return false;
}

/// <summary>
/// 処理中フェーズの毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void BootLevel::RenderProcessing() {
	//仮表示
	DrawString(0, 0, "BootLevel", GetColor(255,255,255));

}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool BootLevel::UpdateFinalize(float frame_second) {
	return true;

}