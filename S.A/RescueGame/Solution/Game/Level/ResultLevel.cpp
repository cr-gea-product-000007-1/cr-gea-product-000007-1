#include "ResultLevel.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
ResultLevel::ResultLevel() {

}

/// <summary>
/// デストラクタ
/// </summary>
ResultLevel::~ResultLevel() {

}

/// <summary>
/// 決定したときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultLevel::OnPushDecide() {
	SwitchLevel(LevelId::kTitleLevel);
}

/// <summary>
/// 初期化処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool ResultLevel::UpdateInitialize(float frame_second) {
	menu_controller_ = new MenuController();

	background_ = new ResultBackground();
	evaluate_ = new ResultEvaluate();

	menu_controller_->Initialize();
	menu_controller_->SetMenuControllerEvent(this);

	if (task_manager_ != nullptr) {
		task_manager_->PushTask(menu_controller_);
		task_manager_->PushTask(background_);
		task_manager_->PushTask(evaluate_);
	}
	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool ResultLevel::UpdateProcessing(float frame_second) {
	return false;
}

/// <summary>
/// 処理中フェーズの毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void ResultLevel::RenderProcessing() {
}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool ResultLevel::UpdateFinalize(float frame_second) {
	if (task_manager_ == nullptr) return false;

	switch (finish_phase_) {
	case FinishPhase::kReleaseTask:

		if (menu_controller_ != nullptr) {
			task_manager_->RemoveTask(menu_controller_);
		}
		if (background_ != nullptr) {
			task_manager_->RemoveTask(background_);
		}
		if (evaluate_ != nullptr) {
			task_manager_->RemoveTask(evaluate_);
		}
		finish_phase_ = FinishPhase::kDeleteTask;
		return false;


	case FinishPhase::kDeleteTask:
		if (menu_controller_ != nullptr) {
			delete menu_controller_;
			menu_controller_ = nullptr;
		}
		if (background_ != nullptr) {
			delete background_;
			background_ = nullptr;
		}
		if (evaluate_ != nullptr) {
			delete evaluate_;
			evaluate_ = nullptr;
		}
		return true;


	default:
		break;
	}

	return false;
}