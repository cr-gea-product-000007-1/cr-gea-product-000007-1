#pragma once
#include "Game/Character/ICharacterControllerEvent.h"
#include "Game/Character/ICharacterCreateEvent.h"
#include "Game/Character/IRescuePlayerEvent.h"
#include "Game/Character/IRescuerEvent.h"
#include "Game/Character/ITriggerPlayerEvent.h"
#include "Game/Character/IShotPlayerEvent.h"
#include "Game/Character/ITriggerPlayerBulletEvent.h"
#include "Game/Character/IShotEnemyEvent.h"
#include "Game/Character/ITriggerEnemyBulletEvent.h"
#include "System/IMenuControllerEvent.h"

/// <summary>
/// インゲームレベルで使われるインターフェースをまとめたもの
/// </summary>
class IInGameLevelEventInterface
	: public ICharacterControllerEvent
  , public ICharacterCreateEvent 
  , public IRescuePlayerEvent
  , public IRescuerEvent
	, public ITriggerPlayerEvent
	, public IShotPlayerEvent
	, public ITriggerPlayerBulletEvent
	, public IShotEnemyEvent
	, public ITriggerEnemyBulletEvent
  , public IMenuControllerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IInGameLevelEventInterface() { }

	/// <summary>
	/// キャラクターを移動させるイベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <param>移動方向</param>
	/// <returns>移動の結果</returns>
	virtual MoveResult MoveCharacter(Character*, TwoDimensional, MoveType = MoveType::kFreeMove) = 0;

	/// <summary>
	/// 避難所を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	virtual Shelter* CreateShelter() = 0;

	/// <summary>
	/// 救助者を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	virtual Rescuer* CreateRescuer() = 0;

	/// <summary>
	/// 敵を生成する
	/// </summary>
	/// <param>生成する敵のID</param>
	/// <returns>生成したキャラクター</returns>
	virtual EnemyBase* CreateEnemy(EnemyId) = 0;

	/// <summary>
	/// 接触したヘリポートに救助者を引き渡す
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	virtual void ExtraditeHeliport(Player*) = 0;

	/// <summary>
	/// 接触した避難所から救助者を救助する
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	virtual void RescueShelter(Player*) = 0;

	/// <summary>
	/// 救助中の救助者が散らばるイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	virtual void FlightRescuers(Player*) = 0;

	/// <summary>
	/// プレイヤーに救助されに行くイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void CallForHelpPlayer(Rescuer*) = 0;

	/// <summary>
	/// プレイヤーに救助されるイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void RescuePlayer(Rescuer*) = 0;

	/// <summary>
	/// プレイヤーの方向を取得する
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>プレイヤーの方向</returns>
	virtual TwoDimensional GetPlayerDirection(Rescuer*) = 0;

	/// <summary>
	/// 救助者の当たり判定イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerRescuerEvent(Rescuer*) = 0;

	/// <summary>
	/// 救助者の死亡イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	virtual void RescuerDieEvent(Rescuer*) = 0;

	/// <summary>
	/// プレイヤーの当たり判定イベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerPlayerEvent(Character*) = 0;

	/// <summary>
	/// プレイヤーの弾を発射するイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	virtual void ShotPlayerEvent(Player*) = 0;

	/// <summary>
	/// プレイヤーの弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerPlayerBulletEvent(PlayerBullet*) = 0;

	/// <summary>
	/// プレイヤーの弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void HitPlayerBulletEvent(PlayerBullet*) = 0;

	/// <summary>
	/// プレイヤーの弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void DeletePlayerBulletEvent(PlayerBullet*) = 0;

	/// <summary>
	/// 敵の弾を発射するイベント
	/// </summary>
	/// <param>敵のポインタ</param>
  /// <param>弾の種類</param>
	/// <returns>なし</returns>
	virtual void ShotEnemyEvent(EnemyBase*, EnemyBulletId) = 0;

	/// <summary>
	/// 敵の弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	virtual bool TriggerEnemyBulletEvent(EnemyBulletBase*) = 0;

	/// <summary>
	/// 敵の弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void HitEnemyBulletEvent(EnemyBulletBase*) = 0;

	/// <summary>
	/// 敵の弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	virtual void DeleteEnemyBulletEvent(EnemyBulletBase*) = 0;

	/// <summary>
	/// 決定したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushDecide() = 0;

	/// <summary>
	/// ポーズしたときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushPause() = 0;

	/// <summary>
	/// 上入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushUp() = 0;

	/// <summary>
	/// 下入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushDown() = 0;
};