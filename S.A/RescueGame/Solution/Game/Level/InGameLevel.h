#pragma once
#include "System/Level.h"
#include "Game/Field.h"
#include "IInGameLevel.h"
#include "Game/Character/Character.h"
#include "Game/Character/Player.h"
#include "Game/Character/PlayerController.h"
#include "Game/Character/PlayerBullet.h"
#include "Game/Character/Shelter.h"
#include "Game/Character/EnemyBase.h"
#include "Game/Character/EnemyBulletBase.h"
#include "System/EnemyFactory.h"
#include "System/MenuController.h"
#include "Game//UI/InGameStartUI.h"
#include "Game//UI/InGameFinishUI.h"
#include "Game//UI/InGamePauseUI.h"
#include "Game//UI/InGameBottomUI.h"
#include "Game//UI/InGameUpperUI.h"
#include "System/RescueRankFactory.h"

class InGameLevel
	: public Level
	, public IInGameLevelEventInterface {
private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class InGamePhaseType {
		kPrepare,        //準備
		kStartUi,        //スタートUI表示
		kStartUiEnd,     //スタートUI表示終了待機
		kFinishUi,       //フィニッシュUI表示
		kFinishUiEnd,    //フィニッシュUI表示終了待機
		kPlay,           //プレイ
		kPlaySuspended,  //プレイ中断
		kInGameEnd,      //戦闘終了
		kLevelChange,    //レベル遷移
		kInGameLevelEnd, //終了済み
	};

	/// <summary>
	/// 終了フェーズの種類
	/// </summary>
	enum class FinishPhase {
		kReleaseTask, //タスクを降ろす
		kDeleteTask,  //タスクを破棄する
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	InGameLevel();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~InGameLevel();

	/// <summary>
	/// キャラクターを移動させるイベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <param>移動方向</param>
	/// <returns>なし</returns>
	MoveResult MoveCharacter(Character*, TwoDimensional, MoveType = MoveType::kFreeMove) override;

	/// <summary>
	/// 避難所を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	Shelter* CreateShelter() override;

	/// <summary>
	/// 救助者を生成する
	/// </summary>
	/// <param>なし</param>
	/// <returns>生成したキャラクター</returns>
	Rescuer* CreateRescuer() override;

	/// <summary>
	/// 敵を生成する
	/// </summary>
	/// <param>生成する敵のID</param>
	/// <returns>生成したキャラクター</returns>
	EnemyBase* CreateEnemy(EnemyId) override;

	/// <summary>
	/// 接触したヘリポートに救助者を引き渡す
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	void ExtraditeHeliport(Player*) override;

	/// <summary>
	/// 接触した避難所から救助者を救助する
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>なし</returns>
	void RescueShelter(Player*) override;

	/// <summary>
	/// 救助中の救助者が散らばるイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	void FlightRescuers(Player*) override;

	/// <summary>
	/// プレイヤーに救助されに行くイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	void CallForHelpPlayer(Rescuer*) override;

	/// <summary>
	/// プレイヤーに救助されるイベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	void RescuePlayer(Rescuer*) override;

	/// <summary>
	/// プレイヤーの方向を取得する
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>プレイヤーの方向</returns>
	TwoDimensional GetPlayerDirection(Rescuer*) override;

	/// <summary>
	/// 救助者の当たり判定イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>当たった = true</returns>
	bool TriggerRescuerEvent(Rescuer*) override;

	/// <summary>
	/// 救助者の死亡イベント
	/// </summary>
	/// <param>救助者のポインタ</param>
	/// <returns>なし</returns>
	void RescuerDieEvent(Rescuer*) override;

	/// <summary>
	/// プレイヤーの当たり判定イベント
	/// </summary>
	/// <param>キャラクターのポインタ</param>
	/// <returns>当たった = true</returns>
	bool TriggerPlayerEvent(Character*) override;

	/// <summary>
	/// プレイヤーの弾を発射するイベント
	/// </summary>
	/// <param>プレイヤーのポインタ</param>
	/// <returns>なし</returns>
	void ShotPlayerEvent(Player*) override;

	/// <summary>
	/// プレイヤーの弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	bool TriggerPlayerBulletEvent(PlayerBullet*) override;

	/// <summary>
	/// プレイヤーの弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	void HitPlayerBulletEvent(PlayerBullet*) override;

	/// <summary>
	/// プレイヤーの弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	void DeletePlayerBulletEvent(PlayerBullet*) override;

	/// <summary>
	/// 敵の弾を発射するイベント
	/// </summary>
	/// <param>敵のポインタ</param>
	/// <returns>なし</returns>
	void ShotEnemyEvent(EnemyBase*, EnemyBulletId) override;

	/// <summary>
	/// 敵の弾の当たり判定イベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>当たった = true</returns>
	bool TriggerEnemyBulletEvent(EnemyBulletBase*) override;

	/// <summary>
	/// 敵の弾が何かに命中した後のイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	void HitEnemyBulletEvent(EnemyBulletBase*) override;

	/// <summary>
	/// 敵の弾を破棄するイベント
	/// </summary>
	/// <param>弾のポインタ</param>
	/// <returns>なし</returns>
	void DeleteEnemyBulletEvent(EnemyBulletBase*) override;

	/// <summary>
	/// 決定したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDecide() override;

	/// <summary>
	/// ポーズしたときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushPause() override;

	/// <summary>
	/// 上入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushUp() override { }

	/// <summary>
	/// 下入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDown() override { }

protected:
	/// <summary>
	/// 初期化処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateInitialize(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateProcessing(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム描画処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RenderProcessing() override;

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateFinalize(float frame_second) override;

private:
	/// <summary>
	/// プレイ中にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void PlayPhase();

	/// <summary>
	/// プレイを中断する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void PlaySuspendedPhase();

	/// <summary>
	/// バトルを終了する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CharacterFinalize();

	/// <summary>
	/// カウンドダウンのイベント
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>なし</returns>
	void CountDown(float frame_second);

	/// <summary>
	/// 全ての救助者の救助が終わったイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CompleteRescuers();

	/// <summary>
	/// フィールドの生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateField();

	/// <summary>
	/// プレイヤーの生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreatePlayer();

	/// <summary>
	/// UIの生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void CreateUI();

	/// <summary>
	/// 自身が持つタスクを降ろす
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RemoveTask();

	/// <summary>
	/// 自身が作ったオブジェクトを破棄する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void DeleteMember();

	/// <summary>
  /// 接触している避難所を返す
	/// </summary>
  /// <param>接触しているか調べるフィールド座標</param>
  /// <returns>接触しているヘリポートのポインタ</returns>
	Shelter* GetTouchShelter(TwoDimensional);

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	InGamePhaseType in_game_phase_;

	/// <summary>
	/// 終了フェーズ種類
	/// </summary>
	FinishPhase finish_phase_;

	/// <summary>
	/// メニューコントローラー
	/// </summary>
	MenuController* menu_controller_;

	/// <summary>
	/// フィールド
	/// </summary>
	Field* field_;

	/// <summary>
	/// プレイヤー
	/// </summary>
	Player* player_;

	/// <summary>
	/// プレイヤーコントローラー
	/// </summary>
	PlayerController* player_controller_;

	/// <summary>
	/// プレイヤーの弾一覧
	/// </summary>
	std::vector <PlayerBullet*> player_bullet_list_;

	/// <summary>
	/// ヘリポート
	/// </summary>
	Heliport* heliport_;

	/// <summary>
	/// 避難所一覧
	/// </summary>
	std::vector<Shelter*> shelter_list_;

	/// <summary>
	/// 救助者一覧
	/// </summary>
	std::vector<Rescuer*> rescuer_list_;

	/// <summary>
	/// 敵一覧
	/// </summary>
	std::vector <EnemyBase*> enemy_list_;

	/// <summary>
	/// 敵を生成する
	/// </summary>
	EnemyFactory enemy_factory_;

	/// <summary>
	/// 救助ランクを生成する
	/// </summary>
	RescueRankFactory rescue_rank_factory_;

	/// <summary>
	/// 敵の弾一覧
	/// </summary>
	std::vector <EnemyBulletBase*> enemy_bullet_list_;

	/// <summary>
	/// 開始UI
	/// </summary>
	InGameStartUI* start_ui_;

	/// <summary>
	/// 終了UI
	/// </summary>
	InGameFinishUI* finish_ui_;

	/// <summary>
	/// ポーズUI
	/// </summary>
	InGamePauseUI* pause_ui_;

	/// <summary>
	/// 下部UI
	/// </summary>
	InGameBottomUI* bottom_ui_;

	/// <summary>
	/// 上部UI
	/// </summary>
	InGameUpperUI* upper_ui_;

	/// <summary>
	/// 全ての救助者
	/// </summary>
	int all_rescuer_num_;

	/// <summary>
	/// 救助中の救助者
	/// </summary>
	int rescue_rescuer_num_;

	/// <summary>
	/// 助けた救助者
	/// </summary>
	int complete_rescuer_num_;

	/// <summary>
	/// 死亡した救助者
	/// </summary>
	int die_rescuer_num_;

	/// <summary>
	/// 残り時間
	/// </summary>
	int remaining_time_;

	/// <summary>
	/// 蓄積時間
	/// </summary>
	float accumulation_time_;
};