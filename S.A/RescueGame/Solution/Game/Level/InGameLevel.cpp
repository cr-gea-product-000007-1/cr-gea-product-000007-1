#include "InGameLevel.h"
#include <float.h>
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
InGameLevel::InGameLevel()
	: in_game_phase_()
	, finish_phase_()
	, menu_controller_(nullptr)
  , field_(nullptr)
  , player_(nullptr)
  , player_controller_(nullptr) 
  , heliport_(nullptr)
  , shelter_list_()
  , rescuer_list_()
  , enemy_list_()
  , enemy_factory_() 
	, rescue_rank_factory_()
  , enemy_bullet_list_() 
  , start_ui_(nullptr)
  , finish_ui_(nullptr)
  , pause_ui_(nullptr)
  , bottom_ui_(nullptr)
  , upper_ui_(nullptr)
  , all_rescuer_num_(0)
	, rescue_rescuer_num_(0)
	, complete_rescuer_num_(0)
	, die_rescuer_num_(0)
  , remaining_time_(0)
  , accumulation_time_(0.0f) {

}

/// <summary>
/// デストラクタ
/// </summary>
InGameLevel::~InGameLevel() {

}


//-------------------------------------------------------------------
// 継承したインターフェースのイベント
//-------------------------------------------------------------------


/// <summary>
/// キャラクターを移動させる
/// </summary>
/// <param>キャラクターのポインタ</param>
/// <param>移動方向</param>
/// <returns>移動の結果</returns>
MoveResult InGameLevel::MoveCharacter(Character* character, TwoDimensional move_position, MoveType move_type) {
	if (character == nullptr) return MoveResult::kNotMove;
	if (field_ == nullptr) return MoveResult::kNotMove;

	MoveResult move_result = field_->MoveCharacter(character, move_position, move_type);
	return move_result;
}

/// <summary>
/// 避難所を生成する
/// </summary>
/// <param>なし</param>
/// <returns>生成したキャラクター</returns>
Shelter* InGameLevel::CreateShelter() {
	if (task_manager_ == nullptr) return nullptr;

	Shelter* shelter = new Shelter();

	shelter_list_.push_back(shelter);

	task_manager_->PushTask(shelter);

	return shelter;
}

/// <summary>
/// 救助者を生成する
/// </summary>
/// <param>なし</param>
/// <returns>生成したキャラクター</returns>
Rescuer* InGameLevel::CreateRescuer() {
	if (task_manager_ == nullptr) return nullptr;

	Rescuer* rescuer = new Rescuer();
	CharacterController* character_contoller = new CharacterController();


	rescuer->SetCharacterController(character_contoller);
	rescuer->SetIRescuerEvent(this);
	rescuer->SetMoveSpeed(kRescuerMoveSpeed);
	rescuer->SetRadius(kRescuerRadius);

	character_contoller->SetCharacter(rescuer);
	character_contoller->SetICharacterControllerEvent(this);

	rescuer_list_.push_back(rescuer);

	task_manager_->PushTask(rescuer);

	all_rescuer_num_++;
	upper_ui_->SetAllRescuerNum(all_rescuer_num_);

	return rescuer;
}

/// <summary>
/// 敵を生成する
/// </summary>
/// <param>生成する敵のID</param>
/// <returns>生成したキャラクター</returns>
EnemyBase* InGameLevel::CreateEnemy(EnemyId enemy_id) {
	if (task_manager_ == nullptr) return nullptr;

	EnemyBase* enemy = enemy_factory_.CreateEnemy(enemy_id);

	CharacterController* character_contoller = new CharacterController();

	enemy->SetCharacterController(character_contoller);
	enemy->SetIShotEnemyEvent(this);

	enemy->SetMoveSpeed(kEnemyMoveSpeed);
	enemy->SetRadius(kCharacterRadius);

	character_contoller->SetCharacter(enemy);
	character_contoller->SetICharacterControllerEvent(this);

	enemy_list_.push_back(enemy);

	task_manager_->PushTask(enemy);

	return enemy;
}

/// <summary>
/// 接触したヘリポートに救助者を引き渡す
/// </summary>
/// <param>キャラクターのポインタ</param>
/// <returns>なし</returns>
void InGameLevel::ExtraditeHeliport(Player* player) {
	if (player == nullptr) return;
	if (heliport_ == nullptr) return;

	TwoDimensional player_pos = player->GetPosition();
	TwoDimensional heliport_pos = heliport_->GetPosition();

	TwoDimensional distance = heliport_pos - player_pos;

	if (!distance.NearDistance(kTouchBuildDistance)) return;

	if (!player->InRescuer()) return;

	while (player->InRescuer()) {
		Rescuer* rescuer = player->ExtraditeRescuer();
		heliport_->Evacuate(rescuer);

		rescue_rescuer_num_--;
		complete_rescuer_num_++;
	}

	if (upper_ui_ == nullptr) return;
	upper_ui_->SetRescueRescuerNum(rescue_rescuer_num_);
	upper_ui_->SetCompleteRescuerNum(complete_rescuer_num_);

	CompleteRescuers();
}

/// <summary>
/// 接触した避難所から救助者を救助する
/// </summary>
/// <param>キャラクターのポインタ</param>
/// <returns>なし</returns>
void InGameLevel::RescueShelter(Player* player) {
	if (player == nullptr) return;

	TwoDimensional player_pos = player->GetPosition();
	Shelter* shelter = GetTouchShelter(player->GetPosition());

	if (shelter == nullptr) return;
	if (!shelter->IsRescue()) return;

	Rescuer* rescuer = shelter->Rescue();

	rescuer->SetRescuerStatus(RescuerStatus::kBoarding);
	player->Rescue();
}

/// <summary>
/// 救助中の救助者が散らばるイベント
/// </summary>
/// <param>プレイヤーのポインタ</param>
/// <returns>なし</returns>
void InGameLevel::FlightRescuers(Player* player) {
	if (player == nullptr) return;

	while (player->InRescuer()) {
		Rescuer* rescuer = player->ExtraditeRescuer();

		rescuer->SetPosition(player->GetPosition());
		rescuer->SetRescuerStatus(RescuerStatus::kFlightStart);

		rescue_rescuer_num_--;
	}

	if (upper_ui_ == nullptr) return;
	upper_ui_->SetRescueRescuerNum(rescue_rescuer_num_);
}

/// <summary>
/// プレイヤーに救助されに行くイベント
/// </summary>
/// <param>救助者のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::CallForHelpPlayer(Rescuer* rescuer) {
	if (player_ == nullptr) return;
	if (!player_->IsGrounded())return;

	if (GetPlayerDirection(rescuer).NearDistance(kCallForHelpDistance)) {
		rescuer->SetRescuerStatus(RescuerStatus::kBoarding);
	}
}

/// <summary>
/// プレイヤーに救助されるイベント
/// </summary>
/// <param>救助者のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::RescuePlayer(Rescuer* rescuer) {
	if (player_ == nullptr) return;

	if (!player_->IsGrounded()) {
		rescuer->SetRescuerStatus(RescuerStatus::kFlightStart);
		return;
	}

	if (GetPlayerDirection(rescuer).NearDistance(kRescueDistance)) {
		player_->RescueRescuer(rescuer);

		rescuer->SetRescuerStatus(RescuerStatus::kRescue);

		rescue_rescuer_num_++;

		if (upper_ui_ == nullptr) return;
		upper_ui_->SetRescueRescuerNum(rescue_rescuer_num_);
	}
}

/// <summary>
/// プレイヤーの方向を取得する
/// </summary>
/// <param>救助者のポインタ</param>
/// <returns>プレイヤーの方向</returns>
TwoDimensional InGameLevel::GetPlayerDirection(Rescuer* rescuer) {
	TwoDimensional player_pos = player_->GetPosition();
	TwoDimensional rescuer_pos = rescuer->GetPosition();
	return player_pos - rescuer_pos;
}

/// <summary>
/// 救助者の当たり判定イベント
/// </summary>
/// <param>救助者のポインタ</param>
/// <returns>当たった = true</returns>
bool InGameLevel::TriggerRescuerEvent(Rescuer* rescuer) {
	TwoDimensional rescuer_pos = rescuer->GetPosition();
	float rescuer_square_radius = (float)pow(rescuer->GetRadius() / kHalfTileSize, kSquare);

	for (Character* enemy : enemy_list_) {
		TwoDimensional enemy_pos = enemy->GetPosition();
		float enemy_square_radius = (float)pow(enemy->GetRadius() / kHalfTileSize, kSquare);

		TwoDimensional enemy_distance = enemy_pos - rescuer_pos;
		float trigger_square_distance = abs(enemy_distance.GetSquareDistance() - enemy_square_radius);

		if (trigger_square_distance < rescuer_square_radius) {
			return true;
		}
	}

	for (Character* bullet : enemy_bullet_list_) {
		TwoDimensional enemy_pos = bullet->GetPosition();
		float bullet_square_radius = (float)pow(bullet->GetRadius() / kHalfTileSize, kSquare);

		TwoDimensional enemy_distance = enemy_pos - rescuer_pos;
		float trigger_square_distance = abs(enemy_distance.GetSquareDistance() - bullet_square_radius);

		if (trigger_square_distance < rescuer_square_radius) {
			return true;
		}
	}

	return false;
}

/// <summary>
/// 救助者の死亡イベント
/// </summary>
/// <param>救助者のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::RescuerDieEvent(Rescuer* rescuer) {
	if (rescuer == nullptr) return;
	if (task_manager_ == nullptr) return;

	rescuer->SetRescuerStatus(RescuerStatus::kDie);

	task_manager_->RemoveTask(rescuer);

	die_rescuer_num_++;

	CompleteRescuers();
}

/// <summary>
/// プレイヤーの当たり判定イベント
/// </summary>
/// <param>キャラクターのポインタ</param>
/// <returns>当たった = true</returns>
bool InGameLevel::TriggerPlayerEvent(Character* player) {

	TwoDimensional player_pos = player->GetPosition();
	float player_square_radius = (float)pow(player->GetRadius() / kHalfTileSize, kSquare);

	for (Character* enemy : enemy_list_) {
		TwoDimensional enemy_pos = enemy->GetPosition();
		float enemy_square_radius = (float)pow(enemy->GetRadius() / kHalfTileSize, kSquare);

		TwoDimensional enemy_distance = enemy_pos - player_pos;
		float trigger_square_distance = abs(enemy_distance.GetSquareDistance() - enemy_square_radius);

		if (trigger_square_distance < player_square_radius) {
			return true;
		}
	}

	for (Character* bullet : enemy_bullet_list_) {
		TwoDimensional bullet_pos = bullet->GetPosition();
		float bullet_square_radius = (float)pow(bullet->GetRadius() / kHalfTileSize, kSquare);

		TwoDimensional bullet_distance = bullet_pos - player_pos;
		float trigger_square_distance = abs(bullet_distance.GetSquareDistance() - bullet_square_radius);

		if (trigger_square_distance < player_square_radius) {
			return true;
		}
	}

	return false;
}

/// <summary>
/// プレイヤーの弾を発射するイベント
/// </summary>
/// <param>プレイヤーのポインタ</param>
/// <returns>なし</returns>
void InGameLevel::ShotPlayerEvent(Player* player) {
	PlayerBullet* bullet = new PlayerBullet();
	CharacterController* character_contoller = new CharacterController();

	bullet->SetPosition(player->GetPosition());
	bullet->SetDirection(player->GetDirection());

	bullet->SetCharacterController(character_contoller);
	bullet->SetMoveSpeed(kPlayerShotMoveSpeed);
	bullet->SetRadius(kBulletRadius);

	bullet->SetITriggerPlayerBulletEvent(this);

	player_bullet_list_.push_back(bullet);

	character_contoller->SetCharacter(bullet);
	character_contoller->SetICharacterControllerEvent(this);

	if (task_manager_ == nullptr) return;
	task_manager_->PushTask(bullet);
}

/// <summary>
/// プレイヤーの弾の当たり判定イベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>当たった = true</returns>
bool InGameLevel::TriggerPlayerBulletEvent(PlayerBullet* bullet) {
	TwoDimensional bullet_pos = bullet->GetPosition();
	float bullet_square_radius = (float)pow(bullet->GetRadius() / kHalfTileSize, kSquare);

	for (EnemyBase* enemy : enemy_list_) {
		TwoDimensional enemy_pos = enemy->GetPosition();
		float enemy_square_radius = (float)pow(enemy->GetRadius() / kHalfTileSize, kSquare);

		TwoDimensional enemy_distance = enemy_pos - bullet_pos;
		float trigger_square_distance = enemy_distance.GetSquareDistance();

		if (trigger_square_distance < bullet_square_radius + enemy_square_radius) {
			enemy->Sleep();
			return true;
		}
	}

	return false;
}

/// <summary>
/// プレイヤーの弾が何かに命中した後のイベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::HitPlayerBulletEvent(PlayerBullet* bullet) {
	if (bullet == nullptr) return;
	if (task_manager_ == nullptr) return;

	task_manager_->RemoveTask(bullet);

	auto bullet_it = std::find_if(player_bullet_list_.begin(), player_bullet_list_.end(),
		[bullet](PlayerBullet* other_bullet) { return bullet == other_bullet; });
	
	if (bullet_it != player_bullet_list_.end()) {
		player_bullet_list_.erase(bullet_it);
	}
}

/// <summary>
/// プレイヤーの弾を破棄するイベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::DeletePlayerBulletEvent(PlayerBullet*  bullet) {
	if (bullet == nullptr) return;

	delete bullet;
	bullet = nullptr;
}

/// <summary>
/// 敵の弾を発射するイベント
/// </summary>
/// <param>敵のポインタ</param>
/// <param>弾の種類</param>
/// <returns>なし</returns>
void InGameLevel::ShotEnemyEvent(EnemyBase* enemy, EnemyBulletId id) {
	if (enemy == nullptr) return;

	EnemyBulletBase* bullet = enemy_factory_.CreateEnemyBullet(id);

	CharacterController* character_contoller = new CharacterController();

	bullet->SetPosition(enemy->GetPosition());

	switch (id)	{
	case EnemyBulletId::kEnemyBulletA: {
		if (player_ == nullptr) break;

		TwoDimensional player_pos = player_->GetPosition();
		TwoDimensional enemy_pos = enemy->GetPosition();
		TwoDimensional move_direction = player_pos - enemy_pos;
		move_direction = move_direction.GetNormalized();
		move_direction = move_direction * kEnemyShotMoveSpeed;

		bullet->SetMoveDirection(move_direction);
		break;
	}
	default:
		break;
	}

	bullet->SetCharacterController(character_contoller);
	bullet->SetRadius(kBulletRadius);

	bullet->SetITriggerEnemyBulletEvent(this);

	enemy_bullet_list_.push_back(bullet);

	character_contoller->SetCharacter(bullet);
	character_contoller->SetICharacterControllerEvent(this);

	if (task_manager_ == nullptr) return;
	task_manager_->PushTask(bullet);
}

/// <summary>
/// 敵の弾の当たり判定イベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>当たった = true</returns>
bool InGameLevel::TriggerEnemyBulletEvent(EnemyBulletBase* bullet) {
	return false;
}

/// <summary>
/// 敵の弾が何かに命中した後のイベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::HitEnemyBulletEvent(EnemyBulletBase* bullet) {
	if (bullet == nullptr) return;
	if (task_manager_ == nullptr) return;

	task_manager_->RemoveTask(bullet);

	auto bullet_it = std::find_if(enemy_bullet_list_.begin(), enemy_bullet_list_.end(),
		[bullet](EnemyBulletBase* other_bullet) { return bullet == other_bullet; });

	if (bullet_it != enemy_bullet_list_.end()) {
		enemy_bullet_list_.erase(bullet_it);
	}
}

/// <summary>
/// 敵の弾を破棄するイベント
/// </summary>
/// <param>弾のポインタ</param>
/// <returns>なし</returns>
void InGameLevel::DeleteEnemyBulletEvent(EnemyBulletBase* bullet) {
	if (bullet == nullptr) return;

	delete bullet;
	bullet = nullptr;
}

/// <summary>
/// 決定したときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::OnPushDecide() {
	if (in_game_phase_ == InGamePhaseType::kStartUiEnd) {
		if (task_manager_ == nullptr) return;
		if (start_ui_ == nullptr) return;

		PlayPhase();

		task_manager_->RemoveTask(start_ui_);

		in_game_phase_ = InGamePhaseType::kPlay;
	}
}

/// <summary>
/// ポーズしたときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::OnPushPause() {
	switch (in_game_phase_) {
	case InGamePhaseType::kPlay:
		PlaySuspendedPhase();

		if (task_manager_ == nullptr) return;
		task_manager_->PushTask(pause_ui_);

		in_game_phase_ = InGamePhaseType::kPlaySuspended;
		break;

	case InGamePhaseType::kPlaySuspended:
		PlayPhase();

		if (task_manager_ == nullptr) return;
		task_manager_->RemoveTask(pause_ui_);

		in_game_phase_ = InGamePhaseType::kPlay;
		break;

	default:
		break;
	}
}


//-------------------------------------------------------------------
// 各更新処理
//-------------------------------------------------------------------


/// <summary>
/// 初期化処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool InGameLevel::UpdateInitialize(float frame_second) {
	CreateUI();
	CreateField();
	CreatePlayer(); 

	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool InGameLevel::UpdateProcessing(float frame_second) {
	switch (in_game_phase_) {
	case InGamePhaseType::kPrepare: {
		if (field_ == nullptr) break;

		if (!field_->IsInitializeEnd()) break;

		remaining_time_ = kTimeLimit;
		if (upper_ui_ != nullptr) {
			upper_ui_->SetTime(remaining_time_);
		}

		in_game_phase_ = InGamePhaseType::kStartUi;
		break;
	}
	case InGamePhaseType::kStartUi:
		if (task_manager_ == nullptr) break;
		if (start_ui_ == nullptr) break;

		task_manager_->PushTask(start_ui_);

		PlaySuspendedPhase();

		in_game_phase_ = InGamePhaseType::kStartUiEnd;
		break;

	case InGamePhaseType::kStartUiEnd:
		break;

	case InGamePhaseType::kFinishUi:
		if (task_manager_ == nullptr) break;
		if (finish_ui_ == nullptr) break;

		task_manager_->PushTask(finish_ui_);

		PlaySuspendedPhase();

		in_game_phase_ = InGamePhaseType::kFinishUiEnd;
		break;

	case InGamePhaseType::kFinishUiEnd:
		if (finish_ui_->IsFinish()) {
			in_game_phase_ = InGamePhaseType::kInGameEnd;
			CharacterFinalize();
		}
		break;

	case InGamePhaseType::kPlay:
		CountDown(frame_second);
		break;

	case InGamePhaseType::kInGameEnd: {
		GameInfo* game_info = GameInfo::GetInstance();
		if (game_info != nullptr) {
			float rescue_rate = (float)complete_rescuer_num_ / (float)all_rescuer_num_;

			ResultStruct result;

			result.time_ = kTimeLimit - remaining_time_;
			result.complete_rescuer_num_ = complete_rescuer_num_;
			result.not_rescuer_num_ = all_rescuer_num_ - complete_rescuer_num_;
			result.rank_ = rescue_rank_factory_.CreateRescueRank(rescue_rate);

			game_info->SetResult(result);
		}

		in_game_phase_ = InGamePhaseType::kLevelChange;
		break;
	}
	case InGamePhaseType::kLevelChange: {
		SwitchLevel(LevelId::kResultLevel);

		in_game_phase_ = InGamePhaseType::kInGameLevelEnd;
		return true;
	}
	case InGamePhaseType::kInGameLevelEnd:
		break;

	default:
		break;
	}

	return false;
}

/// <summary>
/// 処理中フェーズの毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::RenderProcessing() {


}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool InGameLevel::UpdateFinalize(float frame_second) {
	switch (finish_phase_) {
	case FinishPhase::kReleaseTask:
		RemoveTask();
		finish_phase_ = FinishPhase::kDeleteTask;
		return false;

	case FinishPhase::kDeleteTask:
		DeleteMember();
		return true;

	default:
		break;
	}
	return false;
}


//-------------------------------------------------------------------
// 初期化で生成するキャラクター
//-------------------------------------------------------------------


/// <summary>
/// フィールドの生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::CreateField() {
	field_ = new Field();

	field_->SetICharacterCreateEvent(this);

	if (task_manager_ == nullptr) return;
	task_manager_->PushTask(field_);
}

/// <summary>
/// プレイヤーの生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::CreatePlayer() {
	player_ = new Player();
	player_controller_ = new PlayerController();
	heliport_ = new Heliport();

	player_->SetIRescuePlayerEvent(this);
	player_->SetITriggerPlayerEvent(this);

	player_->SetMoveSpeed(kPlayerMoveSpeed);
	player_->SetRadius(kCharacterRadius);

	player_controller_->Initialize();

	player_controller_->SetPlayer(player_);
	player_controller_->SetICharacterControllerEvent(this);
	player_controller_->SetIShotPlayerEvent(this);


	if (task_manager_ == nullptr) return;
	task_manager_->PushTask(player_);
	task_manager_->PushTask(player_controller_);
	task_manager_->PushTask(heliport_);

	if (field_ == nullptr) return;
	field_->RegistrationPlayer(player_);
	field_->RegistrationHeliport(heliport_);
}

/// <summary>
/// UIの生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::CreateUI() {
	menu_controller_ = new MenuController();
	menu_controller_->SetMenuControllerEvent(this);
	menu_controller_->Initialize();

	start_ui_ = new InGameStartUI();
	finish_ui_ = new InGameFinishUI();
	pause_ui_ = new InGamePauseUI();

	bottom_ui_ = new InGameBottomUI();
	bottom_ui_->Initialize();

	upper_ui_ = new InGameUpperUI();
	upper_ui_->Initialize();

	if (task_manager_ != nullptr) {
		task_manager_->PushTask(menu_controller_);
		task_manager_->PushTask(bottom_ui_);
		task_manager_->PushTask(upper_ui_);
	}
}


//-------------------------------------------------------------------
// 終了時の処理
//-------------------------------------------------------------------


/// <summary>
/// 自身が持つタスクを降ろす
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::RemoveTask() {
	if (task_manager_ == nullptr) return;

	if (menu_controller_ != nullptr) {
		task_manager_->RemoveTask(menu_controller_);
	}

	if (field_ != nullptr) {
		task_manager_->RemoveTask(field_);
	}

	if (player_ != nullptr) {
		task_manager_->RemoveTask(player_);
	}

	for (PlayerBullet* bullet : player_bullet_list_) {
		if (bullet == nullptr) continue;
		task_manager_->RemoveTask(bullet);
	}

	if (player_controller_ != nullptr) {
		task_manager_->RemoveTask(player_controller_);
	}

	if (heliport_ != nullptr) {
		task_manager_->RemoveTask(heliport_);
	}

	for (Shelter* shelter : shelter_list_) {
		if (shelter == nullptr) continue;
		task_manager_->RemoveTask(shelter);
	}

	for (Rescuer* rescuer : rescuer_list_) {
		if (rescuer == nullptr) continue;
		task_manager_->RemoveTask(rescuer);
	}

	for (EnemyBase* enemy : enemy_list_) {
		if (enemy == nullptr) continue;
		task_manager_->RemoveTask(enemy);
	}

	for (EnemyBulletBase* bullet : enemy_bullet_list_) {
		if (bullet == nullptr) continue;
		task_manager_->RemoveTask(bullet);
	}



	if (start_ui_ != nullptr) {
		task_manager_->RemoveTask(start_ui_);
	}

	if (finish_ui_ != nullptr) {
		task_manager_->RemoveTask(finish_ui_);
	}

	if (bottom_ui_ != nullptr) {
		task_manager_->RemoveTask(bottom_ui_);
	}

	if (upper_ui_ != nullptr) {
		task_manager_->RemoveTask(upper_ui_);
	}
}

/// <summary>
/// 自身が作ったオブジェクトを破棄する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::DeleteMember() {
	if (menu_controller_ != nullptr) {
		delete menu_controller_;
		menu_controller_ = nullptr;
	}

	if (field_ != nullptr) {
		delete field_;
		field_ = nullptr;
	}

	if (player_ != nullptr) {
		delete player_;
		player_ = nullptr;
	}

	if (player_controller_ != nullptr) {
		delete player_controller_;
		player_controller_ = nullptr;
	}

	for (PlayerBullet*& bullet : player_bullet_list_) {
		if (bullet != nullptr) {
			CharacterController* character_controller = bullet->GetCharacterController();
			if (character_controller != nullptr) {
				delete character_controller;
				character_controller = nullptr;
			}

			delete bullet;
			bullet = nullptr;
			
		}
	}

	if (heliport_ != nullptr) {
		delete heliport_;
		heliport_ = nullptr;
	}

	for (Shelter*& shelter : shelter_list_) {
		if (shelter != nullptr) {
			delete shelter;
			shelter = nullptr;
		}
	}

	for (Rescuer*& rescuer : rescuer_list_) {
		if (rescuer != nullptr) {
			CharacterController* character_controller = rescuer->GetCharacterController();
			if (character_controller != nullptr) {
				delete character_controller;
				character_controller = nullptr;
			}

			delete rescuer;
			rescuer = nullptr;
		}
	}

	for (EnemyBase*& enemy : enemy_list_) {
		if (enemy != nullptr) {
			CharacterController* character_controller = enemy->GetCharacterController();
			if (character_controller != nullptr) {
				delete character_controller;
				character_controller = nullptr;
			}

			enemy_factory_.DeleteEnemy(enemy);
		}
	}

	for (EnemyBulletBase*& bullet : enemy_bullet_list_) {
		if (bullet != nullptr) {
			CharacterController* character_controller = bullet->GetCharacterController();
			if (character_controller != nullptr) {
				delete character_controller;
				character_controller = nullptr;
			}

			delete bullet;
			bullet = nullptr;

		}
	}

	if (start_ui_ != nullptr) {
		delete start_ui_;
		start_ui_ = nullptr;
	}

	if (finish_ui_ != nullptr) {
		delete finish_ui_;
		finish_ui_ = nullptr;
	}

	if (pause_ui_ != nullptr) {
		delete pause_ui_;
		pause_ui_ = nullptr;
	}

	if (bottom_ui_ != nullptr) {
		delete bottom_ui_;
		bottom_ui_ = nullptr;
	}

	if (upper_ui_ != nullptr) {
		upper_ui_->DeleteGraphic();

		delete upper_ui_;
		upper_ui_ = nullptr;
	}
}


/// <summary>
/// 接触している避難所を返す
/// </summary>
/// <param>接触しているか調べるフィールド座標</param>
/// <returns>接触しているヘリポートのポインタ</returns>
Shelter* InGameLevel::GetTouchShelter(TwoDimensional pos) {

	for (Shelter* shelter : shelter_list_) {
		TwoDimensional shelter_pos = shelter->GetPosition();
		TwoDimensional distance = shelter_pos - pos;

		if (distance.NearDistance(kTouchBuildDistance)) return shelter;
	}

	return nullptr;
}

/// <summary>
/// プレイ中にする
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::PlayPhase() {
	if (player_ != nullptr) {
		player_->Processing();
	}

	for (Character* enemy : enemy_list_) {
		if (enemy != nullptr) {
			enemy->Processing();
		}
	}

	if (heliport_ != nullptr) {
		heliport_->Processing();
	}

	for (Character* shelter : shelter_list_) {
		if (shelter != nullptr) {
			shelter->Processing();
		}
	}

	for (Character* rescuer : rescuer_list_) {
		if (rescuer != nullptr) {
			rescuer->Processing();
		}
	}

	for (Character* bullet : player_bullet_list_) {
		if (bullet != nullptr) {
			bullet->Processing();
		}
	}

	for (Character* bullet : enemy_bullet_list_) {
		if (bullet != nullptr) {
			bullet->Processing();
		}
	}
}

/// <summary>
/// プレイを中断する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::PlaySuspendedPhase() {
	if (player_ != nullptr) {
		player_->ProcessingSuspended();
	}

	for (Character* enemy : enemy_list_) {
		if (enemy != nullptr) {
			enemy->ProcessingSuspended();
		}
	}

		if (heliport_ != nullptr) {
			heliport_->ProcessingSuspended();
		}

	for (Character* shelter : shelter_list_) {
		if (shelter != nullptr) {
			shelter->ProcessingSuspended();
		}
	}

	for (Character* rescuer : rescuer_list_) {
		if (rescuer != nullptr) {
			rescuer->ProcessingSuspended();
		}
	}

	for (Character* bullet : player_bullet_list_) {
		if (bullet != nullptr) {
			bullet->ProcessingSuspended();
		}
	}

	for (Character* bullet : enemy_bullet_list_) {
		if (bullet != nullptr) {
			bullet->ProcessingSuspended();
		}
	}
}

/// <summary>
/// バトルを終了する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::CharacterFinalize() {
	if (player_ != nullptr) {
		player_->FinalizePhase();
	}

	for (Character* enemy : enemy_list_) {
		if (enemy != nullptr) {
			enemy->FinalizePhase();
		}
	}

	if (heliport_ != nullptr) {
		heliport_->FinalizePhase();
	}

	for (Character* shelter : shelter_list_) {
		if (shelter != nullptr) {
			shelter->FinalizePhase();
		}
	}

	for (Character* rescuer : rescuer_list_) {
		if (rescuer != nullptr) {
			rescuer->FinalizePhase();
		}
	}

	for (Character* bullet : player_bullet_list_) {
		if (bullet != nullptr) {
			bullet->FinalizePhase();
		}
	}

	for (Character* bullet : enemy_bullet_list_) {
		if (bullet != nullptr) {
			bullet->FinalizePhase();
		}
	}

}

/// <summary>
/// カウンドダウンのイベント
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>なし</returns>
void InGameLevel::CountDown(float frame_second) {

	accumulation_time_ += frame_second;

	//1秒蓄積したらカウントを進める
	if (accumulation_time_ < kOneSeconds) return;

	remaining_time_ -= kOneSeconds;
	accumulation_time_ -= kOneSeconds;

	if (upper_ui_ != nullptr) {
		upper_ui_->SetTime(remaining_time_);
	}

	if (remaining_time_ <= kZero) {
		in_game_phase_ = InGamePhaseType::kFinishUi;
	}
}

/// <summary>
/// 全ての救助者の救助が終わったイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void InGameLevel::CompleteRescuers() {
	if (complete_rescuer_num_ + die_rescuer_num_ == all_rescuer_num_) {
		in_game_phase_ = InGamePhaseType::kFinishUi;
	}
}