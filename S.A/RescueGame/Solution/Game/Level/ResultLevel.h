#pragma once
#include "System/Level.h"
#include "System/MenuController.h"
#include "System/IMenuControllerEvent.h"
#include "Game/UI/ResultBackground.h"
#include "Game/UI/ResultEvaluate.h"

class ResultLevel
	: public Level
	, public IMenuControllerEvent {

private:

	/// <summary>
	/// 終了フェーズの種類
	/// </summary>
	enum class FinishPhase {
		kReleaseTask, //タスクを降ろす
		kDeleteTask,  //タスクを破棄する
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	ResultLevel();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~ResultLevel();

	/// <summary>
	/// 決定したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDecide() override;

	/// <summary>
	/// ポーズしたときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushPause() override { }

	/// <summary>
	/// 上入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushUp() override { }

	/// <summary>
	/// 下入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDown() override { }

protected:
	/// <summary>
	/// 初期化処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateInitialize(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateProcessing(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム描画処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RenderProcessing() override;

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateFinalize(float frame_second) override;

private:
	/// <summary>
	/// メニューコントローラー
	/// </summary>
	MenuController* menu_controller_;

	/// <summary>
	/// 終了フェーズ種類
	/// </summary>
	FinishPhase finish_phase_;

	/// <summary>
	/// 背景
	/// </summary>
	ResultBackground* background_;

	/// <summary>
	/// ゲームの評価
	/// </summary>
	ResultEvaluate* evaluate_;
};