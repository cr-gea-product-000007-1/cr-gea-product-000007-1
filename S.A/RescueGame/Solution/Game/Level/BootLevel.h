#pragma once
#include "System/Level.h"

class BootLevel : public Level {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	BootLevel();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~BootLevel();

protected:
	/// <summary>
	/// 初期化処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateInitialize(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateProcessing(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム描画処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RenderProcessing() override;

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateFinalize(float frame_second) override;

private:
	/// <summary>
	/// このレベルにいる時間
	/// </summary>
	float standby_time_;
};