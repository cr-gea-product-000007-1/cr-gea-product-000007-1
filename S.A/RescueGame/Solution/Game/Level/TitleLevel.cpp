#include "TitleLevel.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
TitleLevel::TitleLevel() 
  : menu_controller_(nullptr)
	, menu_type_(MenuType::kStart)
	, window_type_(WindowType::kNone)
	, finish_phase_(FinishPhase::kReleaseTask)
  , background_(nullptr)
  , menu_(nullptr) 
  , credit_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
TitleLevel::~TitleLevel() {

}

/// <summary>
/// 決定したときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleLevel::OnPushDecide() {
	if (task_manager_ == nullptr) return;
	if (background_ == nullptr) return;
	if (credit_ == nullptr) return;

	if (window_type_ == WindowType::kCredit) {
		window_type_ = WindowType::kNone;

		task_manager_->RemoveTask(credit_);

		background_->Processing();
		return;
	}

	switch (menu_type_) {
	case MenuType::kStart:
		Finalize();

		SwitchLevel(LevelId::kInGameLevel);
		break;

	case MenuType::kCredit:
		window_type_ = WindowType::kCredit;

		background_->ProcessingSuspended();

		task_manager_->PushTask(credit_);
		break;

	case MenuType::kExit:
		Finalize();

		SwitchLevel(LevelId::kNone);
		break;

	default:
		break;
	}
}

/// <summary>
/// ポーズしたときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleLevel::OnPushPause() {

}

/// <summary>
/// 上入力したときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleLevel::OnPushUp() {
	if (window_type_ == WindowType::kCredit) return;

	switch (menu_type_) {
	case MenuType::kStart:
		menu_type_ = MenuType::kExit;
		break;

	case MenuType::kCredit:
		menu_type_ = MenuType::kStart;
		break;

	case MenuType::kExit:
		menu_type_ = MenuType::kCredit;
		break;

	default:
		break;
	}

	if (menu_ == nullptr)return;
	menu_->SetCursorIndex((int)menu_type_);
}

/// <summary>
/// 下入力したときのイベント
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleLevel::OnPushDown() {
	if (window_type_ == WindowType::kCredit) return;

	switch (menu_type_) {
	case MenuType::kStart:
		menu_type_ = MenuType::kCredit;
		break;

	case MenuType::kCredit:
		menu_type_ = MenuType::kExit;
		break;

	case MenuType::kExit:
		menu_type_ = MenuType::kStart;
		break;

	default:
		break;
	}

	if (menu_ == nullptr)return;
	menu_->SetCursorIndex((int)menu_type_);
}

/// <summary>
/// 初期化処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool TitleLevel::UpdateInitialize(float frame_second) {
	menu_controller_ = new MenuController();

	background_ = new TitleBackground();
	menu_ = new TitleMenuUI();
	credit_ = new TitleCredit();

	menu_controller_->Initialize();
	menu_controller_->SetMenuControllerEvent(this);

	if (task_manager_ != nullptr) {
		task_manager_->PushTask(menu_controller_);
		task_manager_->PushTask(background_);
		task_manager_->PushTask(menu_);
	}
	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool TitleLevel::UpdateProcessing(float frame_second) {
	return false;
}

/// <summary>
/// 処理中フェーズの毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TitleLevel::RenderProcessing() {
}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool TitleLevel::UpdateFinalize(float frame_second) {
	if (task_manager_ == nullptr) return false;

	switch (finish_phase_) {
	case FinishPhase::kReleaseTask:

		if (menu_controller_ != nullptr) {
			task_manager_->RemoveTask(menu_controller_);
		}
		if (background_ != nullptr) {
			task_manager_->RemoveTask(background_);
		}
		if (menu_ != nullptr) {
			task_manager_->RemoveTask(menu_);
		}
		finish_phase_ = FinishPhase::kDeleteTask;
		return false;


	case FinishPhase::kDeleteTask:
		if (menu_controller_ != nullptr) {
			delete menu_controller_;
			menu_controller_ = nullptr;
		}
		if (background_ != nullptr) {
			delete background_;
			background_ = nullptr;
		}
		if (menu_ != nullptr) {
			delete menu_;
			menu_ = nullptr;
		}
		return true;


	default:
		break;
	}

	return false;
}