#pragma once
#include "System/Level.h"
#include "System/MenuController.h"
#include "System/IMenuControllerEvent.h"
#include "Game/UI/TitleBackground.h"
#include "Game/UI/TitleMenuUI.h"
#include "Game/UI/TitleCredit.h"

class TitleLevel 
	: public Level 
	, public IMenuControllerEvent {

private:
	/// <summary>
	/// メニューの種類
	/// </summary>
	enum class MenuType {
		kStart,      //ゲームスタート
		kCredit,     //クレジット
		kExit,       //終了
	};

	/// <summary>
	/// ウインドウの種類
	/// </summary>
	enum class WindowType {
		kNone,    //なし
		kCredit,  //クレジット
	};

	/// <summary>
	/// 終了フェーズの種類
	/// </summary>
	enum class FinishPhase {
		kReleaseTask, //タスクを降ろす
		kDeleteTask,  //タスクを破棄する
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	TitleLevel();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~TitleLevel();

	/// <summary>
	/// 決定したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDecide() override;

	/// <summary>
	/// ポーズしたときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushPause() override;

	/// <summary>
	/// 上入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushUp() override;

	/// <summary>
	/// 下入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void OnPushDown() override;

protected:
	/// <summary>
	/// 初期化処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateInitialize(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateProcessing(float frame_second) override;

	/// <summary>
	/// 処理中フェーズの毎フレーム描画処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RenderProcessing() override;

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	bool UpdateFinalize(float frame_second) override;

private:
	/// <summary>
	/// メニューコントローラー
	/// </summary>
	MenuController* menu_controller_;

	/// <summary>
	/// メニューの種類
	/// </summary>
	MenuType menu_type_;

	/// <summary>
	/// ウインドウの種類
	/// </summary>
	WindowType window_type_;

	/// <summary>
	/// 終了フェーズ種類
	/// </summary>
	FinishPhase finish_phase_;

	/// <summary>
	/// 背景
	/// </summary>
	TitleBackground* background_;

	/// <summary>
	/// メニュー
	/// </summary>
	TitleMenuUI* menu_;

	/// <summary>
	/// クレジット
	/// </summary>
	TitleCredit* credit_;
};