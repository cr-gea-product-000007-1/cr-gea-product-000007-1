#include "SingleGraphic.h"


/// <summary>
/// コンストラクタ
/// </summary>
SingleGraphic::SingleGraphic()
	: BaseGraphic() {

}

/// <summary>
/// デストラクタ
/// </summary>
SingleGraphic::~SingleGraphic() {

}

/// <summary>
/// 描画する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void SingleGraphic::Render(Layer layer) {
	if (layer_ != layer) return;
	if (graphic_handle_list_.empty()) return;

	RenderGraphicHandle(*graphic_handle_list_.begin());
}