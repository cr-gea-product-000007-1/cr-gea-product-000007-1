#pragma once
#include <vector>
#include "BaseGraphic.h"

class AnimationGraphic : public BaseGraphic {

private:
	/// <summary>
	/// 再生状態
	/// </summary>
	enum class AnimationType {
		kNone,    //なし
		kStart,   //再生開始
		kPlay,    //再生中
		kFinish,  //再生終了
	};

	/// <summary>
	/// 終了の仕方
	/// </summary>
	enum class FinishType {
		kNone,         //なし
		kFinish,       //再生終了
		kEndContinue,  //最後のコマを続ける
		kLoop,         //ループ再生
	};

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	AnimationGraphic();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~AnimationGraphic();

	/// <summary>
	/// 描画する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// コマを更新する
	/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void UpdateAnimation(float frame_second);

	/// <summary>
	/// アニメーションのコマ間隔を設定する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetAnimationRate(float rate) { animation_rate_ = rate; }

	/// <summary>
	/// アニメーションを開始する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Play();

	/// <summary>
	/// アニメーションを終了する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finish(){ animation_type_ = AnimationType::kFinish;	}

	/// <summary>
	/// アニメーションが終了しているかを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>アニメーションが終了している = true</returns>
	bool IsFinish() { return animation_type_ == AnimationType::kFinish; }

	/// <summary>
	/// ループさせる
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Loop() { finish_type_ = FinishType::kLoop; }

	/// <summary>
	/// 最後のコマを継続させる
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void EndContinue() { finish_type_ = FinishType::kEndContinue; }

private:
	/// <summary>
	/// 描画するグラフィックのインデックス
	/// </summary>
	int graphic_index_;

	/// <summary>
	/// 再生状態
	/// </summary>
	AnimationType animation_type_;

	/// <summary>
	/// アニメーションのコマ間隔
	/// </summary>
	float animation_rate_;

	/// <summary>
	/// アニメーションのコマ経過時間
	/// </summary>
	float animation_time_;

	/// <summary>
	/// 終了の仕方
	/// </summary>
	FinishType finish_type_;
};