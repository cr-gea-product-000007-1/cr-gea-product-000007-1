#pragma once
#include <vector>
#include "BaseGraphic.h"

class MultiGraphic : public BaseGraphic {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	MultiGraphic();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~MultiGraphic();

	/// <summary>
	/// 描画する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 描画するグラフィックのインデックスを設定する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetGraphicIndex(int index) { graphic_index_ = index; }

private:
	/// <summary>
	/// 描画するグラフィックのインデックス
	/// </summary>
	int graphic_index_;
};