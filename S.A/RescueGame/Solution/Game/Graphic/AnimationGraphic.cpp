#include "AnimationGraphic.h"
#include <iostream>


/// <summary>
/// コンストラクタ
/// </summary>
AnimationGraphic::AnimationGraphic()
  : BaseGraphic()
  , graphic_index_(0)
  , animation_type_(AnimationType::kNone)
  , animation_rate_(0)
  , animation_time_(0) 
  , finish_type_(FinishType::kFinish) {

}

/// <summary>
/// デストラクタ
/// </summary>
AnimationGraphic::~AnimationGraphic() {

}

/// <summary>
/// 描画する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void AnimationGraphic::Render(Layer layer) {
  if (layer_ != layer) return;
  if (animation_type_ != AnimationType::kPlay) return;
  if (graphic_index_ < kBigin) return;
  if (graphic_index_ >= (int)graphic_handle_list_.size()) return;

  RenderGraphicHandle(graphic_handle_list_[graphic_index_]);
}

/// <summary>
/// コマを更新する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
void AnimationGraphic::UpdateAnimation(float frame_second) {
  switch (animation_type_) {
  case AnimationGraphic::AnimationType::kStart:
    animation_type_ = AnimationType::kPlay;
    break;

  case AnimationGraphic::AnimationType::kPlay:
    animation_time_ += frame_second;

    if (animation_time_ >= animation_rate_) {

      if (graphic_index_ < (int)graphic_handle_list_.size()) {
        graphic_index_++;
      }

      if (graphic_index_ >= (int)graphic_handle_list_.size()) {

        switch (finish_type_) {
        case AnimationGraphic::FinishType::kFinish:
          animation_type_ = AnimationType::kFinish;
          break;

        case AnimationGraphic::FinishType::kLoop:
          Play();
          break;

        case AnimationGraphic::FinishType::kEndContinue:
          graphic_index_ = (int)graphic_handle_list_.size() - kOne;
          break;

        default:
          break;
        }
      }
    }
    break;

  case AnimationGraphic::AnimationType::kFinish:
    break;

  default:
    break;
  }
}

/// <summary>
/// アニメーションを開始する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void AnimationGraphic::Play() {
  if (animation_rate_ <= kZeroFloat) {
    std::cout << "コマ間隔が設定されていません。" << std::endl;
    return;
  }
  graphic_index_ = kBigin;
  animation_time_ = kZeroFloat;
  animation_type_ = AnimationType::kStart;
}