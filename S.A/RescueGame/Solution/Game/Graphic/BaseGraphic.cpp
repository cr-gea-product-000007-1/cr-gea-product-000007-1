#include "BaseGraphic.h"
#include "DxLib.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>



/// <summary>
/// コンストラクタ
/// </summary>
BaseGraphic::BaseGraphic() 
  : graphic_handle_list_()
  , position_()
  , pivot_rate_()
  , size_(kGraphicSize)
  , rotate_(0) 
  , layer_(Layer::kBigin) {

}

/// <summary>
/// デストラクタ
/// </summary>
BaseGraphic::~BaseGraphic() {

}

/// <summary>
/// 描画する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void BaseGraphic::Render(Layer) {

}

/// <summary>
/// 読み込んだグラフィックハンドルを破棄する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void BaseGraphic::DeleteGraphicHandle() {
	for (int handle : graphic_handle_list_) {
		DeleteGraph(handle);
	}
	graphic_handle_list_.clear();
}

/// <summary>
/// グラフィックハンドルを読み込む
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
bool BaseGraphic::LoadGraphicHandle(const char* pass) {
	int handle = LoadGraph(pass);

	if (handle == kError) return false;

	graphic_handle_list_.push_back(handle);

	return true;
}

/// <summary>
/// 分割するグラフィックハンドルを読み込む
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
bool BaseGraphic::LoadDivideGraphicHandle(const char* pass, int all_num, int x_num, int y_num, int x_size, int y_size) {
	if (all_num <= kBigin) return false;

	int* handles = new int [all_num];

	int result = LoadDivGraph(pass, all_num, x_num, y_num, x_size, y_size, handles);
	for (int i = 0; i < all_num; i++) {
		graphic_handle_list_.push_back(handles[i]);
	}

	delete[] handles;
	handles = nullptr;

	return true;
}

/// <summary>
/// 描画の基点を左上にする
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void BaseGraphic::SetUpperLeftPivotRate() {
	position_ = TwoDimensional();
}

/// <summary>
/// 描画の基点をグラフィックハンドルの1枚目の中心にする
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void BaseGraphic::SetCenterPivotRate() {
	pivot_rate_ = TwoDimensional(kHalf, kHalf);
}

/// <summary>
/// グラフィックを指定して描画する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void BaseGraphic::RenderGraphicHandle(int handle) {
	if (handle == kError) return;

	if (size_ == kGraphicSize && rotate_ == kZeroFloat) {
		//中心座標を求める
	  int width = 0, height = 0;
	  GetGraphSize(handle, &width, &height);
		TwoDimensional size(width, height);
		TwoDimensional pivot = size * pivot_rate_;
	  TwoDimensional center_position = position_ - pivot;

		DrawGraph(center_position.GetIntX(), center_position.GetIntY(), handle, true);
	}
	else {
		//中心座標を求める
		int width = 0, height = 0;
		GetGraphSize(handle, &width, &height);
		TwoDimensional size(width, height);
		TwoDimensional graph_center = size / kQuotientHalf;
		TwoDimensional pivot = size * pivot_rate_;

		TwoDimensional center_position = position_ - pivot;
		center_position = center_position + graph_center;

		DrawRotaGraph(center_position.GetIntX(), center_position.GetIntY(), size_, rotate_, handle, true, false);
	}
}