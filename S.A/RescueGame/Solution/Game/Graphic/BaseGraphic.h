#pragma once
#include <vector>
#include "System/TwoDimensional.h"
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "System/Layer.h"

class BaseGraphic {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	BaseGraphic();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~BaseGraphic();

	/// <summary>
	/// 描画する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void Render(Layer);

	/// <summary>
	/// 読み込んだグラフィックハンドルを破棄する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void DeleteGraphicHandle();

	/// <summary>
	/// グラフィックハンドルを読み込む
	/// </summary>
	/// <param>グラフィックハンドルのパス</param>
	/// <returns>なし</returns>
	bool LoadGraphicHandle(const char* pass);

	/// <summary>
	/// 分割するグラフィックハンドルを読み込む
	/// </summary>
	/// <param>グラフィックハンドルのパス</param>
	/// <param>グラフィックハンドルの総毎数</param>
	/// <param>グラフィックハンドルのX毎数</param>
	/// <param>グラフィックハンドルのY毎数</param>
	/// <param>グラフィックハンドルのXサイズ</param>
	/// <param>グラフィックハンドルのYサイズ</param>
	/// <returns>なし</returns>
	bool LoadDivideGraphicHandle(const char* pass, int all_num, int x_num, int y_num, int x_size, int y_size);

	/// <summary>
	/// 描画座標を設定する
	/// </summary>
	/// <param>描画座標</param>
	/// <returns>なし</returns>
	void SetPosition(TwoDimensional pos) { position_ = pos; }

	/// <summary>
	/// 描画座標を返す
	/// </summary>
	/// <param>描画の基点</param>
	/// <returns>なし</returns>
	TwoDimensional GetPosition() { return position_; }

	/// <summary>
	/// 描画の基点を設定する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetPivotRate(TwoDimensional pivot) { pivot_rate_ = pivot; }

	/// <summary>
	/// 描画の基点を左上にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetUpperLeftPivotRate();

	/// <summary>
	/// 描画の基点をグラフィックハンドルの1枚目の中心にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void SetCenterPivotRate();

	/// <summary>
	/// 描画の基点を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>描画の基点</returns>
	TwoDimensional GetPivotRate() { return pivot_rate_; }

	/// <summary>
	/// 大きさを設定する
	/// </summary>
	/// <param>大きさ</param>
	/// <returns>なし</returns>
	void SetSize(float size) { size_ = size; }

	/// <summary>
	/// 大きさ返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>大きさ</returns>
	float GetSize() { return size_; }

	/// <summary>
	/// 回転角度を設定する
	/// </summary>
	/// <param>回転角度</param>
	/// <returns>なし</returns>
	void SetRotate(float rotate) { rotate_ = rotate; }

	/// <summary>
	/// 回転角度を設定する
	/// </summary>
	/// <param>回転角度</param>
	/// <returns>なし</returns>
	void AddRotate(float rotate) { rotate_ += rotate; }

	/// <summary>
	/// 回転角度を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>回転角度</returns>
	float GetRotate() { return rotate_; }

	/// <summary>
	/// 表示レイヤーを設定する
	/// </summary>
	/// <param>表示レイヤー</param>
	/// <returns>なし</returns>
	void SetLayer(Layer layer) { layer_ = layer; }

	/// <summary>
	/// 表示レイヤーを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>表示レイヤー</returns>
	Layer GetLayer() { return layer_; }

protected:
	/// <summary>
	/// グラフィックを指定して描画する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void RenderGraphicHandle(int handle);

protected:
	/// <summary>
	/// グラフィックハンドル一覧
	/// </summary>
	std::vector<int> graphic_handle_list_;

	/// <summary>
	/// 描画する座標
	/// </summary>
	TwoDimensional position_;

	/// <summary>
	/// 描画の基点にするグラフィックの割合位置
	/// </summary>
	TwoDimensional pivot_rate_;

	/// <summary>
	/// 大きさ
	/// </summary>
	float size_;

	/// <summary>
	/// 回転角度
	/// </summary>
	float rotate_;

	/// <summary>
	/// 表示レイヤー
	/// </summary>
	Layer layer_;
};