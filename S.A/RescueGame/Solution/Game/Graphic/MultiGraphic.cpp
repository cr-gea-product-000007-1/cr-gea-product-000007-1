#include "MultiGraphic.h"


/// <summary>
/// コンストラクタ
/// </summary>
MultiGraphic::MultiGraphic() 
  : BaseGraphic() 
  , graphic_index_(0) {

}

/// <summary>
/// デストラクタ
/// </summary>
MultiGraphic::~MultiGraphic() {

}

/// <summary>
/// 描画する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void MultiGraphic::Render(Layer layer) {
	if (layer_ != layer) return;
	if (graphic_index_ < kBigin) return;
	if (graphic_index_ >= (int)graphic_handle_list_.size()) return;

	RenderGraphicHandle(graphic_handle_list_[graphic_index_]);
}