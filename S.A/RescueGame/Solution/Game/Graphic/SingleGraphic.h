#pragma once
#include <vector>
#include "BaseGraphic.h"

class SingleGraphic : public BaseGraphic {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	SingleGraphic();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~SingleGraphic();

	/// <summary>
	/// 描画する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;
};