#include "Field.h"
#include "DxLib.h"
#include "System/CsvLoader.h"
#include "System/Pass.h"
#include <iostream>
#include <cmath>
#include <algorithm>

/// <summary>
/// コンストラクタ
/// </summary>
Field::Field()
	: character_create_event_interface_(nullptr)
	, now_phase(PhaseType::kFieldInitialize)
	, initialize_end_(false)
	, tile_graphic_()
	, field_data_()
	, field_size_()
	, player_(nullptr)
  , load_shelter_info_list_()
  , heliport_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
Field::~Field() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Field::Update(float frame_second) {
	switch (now_phase) {
	case Field::PhaseType::kFieldInitialize:
		if (!FieldInitialize(frame_second)) break;
		SetPhaseType(PhaseType::kCharacterInitialize);
		break;

	case Field::PhaseType::kCharacterInitialize:
		if (!PlayerInitialize(frame_second)) break;
		SetPhaseType(PhaseType::kShelterInitialize);
		break;

	case Field::PhaseType::kShelterInitialize:
		if (!ShelterInitialize(frame_second)) break;
		SetPhaseType(PhaseType::kEnemyInitialize);
		break;

	case Field::PhaseType::kEnemyInitialize:
		if (!EnemyInitialize(frame_second)) break;

		initialize_end_ = true;
		SetPhaseType(PhaseType::kProcessing);
		break;

	case Field::PhaseType::kProcessing:
		UpdateProcessing(frame_second);
		break;

	case Field::PhaseType::kFinalize:
		SetPhaseType(PhaseType::kFinalizeEnd);
		break;

	case Field::PhaseType::kFinalizeEnd:
		break;
	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>描画するレイヤー</param>
/// <returns>なし</returns>
void Field::Render(Layer layer) {
	if (now_phase != Field::PhaseType::kProcessing) return;

	GameInfo* game_info = GameInfo::GetInstance();
	TwoDimensional map_upper_left;
	if (game_info != nullptr) {
		map_upper_left = game_info->GetMapUpperLeft();
	}

	for (int x = kBigin; x < (int)field_data_.size(); x++) {
		for (int y = kBigin; y < (int)field_data_[x].size(); y++) {
			TileType& field_tile = field_data_[x][y];

			TwoDimensional screen_pos(x, y);
			screen_pos = screen_pos * (float)kTileSize;
			screen_pos = screen_pos + map_upper_left;

			tile_graphic_.SetPosition(screen_pos);

			switch (field_tile) {
			case Field::TileType::kRoad:
			case Field::TileType::kPlayerStart:
			case Field::TileType::kEnemyStart:
			case Field::TileType::kShelter:
				tile_graphic_.SetGraphicIndex((int)TileType::kRoad);
				tile_graphic_.Render(layer);
				break;

			case Field::TileType::kWall:
				tile_graphic_.SetGraphicIndex((int)TileType::kWall);
				tile_graphic_.Render(layer);
				break;

			default:
				break;
			}
		}
	}
}

/// <summary>
/// フィールドのデータを読み込む
/// </summary>
/// <param>なし</param>
/// <returns>読込成功 = true</returns>
bool Field::LoadFieldData() {
	CSVLoader csv_loader = CSVLoader();
	std::vector<std::vector<std::string>> field_data_string;
	if (!csv_loader.LoadCsvFile(pass::kFieldDataPass, field_data_string)) {
		return false;
	}

	//フィールド情報の分配列を確保する
	int data_x_num = field_data_string.size();
	int data_y_num = (*field_data_string.begin()).size();
	std::vector<TileType> field_data_x(data_x_num, TileType::kNone);
	field_data_ = std::vector<std::vector<TileType>>(data_y_num, field_data_x);
		
	for (int x = 0; x < data_x_num; x++) {
		for (int y = 0; y < data_y_num; y++) {
			//マップ情報からマップタイルの識別子を取り出す
			const char data_char_id = GetMapTileIdentifier(field_data_string[x][y]);

			//データをセットする
			auto tile_type_it = kTileDataIds.find(data_char_id);
			if (tile_type_it == kTileDataIds.end()) {
				std::cout << "ファイル情報が正しくありません。" << std::endl;
				field_data_[y][x] = TileType::kError;
			}
			else {
				TileType tile_type = (*tile_type_it).second;
				field_data_[y][x] = tile_type;

				//避難所の情報
				if (tile_type == TileType::kShelter) {
					int data_num = GetMapTileNumber(field_data_string[x][y]);
					CharacterInfo load_shelter_info_{ data_num, TwoDimensional(y, x) };

					load_shelter_info_list_.push_back(load_shelter_info_);
				}

				//敵の情報
				if (tile_type == TileType::kEnemyStart) {
					int data_num = GetMapTileNumber(field_data_string[x][y]);
					CharacterInfo load_shelter_info_{ data_num, TwoDimensional(y, x) };

					load_enemy_info_list_.push_back(load_shelter_info_);
				}
			}
		}
	}

	return true;
}

/// <summary>
/// キャラクターの移動
/// </summary>
/// <param>移動するキャラクター</param>
/// <param>移動量</param>
/// <returns> 移動の結果</returns>
MoveResult Field::MoveCharacter(Character* character, TwoDimensional move_pos, MoveType move_type) {
	if (character == nullptr) return MoveResult::kNotMove;

	//元の座標
	TwoDimensional character_pos = character->GetPosition();

	//x, y方向にそれぞれ移動させたときの座標
	TwoDimensional go_to_pos_move_x(character_pos.GetX() + move_pos.GetX(), character_pos.GetY());
	TwoDimensional go_to_pos_move_y(character_pos.GetX(), character_pos.GetY() + move_pos.GetY());


	// X座標が移動できるか試す
	bool is_x_move = true;
	if (move_pos.GetX() != kZeroFloat) {
		is_x_move = CheckCharacterIsPassable(character, go_to_pos_move_x);
	}
	// Y座標が移動できるか試す
	bool is_y_move = true;
	if (move_pos.GetY() != kZeroFloat) {
		is_y_move = CheckCharacterIsPassable(character, go_to_pos_move_y);
	}

	//両方移動
	if (is_x_move && is_y_move) {
	  TwoDimensional go_to_pos = character_pos + move_pos;

		character->SetPosition(go_to_pos);

		return MoveResult::kFreeMove;
	}

	if (move_type != MoveType::kFreeMove) {
		return MoveResult::kNotMove;
	}

	// X座標だけ移動
	if (is_x_move && !is_y_move) {
		character->SetPosition(go_to_pos_move_x);

		return MoveResult::kXMove;
	}
	// Y座標移動
	if (!is_x_move && is_y_move) {
		character->SetPosition(go_to_pos_move_y);

		return MoveResult::kYMove;
	}

	return MoveResult::kNotMove;
}


/// <summary>
/// フィールドの初期化処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::FieldInitialize(float frame_second) {

	LoadFieldData();

	tile_graphic_.LoadGraphicHandle(pass::kRoadImagePass);
	tile_graphic_.LoadGraphicHandle(pass::kWallImagePass);

	tile_graphic_.SetCenterPivotRate();

	tile_graphic_.SetLayer(Layer::kBackground);

	int x_size = field_data_.size();
	int y_size = (*field_data_.begin()).size();

	GameInfo* game_info = GameInfo::GetInstance();
	if (game_info != nullptr) {
		field_size_ = TwoDimensional(x_size, y_size);
		game_info->SetMapSize(field_size_);
	}

	return true;
}

/// <summary>
/// プレイヤーの初期化処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::PlayerInitialize(float frame_second) {

	TwoDimensional player_pos(kNoneIndex, kNoneIndex);
	int player_y = kNoneIndex;

	for (int x = kBigin; x < (int)field_data_.size(); x++) {
		for (int y = kBigin; y < (int)field_data_[x].size(); y++) {
			TileType& field_tile = field_data_[x][y];

			if (field_tile != Field::TileType::kPlayerStart) continue;

			if (player_pos.GetX() != kNoneIndex || player_pos.GetY() != kNoneIndex) {
				std::cout << "プレイヤーが複数存在します。" << std::endl;
				break;
			}

			player_pos = TwoDimensional(x, y);
		}
	}

	if (player_ != nullptr) {
		player_->SetPosition(player_pos);
		player_->SetRespornPosition(player_pos);
	}

	if (heliport_ != nullptr) {
		heliport_->SetPosition(player_pos);
	}

	return true;
}

/// <summary>
/// 避難所の初期化処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::ShelterInitialize(float frame_second) {

	for (CharacterInfo load_shelter_info : load_shelter_info_list_) {
		if (!InField(load_shelter_info.second)) continue;

		CreateShelter(load_shelter_info);
	}
	load_shelter_info_list_.clear();

	return true;
}

/// <summary>
/// 敵の初期化処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::EnemyInitialize(float frame_second) {

	for (CharacterInfo enemy_info : load_enemy_info_list_) {
		if (!InField(enemy_info.second)) continue;

		EnemyBase* enemy = character_create_event_interface_->CreateEnemy((EnemyId)enemy_info.first);
		enemy->SetPosition(enemy_info.second);
	}

	load_enemy_info_list_.clear();

	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::UpdateProcessing(float frame_second) {
	return true;
}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Field::UpdateFinalize(float frame_second) {
	return true;
}

/// <summary>
/// 指定した座標がフィールド内か
/// </summary>
/// <param>指定するタイル座標</param>
/// <returns>フィールド内 = true</returns>
bool Field::InField(TwoDimensional pos) {
	int tile_pos_x = (int)std::round(pos.GetX());
	int tile_pos_y = (int)std::round(pos.GetY());

	if (tile_pos_x < kBigin) return false;
	if (tile_pos_x > field_size_.GetIntX()) return false;
	if (tile_pos_y < kBigin) return false;
	if (tile_pos_y > field_size_.GetIntY()) return false;

	return true;
}

/// <summary>
/// 指定した座標にキャラクターが通れるかを返す
/// </summary>
/// <param>判定するキャラクター</param>
/// <param>指定するタイル座標</param>
/// <returns>通れる = true</returns>
bool Field::CheckCharacterIsPassable(Character* character, TwoDimensional pos) {

	float field_radius = character->GetRadius() / kTileSize;

	std::vector<TwoDimensional> trigger_cones;

	TwoDimensional trigger_cone_bottom_right = TwoDimensional(field_radius, field_radius);
	trigger_cone_bottom_right = trigger_cone_bottom_right + pos;
	trigger_cones.push_back(trigger_cone_bottom_right);

	TwoDimensional trigger_cone_upper_right = TwoDimensional(field_radius, -field_radius);
	trigger_cone_upper_right = trigger_cone_upper_right + pos;
	trigger_cones.push_back(trigger_cone_upper_right);

	TwoDimensional trigger_cone_bottom_left = TwoDimensional(-field_radius, field_radius);
	trigger_cone_bottom_left = trigger_cone_bottom_left + pos;
	trigger_cones.push_back(trigger_cone_bottom_left);

	TwoDimensional trigger_cone_upper_left = TwoDimensional(-field_radius, -field_radius);
	trigger_cone_upper_left = trigger_cone_upper_left + pos;
	trigger_cones.push_back(trigger_cone_upper_left);


	bool is_passable = true;
	for (TwoDimensional trigger_cone : trigger_cones) {
		is_passable = is_passable && CheckTileIsPassable(trigger_cone);
	}

	return is_passable;
}

/// <summary>
/// 指定した座標のタイルが通れるかを返す
/// </summary>
/// <param>指定するタイル座標</param>
/// <returns>通れる = true</returns>
bool Field::CheckTileIsPassable(TwoDimensional pos) {
	if (!InField(pos)) return false;

	int tile_pos_x = (int)std::round(pos.GetX());
	int tile_pos_y = (int)std::round(pos.GetY());
	
	return field_data_[tile_pos_x][tile_pos_y] != TileType::kWall;
}


/// <summary>
/// 読み込んだ文字列のTileTypeを識別する文字を取得する
/// </summary>
/// <param>フェーズの種類</param>
/// <returns>なし</returns>
char Field::GetMapTileIdentifier(std::string tile_data_string) {
	//マップ情報の先頭の1文字がマップタイルの識別子
	const char data_char_id = *tile_data_string.begin();
	return data_char_id;
}

/// <summary>
/// 読み込んだ文字列の識別子を取り除いた数字を取得
/// </summary>
/// <param>フェーズの種類</param>
/// <returns>なし</returns>
int Field::GetMapTileNumber(std::string tile_data_string) {
	int data_num = 0;

	if (tile_data_string.size() < kSecondIndex) return data_num;

	tile_data_string.erase(kBigin, kOne); 
	
	bool string_isdidit = std::all_of(tile_data_string.begin(), tile_data_string.end(), [](char c) { return isdigit(c) != 0; });
	if (!string_isdidit) return data_num;

	data_num = std::stoi(tile_data_string);
	return data_num;
}

/// <summary>
/// 避難所を生成する
/// </summary>
/// <param>避難所の情報</param>
/// <returns>なし</returns>
void Field::CreateShelter(CharacterInfo shelter_info) {
	Shelter* shelter = character_create_event_interface_->CreateShelter();
	shelter->SetPosition(shelter_info.second);

	int rescuer_num = shelter_info.first;
	TwoDimensional field_pos = shelter_info.second;

	for (int i = 0; i < rescuer_num; i++) {
		Rescuer* rescuer = character_create_event_interface_->CreateRescuer();

		shelter->Evacuate(rescuer);
	}
}
