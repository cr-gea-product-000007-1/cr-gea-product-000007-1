#include <iostream>
#include "TaskManager.h"
#include "DefaultConstant.h"

/// <summary>
/// コンストラクタ
/// </summary>
TaskManager::TaskManager()
	: task_list_()
	, add_task_list_()
	, release_task_id_list_()
	, frame_update_(false) {

}

/// <summary>
/// デストラクタ
/// </summary>
TaskManager::~TaskManager() {

}

/// <summary>
/// タスクを積む
/// </summary>
/// <param>タスクのポインタ</param>
/// <returns>成功の有無</returns>
bool TaskManager::PushTask(Task* task) {
	if (task == nullptr) return false;

	int new_task_id = CreateTaskId();

	task->SetTaskId(new_task_id);
	task->ResetRelease();

	if (frame_update_) {
		add_task_list_.push_back(task);
	}
	else {
		task_list_.push_back(task);
	}
	return true;
}

/// <summary>
/// タスクを解放する
/// </summary>
/// <param>タスクのポインタ</param>
/// <returns>なし</returns>
void TaskManager::RemoveTask(Task* task) {
	int task_id = task->GetTaskId();

	auto release_tast_it = std::find_if(task_list_.begin(), task_list_.end(),
		[task_id](Task* task) { return task->GetTaskId() == task_id; });
	if (release_tast_it == task_list_.end()) return;

	Task* release_task = *release_tast_it;

	if (frame_update_) {
		release_task_id_list_.push_back(release_task->GetTaskId());
	}
	else {
	  release_task->Release();
		task_list_.erase(release_tast_it);
	}
}

/// <summary>
/// タスクを解放する
/// </summary>
/// <param>タスクID</param>
/// <returns>なし</returns>
void TaskManager::RemoveTask(int task_id) {
	auto release_tast_it = std::find_if(task_list_.begin(), task_list_.end(),
		[task_id](Task* task) { return task->GetTaskId() == task_id; });
	if (release_tast_it == task_list_.end()) return;

	Task* release_task = *release_tast_it;

	if (frame_update_) {
		release_task_id_list_.push_back(release_task->GetTaskId());
	}
	else {
		release_task->Release();
		task_list_.erase(release_tast_it);
	}
}

/// <summary>
/// 毎フレーム更新処理
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒) </param>
/// <returns>なし</returns>
void TaskManager::Update(float frame_second) {
	//タスクの更新
	frame_update_ = true;
	for (Task* task : task_list_) {
		bool task_release = task->IsRelease();
		if (task_release) continue;
		task->Update(frame_second);
	}
	frame_update_ = false;

	MovePushTask();

	ReleaseTaskId();
}

/// <summary>
/// 毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TaskManager::Render() {
	for (const Layer& layer : Layer()) {
		for (Task* task : task_list_) {
			bool task_release = task->IsRelease();
			if (task_release) continue;
			task->Render(layer);
		}
	}
}

/// <summary>
/// 追加するタスクをタスクリストに移動
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TaskManager::MovePushTask() {
	for (Task* push_task : add_task_list_) {
		task_list_.push_back(push_task);
	}
	add_task_list_.clear();
}

/// <summary>
/// 降ろすタスクをタスクリストから降ろす
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void TaskManager::ReleaseTaskId() {
	for (int task_id : release_task_id_list_) {
		auto release_tast_it = std::find_if(task_list_.begin(), task_list_.end(),
			[task_id](Task* task) { return task->GetTaskId() == task_id; });
		if (release_tast_it == task_list_.end())continue;

		Task* release_task = *release_tast_it;

		release_task->Release();

		RemoveTask(release_task);
	}
	release_task_id_list_.clear();
}


/// <summary>
/// 存在しないタスクIDを生成する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
int TaskManager::CreateTaskId() {
	for (int new_id = kOne; ; new_id++) {
		auto task_id_it = std::find_if(task_list_.begin(), task_list_.end(), [new_id](Task* task) { return task->GetTaskId() == new_id; });
		
		if (task_id_it != task_list_.end()) continue;

		auto add_task_id_it = std::find_if(add_task_list_.begin(), add_task_list_.end(), [new_id](Task* task) { return task->GetTaskId() == new_id; });
		
		if (add_task_id_it != add_task_list_.end()) continue;

		return new_id;
	}
}