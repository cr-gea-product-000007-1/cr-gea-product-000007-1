#pragma once

/// <summary>
/// 移動の種類
/// </summary>
enum class MoveType {
	kFreeMove,         //自由に移動する
	kRestrictMove,     //X,Yどちらかの軸が移動できない場合移動しない
};

/// <summary>
/// 移動の結果
/// </summary>
enum class MoveResult {
	kFreeMove,  //自由に移動可能
	kXMove,     //X座標だけ移動可能
	kYMove,     //Y座標だけ移動可能
	kNotMove,   //移動不可
};