#pragma once
#include "Direction.h"

/// <summary>
/// 敵のID
/// </summary>
enum class EnemyId {
	kEnemyAStart = 0,                                     //敵Aの最初のID（上方向に移動）
	kEnemyAEnd = kEnemyAStart + (int)Direction::kBack,    //敵Aの最後のID（左上方向に移動）
};

/// <summary>
/// 敵のID
/// </summary>
enum class EnemyBulletId{
	kEnemyBulletA,                       //真っすぐ飛ばす
};