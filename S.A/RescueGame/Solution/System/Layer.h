#pragma once
#include <iostream>

/// <summary>
/// グラフィックのレイヤー
/// </summary>
enum class Layer {
	kBackground = 0,   //背景
	kBuild = 1,        //建物
	kCharacter = 2,    //キャラクター
	kBullet = 3,       //弾
	kPlayer = 4,       //プレイヤー
	kInGameUI = 5,     //UI

	kTitleMenu = 1,    //タイトルのメニュー
	kTitleCredit = 2,  //タイトルのクレジット

	kResult = 2,       //リザルトレベルのリザルト

	kBigin = 0,        //先頭
	kEnd = 6,          //終端
	kNone = -1,        //表示しないレイヤー
};

Layer begin(Layer);

Layer end(Layer);

Layer operator* (Layer& layer);

Layer operator++ (Layer& layer);