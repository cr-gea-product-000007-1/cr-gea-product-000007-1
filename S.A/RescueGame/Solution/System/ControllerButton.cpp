#include "ControllerButton.h"
#include "DxLib.h"
#include "System/DefaultConstant.h"


/// <summary>
/// コンストラクタ
/// </summary>
ControllerButton::ControllerButton()
	: key_code_(0)
	, input_type_(InputType::kNotPush)
	, input_time_(0)
	, release_time_(0) {

}

/// <summary>
/// デストラクタ
/// </summary>
ControllerButton::~ControllerButton() {

}

/// <summary>
/// 入力処理を更新する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void ControllerButton::InputUpdate(float frame_second) {
  bool input = CheckHitKey(key_code_);

	switch (input_type_) {
	case InputType::kNotPush:
		if (!input) break;
		input_type_ = InputType::kPressed;
		break;

	case InputType::kPressed:
		if (input) {
      input_type_ = InputType::kPush;
			input_time_ = kZeroFloat;
		}
		else {
			input_type_ = InputType::kNotPush;
		}
		break;

	case InputType::kPush:
		if (input) {
			input_time_ += frame_second;
			if (input_time_ >= kLongPushTime) {
				input_type_ = InputType::kLongPush;
			}
		}
		else {
			input_type_ = InputType::kRelease;
		}
		break;

	case InputType::kLongPush:
		if (input) break;
		input_type_ = InputType::kRelease;
		break;

	case InputType::kRelease:
		if (input) {
			input_type_ = InputType::kPressed;
		}
		else {
			input_type_ = InputType::kReleaseDelay;
			release_time_ = kZeroFloat;
		}
		break;

	case InputType::kReleaseDelay:
		if (input) {
			input_type_ = InputType::kPressed;
		}
		else {
			release_time_ += frame_second;
			if (release_time_ >= kReleaseDelayTime) {
				input_type_ = InputType::kLongPush;
			}
			input_type_ = InputType::kNotPush;
		}
		break;

	default:
		break;
	}
}

/// <summary>
/// 入力されているかを返す
/// </summary>
/// <param>なし</param>
/// <returns>入力されている = true</returns>
bool ControllerButton::GetInput() {
	if (input_type_ == InputType::kPressed) return true;
	if (input_type_ == InputType::kPush) return true;
	if (input_type_ == InputType::kLongPush) return true;

	return false;
}


/// <summary>
/// 同時入力を受け付けるか返す
/// </summary>
/// <param>なし</param>
/// <returns>受け付ける = true</returns>
bool ControllerButton::GetSimultaneousInput() {
	if (input_type_ == InputType::kNotPush) return false;

	return true;
}