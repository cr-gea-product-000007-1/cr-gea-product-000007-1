#pragma once
#include "Direction.h"

class TwoDimensional {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	TwoDimensional();

	/// <summary>
	/// コンストラクタ
	/// X要素
	/// Y要素
	/// </summary>
	TwoDimensional(int x, int y);

	/// <summary>
	/// コンストラクタ
	/// X要素
	/// Y要素
	/// </summary>
	TwoDimensional(float x, float y);

	/// <summary>
	/// コンストラクタ
	/// 方向
	/// 距離
	/// </summary>
	TwoDimensional(Direction dir, float distance);

	/// <summary>
	/// デストラクタ
	/// </summary>
	~TwoDimensional();

	/// <summary>
	/// X要素を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>X要素</returns>
	float GetX() { return x_; }

	/// <summary>
	/// X要素をInt型で返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>X要素</returns>
	int GetIntX() { return (int)x_; }

	/// <summary>
	/// X要素を設定
	/// </summary>
	/// <param>X要素</param>
	/// <returns>なし</returns>
	void SetX(float x) { x_ = x; }

	/// <summary>
	/// Y要素を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>Y要素</returns>
	float GetY() { return y_; }

	/// <summary>
	/// Y要素をInt型で返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>Y要素</returns>
	int GetIntY() { return (int)y_; }

	/// <summary>
	/// Y要素を設定
	/// </summary>
	/// <param>Y要素</param>
	/// <returns>なし</returns>
	void SetY(float y) { y_ = y; }

	/// <summary>
	/// 距離を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>距離</returns>
	float GetDistance();

	/// <summary>
	/// 距離の2乗を返す
	/// 平方根処理を行わないため軽量
	/// </summary>
	/// <param>なし</param>
	/// <returns>距離の2乗</returns>
	float GetSquareDistance();

	/// <summary>
	/// 指定した距離より近いか返す
	/// </summary>
	/// <param>距離</param>
	/// <returns>近い = true</returns>
	bool NearDistance(float distance);

	/// <summary>
	/// 距離が1の同じ方向を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>距離が1の同じ方向</returns>
	TwoDimensional GetNormalized();

	/// <summary>
	/// 要素が設定されているかを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>要素が設定されている = true</returns>
	bool Empty();

private:
	/// <summary>
	/// X要素
	/// </summary>
	float x_;

	/// <summary>
	/// Y要素
	/// </summary>
	float y_;
};

/// <summary>
/// 加算演算
/// </summary>
TwoDimensional operator+ (TwoDimensional&, float);

/// <summary>
/// 加算演算
/// </summary>
TwoDimensional operator+ (TwoDimensional&, TwoDimensional&);

/// <summary>
/// 減算演算
/// </summary>
TwoDimensional operator- (TwoDimensional&, float);

/// <summary>
/// 減算演算
/// </summary>
TwoDimensional operator- (TwoDimensional&, TwoDimensional&);

/// <summary>
/// 乗算演算
/// </summary>
TwoDimensional operator* (TwoDimensional&, float);

/// <summary>
/// 乗算演算
/// </summary>
TwoDimensional operator* (TwoDimensional&, TwoDimensional&);

/// <summary>
/// 除算演算
/// </summary>
TwoDimensional operator/ (TwoDimensional&, float);

/// <summary>
/// 除算演算
/// </summary>
TwoDimensional operator/ (TwoDimensional&, TwoDimensional&);