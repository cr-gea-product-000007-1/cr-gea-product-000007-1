#include "MenuController.h"


/// <summary>
/// コンストラクタ
/// </summary>
MenuController::MenuController()
	: menu_controller_event_interface_pt_(nullptr)
	, decide_key_input()
	, pause_key_input() {

}

/// <summary>
/// デストラクタ
/// </summary>
MenuController::~MenuController() {

}

/// <summary>
/// 初期設定
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void MenuController::Initialize() {
	decide_key_input.SetKeyCode(kDecideKey);
	pause_key_input.SetKeyCode(kPauseKey);
	up_key_input.SetKeyCode(kUpKey);
	down_key_input.SetKeyCode(kDownKey);
}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void MenuController::Update(float frame_second) {
	decide_key_input.InputUpdate(frame_second);
	pause_key_input.InputUpdate(frame_second);
	up_key_input.InputUpdate(frame_second);
	down_key_input.InputUpdate(frame_second);

	if (menu_controller_event_interface_pt_ == nullptr) return;

	if (decide_key_input.IsPressed()) {
		menu_controller_event_interface_pt_->OnPushDecide();
	}

	if (pause_key_input.IsPressed()) {
		menu_controller_event_interface_pt_->OnPushPause();
	}

	if (up_key_input.IsPressed()) {
		menu_controller_event_interface_pt_->OnPushUp();
	}

	if (down_key_input.IsPressed()) {
		menu_controller_event_interface_pt_->OnPushDown();
	}
}
