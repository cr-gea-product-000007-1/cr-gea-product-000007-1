#include "RescueRankFactory.h"
#include <float.h>


/// <summary>
/// コンストラクタ
/// </summary>
RescueRankFactory::RescueRankFactory() {

}

/// <summary>
/// デストラクタ
/// </summary>
RescueRankFactory::~RescueRankFactory() {

}

/// <summary>
/// リザルトランクを生成する
/// </summary>
/// <param>救助率</param>
/// <returns>生成したリザルトランク</returns>
RescueRank RescueRankFactory::CreateRescueRank(float rescue_rate) {

  RescueRank rescue_rank(RescueRank::kNoneRank);

  if (rescue_rate >= kDiamondRate - FLT_EPSILON) {
    rescue_rank = RescueRank::kDiamondRank;
  }
  else if (rescue_rate >= kPlatinumRate - FLT_EPSILON) {
    rescue_rank = RescueRank::kPlatinumRank;
  }
  else if (rescue_rate >= kGoldRate - FLT_EPSILON) {
    rescue_rank = RescueRank::kGoldRank;
  }
  else if (rescue_rate >= kSilverRate - FLT_EPSILON) {
    rescue_rank = RescueRank::kSilverRank;
  }
  else if (rescue_rate >= kBronzeRate - FLT_EPSILON) {
    rescue_rank = RescueRank::kBronzeRank;
  }
  else {
    rescue_rank = RescueRank::kNoneRank;
  }

  return rescue_rank;
}