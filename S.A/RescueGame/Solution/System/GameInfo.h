#pragma once
#include "System/GameConstant.h"
#include "System/DefaultConstant.h"
#include "System/TwoDimensional.h"
#include "System/ResultStruct.h"

class GameInfo {

private:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	GameInfo();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~GameInfo();

public:
	/// <summary>
	/// 唯一のインスタンスを生成
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	static void CreateInstance();

	/// <summary>
	/// インスタンスを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>インスタンスのポインタ</returns>
	static GameInfo* GetInstance();

	/// <summary>
	/// 唯一のインスタンスを解放
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	static void ReleaseInstance();

	/// <summary>
	/// 初期化する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize();

	/// <summary>
	/// 解像度のサイズを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>解像度のサイズ</returns>
	TwoDimensional GetResolution() { return resolution_; }

	/// <summary>
	/// 画面の中心の座標を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>画面の中心の座標</returns>
	TwoDimensional GetScreenCenter() { return screen_center_; }

	/// <summary>
	/// マップの大きさを設定する
	/// </summary>
	/// <param>マップのサイズ</param>
	/// <returns>なし</returns>
	void SetMapSize(TwoDimensional);

	/// <summary>
	/// マップの大きさを返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>マップのの大きさ</returns>
	TwoDimensional GetMapSize() { return map_size_; }

	/// <summary>
	/// マップの左上の描画座標を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>マップの左上の描画座標</returns>
	TwoDimensional GetMapUpperLeft() { return map_upper_left_; }

	/// <summary>
	/// 今回のゲーム結果を設定
	/// </summary>
	/// <param>今回のゲーム結果</param>
	/// <returns>なし</returns>
	void SetResult(ResultStruct result) { result_ = result; }

	/// <summary>
	/// 今回のゲーム結果を返す
	/// </summary>
	/// <param>なし</param>
	/// <returns>今回のゲーム結果</returns>
	ResultStruct GetResult() { return result_; }

private:
	/// <summary>
	/// インスタンス
	/// </summary>
	static GameInfo* instance_;

	/// <summary>
	/// 解像度のサイズ
	/// </summary>
	TwoDimensional resolution_;

	/// <summary>
	/// 画面の中心の座標
	/// </summary>
	TwoDimensional screen_center_;

	/// <summary>
	/// マップの大きさ
	/// </summary>
	TwoDimensional map_size_;

	/// <summary>
	/// マップの左上の描画座標
	/// </summary>
	TwoDimensional map_upper_left_;

	/// <summary>
	/// 今回のゲーム結果
	/// </summary>
	ResultStruct result_;
};