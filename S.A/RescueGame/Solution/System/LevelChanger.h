#pragma once
#include "TaskManager.h"
#include "Level.h"
#include "LevelId.h"



/// <summary>
/// 起動時のレベル
/// </summary>
const LevelId kBootLevelId = LevelId::kBootLevel;

/// <summary>
/// レベルを変更する
/// </summary>
class LevelChanger : public Task {

public:
  /// <summary>
  /// フェーズの種類
  /// </summary>
  enum class PhaseType {
    kInitialize,  //初期化
    kProcessing,  //処理
    kFinalize,    //終了
    kFinalizeEnd, //終了済み
  };

  /// <summary>
  /// レベル遷移のフェーズの種類
  /// </summary>
  enum class LevelChangePhaseType {
    kNone,            //何もしない
    kFinalize,        //終了処理
    kFinalizeEnd,     //終了処理終了
    kCreateNextLevel, //次のレベル生成
    kInitialize,      //開始処理
    kInitializeEnd,   //開始処理終了
  };

  /// <summary>
  /// コンストラクタ
  /// </summary>
  LevelChanger();

  /// <summary>
  /// デストラクタ
  /// </summary>
  ~LevelChanger();

  /// <summary>
  /// タスクマネージャーを設定
  /// </summary>
  /// <param>タスクマネージャーのポインタ</param>
  /// <returns>なし</returns>
  void SetTaskManager(TaskManager* task_manager) { task_manager_ = task_manager; }

  /// <summary>
  /// 毎フレーム更新処理
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void Update(float frame_second) override;

  /// <summary>
  /// 毎フレーム描画処理
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Render(Layer) override;

  /// <summary>
  /// 終了フェーズに設定する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Finalize() { phase_type_ = PhaseType::kFinalize; }

  /// <summary>
  /// レベルチェンジャーが終了済みかの有無を取得する
  /// </summary>
  /// <param>なし</param>
  /// <returns>終了しているかの有無</returns>
  bool IsPhaseFinalize() { return phase_type_ == PhaseType::kFinalizeEnd; }

private:
  /// <summary>
  /// フェーズの種類を変更する
  /// </summary>
  /// <param>フェーズの種類</param>
  /// <returns>なし</returns>
  void SetPhase(PhaseType phase_type) { phase_type_ = phase_type; }

  /// <summary>
  /// レベル遷移フェーズの種類を変更する
  /// </summary>
  /// <param>レベル遷移フェーズの種類</param>
  /// <returns>なし</returns>
  void SetLevelChangePhaseType(LevelChangePhaseType level_change_phase_type) { level_change_phase_type_ = level_change_phase_type; }

  /// <summary>
  /// フェーズが終了の時の1フレームの更新内容
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateFinalize();

  /// <summary>
  /// フェーズの終了が完了の時の1フレームの更新内容
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateFinalizeEnd();

  /// <summary>
  /// 次のレベルに移る時の1フレームの更新内容
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateCreateNextLevel();

  /// <summary>
  /// フェーズが初期化の時の1フレームの更新内容
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateInitialize();

  /// <summary>
  /// フェーズが初期化が終了した時の1フレームの更新内容
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void UpdateInitializeEnd();

  /// <summary>
  /// レベル遷移フェーズを条件で変更する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void ChangeLevelChangePhaseType();

  /// <summary>
  /// レベル処理を生成する
  /// </summary>
  /// <param>生成するレベルのタスクID</param>
  /// <returns>レベル処理のポインタ</returns>
  Level* CreateLevel(LevelId);

  /// <summary>
  /// 次のレベルに移る準備
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void SetupNextLevel();

  /// <summary>
  /// 処理中のレベルのポインタを解放する
  /// </summary>
  /// <param>なし</param>
  /// <returns>解放された true</returns>
  bool DeleteNowLevel();

private:
  /// <summary>
  /// タスクマネージャーのポインタ
  /// </summary>
  TaskManager* task_manager_;

  /// <summary>
  /// フェーズの種類
  /// </summary>
  PhaseType phase_type_;

  /// <summary>
  /// レベル遷移フェーズの種類
  /// </summary>
  LevelChangePhaseType level_change_phase_type_;

  /// <summary>
  /// 処理中のレベルのポインタ
  /// </summary>
  Level* now_level_;

  /// <summary>
  /// 次に遷移するレベルのID
  /// </summary>
  LevelId next_task_id_;

};
