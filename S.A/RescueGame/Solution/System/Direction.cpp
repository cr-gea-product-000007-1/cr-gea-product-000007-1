#include "Direction.h"

Direction begin(Direction) { return Direction::kBigin; }

Direction end(Direction) { return Direction::kEnd; }

Direction operator++ (Direction& dir) {
	if (dir >= Direction::kEnd) return dir;
	return dir = Direction(std::underlying_type<Direction>::type(dir) + 1);
}