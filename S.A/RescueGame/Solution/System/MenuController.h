#pragma once
#include <unordered_map>
#include "System/Task.h"
#include "IMenuControllerEvent.h"
#include "DefaultConstant.h"
#include "ControllerButton.h"
#include "System/KeyInput.h"

class MenuController : public Task {

public:
  /// <summary>
  /// コンストラクタ
  /// </summary>
  MenuController();

  /// <summary>
  /// デストラクタ
  /// </summary>
  virtual ~MenuController();

  /// <summary>
  /// 初期設定
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Initialize();

  /// <summary>
  /// メニューコントローラーインターフェースを設定する
  /// </summary>
  /// <param>メニューコントローラーインターフェース</param>
  /// <returns>なし</returns>
  void SetMenuControllerEvent(IMenuControllerEvent* event_interface) { menu_controller_event_interface_pt_ = event_interface; }

  /// <summary>
  /// １フレームの処理を実行する
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void Update(float frame_second) override;

private:
  /// <summary>
  /// メニューコントローラーインターフェース
  /// </summary>
  IMenuControllerEvent* menu_controller_event_interface_pt_;

  /// <summary>
  /// 決定キーの入力
  /// </summary>
  ControllerButton decide_key_input;

  /// <summary>
  /// ポーズキーの入力
  /// </summary>
  ControllerButton pause_key_input;

  /// <summary>
  /// 上キーの入力
  /// </summary>
  ControllerButton up_key_input;

  /// <summary>
  /// 下キーの入力
  /// </summary>
  ControllerButton down_key_input;
};

