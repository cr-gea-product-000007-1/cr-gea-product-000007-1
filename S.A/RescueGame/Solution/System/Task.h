#pragma once
#include "System/Layer.h"

/// <summary>
/// 1秒
/// </summary>
const int kOneSeconds = 1;

/// <summary>
/// タスク1つごと
/// </summary>
class Task {

public:
  /// <summary>
  /// コンストラクタ
  /// </summary>
  Task();

  /// <summary>
  /// デストラクタ
  /// </summary>
  virtual ~Task();

  /// <summary>
  /// タスクIDを取得する
  /// </summary>
  /// <param>なし</param>
  /// <returns>タスクID</returns>
  int GetTaskId() { return task_id_; }

  /// <summary>
  /// タスクIDを設定する
  /// </summary>
  /// <param>タスクID</param>
  /// <returns>なし</returns>
  void SetTaskId(int id) { task_id_ = id; }

  /// <summary>
  /// 解放の有無を取得する
  /// </summary>
  /// <param>なし</param>
  /// <returns>解放の有無</returns>
  bool IsRelease() { return release_; }

  /// <summary>
  /// 解放を設定する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Release() { release_ = true; }

  /// <summary>
  /// 解放設定をリセットする
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void ResetRelease() { release_ = false; }

  /// <summary>
  /// １フレームの処理を実行する
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒) </param>
  /// <returns>なし</returns>
  virtual void Update(float frame_second);

  /// <summary>
  /// １フレームの描画処理を実行する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  virtual void Render(Layer);

private:

  /// <summary>
  /// タスクID
  /// タスクマネージャー内でタスクを一意にするために使用
  /// </summary>
  int task_id_;

  /// <summary>
  /// 解放の有無
  /// </summary>
  bool release_;
};

