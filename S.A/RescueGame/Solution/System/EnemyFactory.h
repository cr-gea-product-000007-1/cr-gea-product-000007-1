#pragma once
#include "Game/Character/EnemyBase.h"
#include "Game/Character/EnemyBulletBase.h"

class EnemyFactory {

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	EnemyFactory();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~EnemyFactory();

	/// <summary>
	/// 敵を生成する
	/// </summary>
	/// <param>生成する敵のID</param>
	/// <returns>生成した敵</returns>
	EnemyBase* CreateEnemy(EnemyId);

	/// <summary>
	/// 敵を破棄する
	/// </summary>
	/// <param>敵のポインタ</param>
	/// <returns>なし</returns>
	void DeleteEnemy(EnemyBase*&);

	/// <summary>
	/// 敵の弾を生成する
	/// </summary>
	/// <param>生成する敵のID</param>
	/// <returns>生成した弾</returns>
	EnemyBulletBase* CreateEnemyBullet(EnemyBulletId);

	/// <summary>
	/// 敵の弾を破棄する
	/// </summary>
	/// <param>敵のポインタ</param>
	/// <returns>なし</returns>
	void DeleteEnemyBullet(EnemyBulletBase*&);
};