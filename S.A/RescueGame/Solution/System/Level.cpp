#include "Level.h"

/// <summary>
/// コンストラクタ
/// </summary>
Level::Level()
	: phase_type_(PhaseType::kInitialize)
	, next_level_id_(LevelId::kNone)
	, error(false)
	, task_manager_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
Level::~Level() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void Level::Update(float frame_second) {
	switch (phase_type_) {
	case PhaseType::kNone:
		break;

	case PhaseType::kInitialize:
		if (!UpdateInitialize(frame_second)) break;
		SetPhase(PhaseType::kInitializeEnd);
		break;

	case PhaseType::kInitializeEnd:
		break;

	case PhaseType::kProcessing:
	case PhaseType::kFinalizeRequest:
		UpdateProcessing(frame_second);
		break;

	case PhaseType::kFinalize:
		if (!UpdateFinalize(frame_second)) break;
		SetPhase(PhaseType::kFinalizeEnd);
		break;

	case PhaseType::kFinalizeEnd:
		break;

	default:
		break;
	}
}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Level::Render(Layer) {
	if (phase_type_ != PhaseType::kProcessing) return;
	RenderProcessing();
}

/// <summary>
/// レベルを切り替える指示を出す
/// </summary>
/// <param>次のレベルのタスクID</param>
/// <returns>切り替え開始の成功の有無</returns>
bool Level::SwitchLevel(LevelId task_id) {
	next_level_id_ = task_id;
	FinishRequest();
	return true;
}

/// <summary>
/// 初期化処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Level::UpdateInitialize(float frame_second) {
	SetPhase(PhaseType::kInitializeEnd);
	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Level::UpdateProcessing(float frame_second) {
	return true;
}

/// <summary>
/// 処理中フェーズの毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Level::RenderProcessing() {

}

/// <summary>
/// 終了処理フェーズの毎フレーム更新処理
/// </summary>
/// <param>最後のフレームを完了するのに要した時間(秒)</param>
/// <returns>処理終了：true、処理継続：false</returns>
bool Level::UpdateFinalize(float frame_second) {
	SetPhase(PhaseType::kFinalizeEnd);
	return true;
}