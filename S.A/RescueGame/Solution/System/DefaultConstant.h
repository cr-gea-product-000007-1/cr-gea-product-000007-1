#pragma once


/// <summary>
/// 0
/// </summary>
const int kZero = 0;

/// <summary>
/// float型の0
/// </summary>
const float kZeroFloat = 0.0f;

/// <summary>
/// 1
/// </summary>
const int kOne = 1;

/// <summary>
/// 2
/// </summary>
const int kTwo = 2;


/// <summary>
/// 正負を反転させる
/// </summary>
const int kReverse = -1;

/// <summary>
/// 2で割って半分にする
/// </summary>
const int kQuotientHalf = 2;

/// <summary>
/// 2乗する
/// </summary>
const int kSquare = 2;

/// <summary>
/// 1の半分（0.5）
/// </summary>
const float kHalf = 0.5f;

/// <summary>
/// 桁を1つずらす
/// </summary>
const int kDigit = 10;


/// <summary>
/// 円周率
/// </summary>
const float kPi = 3.141592f;


/// <summary>
/// 小文字のa文字
/// </summary>
const char kChar_a = 'a';

/// <summary>
/// 小文字のz文字
/// </summary>
const char kChar_z = 'z';


/// <summary>
/// 配列の先頭を示すインデックス
/// </summary>
const int kBigin = 0;

/// <summary>
/// 配列の2番目を示すインデックス
/// </summary>
const int kSecondIndex = 1;

/// <summary>
/// 配列の3番目を示すインデックス
/// </summary>
const int kThirdIndex = 2;

/// <summary>
/// 配列の4番目を示すインデックス
/// </summary>
const int kFourthIndex = 3;

/// <summary>
/// 存在しないインデックス
/// </summary>
const int kNoneIndex = -1;

/// <summary>
/// インデックスを隣にずらす
/// </summary>
const int kMoveIndex = 1;


/// <summary>
/// エラーを示す数値
/// </summary>
const int kError = -1;


/// <summary>
/// 8ビットの最大数（255）
/// </summary>
const int kEightBitMax = 255;