#pragma once


/// <summary>
/// 解像度のXサイズ
/// </summary>
const int kResolutionX = 1024;

/// <summary>
/// 解像度のYサイズ
/// </summary>
const int kResolutionY = 768;


/// <summary>
/// グラフィックの初期サイズ
/// </summary>
const float kGraphicSize = 1.0f;


/// <summary>
/// プレイヤーの移動速度
/// </summary>
const float kPlayerMoveSpeed = 0.1f;

/// <summary>
/// 敵の移動速度
/// </summary>
const float kEnemyMoveSpeed = 0.08f;


/// <summary>
/// プレイヤー弾の移動速度
/// </summary>
const float kPlayerShotMoveSpeed = 0.15f;


/// <summary>
/// 敵の弾の移動速度
/// </summary>
const float kEnemyShotMoveSpeed = 0.1f;



/// <summary>
/// 救助者の移動速度
/// </summary>
const float kRescuerMoveSpeed = 0.02f;

/// <summary>
/// 救助者が救助されるプレイヤーとの距離
/// </summary>
const float kRescueDistance = 0.05f;

/// <summary>
/// 救助者が救助されにいくプレイヤーとの距離
/// </summary>
const float kCallForHelpDistance = 2.5f;

/// <summary>
/// 避難所やヘリポートと接触する距離
/// </summary>
const float kTouchBuildDistance = 1.2f;



/// <summary>
/// マップのX座標の最大値
/// </summary>
const int kFileldMaxX = 15;

/// <summary>
/// マップのY座標の最大値
/// </summary>
const int kFileldMaxY = 10;

/// <summary>
/// マップのタイルの大きさ
/// </summary>
const int kTileSize = 32;

/// <summary>
/// マップのタイルの半分の大きさ
/// </summary>
const int kHalfTileSize = 16;


/// <summary>
/// キャラクターの大きさの半径
/// </summary>
const int kCharacterRadius = 10;

/// <summary>
/// 弾の大きさの半径
/// </summary>
const int kBulletRadius = 2;

/// <summary>
/// 救助者の大きさの半径
/// </summary>
const int kRescuerRadius = 4;

/// <summary>
/// 制限時間
/// </summary>
const int kTimeLimit = 120;