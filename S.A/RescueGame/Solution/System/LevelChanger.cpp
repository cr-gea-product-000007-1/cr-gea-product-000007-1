#include "LevelChanger.h"
#include "Game/Level/BootLevel.h"
#include "Game/Level/TitleLevel.h"
#include "Game/Level/InGameLevel.h"
#include "Game/Level/ResultLevel.h"
#include <iostream>



/// <summary>
/// コンストラクタ
/// </summary>
LevelChanger::LevelChanger()
	: task_manager_(nullptr)
	, phase_type_(PhaseType::kInitialize)
	, level_change_phase_type_(LevelChangePhaseType::kNone)
	, now_level_(nullptr)
	, next_task_id_(LevelId::kNone) {

}

/// <summary>
/// デストラクタ
/// </summary>
LevelChanger::~LevelChanger() {

}

/// <summary>
/// 毎フレーム更新処理
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
/// <returns>なし</returns>
void LevelChanger::Update(float frame_second) {

	ChangeLevelChangePhaseType();

	switch (level_change_phase_type_) {
	case LevelChangePhaseType::kFinalize:
		UpdateFinalize();

		//終了処理が終了していたら、次のフェーズ
		if (!IsPhaseFinalize()) break;
		SetLevelChangePhaseType(LevelChangePhaseType::kFinalizeEnd);
		break;

	case LevelChangePhaseType::kFinalizeEnd:
		UpdateFinalizeEnd();
		break;

	case LevelChangePhaseType::kCreateNextLevel:
		UpdateCreateNextLevel();

		//レベル生成が終了していたら、次のフェーズ
		if (!IsPhaseFinalize()) break;
		SetLevelChangePhaseType(LevelChangePhaseType::kInitialize);
		break;

	case LevelChangePhaseType::kInitialize:
		UpdateInitialize();

		//レベル開始処理が終了していたら、次のフェーズ
		if (!IsPhaseFinalize()) break;
		SetLevelChangePhaseType(LevelChangePhaseType::kInitializeEnd);
		break;

	case LevelChangePhaseType::kInitializeEnd:
		UpdateInitializeEnd();

		if (now_level_ == nullptr) {
			std::cout << "レベルが存在しません。" << std::endl;
			break;
		}
		if (!now_level_->IsInitializeEnd()) break;
		SetLevelChangePhaseType(LevelChangePhaseType::kNone);
		break;

	case LevelChangePhaseType::kNone:
		if (phase_type_ == PhaseType::kInitialize) {
			phase_type_ = PhaseType::kProcessing;
		}

	default:
		break;
	}
}

/// <summary>
/// 毎フレーム描画処理
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::Render(Layer) {

}

/// <summary>
/// フェーズが終了の時の1フレームの更新内容
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::UpdateFinalize() {
	if (phase_type_ != PhaseType::kProcessing) return;
	if (now_level_ == nullptr) return;

	now_level_->Finalize();

	SetLevelChangePhaseType(LevelChangePhaseType::kFinalizeEnd);
}

/// <summary>
/// フェーズの終了が完了の時の1フレームの更新内容
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::UpdateFinalizeEnd() {
	if (phase_type_ != PhaseType::kProcessing) return;
	if (now_level_ == nullptr) return;

	bool now_level_finish = now_level_->IsPhaseFinalize();
	if (!now_level_finish) return;

	SetupNextLevel();

	//次のレベルが存在していたらレベルを作る
	if (next_task_id_ != LevelId::kNone) {
		SetLevelChangePhaseType(LevelChangePhaseType::kCreateNextLevel);
	}
	else {
		SetPhase(PhaseType::kFinalizeEnd);
		SetLevelChangePhaseType(LevelChangePhaseType::kNone);
	}
}

/// <summary>
/// 次のレベルに移る時の1フレームの更新内容
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::UpdateCreateNextLevel() {
	if (phase_type_ != PhaseType::kProcessing) return;

	if (now_level_ != nullptr) {
		delete now_level_;
		now_level_ = nullptr;
	}

	if (next_task_id_ != LevelId::kNone) {
		now_level_ = CreateLevel(next_task_id_);
		next_task_id_ = LevelId::kNone;
	}

	SetLevelChangePhaseType(LevelChangePhaseType::kInitialize);
}

/// <summary>
/// フェーズが初期化の時の1フレームの更新内容
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::UpdateInitialize() {
	if (phase_type_ != PhaseType::kProcessing) return;
	if (now_level_ == nullptr) return;

	task_manager_->PushTask(now_level_);

	now_level_->Initialize();

	SetLevelChangePhaseType(LevelChangePhaseType::kInitializeEnd);
}

/// <summary>
	/// フェーズが初期化が終了した時の1フレームの更新内容
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::UpdateInitializeEnd() {
	if (phase_type_ != PhaseType::kProcessing) return;
	if (now_level_ == nullptr) return;

	bool now_level_initialize_finish = now_level_->IsInitializeEnd();

	if (!now_level_initialize_finish) return;
	now_level_->Processing();

}

/// <summary>
/// レベル遷移フェーズを条件で変更する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::ChangeLevelChangePhaseType() {
	if (phase_type_ != PhaseType::kProcessing) return;

	//最初のレベルがない場合レベルを作る準備をする
	if (now_level_ == nullptr) {
		next_task_id_ = LevelId::kBootLevel;
		SetLevelChangePhaseType(LevelChangePhaseType::kCreateNextLevel);
	}
	//レベルを遷移し始める準備
	else {
		if (now_level_->IsFinishRequest()) {
			SetLevelChangePhaseType(LevelChangePhaseType::kFinalize);
		}
	}
}

/// <summary>
/// レベル処理を生成する
/// </summary>
/// <param>生成するレベルのタスクID</param>
/// <returns>レベル処理のポインタ</returns>
Level* LevelChanger::CreateLevel(LevelId level_id) {
	Level* return_level = nullptr;
	switch (level_id) {
	case LevelId::kBootLevel: {
		return_level = new BootLevel();
		break;
	}

	case LevelId::kTitleLevel: {
		return_level = new TitleLevel();
		break;
	}

	case LevelId::kInGameLevel: {
		return_level = new InGameLevel();
		break;
	}

	case LevelId::kResultLevel: {
		return_level = new ResultLevel();
		break;
	}

	default:
		break;
	}

	return_level->Initialize();
	return_level->SetTastManager(task_manager_);
	return return_level;
}

/// <summary>
/// 次のレベルに移る準備
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LevelChanger::SetupNextLevel() {
	if (now_level_ == nullptr) return;
	if (task_manager_ == nullptr) return;

	task_manager_->RemoveTask(now_level_);

	next_task_id_ = now_level_->NextLevelId();
}

/// <summary>
/// 処理中のレベルのポインタを解放する
/// </summary>
/// <param>なし</param>
/// <returns>解放された true</returns>
bool LevelChanger::DeleteNowLevel() {
	if (now_level_ == nullptr) return false;
	delete now_level_;
	now_level_ = nullptr;
	return true;
}
