#pragma once

enum class RescueRank {
	kDiamondRank,
	kPlatinumRank,
	kGoldRank,
	kSilverRank,
	kBronzeRank,
	kNoneRank,
};

class RescueRankFactory {

private:
	/// <summary>
	/// ダイヤモンドランクの救助率
	/// </summary>
	const float kDiamondRate = 1.0f;

	/// <summary>
	/// プラチナランクの救助率
	/// </summary>
	const float kPlatinumRate = 0.9f;

	/// <summary>
	/// ゴールドランクの救助率
	/// </summary>
	const float kGoldRate = 0.8f;

	/// <summary>
	/// シルバーランクの救助率
	/// </summary>
	const float kSilverRate = 0.7f;

	/// <summary>
	/// ブロンズランクの救助率
	/// </summary>
	const float kBronzeRate = 0.6f;

public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	RescueRankFactory();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~RescueRankFactory();

	/// <summary>
	/// 救助ランクを生成する
	/// </summary>
	/// <param>救助率</param>
	/// <returns>生成した救助ランク</returns>
	RescueRank CreateRescueRank(float);
};