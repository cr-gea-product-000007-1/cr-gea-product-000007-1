#include "EnemyFactory.h"
#include "Game/Character/EnemyA.h"
#include "Game/Character/EnemyBulletA.h"

/// <summary>
/// コンストラクタ
/// </summary>
EnemyFactory::EnemyFactory() {

}

/// <summary>
/// デストラクタ
/// </summary>
EnemyFactory::~EnemyFactory() {

}

/// <summary>
/// 敵を生成する
/// </summary>
/// <param>生成する敵のID</param>
/// <returns>生成した敵</returns>
EnemyBase* EnemyFactory::CreateEnemy(EnemyId enemy_id) {
	EnemyBase* create_enemy = nullptr;

	if (enemy_id >= EnemyId::kEnemyAStart && enemy_id <= EnemyId::kEnemyAEnd) {
		EnemyA* enemy_a =  new EnemyA();

		enemy_a->SetStartDirection((Direction)((int)enemy_id - (int)EnemyId::kEnemyAStart));

		create_enemy = enemy_a;
	}

	return create_enemy;
}

/// <summary>
/// 敵を破棄する
/// </summary>
/// <param>敵のポインタ</param>
/// <returns>なし</returns>
void EnemyFactory::DeleteEnemy(EnemyBase*& enemy) {
	delete enemy;
	enemy = nullptr;
}

/// <summary>
/// 敵の弾を生成する
/// </summary>
/// <param>生成する敵のID</param>
/// <returns>生成した弾</returns>
EnemyBulletBase* EnemyFactory::CreateEnemyBullet(EnemyBulletId bullet_id) {
	EnemyBulletBase* create_bullet = nullptr;
	switch (bullet_id)	{
	case EnemyBulletId::kEnemyBulletA: {
		EnemyBulletA* bullet_a = new EnemyBulletA();
		create_bullet = bullet_a;
		break;
	}
	default:
		break;
	}

	return create_bullet;
}

/// <summary>
/// 敵の弾を破棄する
/// </summary>
/// <param>敵のポインタ</param>
/// <returns>なし</returns>
void EnemyFactory::DeleteEnemyBullet(EnemyBulletBase*& bullet) {
	delete bullet;
	bullet = nullptr;
}