#pragma once


/// <summary>
/// 入力状態
/// </summary>
enum class InputType {
  kNotPush,       //押していない
  kPressed,       //押しはじめ
  kPush,          //押している
  kLongPush,      //長押し判定
  kRelease,       //離した
  kReleaseDelay,  //離してからの同時押し猶予判定
};

/// <summary>
/// コントローラーのボタン1つの入力
/// </summary>
class ControllerButton {

public:

  /// <summary>
  /// 長押し時間
  /// </summary>
  const float kLongPushTime = 1.0f;

  /// <summary>
  /// 離してからの同時押し猶予時間
  /// </summary>
  const float kReleaseDelayTime = 0.15f;

  /// <summary>
  /// コンストラクタ
  /// </summary>
  ControllerButton();

  /// <summary>
  /// デストラクタ
  /// </summary>
  ~ControllerButton();

  /// <summary>
  /// 入力処理を更新する
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
  /// <returns>なし</returns>
  void InputUpdate(float frame_second) ;

  /// <summary>
  /// 入力を判定するキーのキーコードを設定する
  /// </summary>
  /// <param>入力を判定するキーのキーコード</param>
  /// <returns>なし</returns>
  void SetKeyCode(int key_code) { key_code_ = key_code; }

  /// <summary>
  /// 押しはじめかを返す
  /// </summary>
  /// <param>なし</param>
  /// <returns>押しはじめか = true</returns>
  bool IsPressed() { return input_type_ == InputType::kPressed; }

  /// <summary>
  /// 入力されているかを返す
  /// </summary>
  /// <param>なし</param>
  /// <returns>入力されている = true</returns>
  bool GetInput();

  /// <summary>
  /// 同時入力を受け付けるか返す
  /// </summary>
  /// <param>なし</param>
  /// <returns>受け付ける = true</returns>
  bool GetSimultaneousInput();

  /// <summary>
  /// 同時押し猶予状態かを返す
  /// </summary>
  /// <param>なし</param>
  /// <returns>同時押し猶予状態 = true</returns>
  bool IsReleaseDelay() { return input_type_ == InputType::kReleaseDelay; }

private:

  /// <summary>
  /// 入力を判定するキーのキーコード
  /// </summary>
  int key_code_;

  /// <summary>
  /// 入力状態
  /// </summary>
  InputType input_type_;

  /// <summary>
  /// 入力時間
  /// </summary>
  float input_time_;

  /// <summary>
  /// 離してからの時間
  /// </summary>
  float release_time_;
};

