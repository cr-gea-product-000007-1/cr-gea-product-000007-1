#pragma once
#include "DxLib.h"

/// <summary>
/// コンソールの大きさのX
/// </summary>
const int kCoordX = 1000;

/// <summary>
/// コンソールの大きさのY
/// </summary>
const int kCoordY = 1000;

/// <summary>
/// ログを表示させる
/// </summary>
class LogConsore {

public:

  /// <summary>
  /// コンストラクタ
  /// </summary>
  LogConsore();

  /// <summary>
  /// デストラクタ
  /// </summary>
  ~LogConsore();

  /// <summary>
  /// コンソールを生成する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void CreateConsore();

  /// <summary>
  /// コンソールを破棄する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void DestroyConsore();

private:
  /// <summary>
  /// 通常出力するコンソール
  /// </summary>
  FILE* std_output_;

  /// <summary>
  /// エラーを出力するコンソール
  /// </summary>
  FILE* std_errput_;

};