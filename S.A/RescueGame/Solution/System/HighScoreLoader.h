#pragma once
#include <iostream>
#include <vector>
#include "ResultStruct.h"


/// <Summary>
/// バイナリファイルの読み書き
/// </Summary>
class HighScoreLoader {
public:
  /// <Summary>
  /// コンストラクタ
  /// </Summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  HighScoreLoader();

  /// <Summary>
  /// デストラクタ
  /// </Summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  ~HighScoreLoader();

  /// <Summary>
  /// ゲームのハイスコアを書き込む
  /// </Summary>
  /// <param>ゲームのハイスコア</param>
  /// <returns>書き込み成功 = true</returns>
  bool SaveHighScore(ResultStruct&);

  /// <Summary>
  /// ゲームのハイスコアを読み込む
  /// </Summary>
  /// <param>ゲームのハイスコアを書き込む参照</param>
  /// <returns>読み込み成功 = true</returns>
  bool LoadHighScore(ResultStruct&);
};