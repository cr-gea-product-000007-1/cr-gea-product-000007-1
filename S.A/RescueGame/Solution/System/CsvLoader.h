#pragma once
#include <string>
#include <vector>

/// <Summary>
/// csvファイルを読み込む
/// </Summary>
class CSVLoader {
public:
  /// <Summary>
  /// コンストラクタ
  /// </Summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  CSVLoader();

  /// <Summary>
  /// デストラクタ
  /// </Summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  ~CSVLoader();

  /// <Summary>
  /// csvファイルを読み込む
  /// </Summary>
  /// <param>読み込むcsvファイルのパス</param>
  /// <param>csvの内容を出力する配列</param>
  /// <returns>読み込み成功 = true</returns>
  bool LoadCsvFile(const char* pass, std::vector<std::vector<std::string>>& out_data);
};