#include "LogConsore.h"
#include "DxLib.h"


/// <summary>
/// コンストラクタ
/// </summary>
LogConsore::LogConsore()
	: std_output_(nullptr)
	, std_errput_(nullptr) {

}

/// <summary>
/// デストラクタ
/// </summary>
LogConsore::~LogConsore() {

}

/// <summary>
/// コンソールを生成する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LogConsore::CreateConsore() {
	AllocConsole();
	HANDLE handle = GetStdHandle(STD_INPUT_HANDLE);

	COORD coord;
	coord.X = kCoordX;
	coord.Y = kCoordY;

	SetConsoleScreenBufferSize(handle, coord);

	//====================================================
	//標準出力(stdout)をアクティブなコンソール（新しいコンソール）に書き込み設定で向ける 
	//====================================================
	freopen_s(&std_output_, "CONOUT$", "w", stdout);

	//====================================================
	//標準エラー出力(stderr)をアクティブなコンソール（新しいコンソール）に書き込み設定で向ける 
	//====================================================
	freopen_s(&std_errput_, "CONOUT$", "w", stderr);
}

/// <summary>
/// コンソールを破棄する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void LogConsore::DestroyConsore() {
//====================================================
//使用した標準出力のファイルハンドルを閉じる
//====================================================
	fclose(std_output_);

	//====================================================
	//使用した標準エラー出力のファイルハンドルを閉じる
	//====================================================
	fclose(std_errput_);

	//====================================================
	//コンソールを解放する
	//====================================================
	FreeConsole();
}