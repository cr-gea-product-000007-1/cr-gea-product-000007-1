#pragma once
#include "DxLib.h"


/// <summary>
/// 決定キーが割り当てられているキー
/// </summary>
const int kDecideKey = KEY_INPUT_RETURN;

/// <summary>
/// ポーズキーが割り当てられているキー
/// </summary>
const int kPauseKey = KEY_INPUT_ESCAPE;

/// <summary>
/// 上キーが割り当てられているキー
/// </summary>
const int kUpKey = KEY_INPUT_UP;

/// <summary>
/// 下キーが割り当てられているキー
/// </summary>
const int kDownKey = KEY_INPUT_DOWN;

/// <summary>
/// 左キーが割り当てられているキー
/// </summary>
const int kLeftKey = KEY_INPUT_LEFT;

/// <summary>
/// 右キーが割り当てられているキー
/// </summary>
const int kRightKey = KEY_INPUT_RIGHT;

/// <summary>
/// 飛行状態切り替えが割り当てられているキー
/// </summary>
const int kFlyKey = KEY_INPUT_Z;

/// <summary>
/// ショットが割り当てられているキー
/// </summary>
const int kShotKey = KEY_INPUT_SPACE;