#pragma once

namespace pass {



	//--------------------------------------------------------------------
	// データファイル
	//--------------------------------------------------------------------

	/// <summary>
	/// フィールドデータのCSVファイルのパス
	/// </summary>
	const char kFieldDataPass[] = "Assets/field_data.csv";

	/// <summary>
	/// ハイスコアデータのCSVファイルのパス
	/// </summary>
	const char	kHighScoreDataPass[] = "Assets/high_score.bin";


	//--------------------------------------------------------------------
	// インゲームオブジェクト
	//--------------------------------------------------------------------

	/// <summary>
	/// 道の画像のパス
	/// </summary>
	const char kRoadImagePass[] = "Assets/road.png";

	/// <summary>
	/// 壁の画像のパス
	/// </summary>
	const char kWallImagePass[] = "Assets/wall.png";

	/// <summary>
	/// プレイヤーの画像のパス
	/// </summary>
	const char kPlayerImagePass[] = "Assets/helicopter.png";

	/// <summary>
	/// プレイヤーの羽の画像のパス
	/// </summary>
	const char kPropellerImagePass[] = "Assets/helicopter_wing.png";

	/// <summary>
	/// 爆破の画像のパス
	/// </summary>
	const char kExplosionImagePass[] = "Assets/helicopter_explosion_effect.png";

	/// <summary>
	/// プレイヤーの弾の画像のパス
	/// </summary>
	const char kPlayerBulletImagePass[] = "Assets/bullet.png";

	/// <summary>
	/// 敵の弾の画像のパス
	/// </summary>
	const char kEnemyBulletBaseImagePass[] = "Assets/bullet.png";

	/// <summary>
	/// 避難所の画像のパス
	/// </summary>
	const char kShelterImagePass[] = "Assets/home.png";

	/// <summary>
	/// 救助者の画像のパス
	/// </summary>
	const char kRescuerImagePass[] = "Assets/dot.png";

	/// <summary>
	/// ヘリポートの画像のパス
	/// </summary>
	const char kHeliportImagePass[] = "Assets/start_and_goal.png";

	/// <summary>
	/// 敵Aの画像のパス
	/// </summary>
	const char kEnemyAImagePass[] = "Assets/enemy_a.png";

	/// <summary>
	/// 敵Bの画像のパス
	/// </summary>
	const char kEnemyBImagePass[] = "Assets/enemy_b.png";

	/// <summary>
	/// 敵cの画像のパス
	/// </summary>
	const char kEnemyCImagePass[] = "Assets/enemy_c.png";

	/// <summary>
	/// 睡眠の画像のパス
	/// </summary>
	const char kSleepImagePass[] = "Assets/enemy_sleep_effect.png";


	//--------------------------------------------------------------------
	// タイトルのUI
	//--------------------------------------------------------------------

	/// <summary>
	/// タイトルの背景の画像のパス
	/// </summary>
	const char kTitleBackgroundImagePass[] = "Assets/title_screen_background.png";

	/// <summary>
	/// タイトルの背景のヘリコプターの画像のパス
	/// </summary>
	const char kTitleHelicopterImagePass[] = "Assets/title_screen_helicopter.png";

	/// <summary>
	/// タイトルのメニューの画像のパス
	/// </summary>
	const char kTitleMenuImagePass[] = "Assets/title_screen_menu.png";

	/// <summary>
	/// タイトルのカーソルの画像のパス
	/// </summary>
	const char kTitleCursorImagePass[] = "Assets/title_screen_cursor.png";

	/// <summary>
	/// タイトルのウインドウの画像のパス
	/// </summary>
	const char kTitleCreditWindowImagePass[] = "Assets/title_screen_credit_window_frame.png";

	/// <summary>
	/// タイトルのクレジットの文字の画像のパス
	/// </summary>
	const char kTitleCreditTextImagePass[] = "Assets/title_screen_credit_window_info.png";

	/// <summary>
	/// 英語単文字の画像のパス
	/// </summary>
	const char kAlphabetImagePass[] = "Assets/title_screen_credit_font.png";

	/// <summary>
	/// タイトルのクレジットのアイコンの画像のパス
	/// </summary>
	const char kTitleCreditIconImagePass[] = "Assets/title_screen_credit_window_enter_icon_frame.png";

	//--------------------------------------------------------------------
	// インゲームのUI
	//--------------------------------------------------------------------

	/// <summary>
	/// インゲームの開始時UIの画像のパス
	/// </summary>
	const char kInGameStartUIImagePass[] = "Assets/Announcement_ForStart.png";

	/// <summary>
	/// インゲームの終了時UIの画像のパス
	/// </summary>
	const char kInGameFinishUIImagePass[] = "Assets/Announcement_ForFinish_Effect.png";

	/// <summary>
	/// インゲームのポーズ時UIの画像のパス
	/// </summary>
	const char kInGamePauseUIImagePass[] = "Assets/Announcement_ForPause.png";

	/// <summary>
	/// インゲームの下部UIの画像のパス
	/// </summary>
	const char kInGameBottomUIImagePass[] = "Assets/ingame_screen_footer.png";

	/// <summary>
	/// インゲームの上部UIの画像のパス
	/// </summary>
	const char kInGameUpperUIImagePass[] = "Assets/ingame_screen_header.png";

	/// <summary>
	/// 救助者アイコンの画像のパス
	/// </summary>
	const char kRescuerIconImagePass[] = "Assets/ingame_screen_rescue_icon.png";

	/// <summary>
	/// 大きい数字の画像のパス
	/// </summary>
	const char kBigNumberImagePass[] = "Assets/ingame_screen_big_number.png";

	/// <summary>
	/// 中くらいの数字の画像のパス
	/// </summary>
	const char kMiddleNumberImagePass[] = "Assets/ingame_screen_mid_number.png";

	/// <summary>
	/// 小さい数字の画像のパス
	/// </summary>
	const char kSmallNumberImagePass[] = "Assets/ingame_screen_small_number.png";

	//--------------------------------------------------------------------
	// リザルトのUI
	//--------------------------------------------------------------------

	/// <summary>
	/// リザルトのタイトルの画像のパス
	/// </summary>
	const char kResultTitleImagePass[] = "Assets/result_screen_title.png";

	/// <summary>
	/// リザルトのウインドウの画像のパス
	/// </summary>
	const char kResultWindowImagePass[] = "Assets/result_screen_frame.png";

	/// <summary>
	/// リザルトのキー案内の画像のパス
	/// </summary>
	const char kResultKeyInfoImagePass[] = "Assets/result_screen_push_enter_key.png";

	/// <summary>
	/// リザルトのゲームクリア時の画像のパス
	/// </summary>
	const char kResultMissionClearImagePass[] = "Assets/result_screen_mission_clear.png";

	/// <summary>
	/// リザルトのゲーム失敗の画像のパス
	/// </summary>
	const char kResultMissionNotClearImagePass[] = "Assets/result_screen_mission_failed.png";

	/// <summary>
	/// リザルトのnew recodeの画像のパス
	/// </summary>
	const char kResultNewRecordImagePass[] = "Assets/result_screen_mission_new_record.png";

	/// <summary>
	/// リザルトのダイヤモンドランクの画像のパス
	/// </summary>
	const char kResultDiamondRankImagePass[] = "Assets/result_screen_mission_rank_1.png";

	/// <summary>
	/// リザルトのプラチナランクの画像のパス
	/// </summary>
	const char kResultPlatinumRankImagePass[] = "Assets/result_screen_mission_rank_2.png";

	/// <summary>
	/// リザルトのゴールドランクの画像のパス
	/// </summary>
	const char kResultGoldRankImagePass[] = "Assets/result_screen_mission_rank_3.png";

	/// <summary>
	/// リザルトのシルバーランクの画像のパス
	/// </summary>
	const char kResultSilverRankImagePass[] = "Assets/result_screen_mission_rank_4.png";

	/// <summary>
	/// リザルトのブロンズランクの画像のパス
	/// </summary>
	const char kResultBronzeRankImagePass[] = "Assets/result_screen_mission_rank_5.png";

	/// <summary>
	/// リザルトのランク外の画像のパス
	/// </summary>
	const char kResultOutRankImagePass[] = "Assets/result_screen_mission_rank_6.png";

	/// <summary>
	/// リザルトの数字の画像のパス
	/// </summary>
	const char kResultNumberImagePass[] = "Assets/result_screen_score_number.png";
}
