#include "TwoDimensional.h"
#include "DefaultConstant.h"
#include <cmath>

/// <summary>
/// コンストラクタ
/// </summary>
TwoDimensional::TwoDimensional() 
  : x_(0.0f)
  , y_(0.0f) {

}

/// <summary>
/// コンストラクタ
/// </summary>
TwoDimensional::TwoDimensional(int x, int y)
	: x_((float)x)
	, y_((float)y) {

}

/// <summary>
/// コンストラクタ
/// </summary>
TwoDimensional::TwoDimensional(float x, float y)
	: x_(x)
	, y_(y) {

}

/// <summary>
/// コンストラクタ
/// 方向
/// 距離
/// </summary>
TwoDimensional::TwoDimensional(Direction dir, float distance)
	: x_(0.0f)
	, y_(0.0f) {
	float arc =  (float)dir / (float)Direction::kEnd * kPi * kTwo;
	x_ = std::sin(arc) * distance;
	y_ = std::cos(arc) * distance * kReverse;
}

/// <summary>
/// デストラクタ
/// </summary>
TwoDimensional::~TwoDimensional() {

}

/// <summary>
/// 距離を返す
/// </summary>
/// <param>なし</param>
/// <returns>距離</returns>
float TwoDimensional::GetDistance() {
	float square_distance = GetSquareDistance();
	float distance = sqrt(square_distance);
	return distance;
}

/// <summary>
/// 距離の2乗を返す
/// 平方根処理を行わないため軽量
/// </summary>
/// <param>なし</param>
/// <returns>距離の2乗</returns>
float TwoDimensional::GetSquareDistance() {
	float square_distance = (float)pow(x_, kSquare) + (float)pow(y_, kSquare);
	return square_distance;
}

/// <summary>
/// 距離が1の同じ方向を返す
/// </summary>
/// <param>なし</param>
/// <returns>距離が1の同じ方向</returns>
TwoDimensional TwoDimensional::GetNormalized() {
	float sum_x_y = abs(x_) + abs(y_);

	float normalized_x = abs(x_) / sum_x_y;
	float normalized_y = kOne - normalized_x;
	if (signbit(x_)) {
		normalized_x *= kReverse;
	}
	if (signbit(y_)) {
		normalized_y *= kReverse;
	}

	TwoDimensional normalized(normalized_x, normalized_y);

	return normalized;
}

/// <summary>
/// 指定した距離より近いか返す
/// </summary>
/// <param>距離</param>
/// <returns>近い = true</returns>
bool TwoDimensional::NearDistance(float distance) {
	return pow(distance, kSquare) >= GetSquareDistance();
}

/// <summary>
/// 要素が設定されているかを返す
/// </summary>
/// <param>なし</param>
/// <returns>要素が設定されている = true</returns>
bool TwoDimensional::Empty() {
	return (x_ == kZeroFloat && y_ == kZeroFloat);
}


/// <summary>
/// 加算演算
/// </summary>
TwoDimensional operator+ (TwoDimensional& value1, float value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() + value2);
	td.SetY(value1.GetY() + value2);
	return td;
}

/// <summary>
/// 加算演算
/// </summary>
TwoDimensional operator+ (TwoDimensional& value1, TwoDimensional& value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() + value2.GetX());
	td.SetY(value1.GetY() + value2.GetY());
	return td;
}

/// <summary>
/// 減算演算
/// </summary>
TwoDimensional operator- (TwoDimensional& value1, float value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() - value2);
	td.SetY(value1.GetY() - value2);
	return td;
}

/// <summary>
/// 減算演算
/// </summary>
TwoDimensional operator- (TwoDimensional& value1, TwoDimensional& value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() - value2.GetX());
	td.SetY(value1.GetY() - value2.GetY());
	return td;
}

/// <summary>
/// 乗算演算
/// </summary>
TwoDimensional operator* (TwoDimensional& value1, float value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() * value2);
	td.SetY(value1.GetY() * value2);
	return td;
}

/// <summary>
/// 乗算演算
/// </summary>
TwoDimensional operator* (TwoDimensional& value1, TwoDimensional& value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() * value2.GetX());
	td.SetY(value1.GetY() * value2.GetY());
	return td;
}

/// <summary>
/// 除算演算
/// </summary>
TwoDimensional operator/ (TwoDimensional& value1, float value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() / value2);
	td.SetY(value1.GetY() / value2);
	return td;
}

/// <summary>
/// 除算演算
/// </summary>
TwoDimensional operator/ (TwoDimensional& value1, TwoDimensional& value2) {
	TwoDimensional td;
	td.SetX(value1.GetX() / value2.GetX());
	td.SetY(value1.GetY() / value2.GetY());
	return td;
}
