#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <sstream>
#include "CSVLoader.h"

/// <Summary>
/// コンストラクタ
/// </Summary>
/// <param>なし</param>
/// <returns>なし</returns>
CSVLoader::CSVLoader() {

}

/// <Summary>
/// デストラクタ
/// </Summary>
/// <param>なし</param>
/// <returns>なし</returns>
CSVLoader::~CSVLoader() {

}

/// <Summary>
/// csvファイルを読み込む
/// </Summary>
/// <param>読み込むcsvファイルのパス</param>
/// <param>csvの内容を出力する配列</param>
/// <returns>読み込み成功 = true</returns>
bool CSVLoader::LoadCsvFile(const char* pass, std::vector<std::vector<std::string>>& out_data) {
  //ファイルストリームを宣言と同時にファイルを開く
  std::ifstream stream(pass, std::ios::in);
  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << pass << "が開けません" << std::endl;
    return false;
  }

  out_data.clear();

  //ファイルから読み込む
  std::string line = "";    //1行ずつ区切る
  std::string element = ""; //1項目ずつ区切る

  // 1行ずつ読み込む
  while (getline(stream, line)) {
    // 1行追加
    out_data.push_back(std::vector<std::string>());

    // 「,」区切りごとにデータを読み込むためにistringstream型にする
    std::istringstream  i_stream(line);

    //１項目ずつ読み込む
    while (getline(i_stream, element, ',')) {
      out_data.back().push_back(element);
    }
  }
  //ファイルを閉じる
  stream.close();
  return true;
}