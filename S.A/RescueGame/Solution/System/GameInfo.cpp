#include "GameInfo.h"

/// <summary>
/// インスタンスの初期化
/// </summary>
GameInfo* GameInfo::instance_ = nullptr;

/// <summary>
/// コンストラクタ
/// </summary>
GameInfo::GameInfo()
	: resolution_()
	, screen_center_()
	, map_size_()
	, map_upper_left_()
	, result_() {

}

/// <summary>
/// デストラクタ
/// </summary>
GameInfo::~GameInfo() {

}

/// <summary>
/// 唯一のインスタンスを生成
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void GameInfo::CreateInstance() {
	if (instance_ != nullptr) return;
	instance_ = new GameInfo();
}

/// <summary>
/// 唯一のインスタンスを生成
/// </summary>
/// <param>なし</param>
/// <returns>インスタンスのポインタ</returns>
GameInfo* GameInfo::GetInstance() {
	return instance_;
}

/// <summary>
/// 唯一のインスタンスを解放
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void GameInfo::ReleaseInstance() {
	if (instance_ == nullptr) return;
	delete instance_;
	instance_ = nullptr;
}

/// <summary>
/// 初期化する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void GameInfo::Initialize() {
	resolution_= TwoDimensional(kResolutionX, kResolutionY);

	screen_center_= resolution_ / kQuotientHalf;
}

/// <summary>
/// マップの大きさを設定する
/// </summary>
/// <param>マップのサイズ</param>
/// <returns>なし</returns>
void GameInfo::SetMapSize(TwoDimensional size) {
	map_size_= size;

	TwoDimensional map_screen_size = size * kHalfTileSize;
	map_upper_left_ = screen_center_ - map_screen_size;
	map_upper_left_ = map_upper_left_ + kHalfTileSize;
}