#pragma once
#include "Task.h"
#include <vector>
#include "Layer.h"

/// <summary>
/// タスクを管理する
/// </summary>
class TaskManager {

public:
  /// <summary>
  /// コンストラクタ
  /// </summary>
  TaskManager();

  /// <summary>
  /// デストラクタ
  /// </summary>
  ~TaskManager();

  /// <summary>
  /// タスクを積む
  /// </summary>
  /// <param>タスクのポインタ</param>
  /// <returns>成功の有無</returns>
  bool PushTask(Task*);

  /// <summary>
  /// タスクを解放する
  /// </summary>
  /// <param>タスクのポインタ</param>
  /// <returns>なし</returns>
  void RemoveTask(Task*);

  /// <summary>
  /// タスクを解放する
  /// </summary>
  /// <param>タスクID</param>
  /// <returns>なし</returns>
  void RemoveTask(int);

  /// <summary>
  /// 毎フレーム更新処理
  /// </summary>
  /// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒) </param>
  /// <returns>なし</returns>
  void Update(float frame_second);

  /// <summary>
  /// 毎フレーム描画処理
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void Render();

private:
  /// <summary>
  /// 追加するタスクをタスクリストに移動
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void MovePushTask();

  /// <summary>
  /// 降ろすタスクをタスクリストから降ろす
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  void ReleaseTaskId();

  /// <summary>
  /// 存在しないタスクIDを生成する
  /// </summary>
  /// <param>なし</param>
  /// <returns>なし</returns>
  int CreateTaskId();

private:
  /// <summary>
  /// タスクリスト
  /// </summary>
  std::vector<Task*> task_list_;

  /// <summary>
  /// 追加するタスクのポインタ
  /// </summary>
  std::vector<Task*> add_task_list_;

  /// <summary>
  /// 降ろすタスクのID
  /// </summary>
  std::vector<int> release_task_id_list_;

  /// <summary>
  /// 毎フレーム更新処理中かの有無
  /// </summary>
  bool frame_update_;

};

