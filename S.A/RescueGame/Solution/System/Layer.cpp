#include "Layer.h"

Layer begin(Layer) { return Layer::kBigin; }

Layer end(Layer) { return Layer::kEnd; }

Layer operator*(Layer& layer) { return layer; }

Layer operator++ (Layer& layer) {
	if (layer >= Layer::kEnd) return layer;
	return layer = Layer(std::underlying_type<Layer>::type(layer) + 1);
}