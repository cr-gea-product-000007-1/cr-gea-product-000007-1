#pragma once
#include "DxLib.h"

/// <summary>
/// メニューを操作するインターフェース
/// </summary>
class IMenuControllerEvent {

public:
	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~IMenuControllerEvent() { }

	/// <summary>
	/// 決定したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushDecide() = 0;

	/// <summary>
	/// ポーズしたときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushPause() = 0;

	/// <summary>
	/// 上入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushUp() = 0;

	/// <summary>
	/// 下入力したときのイベント
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void OnPushDown() = 0;
};

