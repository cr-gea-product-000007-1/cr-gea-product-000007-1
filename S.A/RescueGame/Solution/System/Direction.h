#pragma once
#include <iostream>



/// <summary>
/// 向き
/// 上から時計回り
/// </summary>
enum class Direction {
	kBigin = 0,       //先頭
	kUp = 0,          //上
	kUpperRight = 1,  //右上
	kRight = 2,       //右
	kBottomRight = 3, //右下
	kDown = 4,        //下
	kBottomLeft = 5,  //右下
	kLeft = 6,        //左
	kUpperLeft = 7,   //左上
	kBack = 7,        //最後の数値
	kEnd = 8,         //最後の次の数値（角度の範囲外の最初の数値）
};

Direction begin(Direction);

Direction end(Direction);

Direction operator++ (Direction& dir);