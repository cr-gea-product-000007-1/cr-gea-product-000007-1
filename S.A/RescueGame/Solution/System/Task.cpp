#include "Task.h"
#include "DxLib.h"

/// <summary>
/// コンストラクタ
/// </summary>
Task::Task()
  : task_id_(0)
  , release_(false) {

}

/// <summary>
/// デストラクタ
/// </summary>
Task::~Task() {

}

/// <summary>
/// １フレームの処理を実行する
/// </summary>
/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒) </param>
/// <returns>なし</returns>
void Task::Update(float frame_second) {

}

/// <summary>
/// １フレームの描画処理を実行する
/// </summary>
/// <param>なし</param>
/// <returns>なし</returns>
void Task::Render(Layer) {

}