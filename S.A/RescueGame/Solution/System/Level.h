#pragma once
#include "TaskManager.h"
#include "Task.h"
#include "LevelId.h"

/// <summary>
/// レベル1つごと
/// </summary>
class Level : public Task {

public:

	/// <summary>
	/// フェーズの種類
	/// </summary>
	enum class PhaseType {
		kNone,            //なにもしない
		kInitialize,      //初期化
		kInitializeEnd,   //初期化済み
		kProcessing,      //処理
		kFinalizeRequest, //終了リクエスト
		kFinalize,        //終了
		kFinalizeEnd,     //終了済み
	};

	/// <summary>
	/// コンストラクタ
	/// </summary>
	Level();

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Level();

	/// <summary>
	/// タスクマネージャーを設定
	/// </summary>
	/// <param>タスクマネージャーのポインタ</param>
	/// <returns>なし</returns>
	void SetTastManager(TaskManager* task_manager) { task_manager_ = task_manager; }

	/// <summary>
	/// １フレームの処理を実行する
	/// </summary>
	/// <param>前回のフレームでメインループ処理に掛かった時間（小数）（秒)</param>
	/// <returns>なし</returns>
	void Update(float frame_second) override;

	/// <summary>
	/// １フレームの描画処理を実行する
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Render(Layer) override;

	/// <summary>
	/// 終了リクエストのフェーズかの有無を取得する
	/// </summary>
	/// <param>なし</param>
	/// <returns>現在、終了リクエストフェーズかの有無</returns>
	bool IsFinishRequest() { return phase_type_ == PhaseType::kFinalizeRequest; }

	/// <summary>
	/// 初期化処理の開始
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Initialize() { phase_type_ = PhaseType::kInitialize; }

	/// <summary>
	/// 初期化済みのフェーズかの有無を取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>現在、初期化済みフェーズかの有無</returns>
	bool IsInitializeEnd() { return phase_type_ == PhaseType::kInitializeEnd; }

	/// <summary>
	/// 終了処理の開始
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Finalize() { phase_type_ = PhaseType::kFinalize; }

	/// <summary>
	/// 終了処理済みのフェーズかの有無を取得
	/// </summary>
	/// <param>なし</param>
	/// <returns>現在、終了済みフェーズかの有無</returns>
	bool IsPhaseFinalize() { return phase_type_ == PhaseType::kFinalizeEnd; }

	/// <summary>
	/// レベル処理の開始
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Processing() { phase_type_ = PhaseType::kProcessing; }

	/// <summary>
	/// レベルを切り替える指示を出す
	/// </summary>
	/// <param>次のレベルのタスクID</param>
	/// <returns>切り替え開始の成功の有無</returns>
	bool SwitchLevel(LevelId);

	/// <summary>
	/// 次のレベルに遷移するタスクIDを取得する
	/// </summary>
	/// <param>なし</param>
	/// <returns>次のレベルに遷移するタスクID</returns>
	LevelId NextLevelId() { return next_level_id_; }

	/// <summary>
	/// レベルの終了をリクエストする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void FinishRequest() { phase_type_ = PhaseType::kFinalizeRequest; }

	/// <summary>
	/// エラー発生状態にする
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	void Error() { error = true; }

	/// <summary>
	/// エラー発生の有無を取得する
	/// </summary>
	/// <param>なし</param>
	/// <returns>エラー：true、エラーではない：false</returns>
	bool IsError() { return error; }

protected:

	/// <summary>
	/// 初期化処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	virtual bool UpdateInitialize(float frame_second);

	/// <summary>
	/// 処理中フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)	</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	virtual bool UpdateProcessing(float frame_second);

	/// <summary>
	/// 処理中フェーズの毎フレーム描画処理
	/// </summary>
	/// <param>なし</param>
	/// <returns>なし</returns>
	virtual void RenderProcessing();

	/// <summary>
	/// 終了処理フェーズの毎フレーム更新処理
	/// </summary>
	/// <param>最後のフレームを完了するのに要した時間(秒)</param>
	/// <returns>処理終了：true、処理継続：false</returns>
	virtual bool UpdateFinalize(float frame_second);

private:

	/// <summary>
	/// フェーズの種類を変更する
	/// </summary>
	/// <param>フェーズの種類</param>
	/// <returns>なし</returns>
	void SetPhase(PhaseType phase_type) { phase_type_ = phase_type; }

private:
	/// <summary>
	/// フェーズの種類
	/// </summary>
	PhaseType phase_type_;

	/// <summary>
	/// 次に遷移するタスクのID
	/// </summary>
	LevelId next_level_id_;

	/// <summary>
	/// エラー発生の有無
	/// </summary>
	bool error;

protected:
	/// <summary>
	/// タスクマネージャー
	/// </summary>
	TaskManager* task_manager_;
};

