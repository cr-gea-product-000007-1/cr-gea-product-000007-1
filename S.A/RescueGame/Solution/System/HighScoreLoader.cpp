#include <iostream>
#include <fstream>
#include <cmath>
#include "HighScoreLoader.h"
#include "DefaultConstant.h"
#include "GameConstant.h"
#include "Pass.h"
#include <stdio.h>


/// <Summary>
/// コンストラクタ
/// </Summary>
/// <param>なし</param>
/// <returns>なし</returns>
HighScoreLoader::HighScoreLoader() {

}

/// <Summary>
/// デストラクタ
/// </Summary>
/// <param>なし</param>
/// <returns>なし</returns>
HighScoreLoader::~HighScoreLoader() {

}

/// <Summary>
/// ゲームのハイスコアを書き込む
/// </Summary>
/// <param>ゲームのハイスコア</param>
/// <returns>書き込み成功 = true</returns>
bool HighScoreLoader::SaveHighScore(ResultStruct& result) {//ファイルストリームを宣言と同時にファイルを開く
  
  std::fstream stream(pass::kHighScoreDataPass, std::ios::out | std::ios_base::binary);

  //ファイルが開けないならエラー
  if (!stream) {
    std::cout << pass::kHighScoreDataPass << "が開けません" << std::endl;
    return false;
  }
  //データを書き込む
  stream.write(reinterpret_cast<char*>(&result), sizeof(ResultStruct));

  //ファイルを閉じる
  stream.close();
  
  return true;
}

/// <Summary>
/// ゲームのハイスコアを読み込む
/// </Summary>
/// <param>ゲームのハイスコアを書き込む参照</param>
/// <returns>読み込み成功 = true</returns>
bool HighScoreLoader::LoadHighScore(ResultStruct& result) {//ファイルストリームを宣言と同時にファイルを開く

  std::fstream stream(pass::kHighScoreDataPass, std::ios::in | std::ios::binary);

  //データを最低値にする
  result.rank_ = RescueRank::kNoneRank;
  result.time_ = kTimeLimit;
  result.not_rescuer_num_ = INT_MAX;
  result.complete_rescuer_num_ = kZero;

  if (!stream) {
    return false;
  }

  //データ数を読み込む
  char* result_buff_str = reinterpret_cast<char*>(&result);
  stream.read(result_buff_str, sizeof(ResultStruct));

  //ファイルを閉じる
  stream.close();

  return true;
}