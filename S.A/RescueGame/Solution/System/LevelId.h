#pragma once


/// <summary>
/// xÌID
/// </summary>
enum class LevelId {
  kNone,
  kBootLevel,
  kTitleLevel,
  kInGameLevel,
  kResultLevel,
};