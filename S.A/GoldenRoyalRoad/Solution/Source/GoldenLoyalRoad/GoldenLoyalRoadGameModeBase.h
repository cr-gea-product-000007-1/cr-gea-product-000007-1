﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GoldenLoyalRoadGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GOLDENLOYALROAD_API AGoldenLoyalRoadGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
