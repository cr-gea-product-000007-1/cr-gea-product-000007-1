﻿#pragma once
#include "Containers/UnrealString.h"
#include "GameConstant.h"

namespace Path
{
	const char BootWidgetPath[] = "/Game/Widgets/WBP_Boot.WBP_Boot_C";



	const char TitleWidgetPath[] = "/Game/Widgets/WBP_Title.WBP_Title_C";
	const char CreditWidgetPath[] = "/Game/Widgets/WBP_Credit.WBP_Credit_C";



	const char CsvPath[] = "Content/Data/GameBoardData.csv";

	const char NormalSpacePath[] = "/Game/Blueprints/BP_NormalSpace.BP_NormalSpace_C";

	const char GetMoneySpacePath[] = "/Game/Blueprints/BP_GetMoneySpace.BP_GetMoneySpace_C";
	const char LostMoneySpacePath[] = "/Game/Blueprints/BP_LostMoneySpace.BP_LostMoneySpace_C";

	const char GoSpacesSpacePath[] = "/Game/Blueprints/BP_GoSpacesSpace.BP_GoSpacesSpace_C";
	const char BackSpacesSpacePath[] = "/Game/Blueprints/BP_BackSpacesSpace.BP_BackSpacesSpace_C";

	const char WarpSpacePath[] = "/Game/Blueprints/BP_WarpSpace.BP_WarpSpace_C";

	const char StartSpacePath[] = "/Game/Blueprints/BP_StartSpace.BP_StartSpace_C";
	const char GoalSpacePath[] = "/Game/Blueprints/BP_GoalSpace.BP_GoalSpace_C";


	const char PlayerPiecePath[] = "/Game/Blueprints/BP_PlayerPiece.BP_PlayerPiece_C";

	const char Enemy1PiecePath[] = "/Game/Blueprints/BP_Enemy1Piece.BP_Enemy1Piece_C";
	const char Enemy2PiecePath[] = "/Game/Blueprints/BP_Enemy2Piece.BP_Enemy2Piece_C";


	const char InGameCameraPath[] = "/Game/Blueprints/BP_InGameCameraActor.BP_InGameCameraActor_C";


	const char DiceWidgetPath[] = "/Game/Widgets/WBP_Dice.WBP_Dice_C";
	const char DicePath[] = "/Game/Images/Dice_1.Dice_1";

	const char DiceImagePath[GameConstant::DiceMaxValue][27] =
	{
		"/Game/Images/Dice_1.Dice_1",
		"/Game/Images/Dice_2.Dice_2",
		"/Game/Images/Dice_3.Dice_3",
		"/Game/Images/Dice_4.Dice_4",
		"/Game/Images/Dice_5.Dice_5",
		"/Game/Images/Dice_6.Dice_6"
	};


	const char ArrowPath[] = "/Game/Blueprints/BP_Arrow.BP_Arrow_C";

	const char MoneyWidgetPath[] = "/Game/Widgets/WBP_Money.WBP_Money_C";
	const char MoneyRankWidgetPath[] = "/Game/Widgets/WBP_MoneyRank.WBP_MoneyRank_C";

	const char MessageWidgetPath[] = "/Game/Widgets/WBP_Message.WBP_Message_C";

	const char CameraMoveWidgetPath[] = "/Game/Widgets/WBP_CameraMove.WBP_CameraMove_C";

	const char ResultRankWidgetPath[] = "/Game/Widgets/WBP_ResultRank.WBP_ResultRank_C";
	const char ResultWidgetPath[] = "/Game/Widgets/WBP_Result.WBP_Result_C";


	const char GetSilverCoinPath[] = "/Game/Blueprints/BP_GetSilverCoin.BP_GetSilverCoin_C";
	const char GetGoldCoinPath[] = "/Game/Blueprints/BP_GetGoldCoin.BP_GetGoldCoin_C";
	const char LostSilverCoinPath[] = "/Game/Blueprints/BP_LostSilverCoin.BP_LostSilverCoin_C";
	const char LostGoldCoinPath[] = "/Game/Blueprints/BP_LostGoldCoin.BP_LostGoldCoin_C";
}
