﻿
#pragma once
#include "Private/InGameLevel/Direction.h"

namespace GameConstant
{
	/// <summary>
	/// サイコロの目の最大値
	/// </summary>
	const int DiceMaxValue = 6;

	/// <summary>
	/// ゴールまでの距離の最大値
	/// 最小値の初期値と再起呼び出し数に使用
	/// </summary>
	const int MaxGoalDistance = 200;

	/// <summary>
	/// 向きを変換する
	/// zは第2引数
	/// </summary>
	TMap<const UDirection, const FRotator> DirectionRotatos
	{
		{ UDirection::Down, FRotator(0.0f ,90.0f, 0.0f) },
		{ UDirection::Left, FRotator(0.0f, 180.0f ,0.0f) },
		{ UDirection::Right, FRotator(0.0f, 0.0f ,0.0f) },
		{ UDirection::Up, FRotator(0.0f, 270.0f ,0.0f) }
	};
}
