﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Components/Image.h"
#include "DiceWidget.generated.h"

/**
 * 
 */
UCLASS()
class UDiceWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:

	/// <summary>
	/// コンストラクタ
	/// </summary>
	UDiceWidget();

	/// <summary>
	/// Imageの読み込み
	/// </summary>
	void LoadImage();

	/// <summary>
	/// 端に移動させるのイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SideMove();

	/// <summary>
	/// 中心に移動させるのイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void CenterMove();

	/// <summary>
	/// 目を設定する
	/// </summary>
	void SetValue(int Value);

public:

	/// <summary>
	/// 各目の画像
	/// </summary>
	UPROPERTY(BlueprintReadWrite)
	TArray<UTexture2D*> ValueImages;

	/// <summary>
	/// イメージのクラス
	/// </summary>
	UPROPERTY()
	UImage* Image;
};
