// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "InGameManager.h"
#include "InGameLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class AInGameLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	// コンストラクタ
	AInGameLevelScriptActor();

private:
	enum class PhaseType
	{
		Create,   //生成
		GamePlay, //ゲームプレイ
		Result,   //リザルト
		End,      //終了
	};
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/// <summary>
	/// 現在のフェーズ
	/// </summary>
	PhaseType MyPhaseType;
	
	UPROPERTY()
	UInGameManager* InGameManager;
};
