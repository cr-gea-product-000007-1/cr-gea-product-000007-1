﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Direction.h"
#include "MoveDirectionStruct.generated.h"

class ABaseSpace;
/**
 * 
 */
USTRUCT(BlueprintType)
struct FMoveDirectionStruct
{
	GENERATED_BODY()

	/// <summary>
	/// 移動する方向
	/// </summary>
	UPROPERTY()
	UDirection MoveDirection;

	/// <summary>
	/// 移動先のマスのポインタ
	/// </summary>
	UPROPERTY()
	ABaseSpace* GoToSpace;

	/// <summary>
	/// ゴールまでの最短マス数
	/// </summary>
	int32 GoalMinDistance;

	/// <summary>
	/// ゴールまでの最長マス数
	/// </summary>
	int32 GoalMaxDistance;
};
