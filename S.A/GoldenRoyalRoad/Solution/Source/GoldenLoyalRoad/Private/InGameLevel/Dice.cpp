﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Dice.h"
#include "../../GameConstant.h"

/// <summary>
/// 次の目に移るまでの最大時間
/// </summary>
float NextValueMaxTime = 0.1f;

/// <summary>
/// 投げ終わるまでの時間
/// </summary>
float ThrowEndTime = 0.55f;

/// <summary>
/// 表示し終わるまでの時間
/// </summary>
float ViewEndTime = 1.5f;

/// <summary>
/// 閉じ終わるまでの時間
/// </summary>
float CloseEndTime = 1.5f;

/// <summary>
/// 更新処理
/// </summary>
	/// <param> 前フレームの処理時間 </param>
void UDice::Update(float DeltaTime)
{
	if (!IsValid(DiceWidget)) return;

	switch (MyPhase)
	{
	case UDice::DicePhase::Start:
		DiceWidget->CenterMove();
		DiceWidget->Open();
		MyPhase = UDice::DicePhase::Idel;
		break;

	case UDice::DicePhase::Roll:
		//次の目に移る時間がたったら目を1増やす
		NextValueTime -= DeltaTime;
		if (NextValueTime < 0.0f) {
			Value++;
			if (Value > GameConstant::DiceMaxValue) Value = 1;
			NextValueTime = FMath::RandRange(0.0f, NextValueMaxTime);
			DiceWidget->SetValue(Value);
		}
		break;

	case UDice::DicePhase::ThrowStart:
		DiceWidget->SideMove();
		ThrowTime = 0.0f;
		MyPhase = UDice::DicePhase::Throw;
		break;

	case UDice::DicePhase::Throw:
		//次の目に移る時間がたったら目を1増やす
		NextValueTime -= DeltaTime;
		if (NextValueTime < 0.0f) {
			Value++;
			if (Value > GameConstant::DiceMaxValue) Value = 1;
			NextValueTime = FMath::RandRange(0.0f, NextValueMaxTime);
			DiceWidget->SetValue(Value);
		}
		//投げ終わったら目を確定
		ThrowTime += DeltaTime;
		if (ThrowTime > ThrowEndTime) {
			ViewTime = 0.0f;
			MyPhase = UDice::DicePhase::View;
		}
		break;

	case UDice::DicePhase::View:
		ViewTime += DeltaTime;
		if (ViewTime > ViewEndTime) {
			MyPhase = UDice::DicePhase::ViewEnd;
		}
		break;

	case UDice::DicePhase::CloseStart:
		DiceWidget->Close();
		CloseTime = 0;
		MyPhase = UDice::DicePhase::Close;
		break;

	case UDice::DicePhase::Close:
		CloseTime += DeltaTime;
		if (CloseTime > CloseEndTime) {
			MyPhase = UDice::DicePhase::End;
		}
		break;
	}
}

/// <summary>
/// サイコロの目を設定する
/// </summary>
/// <param> サイコロの目 </param>
void UDice::SetValue(int NewValue)
{
	if (NewValue <= 0 || NewValue > GameConstant::DiceMaxValue)
	{
		Value = 0;
		MyPhase = UDice::DicePhase::CloseStart;
		return;
	}
	if (!DiceWidget->GetIsOpen())
		DiceWidget->Open();
	Value = NewValue;
	DiceWidget->SetValue(Value);
}

/// <summary>
/// サイコロの目を1減らす
/// </summary>
void UDice::CountDown()
{
	if (Value >= 0) {
		Value--;
	}

	if (Value > 0)
		DiceWidget->SetValue(Value);
	else
		MyPhase = UDice::DicePhase::CloseStart;
}