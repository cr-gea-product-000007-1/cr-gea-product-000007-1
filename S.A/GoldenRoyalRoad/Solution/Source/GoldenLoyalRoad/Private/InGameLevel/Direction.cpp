﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Direction.h"

/// <summary>
/// 反対方向を返す
/// </summary>
/// <param> 方向 </param>
/// <returns> 反対方向 </returns>
UDirection GetBoth(UDirection Direction)
{
	return UDirection((int)UDirection::Both - (int)Direction);
}