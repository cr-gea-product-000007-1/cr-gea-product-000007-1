﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Arrow.h"
#include "Components/SceneComponent.h"



/// <summary>
/// 基準の大きさ
/// </summary>
const float DefaultScale = 0.3f;

/// <summary>
/// 選ばれている時に大きくなる大きさの最大値
/// </summary>
const float SelectAddMaxScale = 0.3f;

/// <summary>
/// 時間で変わる大きさの最大値
/// </summary>
const float TimeMaxScale = 0.05f;

/// <summary>
/// 選ばれている時に大きくなる大きさの変化速度
/// </summary>
const float SelectAddScaleSpeed = 0.1f;

/// <summary>
/// 時間で変わる大きさの変化速度
/// </summary>
const float TimeScaleSpeed = 5.0f;

/// <summary>
/// 球の大きさの比率
/// </summary>
const float BallScaleRate = 0.4f;

// Sets default values
AArrow::AArrow()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bVisible = true;
}

// Called when the game starts or when spawned
void AArrow::BeginPlay()
{
	Super::BeginPlay();
	
	TArray<UStaticMeshComponent*>Meshs;
	GetComponents(Meshs);
	if (Meshs.Num() < 2) return;
	Arrow = Meshs[0];
	Ball = Meshs[1];
}

// Called every frame
void AArrow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Timer += DeltaTime;
	TimeScale = FMath::Sin(Timer * TimeScaleSpeed) * TimeMaxScale + 1.0f;

	switch (ArrowPhase)
	{
	case AArrow::ArrowPhaseType::Select:
		if (SelectAddScale < SelectAddMaxScale)
		{
			SelectAddScale += SelectAddScaleSpeed;
			if (SelectAddScale > SelectAddMaxScale)
				SelectAddScale = SelectAddMaxScale;
		}
		break;

	case AArrow::ArrowPhaseType::NotSelect:
		if (SelectAddScale > 0)
		{
			SelectAddScale -= SelectAddScaleSpeed;
			if (SelectAddScale < 0)
				SelectAddScale = 0;
		}
		break;
	}

	float Scale = DefaultScale + SelectAddScale * TimeScale;
	FVector ScaleVector = FVector(Scale, Scale, Scale);

	Arrow->SetRelativeScale3D(ScaleVector);
	Ball->SetRelativeScale3D(ScaleVector * BallScaleRate);

}

/// <summary>
/// 表示させる
/// </summary>
void AArrow::Visible()
{
	if (bVisible) return;
	bVisible = true;

	Arrow->SetVisibleFlag(true);
	Arrow->SetHiddenInGame(false);
	Ball->SetVisibleFlag(true);
	Ball->SetHiddenInGame(false);
}

/// <summary>
/// 表示をやめる
/// </summary>
void AArrow::NotVisible()
{
	if (!bVisible) return;
	bVisible = false;

	Arrow->SetVisibleFlag(false);
	Arrow->SetHiddenInGame(true);
	Ball->SetVisibleFlag(false);
	Ball->SetHiddenInGame(true);
}