﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Direction.h"
#include "UIInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUIInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IUIInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 分岐選択を開くイベント
	/// </summary>
	virtual void OpenSelectBranch() = 0;

	/// <summary>
	/// 分岐選択を閉じるイベント
	/// </summary>
	virtual void CloseSelectBranch() = 0;

	/// <summary>
	/// 分岐を選択するイベント
	/// </summary>
	virtual void SelectBranch(UDirection Direction) = 0;
};
