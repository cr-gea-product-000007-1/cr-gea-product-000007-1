﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/ResultRankWidget.h"


/// <summary>
/// 所持金を設定する
/// </summary>
/// <param>所持金</param>
void UResultRankWidget::SetMoney_Implementation(int32 NewMoney)
{
	Money = NewMoney;
}