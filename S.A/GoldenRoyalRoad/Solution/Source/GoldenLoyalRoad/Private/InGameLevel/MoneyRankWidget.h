﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MoneyWidget.h"
#include "Piece/BasePiece.h"
#include "MoneyRankWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMoneyRankWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ウィジットの読み込み
	/// </summary>
	void LoadWidget();

	/// <summary>
	/// 表示するコマの追加
	/// </summary>
	/// <param>追加するピース</param>
	void AddPiece(ABasePiece* Target);

	/// <summary>
	/// 所持金が増えるアニメーション
	/// </summary>
	/// <param> 対象コマのインデックス </param>
	/// <param> 変更後の金額 </param>
	void AddMoneyAnimation(int Index, int Value);

	/// <summary>
	/// 所持金が増えるアニメーション
	/// </summary>
	/// <param> 対象コマのインデックス </param>
	/// <param> 変更後の金額 </param>
	void LostMoneyAnimation(int Index, int Value);

	/// <summary>
	/// ターン開始アニメーション
	/// </summary>
	/// <param> 対象コマのインデックス </param>
	void TurnStartAnimation(int Index);

	/// <summary>
	/// ターン終了アニメーション
	/// </summary>
	/// <param> 対象コマのインデックス </param>
	void TurnEndAnimation(int Index);

	/// <summary>
	/// 閉じるイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Close();

private:
	/// <summary>
	/// 所持金を表示するUIの本データ
	/// </summary>
	UPROPERTY()
	TSubclassOf<UMoneyWidget> MoneyWidgetClass;

	/// <summary>
	/// 所持金を表示するUI
	/// </summary>
	UPROPERTY()
	TArray<UMoneyWidget*> MoneyWidgets;
	
};
