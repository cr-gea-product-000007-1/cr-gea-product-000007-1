﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Spaces/BaseSpace.h"
#include "Spaces/WarpSpace.h"
#include "Spaces/GoalSpace.h"
#include "Spaces/SpaceFactory.h"
#include "MessageInterface.h"
#include "GoalInterface.h"
#include "GameBoard.generated.h"

/**
 * 
 */
UCLASS()
class UGameBoard : public UObject
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// 終了処理
	/// </summary>
	void Finalize();

	/// <summary>
	/// Csvファイルのマップデータを読み込む
	/// </summary>
	/// <param> csvファイルのパス </param>
	/// <return> 読み込み成功 = true </return>
	bool LoadCsvData(FString& CsvPath);

	/// <summary>
	/// 各マスの移動先マスを設定
	/// </summary>
	void ConnectAllSpaces();

	/// <summary>
	/// 各マスの初期設定
	/// </summary>
	void InitializeSpaces();

private:
	/// <summary>
	/// 指定マスの移動先マスを設定
	/// </summary>
	void ConnectSpace(int XIndex, int YIndex);

public:
	/// <summary>
	/// 指定したマスを返す
	/// </summary>
	/// <param> 指定するインデックス </param>
	/// <return> マスの基底クラスのポインタ </return>
	ABaseSpace* GetSpace(int32 IndexX, int32 IndexY);

	/// <summary>
	/// スタートマスを取得する
	/// </summary>
	/// <return> スタートマス </return>
	ABaseSpace* GetStartSpace() { return StartSpace; };

	/// <summary>
	/// ゴールマスを取得する
	/// </summary>
	/// <return> ゴールマス </return>
	ABaseSpace* GetGoalSpace() { return GoalSpace; };

	/// <summary>
	/// 全てのイベントマスにメッセージ表示のイベントインターフェースを設定する
	/// </summary>
	void SetMessageEventInterfaceForEventSpaces(const TScriptInterface<IMessageInterface> Interface);

	/// <summary>
	/// ゴールマスにイベントインターフェースを設定する
	/// </summary>
	void SetGoalInterface(const TScriptInterface<IGoalInterface> Interface);

private:
	/// <summary>
	/// マスのタイルマップ
	/// </summary>
	TArray<TArray<ABaseSpace*>> SpaceTiles;

	/// <summary>
	/// スタートマス
	/// </summary>
	UPROPERTY()
	ABaseSpace* StartSpace;

	/// <summary>
	/// ゴールマス
	/// </summary>
	UPROPERTY()
	AGoalSpace* GoalSpace;

	/// <summary>
	/// ワープするマス
	/// </summary>
	UPROPERTY()
	TArray<AWarpSpace*> WarpSpaces;

	/// <summary>
	/// マスを生成する
	/// </summary>
	UPROPERTY()
	USpaceFactory* SpaceFactory;
};
