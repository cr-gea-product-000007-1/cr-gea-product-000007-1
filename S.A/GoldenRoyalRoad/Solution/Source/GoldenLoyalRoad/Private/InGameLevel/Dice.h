﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DiceWidget.h"
#include "Dice.generated.h"


/**
 * 
 */
UCLASS()
class UDice : public UObject
{
	GENERATED_BODY()

private:
	enum class DicePhase : uint8
	{
		NotView,        //表示しない
		Start,          //開始
		Idel,           //転がすまで待機
		Roll,           //転がす
		ThrowStart,     //投げる
		Throw,          //投げる
		View,           //目の表示
		ViewEnd,        //目の表示終了
		CloseStart,     //閉じ始める
		Close,          //閉じている
		End,            //終了
	};

public:
	/// <summary>
	/// 更新処理
	/// </summary>
	/// <param> 前フレームの処理時間 </param>
	void Update(float DeltaTime);

	/// <summary>
	/// 表記するウィジットを設定する
	/// </summary>
	/// <param> サイコロのウィジット </param>
	void SetWidget(UDiceWidget* Widget) { DiceWidget = Widget; };

	/// <summary>
	/// サイコロを振り始める
	/// </summary>
	void Start() { MyPhase = DicePhase::Start; };

	/// <summary>
	/// 転がす
	/// </summary>
	void Roll() { MyPhase = DicePhase::Roll; };

	/// <summary>
	/// 投げる
	/// </summary>
	void Throw() { MyPhase = DicePhase::ThrowStart; };

	/// <summary>
	/// サイコロが振り終わっているか
	/// </summary>
	/// <return> 振り終わった = true </return>
	bool IsThrowEnd() { return MyPhase >= DicePhase::ViewEnd; };

	/// <summary>
	/// サイコロが終了したか
	/// </summary>
	/// <return> 終了した = true </return>
	bool IsEnd() { return MyPhase == DicePhase::End; };

	/// <summary>
	/// サイコロの目を設定する
	/// </summary>
	/// <param> サイコロの目 </param>
	void SetValue(int NewValue);

	/// <summary>
	/// サイコロの目を返す
	/// </summary>
	/// <return> サイコロの目 </return>
	int GetValue() { return Value; };

	/// <summary>
	/// サイコロの目を1減らす
	/// </summary>
	void CountDown();

private:
	/// <summary>
	/// サイコロのフェーズ
	/// </summary>
	DicePhase MyPhase;

	/// <summary>
	/// 表記するウィジット
	/// </summary>
	UPROPERTY()
	UDiceWidget* DiceWidget;

	/// <summary>
	/// 目の値
	/// </summary>
	int Value;

	/// <summary>
	/// 次の目に移るまでの時間
	/// </summary>
	float NextValueTime;

	/// <summary>
	/// 投げている時間
	/// </summary>
	float ThrowTime;

	/// <summary>
	/// 表示している時間
	/// </summary>
	float ViewTime;

	/// <summary>
	/// 閉じている時間
	/// </summary>
	float CloseTime;
};
