﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Piece/BasePiece.h"
#include "ResultRankWidget.h"
#include "ResultWidget.generated.h"

/**
 * 
 */
UCLASS()
class UResultWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/// <summary>
	/// ウィジットの読み込み
	/// </summary>
	void LoadWidget();

	/// <summary>
	/// 表示するコマの追加
	/// </summary>
	/// <param>追加するピース</param>
	void AddPiece(ABasePiece* Target);

	/// <summary>
	/// 所持金を設定
	/// </summary>
	/// <param>設定するピース</param>
	void SetMoneyForAllPiece(TArray<ABasePiece*>& Pieces);

	/// <summary>
	/// UIの配置
	/// </summary>
	void Arrangement();

	/// <summary>
	/// 順位UIを開く
	/// </summary>
	/// <param> 対象コマのインデックス </param>
	void OpenRank(int Index);

	/// <summary>
	/// 一番下の開いていない順位UIを開く
	/// </summary>
	void OpenRankForBack();

	/// <summary>
	/// ウィジットを閉じる
	/// </summary>
	void Close();

	/// <summary>
	/// 全てのUIが開いているかを返す
	/// </summary>
	/// <return> 開いている </return>
	bool IsAllOpen();

private:
	/// <summary>
	/// 順位を表示するUIの本データ
	/// </summary>
	UPROPERTY()
	TSubclassOf<UResultRankWidget> ResultRankWidgetClass;

	/// <summary>
	/// 順位を表示するUI
	/// </summary>
	UPROPERTY()
	TArray<UResultRankWidget*> ResultRankWidgets;
	
};
