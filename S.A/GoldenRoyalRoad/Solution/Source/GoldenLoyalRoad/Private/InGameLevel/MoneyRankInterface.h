// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MoneyRankInterface.generated.h"

class ABasePiece;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMoneyRankInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IMoneyRankInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 所持金を増やすイベント
	/// </summary>
	/// <param> 増やしたコマ </param>
	virtual void AddMoney(ABasePiece* target) = 0;

	/// <summary>
	/// 所持金を減らすイベント
	/// </summary>
	/// <param> 減らしたコマ </param>
	virtual void LostMoney(ABasePiece* target) = 0;
};
