﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MoneyWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMoneyWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// コマの名前を設定する
	/// </summary>
	/// <param>コマの名前</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetPieceName(const FString& PieceName);

	/// <summary>
	/// コマの色を設定する
	/// </summary>
	/// <param>コマの名前</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetPieceColor(PieceType PieceType);

	/// <summary>
	/// 所持金増加のイベント
	/// </summary>
	/// <param>増加後の金額</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void AddMoney(int Money);

	/// <summary>
	/// 所持金減少のイベント
	/// </summary>
	/// <param>減少後の金額</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void LostMoney(int Money);

	/// <summary>
	/// ターン開始のイベント
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void TurnStart();

	/// <summary>
	/// ターン終了のイベント
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void TurnEnd();
	
};
