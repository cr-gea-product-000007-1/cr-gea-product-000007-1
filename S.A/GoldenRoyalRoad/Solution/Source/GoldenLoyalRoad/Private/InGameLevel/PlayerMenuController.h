﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerMenuInterface.h"
#include "PlayerMenuController.generated.h"

/**
 * 
 */
UCLASS()
class APlayerMenuController : public APlayerController
{
	GENERATED_BODY()

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft();

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight();


	/// <summary>
	/// 第2の上キーを押したときのイベント
	/// </summary>
	void OnUpOther();

	/// <summary>
	/// 第2の下キーを押したときのイベント
	/// </summary>
	void OnDownOther();

	/// <summary>
	/// 第2の左キーを押したときのイベント
	/// </summary>
	void OnLeftOther();

	/// <summary>
	/// 第2の右キーを押したときのイベント
	/// </summary>
	void OnRightOther();


	/// <summary>
	/// 第2の上キーを離したときのイベント
	/// </summary>
	void OutUpOther();

	/// <summary>
	/// 第2の下キーを離したときのイベント
	/// </summary>
	void OutDownOther();

	/// <summary>
	/// 第2の左キーを離したときのイベント
	/// </summary>
	void OutLeftOther();

	/// <summary>
	/// 第2の右キーを離したときのイベント
	/// </summary>
	void OutRightOther();
	
public:
	/// <summary>
	/// インターフェースを設定する
	/// </summary>
	void SetPlayerMenuInterface(const TScriptInterface<IPlayerMenuInterface>& Interface) { PlayerMenuInterface = Interface; };

	/// <summary>
	/// 第2の上キーが押されているか
	/// </summary>
	bool IsUpOther() { return bIsUpOther; }

	/// <summary>
	/// 第2の下キーが押されているか
	/// </summary>
	bool IsDownOther() { return bIsDownOther; }

	/// <summary>
	/// 第2の左キーが押されているか
	/// </summary>
	bool IsLeftOther() { return bIsLeftOther; }

	/// <summary>
	/// 第2の右キーが押されているか
	/// </summary>
	bool IsRightOther() { return bIsRightOther; }
	
private:
	/// <summary>
	/// インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IPlayerMenuInterface> PlayerMenuInterface;

	/// <summary>
	/// 第2の上キーが押されているか
	/// </summary>
	bool bIsUpOther;

	/// <summary>
	/// 第2の下キーが押されているか
	/// </summary>
	bool bIsDownOther;

	/// <summary>
	/// 第2の左キーが押されているか
	/// </summary>
	bool bIsLeftOther;

	/// <summary>
	/// 第2の右キーが押されているか
	/// </summary>
	bool bIsRightOther;
};
