﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "Piece/BasePiece.h"
#include "ResultRankWidget.generated.h"

/**
 * 
 */
UCLASS()
class UResultRankWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 順位を設定する
	/// </summary>
	/// <param>順位</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetRank(int32 Rank);

	/// <summary>
	/// コマの名前を設定する
	/// </summary>
	/// <param>コマの名前</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetPieceName(const FString& PieceName);

	/// <summary>
	/// コマの色を設定する
	/// </summary>
	/// <param>コマの名前</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetPieceColor(PieceType PieceType);

	/// <summary>
	/// 所持金を設定する
	/// </summary>
	/// <param>所持金</param>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetMoney(int32 NewMoney);

	/// <summary>
	/// 所持金を返す
	/// </summary>
	/// <param>所持金</param>
	int32 GetMoney() { return Money; }

public:
	/// <summary>
	/// 所持金
	/// </summary>
	int32 Money;
};
