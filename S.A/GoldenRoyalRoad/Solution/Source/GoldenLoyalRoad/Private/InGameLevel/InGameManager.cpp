﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/InGameManager.h"
#include "Misc/ConfigCacheIni.h"
#include "PlayerMenuController.h"
#include "../../PathConstant.h"

/// <summary>
/// コマの初期方向
/// マスの繋がりを設定し次第、それに準拠させる
/// </summary>
const UDirection StartPieceDirection = UDirection::Up;

/// <summary>
/// ターン開始までの時間
/// </summary>
float TurnStartMaxTime = 0.5f;

/// <summary>
/// リザルトの表示間隔
/// </summary>
float ResultMaxIntervalTime = 1.0f;

/// <summary>
/// 移動スピード
/// </summary>
const float CameraMoveSpeed = 16.0f;

/// <summary>
/// 初期化処理
/// </summary>
void UInGameManager::Initialize()
{
	CreatePhase = UInGameManager::CreatePhaseType::FieldCreate;
	GamePlayPhase = UInGameManager::GamePlayPhaseType::GameStart;
	ResultPhase = UInGameManager::ResultPhaseType::Start;
	ResultIntervalTime = 0;
}

/// <summary>
/// 生成処理
/// </summary>
/// <return> フェーズ終了 = true </return>
bool UInGameManager::Create()
{
	switch (CreatePhase)
	{
	case UInGameManager::CreatePhaseType::FieldCreate:
	{
		GameBoard = NewObject<UGameBoard>(GetWorld());
		GameBoard->Initialize();

		FString Path = FPaths::ProjectDir() + Path::CsvPath;
		GameBoard->LoadCsvData(Path);
		GameBoard->ConnectAllSpaces();
		GameBoard->InitializeSpaces();
		GameBoard->SetMessageEventInterfaceForEventSpaces(this);
		GameBoard->SetGoalInterface(this);

		CreatePhase = UInGameManager::CreatePhaseType::PieceCreate;
		break;
	}

	case UInGameManager::CreatePhaseType::PieceCreate:
	{
		PieceFactory = NewObject<UPieceFactory>(this);

		int ID = 0;
		for (PieceType CreatePieceType : TEnumRange<PieceType>())
		{
			ABasePiece* NewPiece = PieceFactory->CreatePiece(CreatePieceType);

			if (!IsValid(GameBoard)) continue;
			ABaseSpace* StartSpace = GameBoard->GetStartSpace();
			if (!IsValid(StartSpace)) continue;
			NewPiece->Initialize();
			NewPiece->SetID(ID);
			NewPiece->SetNowSpace(StartSpace);
			NewPiece->SetSpaceIdelNum(ID);
			NewPiece->IdelNowSpaceAnimation();
			NewPiece->TeleportNowSpaceIdel();
			NewPiece->SetDirection(StartPieceDirection);
			NewPiece->SetDiceEventInterface(this);
			NewPiece->SetPieceEventInterface(this);
			NewPiece->SetUIEventInterface(this);
			NewPiece->SetMoneyRankEventInterface(this);
			NewPiece->SetMessageEventInterface(this);
			NewPiece->SetCameraInterface(this);

			Pieces.Add(NewPiece);

			ID++;
		}

		CreatePhase = UInGameManager::CreatePhaseType::DiceCreate;
		break;
	}

	case UInGameManager::CreatePhaseType::DiceCreate:
	{
		Dice = NewObject<UDice>(GetWorld());

		FString Path = Path::DiceWidgetPath;
		TSubclassOf<UDiceWidget> DiceWidgetClass = TSoftClassPtr<UDiceWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(DiceWidgetClass))
		{
			DiceWidget = CreateWidget<UDiceWidget>(GetWorld(), DiceWidgetClass);
			DiceWidget->LoadImage();
			Dice->SetWidget(DiceWidget);
		}

		CreatePhase = UInGameManager::CreatePhaseType::UICreate;
		break;
	}

	case UInGameManager::CreatePhaseType::UICreate:
	{
		SelectBranchArrow = NewObject<USelectBranchArrow>(GetWorld());
		SelectBranchArrow->Initialize();

		CoinEmmiter = NewObject<UCoinEmmiter>(GetWorld());
		CoinEmmiter->LoadCoin();

		//所持金UI
		//Pathだと代入が失敗する
		FString MoneyPath = Path::MoneyRankWidgetPath;
		TSubclassOf<UMoneyRankWidget> MoneyRankWidgetClass = TSoftClassPtr<UMoneyRankWidget>(FSoftObjectPath(*MoneyPath)).LoadSynchronous();
		if (IsValid(MoneyRankWidgetClass))
		{
			MoneyRankWidget = CreateWidget<UMoneyRankWidget>(GetWorld(), MoneyRankWidgetClass);
			MoneyRankWidget->LoadWidget();
			for (ABasePiece* Piece : Pieces)
				MoneyRankWidget->AddPiece(Piece);
		}

		//メッセージUI
		FString MessagePath = Path::MessageWidgetPath;
		TSubclassOf<UMessageWidget> MessageWidgetClass = TSoftClassPtr<UMessageWidget>(FSoftObjectPath(*MessagePath)).LoadSynchronous();
		if (IsValid(MessageWidgetClass))
		{
			MessageWidget = CreateWidget<UMessageWidget>(GetWorld(), MessageWidgetClass);
		}

		//リザルトUI
		FString ResultPath = Path::ResultWidgetPath;
		TSubclassOf<UResultWidget> ResultWidgetClass = TSoftClassPtr<UResultWidget>(FSoftObjectPath(*ResultPath)).LoadSynchronous();
		if (IsValid(ResultWidgetClass))
		{
			ResultWidget = CreateWidget<UResultWidget>(GetWorld(), ResultWidgetClass); MoneyRankWidget->LoadWidget();
			ResultWidget->LoadWidget();
			for (ABasePiece* Piece : Pieces)
				ResultWidget->AddPiece(Piece);
			ResultWidget->Close();
		}

		//カメラ移動UI
		FString CameraPath = Path::CameraMoveWidgetPath;
		TSubclassOf<UUiAssignmentWidget> CameraMoveWidgetClass = TSoftClassPtr<UUiAssignmentWidget>(FSoftObjectPath(*CameraPath)).LoadSynchronous();
		if (IsValid(CameraMoveWidgetClass))
		{
			CameraMoveWidget = CreateWidget<UUiAssignmentWidget>(GetWorld(), CameraMoveWidgetClass);
		}

		CreatePhase = UInGameManager::CreatePhaseType::CameraCreate;
		break;
	}

	case UInGameManager::CreatePhaseType::CameraCreate:
	{
		FString Path = Path::InGameCameraPath;
		TSubclassOf<class AInGameCameraActor> BaseClass = TSoftClassPtr<AInGameCameraActor>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(BaseClass))
		{
			Camera = GetWorld()->SpawnActor<AInGameCameraActor>(BaseClass);
		}

		CreatePhase = UInGameManager::CreatePhaseType::End;
		break;
	}

	case UInGameManager::CreatePhaseType::End:
		return true;
		break;
	}

	return false;
}

/// <summary>
/// ゲームプレイ処理
/// </summary>
/// <param> 前フレームの処理時間 </param>
/// <return> フェーズ終了 = true </return>
bool UInGameManager::GamePlay(float DeltaTime)
{
	MessageWidget->Update(DeltaTime);
	CameraMove();
	switch (GamePlayPhase)
	{
	case UInGameManager::GamePlayPhaseType::GameStart:
	{
		TurnPieceIndex = 0;
		TurnPiece = Pieces[TurnPieceIndex];
		if (SelectBranchArrow != nullptr)
		{
			SelectBranchArrow->PieceSpot(TurnPiece);
			SelectBranchArrow->Reset();
		}
		//プレイヤーコントローラーを取得
		APlayerMenuController* PlayerController = Cast<APlayerMenuController>(GetWorld()->GetFirstPlayerController());
		if (IsValid(PlayerController))
		{
			PlayerController->SetPlayerMenuInterface(this);
			//カメラを設定
			PlayerController->SetViewTarget(Camera);
		}

		GamePlayPhase = UInGameManager::GamePlayPhaseType::TurnSetting;
		break;
	}

	case UInGameManager::GamePlayPhaseType::TurnSetting:
		if (IsValid(Camera))
		{
			Camera->PieceSpot(TurnPiece);
		}
		TurnStartTime = 0;
		MessageWidget->Open();
		MessageWidget->SetText(TurnPiece->GetPieceName() + TEXT("のターンです"));
		if(TurnPiece->IsPlayer())
			MessageWidget->SetSendMessageType(SendMessageType::Input);
		else
			MessageWidget->SetSendMessageType(SendMessageType::AutoTime);
		GamePlayPhase = UInGameManager::GamePlayPhaseType::ViewTurnPiece;
		break;

	case UInGameManager::GamePlayPhaseType::ViewTurnPiece:
		TurnStartTime += DeltaTime;
		if (TurnStartTime > TurnStartMaxTime) {
			TurnPiece->TurnStart();
			MoneyRankWidget->TurnStartAnimation(TurnPieceIndex);
			GamePlayPhase = UInGameManager::GamePlayPhaseType::TurnPieceUpdate;
		}
		break;

	case UInGameManager::GamePlayPhaseType::TurnPieceUpdate:
		if (!IsValid(TurnPiece)) break;
		TurnPiece->Update(DeltaTime);
		Dice->Update(DeltaTime);
		Camera->PieceSpot(TurnPiece);

		if (TurnPiece->IsTurnEnd())
			GamePlayPhase = UInGameManager::GamePlayPhaseType::TurnEnd;
		break;

	case UInGameManager::GamePlayPhaseType::TurnEnd:
		//サイコロが終了まで待機
		Dice->Update(DeltaTime);
		if (Dice->IsEnd())
			GamePlayPhase = UInGameManager::GamePlayPhaseType::NextTurn;
		break;

	case UInGameManager::GamePlayPhaseType::NextTurn:
	{
		TurnPiece->TurnEnd();
		MoneyRankWidget->TurnEndAnimation(TurnPieceIndex);

		if (GoalPieceNum == Pieces.Num())
		{
			GamePlayPhase = UInGameManager::GamePlayPhaseType::GameEnd;
			break;
		}

		int i = 0;
		do {
			TurnPieceIndex = (TurnPieceIndex + 1) % Pieces.Num();
			TurnPiece = Pieces[TurnPieceIndex];

			//全プレイヤーゴールしているのに、ループしていたらエラー
			i++;
			if (i == Pieces.Num())
			{
				UE_LOG(LogTemp, Log, TEXT("All Piece Goal. But Game Is Not Finish."));
				break;
			}
		} while (TurnPiece->IsGoal());

		GamePlayPhase = UInGameManager::GamePlayPhaseType::TurnSetting;
		break;
	}

	case UInGameManager::GamePlayPhaseType::GameEnd:
		return true; 
	}

	return false;
}

/// <summary>
/// リザルト処理
/// </summary>
/// <return> フェーズ終了 = true </return>
bool UInGameManager::Result(float DeltaTime)
{
	switch (ResultPhase)
	{
	case UInGameManager::ResultPhaseType::Start:
		MoneyRankWidget->Close();

		ResultWidget->SetMoneyForAllPiece(Pieces);
		ResultWidget->Arrangement();

		ResultPhase = UInGameManager::ResultPhaseType::View;
		break;

	case UInGameManager::ResultPhaseType::View:
		ResultIntervalTime += DeltaTime;
		if (ResultIntervalTime >= ResultMaxIntervalTime)
		{
			ResultIntervalTime = 0;
			if (!ResultWidget->IsAllOpen())
			{
				ResultWidget->OpenRankForBack();
			}
			else
			{
				ResultPhase = UInGameManager::ResultPhaseType::End;
			}
		}
		break;

	case UInGameManager::ResultPhaseType::GameEnd:
		return true;

	}

	return false;
}

/// <summary>
/// 終了処理
/// </summary>
/// <return> フェーズ終了 = true </return>
bool UInGameManager::End()
{
	return true;
}

/// <summary>
/// サイコロを準備するイベント
/// </summary>
void UInGameManager::StartDice()
{
	if (!IsValid(Dice)) return;
	Dice->Start();
}

/// <summary>
/// サイコロを転がすイベント
/// </summary>
void UInGameManager::RollDice()
{
	if (!IsValid(Dice)) return;
	Dice->Roll();
}

/// <summary>
/// サイコロを投げるイベント
/// </summary>
void UInGameManager::ThrowDice()
{
	if (!IsValid(Dice)) return;
	Dice->Throw();
}

/// <summary>
/// サイコロが振り終わったか
/// </summary>
/// <return>振り終わった = true </return>
bool UInGameManager::IsThrowDiceEnd()
{
	return Dice->IsThrowEnd();
}

/// <summary>
/// サイコロの目を設定する
/// </summary>
/// <return> サイコロの目 </return>
void UInGameManager::SetDiceValue(int Value)
{
	Dice->SetValue(Value);
}

/// <summary>
/// サイコロの目を取得するイベント
/// </summary>
/// <return> サイコロの目 </return>
int UInGameManager::GetDiceValue()
{
	return Dice->GetValue();
}

/// <summary>
/// サイコロの目を1減らすイベント
/// </summary>
/// <return> サイコロの目 </return>
int UInGameManager::ValueCountDown()
{
	Dice->CountDown();
	return Dice->GetValue();;
}

/// <summary>
/// 同じマスにいるコマ数を取得するイベント
/// </summary>
/// <param> コマ数 </param>
int UInGameManager::GetNowSpacePieceNum(ABasePiece* Original)
{
	int Num = 0;
	for (ABasePiece* Piece : Pieces)
	{
		if (Original == Piece) continue;
		if (Original->GetNowSpace() == Piece->GetNowSpace())
			Num++;
	}
	return Num;
}

/// <summary>
/// 同じマスにいる待機コマの位置更新
/// </summary>
/// <param> 移動するコマ </param>
void UInGameManager::UpdateNowSpaceidelPiece(ABasePiece* Original)
{
	for (ABasePiece* Piece : Pieces)
	{
		if (Original == Piece) continue;
		if (Original->GetNowSpace() == Piece->GetNowSpace())
		{
			if (Original->GetSpaceIdelNum() < Piece->GetSpaceIdelNum())
				Piece->SlideSpaceIdel();
				Piece->TeleportNowSpaceIdel();
		}
	}
}

/// <summary>
/// コマがお金を得る
/// </summary>
/// <param> コマ </param>
/// <param> 金額 </param>
void UInGameManager::GetCoineEmmit(ABasePiece* Piece, int Money)
{
	CoinEmmiter->GetCoineEmmit(Piece, Money);
}

/// <summary>
/// コマがお金を失う
/// </summary>
/// <param> コマ </param>
/// <param> 金額 </param>
void UInGameManager::LostCoineEmmit(ABasePiece* Piece, int Money)
{
	CoinEmmiter->LostCoineEmmit(Piece, Money);
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void UInGameManager::OnDecide()
{
	if(ResultPhase == UInGameManager::ResultPhaseType::End)
		ResultPhase = UInGameManager::ResultPhaseType::GameEnd;

	//メッセージ送り
	if (!IsValid(TurnPiece)) return;
	if (!TurnPiece->IsPlayer()) return;

	TurnPiece->InputKey(UKeyType::Dicide);

	if (!IsValid(MessageWidget)) return;
	MessageWidget->SendMessageInputEvent();

	//カメラリセット
	//プレイヤーがカメラリセットを条件にしているのでプレイヤーより後に置く
 	if (!IsValid(Camera)) return;
	if (!IsValid(CameraMoveWidget)) return;

	if (!CameraMoveWidget->GetIsOpen()) return;
	Camera->ResetMoveLocation();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void UInGameManager::OnUp()
{
	if (!IsValid(TurnPiece)) return;
	if (!TurnPiece->IsPlayer()) return;

	TurnPiece->InputKey(UKeyType::Up);
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void UInGameManager::OnDown()
{
	if (!IsValid(TurnPiece)) return;
	if (!TurnPiece->IsPlayer()) return;

	TurnPiece->InputKey(UKeyType::Down);
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void UInGameManager::OnLeft()
{
	if (!IsValid(TurnPiece)) return;
	if (!TurnPiece->IsPlayer()) return;

	TurnPiece->InputKey(UKeyType::Left);
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void UInGameManager::OnRight()
{
	if (!IsValid(TurnPiece)) return;
	if (!TurnPiece->IsPlayer()) return;

	TurnPiece->InputKey(UKeyType::Right);
}

/// <summary>
/// カメラの移動
/// </summary>
void UInGameManager::CameraMove()
{
	APlayerMenuController* PlayerMenuController = Cast<APlayerMenuController>(GetWorld()->GetFirstPlayerController());
	if (PlayerMenuController->IsUpOther()) OnUpOther();
	if (PlayerMenuController->IsDownOther()) OnDownOther();
	if (PlayerMenuController->IsLeftOther()) OnLeftOther();
	if (PlayerMenuController->IsRightOther()) OnRightOther();
}

/// <summary>
/// 第2の上キーを押したときのイベント
/// </summary>
void UInGameManager::OnUpOther()
{
	if (!IsValid(Camera)) return;
	if (!IsValid(CameraMoveWidget)) return;

	if (!CameraMoveWidget->GetIsOpen()) return;
	Camera->AddMoveLocation(FVector::LeftVector * CameraMoveSpeed);
}

/// <summary>
/// 第2の下キーを押したときのイベント
/// </summary>
void UInGameManager::OnDownOther()
{
	if (!IsValid(Camera)) return;
	if (!IsValid(CameraMoveWidget)) return;

	if (!CameraMoveWidget->GetIsOpen()) return;
	Camera->AddMoveLocation(FVector::RightVector * CameraMoveSpeed);
}

/// <summary>
/// 第2の左キーを押したときのイベント
/// </summary>
void UInGameManager::OnLeftOther()
{
	if (!IsValid(Camera)) return;
	if (!IsValid(CameraMoveWidget)) return;

	if (!CameraMoveWidget->GetIsOpen()) return;
	Camera->AddMoveLocation(FVector::BackwardVector * CameraMoveSpeed);
}

/// <summary>
/// 第2の右キーを押したときのイベント
/// </summary>
void UInGameManager::OnRightOther()
{
	if (!IsValid(Camera)) return;
	if (!IsValid(CameraMoveWidget)) return;

	if (!CameraMoveWidget->GetIsOpen()) return;
	Camera->AddMoveLocation(FVector::ForwardVector * CameraMoveSpeed);
}

/// <summary>
/// 分岐選択を開くイベント
/// </summary>
void UInGameManager::OpenSelectBranch()
{
	if (!IsValid(TurnPiece)) return;
	if (SelectBranchArrow == nullptr) return;

	SelectBranchArrow->Reset();
	ABaseSpace* TurnPlayerSpace = TurnPiece->GetNowSpace();
	TMap<UDirection, FMoveDirectionStruct> MoveDirections = TurnPlayerSpace->GetAllMoveDirectionsFillter(GetBoth(TurnPiece->GetDirection()));
	for (TTuple<UDirection, FMoveDirectionStruct> MoveDirection : MoveDirections)
	{
		SelectBranchArrow->Visible(MoveDirection.Key);
	}
	SelectBranchArrow->PieceSpot(TurnPiece);
}

/// <summary>
/// 分岐選択を閉じるイベント
/// </summary>
void UInGameManager::CloseSelectBranch()
{
	if (SelectBranchArrow == nullptr) return;
	SelectBranchArrow->Reset();
}

/// <summary>
/// 分岐を選択するイベント
/// </summary>
void UInGameManager::SelectBranch(UDirection Direction)
{
	SelectBranchArrow->SelectArrow(Direction);
}

/// <summary>
/// 所持金を増やすイベント
/// </summary>
/// <param> 増やしたコマ </param>
void UInGameManager::AddMoney(ABasePiece* target)
{
	MoneyRankWidget->AddMoneyAnimation(target->GetID(), target->GetMoney());
}

/// <summary>
/// 所持金を減らすイベント
/// </summary>
/// <param> 減らしたコマ </param>
void UInGameManager::LostMoney(ABasePiece* target)
{
	MoneyRankWidget->LostMoneyAnimation(target->GetID(), target->GetMoney());
}

/// <summary>
/// メッセージを表示
/// </summary>
/// <param> 内容 </param>
void UInGameManager::OpenMessage(const FString& Text, SendMessageType MessageType)
{
	MessageWidget->Open();
	MessageWidget->SetText(Text);
	MessageWidget->SetSendMessageType(MessageType);
}

/// <summary>
/// メッセージを閉じる
/// </summary>
void UInGameManager::CloseMessage()
{
	MessageWidget->Close();
}

/// <summary>
/// メッセージを表示中かを返す
/// </summary>
bool UInGameManager::IsMessage()
{
	if (!IsValid(MessageWidget)) return false;
	return MessageWidget->GetIsOpen();
}

/// <summary>
/// カメラの移動を開始する
/// </summary>
void UInGameManager::CameraMoveStart()
{
	if (!IsValid(CameraMoveWidget)) return;
	CameraMoveWidget->Open();
}

/// <summary>
/// カメラの移動を終了する
/// </summary>
void UInGameManager::CameraMoveEnd()
{
	if (!IsValid(CameraMoveWidget)) return;
	CameraMoveWidget->Close();
}

/// <summary>
/// カメラが移動しているかを返す
/// </summary>
/// <return>移動中 = true</return>
bool UInGameManager::IsCameraMove()
{
	if (!IsValid(Camera))return false;
	return Camera->IsMoveLocation();
}

/// <summary>
/// コマがゴールしたときのイベント
/// </summary>
/// <param>ゴールしたコマ</param>
void UInGameManager::GoalPiece(ABasePiece* Target)
{
	GoalPieceNum++;
}