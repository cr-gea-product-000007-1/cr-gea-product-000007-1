﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/PlayerMenuController.h"


//SetupInputComponentの継承
void APlayerMenuController::SetupInputComponent()
{
	Super::SetupInputComponent();
	check(InputComponent);

	if (IsValid(InputComponent))
	{
		InputComponent->BindAction("Decide", IE_Pressed, this, &APlayerMenuController::OnDecide);
		InputComponent->BindAction("Up", IE_Pressed, this, &APlayerMenuController::OnUp);
		InputComponent->BindAction("Down", IE_Pressed, this, &APlayerMenuController::OnDown);
		InputComponent->BindAction("Left", IE_Pressed, this, &APlayerMenuController::OnLeft);
		InputComponent->BindAction("Right", IE_Pressed, this, &APlayerMenuController::OnRight);

		InputComponent->BindAction("UpOther", IE_Pressed, this, &APlayerMenuController::OnUpOther);
		InputComponent->BindAction("DownOther", IE_Pressed, this, &APlayerMenuController::OnDownOther);
		InputComponent->BindAction("LeftOther", IE_Pressed, this, &APlayerMenuController::OnLeftOther);
		InputComponent->BindAction("RightOther", IE_Pressed, this, &APlayerMenuController::OnRightOther);

		InputComponent->BindAction("UpOther", IE_Released, this, &APlayerMenuController::OutUpOther);
		InputComponent->BindAction("DownOther", IE_Released, this, &APlayerMenuController::OutDownOther);
		InputComponent->BindAction("LeftOther", IE_Released, this, &APlayerMenuController::OutLeftOther);
		InputComponent->BindAction("RightOther", IE_Released, this, &APlayerMenuController::OutRightOther);
	}
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnDecide()
{
 	if (PlayerMenuInterface)
		PlayerMenuInterface->OnDecide();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnUp()
{
	if (PlayerMenuInterface)
		PlayerMenuInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnDown()
{
	if (PlayerMenuInterface)
		PlayerMenuInterface->OnDown();
}

/// <summary>
/// 左キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnLeft()
{
	if (PlayerMenuInterface != nullptr)
		PlayerMenuInterface->OnLeft();
}

/// <summary>
/// 右キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnRight()
{
	if (PlayerMenuInterface != nullptr)
		PlayerMenuInterface->OnRight();
}


/// <summary>
/// 第2の上キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnUpOther()
{
	bIsUpOther = true;
}

/// <summary>
/// 第2の下キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnDownOther()
{
	bIsDownOther = true;
}

/// <summary>
/// 第2の左キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnLeftOther()
{
	bIsLeftOther = true;
}

/// <summary>
/// 第2の右キーを押したときのイベント
/// </summary>
void APlayerMenuController::OnRightOther()
{
	bIsRightOther = true;
}


/// <summary>
/// 第2の上キーを離したときのイベント
/// </summary>
void APlayerMenuController::OutUpOther()
{
	bIsUpOther = false;
}

/// <summary>
/// 第2の下キーを離したときのイベント
/// </summary>
void APlayerMenuController::OutDownOther()
{
	bIsDownOther = false;
}

/// <summary>
/// 第2の左キーを離したときのイベント
/// </summary>
void APlayerMenuController::OutLeftOther()
{
	bIsLeftOther = false;
}

/// <summary>
/// 第2の右キーを離したときのイベント
/// </summary>
void APlayerMenuController::OutRightOther()
{
	bIsRightOther = false;
}