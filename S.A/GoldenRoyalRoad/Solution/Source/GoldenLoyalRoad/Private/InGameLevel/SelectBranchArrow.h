﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Direction.h"
#include "Arrow.h"
#include "SelectBranchArrow.generated.h"

/**
 * 
 */
UCLASS()
class USelectBranchArrow : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	void Initialize();

	/// <summary>
	/// 矢印を初期状態にする表示
	/// </summary>
	void Reset();

	/// <summary>
	/// 矢印を表示にする
	/// </summary>
	/// <param> 方向 </param>
	void Visible(UDirection KeyDirection);

	/// <summary>
	/// 矢印を選択状態にする
	/// </summary>
	/// <param> 方向 </param>
	void SelectArrow(UDirection KeyDirection);

	/// <summary>
	/// コマに合わせる
	/// </summary>
	/// <param> 対象コマ </param>
	void PieceSpot(ABasePiece* Target);
	
private:
	/// <summary>
	/// 選択方向の矢印
	/// </summary>
	UPROPERTY()
	TMap<UDirection, AArrow*> Arrows;
};
