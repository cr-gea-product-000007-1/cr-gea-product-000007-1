// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Coin.generated.h"

UCLASS()
class ACoin : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoin();

	/// <summary>
	/// 出現時間を設定
	/// </summary>
	void SetDelayTime(float Time) { DelayTime = Time; }

	/// <summary>
	/// 出現時間
	/// </summary>
	UPROPERTY(BlueprintReadOnly)
	float DelayTime;

};
