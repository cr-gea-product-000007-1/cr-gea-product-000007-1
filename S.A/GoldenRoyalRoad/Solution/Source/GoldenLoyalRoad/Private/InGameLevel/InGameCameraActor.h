﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "Piece/BasePiece.h"
#include "InGameCameraActor.generated.h"

/**
 * 
 */
UCLASS()
class AInGameCameraActor : public ACameraActor
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// コマの照準を合わせる
	/// </summary>
	/// <param> 対象コマ </param>
	void PieceSpot(ABasePiece* Target);

	/// <summary>
	/// カメラを動かす
	/// </summary>
	/// <param> 移動ベクトル </param>
	void AddMoveLocation(FVector Move) { MoveLocation += Move; }

	/// <summary>
	/// カメラを初期値に動かす
	/// </summary>
	void ResetMoveLocation() { MoveLocation = FVector::ZeroVector; }

	/// <summary>
	/// カメラが動いた位置にいるかを返す動かす
	/// </summary>
	/// <return>動いた = true</return>
	bool IsMoveLocation() { return MoveLocation != FVector::ZeroVector; }

private:
	/// <summary>
	/// コマに照準を合わせる位置
	/// </summary>
	UPROPERTY(EditAnywhere)
	FVector PieceSpotLocation;

	/// <summary>
	/// コマに照準を合わせる向き
	/// </summary>
	UPROPERTY(EditAnywhere)
	FRotator PieceSpotRotation;

	/// <summary>
	/// カメラの動かした位置
	/// </summary>
	UPROPERTY(EditAnywhere)
	FVector MoveLocation;
};
