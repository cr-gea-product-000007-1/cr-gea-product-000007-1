﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/SelectBranchArrow.h"
#include "../../PathConstant.h"
#include "../../GameConstant.h"
#include "Components/ChildActorComponent.h"


/// <summary>
/// コマに対する表示位置
/// </summary>
const FVector PieceViewLocation = FVector(0.0f, 0.0f, 50.0f);

/// <summary>
/// カメラに合わせる向き
/// </summary>
const FRotator SpotRotation = FRotator(0.0f, 0.0f, 0.0f);

/// <summary>
/// 初期設定
/// </summary>
void USelectBranchArrow::Initialize()
{

	FString Path = Path::ArrowPath;
	TSubclassOf<class AArrow> BaseClass = TSoftClassPtr<AArrow>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(BaseClass))
	{
		AArrow* UpArrow = GetWorld()->SpawnActor<AArrow>(BaseClass);
		UpArrow->SetActorRotation(GameConstant::DirectionRotatos[UDirection::Up]);
		Arrows.Add(TTuple<UDirection, AArrow*>(UDirection::Up, UpArrow));

		AArrow* DownArrow = GetWorld()->SpawnActor<AArrow>(BaseClass);
		DownArrow->SetActorRotation(GameConstant::DirectionRotatos[UDirection::Down]);
		Arrows.Add(TTuple<UDirection, AArrow*>(UDirection::Down, DownArrow));

		AArrow* LeftArrow = GetWorld()->SpawnActor<AArrow>(BaseClass);
		LeftArrow->SetActorRotation(GameConstant::DirectionRotatos[UDirection::Left]);
		Arrows.Add(TTuple<UDirection, AArrow*>(UDirection::Left, LeftArrow));

		AArrow* RightArrow = GetWorld()->SpawnActor<AArrow>(BaseClass);
		RightArrow->SetActorRotation(GameConstant::DirectionRotatos[UDirection::Right]);
		Arrows.Add(TTuple<UDirection, AArrow*>(UDirection::Right, RightArrow));
	}
}

/// <summary>
/// 矢印を初期状態にする表示
/// </summary>
void USelectBranchArrow::Reset()
{
	for (TTuple<UDirection, AArrow*> Arrow : Arrows)
	{
		Arrow.Value->NotVisible();
		Arrow.Value->NotSelect();
	}
}

/// <summary>
/// 矢印を表示にする
/// </summary>
/// <param> 方向 </param>
void USelectBranchArrow::Visible(UDirection KeyDirection)
{
	Arrows[KeyDirection]->Visible();
}

/// <summary>
/// 矢印を選択状態にする
/// </summary>
/// <param> 方向 </param>
void USelectBranchArrow::SelectArrow(UDirection KeyDirection)
{
	for (TTuple<UDirection, AArrow*> Arrow : Arrows)
	{
		Arrow.Value->NotSelect();
	}
	Arrows[KeyDirection]->Select();
}

/// <summary>
/// コマに合わせる
/// </summary>
/// <param> 対象コマ </param>
void USelectBranchArrow::PieceSpot(ABasePiece* Target)
{

	FVector Location = PieceViewLocation;
	if (IsValid(Target))
	{
		Location += Target->GetActorLocation();
	}
	for (TTuple<UDirection, AArrow*> Arrow : Arrows)
	{
		Arrow.Value->SetActorLocation(Location);
	}
	
	/*
	* 子設定方法がわかったら変更
	FVector Location = PieceViewLocation;
	if (IsValid(Target))
	{
		Location += Target->GetActorLocation();
	}

	SetActorLocation(Location);
	SetActorRotation(SpotRotation);
	*/
}