﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/MessageWidget.h"

/// <summary>
/// メッセージを送る時間
/// </summary>
const float SendMaxTime = 1.2f;

/// <summary>
/// 更新する
/// </summary>
void UMessageWidget::Update(float DeltaTime)
{
	if (MyMessageType != SendMessageType::AutoTime) return;
	Timer += DeltaTime;
	if (Timer > SendMaxTime)
	{
		MyMessageType = SendMessageType::None;
		Close();
	}
}

/// <summary>
/// メッセージを送る入力通知
/// </summary>
void UMessageWidget::SendMessageInputEvent()
{
	if (MyMessageType != SendMessageType::Input) return;
	MyMessageType = SendMessageType::None;
	Close();
}

/// <summary>
/// メッセージの飛ばし方を設定する
/// </summary>
/// <param>メッセージの飛ばし方</param>
void UMessageWidget::SetSendMessageType_Implementation(SendMessageType SendMessageType)
{
	Timer = 0;
	MyMessageType = SendMessageType; 
}