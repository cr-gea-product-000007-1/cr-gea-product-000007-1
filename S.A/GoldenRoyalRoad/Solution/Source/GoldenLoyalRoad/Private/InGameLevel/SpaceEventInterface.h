﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SpaceEventInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USpaceEventInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ISpaceEventInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 所持金を増やすイベント
	/// </summary>
	/// <param> 増やすお金 </param>
	virtual void GetMoneyEvent(int MoneyNum) = 0;

	/// <summary>
	/// 所持金を減らすイベント
	/// </summary>
	/// <param> 減らすお金 </param>
	virtual void LostMoneyEvent(int MoneyNum) = 0;

	/// <summary>
	/// マスを進ませるイベント
	/// </summary>
	/// <param> 進むマス数 </param>
	virtual void GoSpacesEvent(int GoNum) = 0;
};
