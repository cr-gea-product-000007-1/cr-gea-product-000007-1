// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Piece/BasePiece.h"
#include "GoalInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGoalInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IGoalInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// コマがゴールしたときのイベント
	/// </summary>
	/// <param>ゴールしたコマ</param>
	virtual void GoalPiece(ABasePiece* Target) = 0;

	/// <summary>
	/// ゴールしたコマ数を返す
	/// </summary>
	/// <return>ゴールしたコマ</return>
	virtual int GetGoalPieceNum() = 0;
};
