﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/MoneyRankWidget.h"
#include "Piece/BasePiece.h"
#include "../../PathConstant.h"

/// <summary>
/// 所持金UIの間隔
/// </summary>
const float MoneyWidgetSpace = 100.0f;

/// <summary>
/// ウィジットの読み込み
/// </summary>
void UMoneyRankWidget::LoadWidget()
{
	FString Path = Path::MoneyWidgetPath;
	MoneyWidgetClass = TSoftClassPtr<UMoneyWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
}

/// <summary>
/// 表示するコマの追加
/// </summary>
	/// <param>追加するピース</param>
void UMoneyRankWidget::AddPiece(ABasePiece* Target)
{
	if (IsValid(MoneyWidgetClass))
	{
		UMoneyWidget* MoneyWidget = CreateWidget<UMoneyWidget>(GetWorld(), MoneyWidgetClass);
		MoneyWidget->SetPositionInViewport(FVector2D(0.0f, MoneyWidgetSpace * MoneyWidgets.Num()));
		MoneyWidget->AddToViewport();
		MoneyWidget->SetPieceName(Target->GetPieceName());
		MoneyWidget->SetPieceColor(Target->GetPieceType());
		MoneyWidgets.Add(MoneyWidget);
	}
}

/// <summary>
/// 所持金が増えるアニメーション
/// </summary>
/// <param> 対象コマのインデックス </param>
/// <param> 変更後の金額 </param>
void UMoneyRankWidget::AddMoneyAnimation(int Index, int Value)
{
	if (Index < 0) return;
	if (Index >= MoneyWidgets.Num()) return;

	MoneyWidgets[Index]->AddMoney(Value);
}

/// <summary>
/// 所持金が増えるアニメーション
/// </summary>
/// <param> 対象コマのインデックス </param>
/// <param> 変更後の金額 </param>
void UMoneyRankWidget::LostMoneyAnimation(int Index, int Value)
{
	if (Index < 0) return;
	if (Index >= MoneyWidgets.Num()) return;

	MoneyWidgets[Index]->LostMoney(Value);
}

/// <summary>
/// ターン開始アニメーション
/// </summary>
/// <param> 対象コマのインデックス </param>
void UMoneyRankWidget::TurnStartAnimation(int Index)
{
	if (Index < 0) return;
	if (Index >= MoneyWidgets.Num()) return;

	MoneyWidgets[Index]->TurnStart();
}

/// <summary>
/// ターン終了アニメーション
/// </summary>
/// <param> 対象コマのインデックス </param>
void UMoneyRankWidget::TurnEndAnimation(int Index)
{
	if (Index < 0) return;
	if (Index >= MoneyWidgets.Num()) return;

	MoneyWidgets[Index]->TurnEnd();
}

/// <summary>
/// 閉じるイベント
/// </summary>
void UMoneyRankWidget::Close_Implementation()
{
	for (UMoneyWidget* MoneyWidget : MoneyWidgets)
	{
		MoneyWidget->RemoveFromParent();
	}
}