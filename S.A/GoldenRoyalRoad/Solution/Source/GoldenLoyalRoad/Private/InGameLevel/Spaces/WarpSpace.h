﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSpace.h"
#include "../Piece/BasePiece.h"
#include "WarpSpace.generated.h"

/**
 * 
 */
UCLASS()
class AWarpSpace : public ABaseSpace
{
	GENERATED_BODY()

	enum class WarpPhaseType
	{
		InWarp,      //ワープに入る
		WarpMove,    //ワープ先に移動
		InIdelTime,  //ワープ内待機時間
		OutWarp,     //ワープから出る
		OutIdelTime, //ワープ後待機時間
	};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// マスの効果値を設定する
	/// </summary>
	void SetEffectValue(int Value) override;

	/// <summary>
	/// マスのWarp先を表記する
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetWarpIcon(int ID);

	/// <summary>
	/// 開始時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool StartEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 演出中のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EffectEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 終了時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EndEvent(float DeltaTime, ABasePiece* Target) override;

public:
	/// <summary>
	/// ワープ先のIDを取得する
	/// </summary>
	/// <return> ワープ先のID </return>
	int GetWarpID() { return WarpID; }

	/// <summary>
	/// ワープ先のマスを設定する
	/// </summary>
	/// <return> ワープ先のID </return>
	void SetGoToSpace(ABaseSpace* Space) { GoToSpace = Space; }

private:
	/// <summary>
	/// ワープ先のID
	/// </summary>
	int WarpID;

	/// <summary>
	/// ワープ先のマス
	/// </summary>
	UPROPERTY()
	ABaseSpace* GoToSpace;

	/// <summary>
	/// ワープのフェーズ
	/// </summary>
	WarpPhaseType WarpPhase;

	/// <summary>
	/// ワープ内待機時間
	/// </summary>
	float InIdelTime;

	/// <summary>
	/// ワープ先待機時間
	/// </summary>
	float OutIdelTime;
};
