﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Spaces/GoalSpace.h"

/// <summary>
/// ゴール時の賞金
/// </summary>
const int GoalMoney[] = { 100000, 50000, 20000 };

/// <summary>
/// ゴール時の賞金がもらえる人数
/// </summary>
const int GoalMoneyNum = 3;

// Called when the game starts or when spawned
void AGoalSpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::BackSpaces;
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoalSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	if (!IsValid(Target))return false;
	Target->Goal();
	GoalInterface->GoalPiece(Target);
	GoalPhase = GoalPhaseType::PieceName;
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoalSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	switch (GoalPhase)
	{
	case AGoalSpace::GoalPhaseType::PieceName:
		MessageInterface->OpenMessage(Target->GetPieceName() + TEXT("はゴールした！"), SendMessageType::AutoTime);
		GoalPhase = GoalPhaseType::PieceNameMessage;
		break;

	case AGoalSpace::GoalPhaseType::PieceNameMessage:
		if (!MessageInterface->IsMessage())
			GoalPhase = GoalPhaseType::Money;
		break;

	case AGoalSpace::GoalPhaseType::Money:
	{
		int Money = 0;
		int Rank = GoalInterface->GetGoalPieceNum() - 1;
		if (Rank < GoalMoneyNum)
			Money = GoalMoney[Rank];
		FString ValueText;
		ValueText.AppendInt(Money);

		Target->AddMoney(Money);

		MessageInterface->OpenMessage(Target->GetPieceName() + TEXT("は賞金 ") + ValueText + TEXT("G を獲得！！"), SendMessageType::AutoTime);
		GoalPhase = GoalPhaseType::MoneyMessage;
		break;
	}

	case AGoalSpace::GoalPhaseType::MoneyMessage:
		if (!MessageInterface->IsMessage())
			GoalPhase = GoalPhaseType::End;
		break;

	case AGoalSpace::GoalPhaseType::End:
		return true;
	}
	return false;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoalSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	//メッセージを閉じる
	return true;
}