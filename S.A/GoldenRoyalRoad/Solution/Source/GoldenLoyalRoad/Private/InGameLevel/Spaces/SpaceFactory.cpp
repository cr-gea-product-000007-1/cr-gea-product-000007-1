// Fill out your copyright notice in the Description page of Project Settings.

#include "InGameLevel/Spaces/SpaceFactory.h"
#include "NormalSpace.h"
#include "GetMoneySpace.h"
#include "LostMoneySpace.h"
#include "GoSpacesSpace.h"
#include "BackSpacesSpace.h"
#include "WarpSpace.h"
#include <iostream>
#include <String>
#include <algorithm>
#include "../../../PathConstant.h"


/// <summary>
/// マスの生成
/// </summary>
/// <param> 生成文字列 </param>
/// <return> 生成したマス </return>
ABaseSpace* USpaceFactory::CreateSpace(FString& ID)
{
	ABaseSpace* NewSpace = nullptr;

	if (ID.IsEmpty()) return NewSpace;

	FString ReadID = ID;

	//--------------------------------------------------
	// データの読み込み
	//--------------------------------------------------

	//マスの種類
	FString SpaceIDFStr = ReadID.Left(2);//先頭2文字を取り出す
	ReadID = ReadID.RightChop(2);//先頭2文字を削る

	// std::stringを経由し、intに変換
	std::string SpaceIDStr = TCHAR_TO_UTF8(*SpaceIDFStr);
	uint8 SpaceID = 0;
	if (std::all_of(SpaceIDStr.cbegin(), SpaceIDStr.cend(), isdigit))
	{
		SpaceID = std::stoi(SpaceIDStr);
	}
	else {
		return NewSpace;
	}


	//分岐の通行
	FString Pass = ReadID.Left(1);//3文字目を取り出す
	ReadID = ReadID.RightChop(1);//3文字目を削る
	bool bPass = (Pass[0] == '*');

	//マスの効果
	FString SpaceValueFStr = ReadID;//残りを全て取り出す

	// std::stringを経由し、intに変換
	std::string SpaceValueStr = TCHAR_TO_UTF8(*SpaceValueFStr);
	int EffectValue = 0;
	if (std::all_of(SpaceValueStr.cbegin(), SpaceValueStr.cend(), isdigit))
	{
		EffectValue = std::stoi(SpaceValueStr);
	}


	//--------------------------------------------------
	// マスの生成
	//--------------------------------------------------

	SpaceType SpaceTypeID = (SpaceType)SpaceID;
	switch (SpaceTypeID)
	{
		case SpaceType::None:
			break;

		case SpaceType::Normal:
		{
			FString Path = Path::NormalSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::GetMoney:
		{
			FString Path = Path::GetMoneySpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::LostMoney:
		{
			FString Path = Path::LostMoneySpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::GoSpaces:
		{
			FString Path = Path::GoSpacesSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::BackSpaces:
		{
			FString Path = Path::BackSpacesSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::Warp:
		{
			FString Path = Path::WarpSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::Start:
		{
			FString Path = Path::StartSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}

		case SpaceType::Goal:
		{
			FString Path = Path::GoalSpacePath;
			TSubclassOf<class ABaseSpace> BaseClass = TSoftClassPtr<ABaseSpace>(FSoftObjectPath(*Path)).LoadSynchronous();
			if (IsValid(BaseClass))
			{
				NewSpace = GetWorld()->SpawnActor<ABaseSpace>(BaseClass);
			}
			break;
		}
	}

	//マスの初期設定
	if (IsValid(NewSpace))
	{
		NewSpace->SetSpaceType(SpaceTypeID);
		NewSpace->SetBranchFromEnter(bPass);
		NewSpace->SetEffectValue(EffectValue);
	}

		
	return NewSpace;
}