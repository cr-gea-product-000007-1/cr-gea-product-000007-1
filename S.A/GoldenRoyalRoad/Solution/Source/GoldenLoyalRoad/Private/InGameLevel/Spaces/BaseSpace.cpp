﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseSpace.h"
#include "../Piece/BasePiece.h"
#include "../../../GameConstant.h"


// Sets default values
ABaseSpace::ABaseSpace()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABaseSpace::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseSpace::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/// <summary>
/// このマスから移動できる方向を追加する
/// </summary>
void ABaseSpace::AddMoveDirectionStruct(UDirection Direction, ABaseSpace* Space)
{
	FMoveDirectionStruct MoveDirectionStruct{ UDirection::None, nullptr, 0, 0 };
	MoveDirectionStruct.MoveDirection = Direction;
	MoveDirectionStruct.GoToSpace = Space;
	TTuple<UDirection, FMoveDirectionStruct> MoveDirection { Direction, MoveDirectionStruct };
	MoveDirections.Add(MoveDirection);
}

/// <summary>
/// ゴールまでの最短距離を設定する
/// </summary>
void ABaseSpace::SetGoalMinDistance(int32 GoalDistance)
{
	for (TTuple<UDirection, FMoveDirectionStruct> MoveDirection : MoveDirections)
	{
		FMoveDirectionStruct MoveDirectionStruct = MoveDirection.Value;
		if (MoveDirectionStruct.GoalMinDistance < GoalDistance || MoveDirectionStruct.GoalMinDistance == 0)
			MoveDirectionStruct.GoalMinDistance = GoalDistance;
	}
}

/// <summary>
/// ゴールまでの最長距離を設定する
/// </summary>
void ABaseSpace::SetGoalMaxDistance(int32 GoalDistance)
{
	for (TTuple<UDirection, FMoveDirectionStruct> MoveDirection : MoveDirections)
	{
		FMoveDirectionStruct MoveDirectionStruct = MoveDirection.Value;
		if (MoveDirectionStruct.GoalMaxDistance > GoalDistance || MoveDirectionStruct.GoalMaxDistance == 0)
			MoveDirectionStruct.GoalMaxDistance = GoalDistance;
	}
}

/// <summary>
/// 隣接するマスのゴールまでの距離を設定する
/// 再帰的に繋がっているマスを伝い、ゴールまで設定する
/// </summary>
/// <param> 進行方向 </param>
TTuple<int, int> ABaseSpace::SetConectAllSpaceGoalDirection(UDirection Direction)
{
	TTuple<int, int> MyGoalDictance{ 0, 0 };
	//隣接マスから繋がっている方向
	for (TTuple<UDirection, FMoveDirectionStruct>& MoveDirection : MoveDirections)
	{
		FMoveDirectionStruct& MoveDirectionStruct = MoveDirection.Value;
		//今来た方向と反対方向は戻ってしまうので除外
		if (MoveDirectionStruct.MoveDirection == GetBoth(Direction)) continue;
		ABaseSpace* GoToSpace = MoveDirectionStruct.GoToSpace;

		if (MoveDirectionStruct.GoalMaxDistance == GameConstant::MaxGoalDistance) {
			GEngine->AddOnScreenDebugMessage(-1, 100.0f, FColor::Red, "MapLoopError。");
			return TTuple<int, int>(0,0);
		}
		//ゴールなら終了
		if (GoToSpace->GetSpaceType() != SpaceType::Goal)
		{
			//隣接マスのこの関数を再帰的に呼び出す
			TTuple<int, int> OtherGoalDictance = GoToSpace->SetConectAllSpaceGoalDirection(MoveDirectionStruct.MoveDirection);
			//隣接マスの距離+1をこのマスの距離に設定
			OtherGoalDictance.Key++;
			OtherGoalDictance.Value++;
			//最短距離設定
			if (MyGoalDictance.Key > OtherGoalDictance.Key || MyGoalDictance.Key == 0)
				MyGoalDictance.Key = OtherGoalDictance.Key;
			//最長距離設定
			if (MyGoalDictance.Value < OtherGoalDictance.Value)
				MyGoalDictance.Value = OtherGoalDictance.Value;
			//距離を設定
			MoveDirectionStruct.GoalMinDistance = OtherGoalDictance.Key;
			MoveDirectionStruct.GoalMaxDistance = OtherGoalDictance.Value;
		}
	}
	return MyGoalDictance;
}

/// <summary>
/// このマスのイベント
/// </summary>
void ABaseSpace::SpaceEvent(float DeltaTime, ABasePiece* Target)
{
	switch (EventPhase)
	{
	case SpaceEventPhase::Start:
		if(StartEvent(DeltaTime, Target))
			EventPhase = SpaceEventPhase::Effect;
		break;

	case SpaceEventPhase::Effect:
		if (EffectEvent(DeltaTime, Target))
			EventPhase = SpaceEventPhase::End;
		break;

	case SpaceEventPhase::End:
		if (EndEvent(DeltaTime, Target))
			EventPhase = SpaceEventPhase::None;
		break;
	}
}

/// <summary>
/// このマスが分岐かを返す
/// </summary>
/// <param> 進行方向 </param>
/// <return> 分岐 = true </return>
bool ABaseSpace::IsBranch(UDirection Key)
{
	int BranchNum = MoveDirections.Num();
	//進行方向は分岐に含まない
	if (MoveDirections.Contains(GetBoth(Key)))
		BranchNum--;

	//複数道があれば分岐
	return BranchNum >= 2;
}

/// <summary>
/// 移動方向が存在するか返す
/// </summary>
/// <param> 方向のキー </param>
/// <returns> 存在する = true </returns>
bool ABaseSpace::ExitMoveDirection(UDirection Key)
{
	return MoveDirections.Contains(Key);
}

/// <summary>
/// 移動方向を取得する
/// </summary>
/// <param> 方向のキー </param>
/// <returns> 方向 </returns>
FMoveDirectionStruct ABaseSpace::GetMoveDirection(UDirection Key)
{
	if (MoveDirections.Contains(Key))
		return MoveDirections[Key];
	else
		return FMoveDirectionStruct{ UDirection::None, nullptr, 0, 0 };
}

/// <summary>
/// 先頭の移動方向を取得する
/// </summary>
/// <returns> 方向 </returns>
FMoveDirectionStruct ABaseSpace::GetBiginMoveDirection()
{
	return MoveDirections.begin().Value();
}

/// <summary>
/// 移動方向を全て取得する
/// </summary>
/// <returns> 方向 </returns>
TMap<UDirection, FMoveDirectionStruct> ABaseSpace::GetAllMoveDirections()
{
	return MoveDirections;
}

/// <summary>
/// 移動方向を1つを除いた全て取得する
/// </summary>
/// <param> 除く方向 </param>
/// <returns> 方向 </returns>
TMap<UDirection, FMoveDirectionStruct> ABaseSpace::GetAllMoveDirectionsFillter(UDirection Fillter)
{
	TMap<UDirection, FMoveDirectionStruct> ReturnMoveDirections;
	for (TTuple<UDirection, FMoveDirectionStruct> MoveDirection : MoveDirections)
	{
		if (MoveDirection.Key == Fillter) continue;
		ReturnMoveDirections.Add(MoveDirection);
	}
	return ReturnMoveDirections;
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABaseSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABaseSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABaseSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}