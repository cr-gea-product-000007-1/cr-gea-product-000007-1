// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseSpace.h"
#include "SpaceFactory.generated.h"

/**
 * 
 */
UCLASS()
class USpaceFactory : public UObject
{
	GENERATED_BODY()

public:

	/// <summary>
	/// マスの生成
	/// </summary>
	/// <param> 生成文字列 </param>
	/// <return> 生成したマス </return>
	ABaseSpace* CreateSpace(FString& ID);

};
