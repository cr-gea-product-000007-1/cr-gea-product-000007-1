﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GetMoneySpace.h"


// Called when the game starts or when spawned
void AGetMoneySpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::Normal;
}

/// <summary>
/// マスの効果値を設定する
/// </summary>
void AGetMoneySpace::SetEffectValue(int Value)
{
	GetMoneyNum = Value;
	SetValueText(Value);
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGetMoneySpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	if (!IsValid(Target))return false;
	Target->AddMoney(GetMoneyNum);

	FString ValueText;
	ValueText.AppendInt(GetMoneyNum);
	MessageInterface->OpenMessage(Target->GetPieceName() + TEXT("は、") + ValueText + TEXT("G ゲットした！"), SendMessageType::AutoTime);
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGetMoneySpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	if (MessageInterface->IsMessage()) return false;
	else return true;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGetMoneySpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}