﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSpace.h"
#include "../Piece/BasePiece.h"
#include "GoSpacesSpace.generated.h"

/**
 * 
 */
UCLASS()
class AGoSpacesSpace : public ABaseSpace
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// マスの効果値を設定する
	/// </summary>
	void SetEffectValue(int Value) override;

	/// <summary>
	/// マスの効果値を表記する
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetValueText(int Value);

	/// <summary>
	/// 開始時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool StartEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 演出中のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EffectEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 終了時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EndEvent(float DeltaTime, ABasePiece* Target) override;

private:
	/// <summary>
	/// 進むマスの数
	/// </summary>
	int GoSpacesNum;
	
};
