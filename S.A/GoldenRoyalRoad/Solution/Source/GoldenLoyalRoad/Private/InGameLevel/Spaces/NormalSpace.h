﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSpace.h"
#include "../Piece/BasePiece.h"
#include "NormalSpace.generated.h"

/**
 * 
 */
UCLASS()
class ANormalSpace : public ABaseSpace
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// 開始時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool StartEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 演出中のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EffectEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 終了時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool EndEvent(float DeltaTime, ABasePiece* Target) override;

private:
	/// <summary>
	/// 待機時間
	/// </summary>
	float IdelTime;
};
