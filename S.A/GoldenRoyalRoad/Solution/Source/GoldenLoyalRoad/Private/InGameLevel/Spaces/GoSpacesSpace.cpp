﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GoSpacesSpace.h"


// Called when the game starts or when spawned
void AGoSpacesSpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::GoSpaces;
}

/// <summary>
/// マスの効果値を設定する
/// </summary>
void AGoSpacesSpace::SetEffectValue(int Value)
{
	GoSpacesNum = Value;
	SetValueText(Value);
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoSpacesSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	if (!IsValid(Target)) return false;
	Target->SetMoveSpaceNum(GoSpacesNum);

	FString ValueText;
	ValueText.AppendInt(GoSpacesNum);
	MessageInterface->OpenMessage(ValueText + TEXT("マス進む"), SendMessageType::AutoTime);
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoSpacesSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	if (MessageInterface->IsMessage()) return false;
	else return true;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AGoSpacesSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}