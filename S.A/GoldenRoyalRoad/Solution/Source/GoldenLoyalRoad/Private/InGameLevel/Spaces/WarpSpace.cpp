﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WarpSpace.h"

/// <summary>
/// ワープ内最大待機時間
/// </summary>
float InIdelMaxTime = 1.0f;;

/// <summary>
/// ワープ先最大待機時間
/// </summary>
float OutIdelMaxTime = 1.0f;

// Called when the game starts or when spawned
void AWarpSpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::Warp;
}

/// <summary>
/// マスの効果値を設定する
/// </summary>
void AWarpSpace::SetEffectValue(int Value)
{
	WarpID = Value;
	SetWarpIcon(Value);
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AWarpSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	WarpPhase = WarpPhaseType::InWarp;
	InIdelTime = 0; 
	OutIdelTime = 0;
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AWarpSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	switch (WarpPhase)
	{
	case AWarpSpace::WarpPhaseType::InWarp:
		if(Target->InWarpAnimation())
			WarpPhase = WarpPhaseType::WarpMove;
		break;

	case AWarpSpace::WarpPhaseType::WarpMove:
		Target->SetNowSpace(GoToSpace);
		Target->TeleportNowSpaceActive();
		Target->ResetSpaceLog();
		WarpPhase = WarpPhaseType::InIdelTime;
		break;

	case AWarpSpace::WarpPhaseType::InIdelTime:
		InIdelTime += DeltaTime;
		if(InIdelTime > InIdelMaxTime)
			WarpPhase = WarpPhaseType::OutWarp;
		break;

	case AWarpSpace::WarpPhaseType::OutWarp:
		if (Target->OutWarpAnimation())
			WarpPhase = WarpPhaseType::OutIdelTime;
		break;

	case AWarpSpace::WarpPhaseType::OutIdelTime:
		OutIdelTime += DeltaTime;
		if (OutIdelTime > OutIdelMaxTime)
			return true;
		break;
	}
	return false;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool AWarpSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}