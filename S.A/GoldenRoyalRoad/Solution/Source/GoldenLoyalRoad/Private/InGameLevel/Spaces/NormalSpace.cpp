﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "NormalSpace.h"

/// <summary>
/// 待機時間
/// </summary>
const float IdelMaxTime = 0.8f;


// Called when the game starts or when spawned
void ANormalSpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::Normal;
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ANormalSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	IdelTime = 0;
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ANormalSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	IdelTime += DeltaTime;
	if(IdelTime >= IdelMaxTime)	return true;
	return false;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ANormalSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}