﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../MoveDirectionStruct.h"
#include "../Direction.h"
#include "../MessageInterface.h"
#include "BaseSpace.generated.h"

class ABasePiece;

enum class SpaceType : uint8
{
	None = 0,        //マスなし
	Normal = 1,      //通常マス
	GetMoney = 2,    //所持金増加
	LostMoney = 3,   //所持金減少
	GoSpaces = 4,    //マスを進める
	BackSpaces = 5,  //マスを戻る
	Warp = 6,        //ワープ
	Start = 10,      //スタート
	Goal = 11,       //ゴール
};

UCLASS()
class ABaseSpace : public AActor
{
	GENERATED_BODY()

private:
	enum class SpaceEventPhase : uint8
	{
		None,    //なし
		Start,   //開始
		Effect,  //演出中
		End,     //終了
	};

public:
	// Sets default values for this character's properties
	ABaseSpace();

	/// <summary>
	/// マスの種類
	/// </summary>
	SpaceType GetSpaceType() { return MySpaceType; };

	/// <summary>
	/// メッセージ表示のイベントインターフェースを設定する
	/// </summary>
	void SetMessageEventInterface(const TScriptInterface<IMessageInterface> Interface) { MessageInterface = Interface; };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/// <summary>
	/// マスの種類を設定する
	/// </summary>
	virtual void SetSpaceType(SpaceType SpaceType) { MySpaceType = SpaceType; }

	/// <summary>
	/// 分岐からこのマスに移動できるか設定する
	/// </summary>
	virtual void SetBranchFromEnter(bool Flag) { bBranchFromEnter = Flag; }

	/// <summary>
	/// マスの効果値を設定する
	/// </summary>
	virtual void SetEffectValue(int Value) { };

	/// <summary>
	/// このマスから移動できる方向を追加する
	/// </summary>
	void AddMoveDirectionStruct(UDirection Direction, ABaseSpace* Space);

	/// <summary>
	/// ゴールまでの最短距離を設定する
	/// </summary>
	void SetGoalMinDistance(int32 GoalDistance);

	/// <summary>
	/// ゴールまでの最長距離を設定する
	/// </summary>
	void SetGoalMaxDistance(int32 GoalDistance);

	/// <summary>
	/// 隣接するマスのゴールまでの距離を設定する
	/// 再帰的に繋がっているマスを伝い、ゴールまで設定する
	/// </summary>
	/// <param> 進行方向 </param>
	/// <return> 最短距離、最長距離 </return>
	TTuple<int, int> SetConectAllSpaceGoalDirection(UDirection Direction);

	/// <summary>
	/// イベントのあるマスかを返す
	/// </summary>
	/// <return> イベント中 = true </return>
	bool IsEventSpace() { return MySpaceType != SpaceType::None && MySpaceType != SpaceType::Normal; }

	/// <summary>
	/// イベントを開始する
	/// </summary>
	void SpaceEventStart() { EventPhase = SpaceEventPhase::Start; }

	/// <summary>
	/// このマスのイベント
	/// </summary>
	void SpaceEvent(float DeltaTime, ABasePiece* Target);

	/// <summary>
	/// イベント中かを返す
	/// </summary>
	/// <return> イベント中 = true </return>
	bool IsEvent() { return EventPhase != SpaceEventPhase::None; }

	/// <summary>
	/// 分岐からこのマスに移動できるかを返す
	/// </summary>
	/// <return> 移動できる = true </return>
	bool IsBranchFromEnter() { return bBranchFromEnter; }

	/// <summary>
	/// このマスが分岐かを返す
	/// </summary>
	/// <param> 進行方向 </param>
	/// <return> 分岐 = true </return>
	bool IsBranch(UDirection Key);

	/// <summary>
	/// 移動方向を取得する
	/// </summary>
	/// <param> 方向のキー </param>
	/// <returns> 方向 </returns>
	FMoveDirectionStruct GetMoveDirection(UDirection Key);

	/// <summary>
	/// 移動方向が存在するか返す
	/// </summary>
	/// <param> 方向のキー </param>
	/// <returns> 存在する =true </returns>
	bool ExitMoveDirection(UDirection Key);

	/// <summary>
	/// 先頭の移動方向を取得する
	/// </summary>
	/// <returns> 方向 </returns>
	FMoveDirectionStruct GetBiginMoveDirection();

	/// <summary>
	/// 移動方向を全て取得する
	/// </summary>
	/// <returns> 方向 </returns>
	TMap<UDirection, FMoveDirectionStruct> GetAllMoveDirections();

	/// <summary>
	/// 移動方向を1つを除いた全てを取得する
	/// </summary>
	/// <param> 除く方向 </param>
	/// <returns> 方向 </returns>
	TMap<UDirection, FMoveDirectionStruct> GetAllMoveDirectionsFillter(UDirection Fillter);

protected:
	/// <summary>
	/// 開始時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool StartEvent(float DeltaTime, ABasePiece* Target);

	/// <summary>
	/// 演出中のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool EffectEvent(float DeltaTime, ABasePiece* Target);

	/// <summary>
	/// 終了時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool EndEvent(float DeltaTime, ABasePiece* Target);

public:
	/// <summary>
	/// このマスから移動できる方向
	/// </summary>
	UPROPERTY(EditAnywhere)
	TMap<UDirection, FMoveDirectionStruct> MoveDirections;

	/// <summary>
	/// 分岐からこのマスに移動できるか
	/// </summary>
	UPROPERTY(EditAnywhere)
	bool bBranchFromEnter;

protected:
	/// <summary>
	/// マスの種類
	/// </summary>
	SpaceType MySpaceType;

	/// <summary>
	/// マスのイベントの状態
	/// </summary
	SpaceEventPhase EventPhase;

	/// <summary>
	/// メッセージ表示のイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IMessageInterface> MessageInterface;
};
