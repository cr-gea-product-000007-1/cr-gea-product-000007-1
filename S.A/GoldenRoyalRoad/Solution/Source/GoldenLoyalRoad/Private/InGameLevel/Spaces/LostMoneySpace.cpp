﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "LostMoneySpace.h"


// Called when the game starts or when spawned
void ALostMoneySpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::LostMoney;
}

/// <summary>
/// マスの効果値を設定する
/// </summary>
void ALostMoneySpace::SetEffectValue(int Value)
{
	LostMoneyNum = Value;
	SetValueText(Value);
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ALostMoneySpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	if (!IsValid(Target))return false;
	Target->LostMoney(LostMoneyNum);

	//外部素材が使えるようになったらエフェクト追加
	FString ValueText;
	ValueText.AppendInt(LostMoneyNum);
	MessageInterface->OpenMessage(Target->GetPieceName() + TEXT("は、") + ValueText + TEXT("G 失った！"), SendMessageType::AutoTime);
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ALostMoneySpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	if (MessageInterface->IsMessage()) return false;
	else return true;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ALostMoneySpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	return true;
}