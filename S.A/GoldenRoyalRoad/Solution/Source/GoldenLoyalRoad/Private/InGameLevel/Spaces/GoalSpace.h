﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGameLevel/Spaces/BaseSpace.h"
#include "../Piece/BasePiece.h"
#include "../GoalInterface.h"
#include "GoalSpace.generated.h"

/**
 * 
 */
UCLASS()
class AGoalSpace : public ABaseSpace
{
	GENERATED_BODY()

	enum class GoalPhaseType
	{
		PieceName,        //ゴール者表示
		PieceNameMessage, //ゴール者表示中
		Money,            //賞金授与
		MoneyMessage,     //賞金授与中
		End,              //終了
	};

public:
	/// <summary>
	/// ゴールのイベントインターフェースを設定する
	/// </summary>
	void SetGoalEventInterface(const TScriptInterface<IGoalInterface> Interface) { GoalInterface = Interface; };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// 開始時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool StartEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 演出中のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EffectEvent(float DeltaTime, ABasePiece* Target) override;

	/// <summary>
	/// 終了時のイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool EndEvent(float DeltaTime, ABasePiece* Target) override;
	
private:
	/// <summary>
	/// ゴールマスの状態
	/// </summary>
	GoalPhaseType GoalPhase;

	/// <summary>
	/// ゴールのイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IGoalInterface> GoalInterface;
};
