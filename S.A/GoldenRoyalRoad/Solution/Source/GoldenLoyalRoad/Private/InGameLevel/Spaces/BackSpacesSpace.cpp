﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BackSpacesSpace.h"


// Called when the game starts or when spawned
void ABackSpacesSpace::BeginPlay()
{
	Super::BeginPlay();

	MySpaceType = SpaceType::BackSpaces;
}

/// <summary>
/// マスの効果値を設定する
/// </summary>
void ABackSpacesSpace::SetEffectValue(int Value)
{
	BackSpacesNum = Value;
	SetValueText(Value);
}

/// <summary>
/// 開始時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABackSpacesSpace::StartEvent(float DeltaTime, ABasePiece* Target)
{
	if (!IsValid(Target))return false;
	Target->SetMoveSpaceNum(-BackSpacesNum);

	FString ValueText;
	ValueText.AppendInt(BackSpacesNum);
	MessageInterface->OpenMessage(ValueText + TEXT("マス戻る"), SendMessageType::AutoTime);
	return true;
}

/// <summary>
/// 演出中のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABackSpacesSpace::EffectEvent(float DeltaTime, ABasePiece* Target)
{
	if (MessageInterface->IsMessage()) return false;
	else return true;
}

/// <summary>
/// 終了時のイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABackSpacesSpace::EndEvent(float DeltaTime, ABasePiece* Target)
{
	//メッセージを閉じる
	return true;
}