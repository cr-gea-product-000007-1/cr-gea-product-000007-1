﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/InGameCameraActor.h"


/// <summary>
/// コマの照準を合わせる
/// </summary>
/// <param> 対象コマ </param>
void AInGameCameraActor::PieceSpot(ABasePiece* Target)
{
	FVector Location = PieceSpotLocation;
	Location = PieceSpotLocation + MoveLocation;
	if (IsValid(Target))
	{
		if(Target->IsActive())
			Location += Target->GetActorLocation();
		else
			Location += Target->GetNowSpace()->GetActorLocation() + SpaceCenterLocation;
	}

	SetActorLocation(Location);
	SetActorRotation(PieceSpotRotation);
}