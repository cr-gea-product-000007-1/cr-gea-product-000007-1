﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/ResultWidget.h"
#include "../../PathConstant.h"

/// <summary>
/// UIの縦幅
/// </summary>
const float Height = 350;

/// <summary>
/// 所持金UIの間隔
/// </summary>
const float ResultRankWidgetSpace = 100.0f;

/// <summary>
/// ウィジットの読み込み
/// </summary>
void UResultWidget::LoadWidget()
{
	FString Path = Path::ResultRankWidgetPath;
	ResultRankWidgetClass = TSoftClassPtr<UResultRankWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
}

/// <summary>
/// 表示するコマの追加
/// </summary>
	/// <param>追加するピース</param>
void UResultWidget::AddPiece(ABasePiece* Target)
{
	if (IsValid(ResultRankWidgetClass))
	{
		UResultRankWidget* ResultRankWidget = CreateWidget<UResultRankWidget>(GetWorld(), ResultRankWidgetClass);
		ResultRankWidget->SetPositionInViewport(FVector2D(0.0f, ResultRankWidgetSpace * ResultRankWidgets.Num()));
		ResultRankWidget->AddToViewport();
		ResultRankWidget->SetPieceName(Target->GetPieceName());
		ResultRankWidget->SetPieceColor(Target->GetPieceType());
		ResultRankWidgets.Add(ResultRankWidget);
	}
}

/// <summary>
/// 所持金を設定
/// </summary>
/// <param>設定するピース</param>
void UResultWidget::SetMoneyForAllPiece(TArray<ABasePiece*>& Pieces)
{
	for (int i = 0; i < Pieces.Num(); i++)
	{
		if (ResultRankWidgets.Num() <= i) continue;
		ResultRankWidgets[i]->SetMoney(Pieces[i]->GetMoney());
	}
}

/// <summary>
/// UIの配置変え
/// </summary>
void UResultWidget::Arrangement()
{
	/*
	const がつくので関数が呼べない
	ResultRankWidgets.Sort([](const UResultRankWidget* ConstA, const UResultRankWidget* ConstB)
		{ 
			UResultRankWidget* A = const_cast<UResultRankWidget*>(A);
			UResultRankWidget* B = const_cast<UResultRankWidget*>(B);
			A->GetMoney() < B->GetMoney();
		});*/

	for (int i = 0; i < ResultRankWidgets.Num() - 1; i++)
	{
		UResultRankWidget*& A = ResultRankWidgets[i];
		for (int j = i + 1; j < ResultRankWidgets.Num(); j++)
		{
			UResultRankWidget*& B = ResultRankWidgets[j];
			//同率の場合、順番が後者を上位にする
			if (A->GetMoney() <= B->GetMoney()) 
			{
				UResultRankWidget* Temp = B;
				B = A;
				A = Temp;
			}
		}
	}
	int PieceNum = ResultRankWidgets.Num();
	for (int i = 0; i < PieceNum; i++)
	{
		UResultRankWidget* ResultRankWidget = ResultRankWidgets[i];
		float CenterRate = (float)i - ((float)PieceNum - 1.0f) / 2.0f;
		ResultRankWidget->SetPositionInViewport(FVector2D(0, Height / ((float)PieceNum - 0.5f) * CenterRate));

		ResultRankWidget->SetRank(i + 1);
	}
}

/// <summary>
/// 順位UIを開く
/// </summary>
/// <param> 対象コマのインデックス </param>
void UResultWidget::OpenRank(int Index)
{
	if (Index < 0) return;
	if (Index >= ResultRankWidgets.Num()) return;

	ResultRankWidgets[Index]->Open();
}

/// <summary>
/// 一番下の開いていない順位UIを開く
/// </summary>
void UResultWidget::OpenRankForBack()
{
	for (int i = ResultRankWidgets.Num() - 1; i >= 0; i--)
	{
		UResultRankWidget* RankWidget = ResultRankWidgets[i];
		if (!RankWidget->GetIsOpen())
		{
			RankWidget->Open();
			return;
		}
	}
}

/// <summary>
/// ウィジットを閉じる
/// </summary>
void UResultWidget::Close()
{
	for (UResultRankWidget* RankWidget : ResultRankWidgets)
	{
		RankWidget->Close();
	}
}

/// <summary>
/// 全てのUIが開いているかを返す
/// </summary>
/// <return> 開いている </return>
bool UResultWidget::IsAllOpen()
{
	for (UResultRankWidget* RankWidget : ResultRankWidgets)
	{
		if (!RankWidget->GetIsOpen()) return false;
	}
	return true;
}

