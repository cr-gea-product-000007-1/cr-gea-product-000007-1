﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameBoard.h"
#include "Piece/BasePiece.h"
#include "Piece/PieceFactory.h"
#include "InGameCameraActor.h"
#include "Dice.h"
#include "DiceWidget.h"
#include "CoinEmmiter.h"
#include "MoneyRankWidget.h"
#include "MessageWidget.h"
#include "ResultWidget.h"
#include "SelectBranchArrow.h"
#include "DiceInterface.h"
#include "PieceInterface.h"
#include "PlayerMenuInterface.h"
#include "MoneyRankInterface.h"
#include "MessageInterface.h"
#include "UIInterface.h"
#include "CameraInterface.h"
#include "GoalInterface.h"
#include "InGameManager.generated.h"

/**
 * 
 */
UCLASS()
class UInGameManager : public UObject, public IDiceInterface,public IPieceInterface, public IPlayerMenuInterface, public IUIInterface, public IMoneyRankInterface, public IMessageInterface, public ICameraInterface, public IGoalInterface
{
	GENERATED_BODY()

private:
	enum class CreatePhaseType
	{
		FieldCreate,
		PieceCreate,
		DiceCreate,
		UICreate,
		CameraCreate,
		End,
	};

	enum class GamePlayPhaseType
	{
		GameStart,       //ゲームの開始処理
		TurnSetting,     //手番の初期設定
		ViewTurnPiece,   //手番のコマ表示
		TurnPieceUpdate, //手番のコマの処理
		TurnEnd,         //ターン終了処理
		NextTurn,        //次の手番のコマに移す
		GameEnd,         //ゲーム終了
	};

	enum class ResultPhaseType
	{
		Start,    //開始処理
		View,     //表示
		End,      //リザルトの終了
		GameEnd,  //ゲームの終了へ
	};

public:
	/// <summary>
	/// 初期化処理
	/// </summary>
	void Initialize();

	/// <summary>
	/// 生成処理
	/// </summary>
	/// <return> フェーズ終了 = true </return>
	bool Create();

	/// <summary>
	/// ゲームプレイ処理
	/// </summary>
	/// <param> 前フレームの処理時間 </param>
	/// <return> フェーズ終了 = true </return>
	bool GamePlay(float DeltaTime);

	/// <summary>
	/// リザルト処理
	/// </summary>
	/// <return> フェーズ終了 = true </return>
	bool Result(float DeltaTime);

	/// <summary>
	/// 終了処理
	/// </summary>
	/// <return> フェーズ終了 = true </return>
	bool End();

	/// <summary>
	/// カメラの取得
	/// </summary>
	/// <return> カメラ </return>
	AInGameCameraActor* GetCamera() { return Camera; };

	/// <summary>
	/// サイコロを準備する
	/// </summary>
	void StartDice() override;

	/// <summary>
	/// サイコロを転がすイベント
	/// </summary>
	void RollDice() override;

	/// <summary>
	/// サイコロを投げるイベント
	/// </summary>
	void ThrowDice() override;

	/// <summary>
	/// サイコロが振り終わったか
	/// </summary>
	/// <return>振り終わった = true </return>
	bool IsThrowDiceEnd() override;

	/// <summary>
	/// サイコロの目を設定する
	/// </summary>
	/// <return> サイコロの目 </return>
	void SetDiceValue(int Value) override;

	/// <summary>
	/// サイコロの目を取得するイベント
	/// </summary>
	/// <return> サイコロの目 </return>
	int GetDiceValue() override;

	/// <summary>
	/// サイコロの目を1減らすイベント
	/// </summary>
	/// <return> サイコロの目 </return>
	int ValueCountDown() override;

	/// <summary>
	/// 同じマスにいるコマ数を取得するイベント
	/// </summary>
	/// <param> コマ数 </param>
	int GetNowSpacePieceNum(ABasePiece* Original) override;

	/// <summary>
	/// 同じマスにいる待機コマの位置更新
	/// </summary>
	/// <param> 移動するコマ </param>
	void UpdateNowSpaceidelPiece(ABasePiece* Original) override;

	/// <summary>
	/// コマがお金を得る
	/// </summary>
	/// <param> コマ </param>
	/// <param> 金額 </param>
	void GetCoineEmmit(ABasePiece* Piece, int Money) override;

	/// <summary>
	/// コマがお金を失う
	/// </summary>
	/// <param> コマ </param>
	/// <param> 金額 </param>
	void LostCoineEmmit(ABasePiece* Piece, int Money) override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown() override;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	void OnLeft() override;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	void OnRight() override;

private:
	/// <summary>
	/// カメラの移動
	/// </summary>
	void CameraMove();

	/// <summary>
	/// 第2の上キーを押したときのイベント
	/// </summary>
	void OnUpOther();// override;

	/// <summary>
	/// 第2の下キーを押したときのイベント
	/// </summary>
	 void OnDownOther();// override;

	/// <summary>
	/// 第2の左キーを押したときのイベント
	/// </summary>
	 void OnLeftOther();// override;

	/// <summary>
	/// 第2の右キーを押したときのイベント
	/// </summary>
	 void OnRightOther();// override;

public:
	/// <summary>
	/// 分岐選択を開くイベント
	/// </summary>
	 void OpenSelectBranch() override;

	/// <summary>
	/// 分岐選択を閉じるイベント
	/// </summary>
	void CloseSelectBranch() override;

	/// <summary>
	/// 分岐を選択するイベント
	/// </summary>
	void SelectBranch(UDirection Direction) override;

	/// <summary>
	/// 所持金を増やすイベント
	/// </summary>
	/// <param> 増やしたコマ </param>
	void AddMoney(ABasePiece* target) override;

	/// <summary>
	/// 所持金を減らすイベント
	/// </summary>
	/// <param> 減らしたコマ </param>
	void LostMoney(ABasePiece* target) override;

	/// <summary>
	/// メッセージを表示
	/// </summary>
	/// <param> 内容 </param>
	void OpenMessage(const FString& Text, SendMessageType MessageType) override;

	/// <summary>
	/// メッセージを閉じる
	/// </summary>
	void CloseMessage() override;

	/// <summary>
	/// メッセージを表示中かを返す
	/// </summary>
	bool IsMessage() override;

	/// <summary>
	/// カメラの移動を開始する
	/// </summary>
	void CameraMoveStart() override;

	/// <summary>
	/// カメラの移動を終了する
	/// </summary>
	void CameraMoveEnd() override;

	/// <summary>
	/// カメラが移動しているかを返す
	/// </summary>
	/// <return>移動中 = true</return>
	bool IsCameraMove() override;

	/// <summary>
	/// コマがゴールしたときのイベント
	/// </summary>
	/// <param>ゴールしたコマ</param>
	void GoalPiece(ABasePiece* Target) override;

	/// <summary>
	/// ゴールしたコマ数を返す
	/// </summary>
	/// <return>ゴールしたコマ</return>
	int GetGoalPieceNum() override { return GoalPieceNum; }

private:
	/// <summary>
	/// 生成処理のフェーズ
	/// </summary>
	CreatePhaseType CreatePhase;

	/// <summary>
	/// ゲームプレイのフェーズ
	/// </summary>
	GamePlayPhaseType GamePlayPhase;

	/// <summary>
	/// リザルトのフェーズ
	/// </summary>
	ResultPhaseType ResultPhase;

	/// <summary>
	/// ゲームボード
	/// </summary>
	UPROPERTY()
	UGameBoard* GameBoard;

	/// <summary>
	/// ゲームのコマ
	/// </summary>
	UPROPERTY()
	TArray<ABasePiece*> Pieces;

	/// <summary>
	/// 手番中のコマ
	/// </summary>
	UPROPERTY()
	ABasePiece* TurnPiece;

	/// <summary>
	/// 手番中のコマのインデックス
	/// </summary>
	int TurnPieceIndex;

	/// <summary>
	/// ゴールしたコマの数
	/// </summary>
	int GoalPieceNum;

	/// <summary>
	/// コマを生成する
	/// </summary>
	UPROPERTY()
	UPieceFactory* PieceFactory;

	/// <summary>
	/// カメラ
	/// </summary>
	UPROPERTY()
	AInGameCameraActor* Camera;

	/// <summary>
	/// サイコロ
	/// </summary>
	UPROPERTY()
	UDice* Dice;

	/// <summary>
	/// サイコロのウィジット
	/// </summary>
	UPROPERTY()
	UDiceWidget* DiceWidget;

	/// <summary>
	/// コイン生成
	/// </summary>
	UPROPERTY()
	UCoinEmmiter* CoinEmmiter;

	/// <summary>
	/// リザルトのウィジット
	/// </summary>
	UPROPERTY()
	UResultWidget* ResultWidget;

	/// <summary>
	/// リザルトの表示間隔
	/// </summary>
	float ResultIntervalTime;

	/// <summary>
	/// 分岐選択の矢印
	/// </summary>
	UPROPERTY()
	USelectBranchArrow* SelectBranchArrow;

	/// <summary>
	/// ターン開始までの時間
	/// </summary>
	float TurnStartTime;

	/// <summary>
	/// 所持金のウィジット
	/// </summary>
	UPROPERTY()
	UMoneyRankWidget* MoneyRankWidget;

	/// <summary>
	/// メッセージのウィジット
	/// </summary>
	UPROPERTY()
	UMessageWidget* MessageWidget;

	/// <summary>
	/// カメラ移動ウィジット
	/// </summary>
	UPROPERTY()
	UUiAssignmentWidget* CameraMoveWidget;
};