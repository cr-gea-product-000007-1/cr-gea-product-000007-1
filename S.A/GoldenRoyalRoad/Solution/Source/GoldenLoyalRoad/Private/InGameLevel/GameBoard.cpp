﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/GameBoard.h"

const float SpaceInterval = 200.0f;

/// <summary>
/// 初期化
/// </summary>
void UGameBoard::Initialize()
{
    SpaceFactory = NewObject<USpaceFactory>(GetWorld());
}

/// <summary>
/// 終了処理
/// </summary>
void UGameBoard::Finalize()
{
    if (IsValid(SpaceFactory))
    {
        delete SpaceFactory;
        SpaceFactory = nullptr;
    }

    for (TArray<ABaseSpace*> Line : SpaceTiles)
    {
        for (ABaseSpace*& Element : Line)
        {
            if (IsValid(Element))
            {
                Element->Destroy();
            }
        }
    }
}

/// <summary>
/// Csvファイルのマップデータを読み込む
/// </summary>
/// <param> csvファイルのパス </param>
/// <return> 読み込み成功 = true </return>
bool UGameBoard::LoadCsvData(FString& CsvPath)
{
    if (!IsValid(SpaceFactory)) {
        return false;
    }

    //CSVファイルからデータを取得する
    FString CsvDataFile; // 読み込んだデータを保持する
    bool Result = FFileHelper::LoadFileToString(CsvDataFile, *CsvPath);     
    if (Result == false) return false;

    //列で分解した値を保持する
    TArray<FString> Row;

    //各要素分解した値を保持する
    TArray<FString> Element;

    // \nでCsvDataFileを区切ったものをRowに1つずつ入れる
    CsvDataFile.ParseIntoArray(Row, TEXT("\r\n"), false);

    for (int i = 0; i < Row.Num(); i++)
    {
        //一列ごとのマス情報を保持する
        TArray<ABaseSpace*> SpaceLine;
        // ,でRawを区切ったものをElementに1つずつ入れる
        Row[i].ParseIntoArray(Element, TEXT(","), false);

        for (int j = 0; j < Element.Num(); j++) {
           ABaseSpace* NewSpace = SpaceFactory->CreateSpace(Element[j]);
           SpaceLine.Add(NewSpace);
           if (!IsValid(NewSpace)) continue;
           FVector Location = FVector(j, i, 0) * SpaceInterval;
           NewSpace->SetActorLocation(Location);

           SpaceType CreateSpaceType = NewSpace->GetSpaceType();

           switch (CreateSpaceType)
           {
           case SpaceType::Start:
               StartSpace = NewSpace;
               break;

           case SpaceType::Goal:
           {
               AGoalSpace* NewGoalSpace = Cast<AGoalSpace>(NewSpace);
               if (!IsValid(NewGoalSpace)) break;
               GoalSpace = NewGoalSpace;
               break;
           }

           case SpaceType::Warp:
           {
               AWarpSpace* NewWarpSpace = Cast<AWarpSpace>(NewSpace);
               if (!IsValid(NewWarpSpace)) break;
               WarpSpaces.Add(NewWarpSpace);
               break;
           }

           default:
               StartSpace = NewSpace;
               break;
           }
        }
        SpaceTiles.Add(SpaceLine);
    }
    return true;
}

/// <summary>
/// 各マスの移動先マスを設定
/// </summary>
void UGameBoard::ConnectAllSpaces()
{
    //進行できる方向を設定
    for (int i = 0; i < SpaceTiles.Num(); i++)
    {
        TArray<ABaseSpace*> SpaceLines = SpaceTiles[i];

        for (int j = 0; j < SpaceLines.Num(); j++)
        {
            ConnectSpace(j, i);
        }
    }

    //スタートからゴールまでの距離を設定する
    //再帰的に繋がっているマスを伝い、ゴールまで行く
    TTuple<int, int>Test = StartSpace->SetConectAllSpaceGoalDirection(UDirection::None);

}

/// <summary>
/// 指定マスの移動先マスを設定
/// </summary>
void UGameBoard::ConnectSpace(int XIndex, int YIndex)
{
    ABaseSpace* ThisSpace = SpaceTiles[YIndex][XIndex];
    if (!IsValid(ThisSpace)) return;

    //隣接したマスを保持
    //先に個数を求めたいため、ここを経由する
    TMap<UDirection, ABaseSpace*> ConnectSpaces;

    //各方向のマスを取得
    ABaseSpace* UpSpace = GetSpace(XIndex, YIndex - 1);
    if (IsValid(UpSpace))
        ConnectSpaces.Add(TTuple<UDirection, ABaseSpace*>(UDirection::Up, UpSpace));

    ABaseSpace* DownSpace = GetSpace(XIndex, YIndex + 1);
    if (IsValid(DownSpace))
        ConnectSpaces.Add(TTuple<UDirection, ABaseSpace*>(UDirection::Down, DownSpace));

    ABaseSpace* LeftSpace = GetSpace(XIndex - 1, YIndex);
    if (IsValid(LeftSpace))
        ConnectSpaces.Add(TTuple<UDirection, ABaseSpace*>(UDirection::Left, LeftSpace));

    ABaseSpace* RightSpace = GetSpace(XIndex + 1, YIndex);
    if (IsValid(RightSpace))
        ConnectSpaces.Add(TTuple<UDirection, ABaseSpace*>(UDirection::Right, RightSpace));

    //移動先を設定
    for (TTuple<UDirection, ABaseSpace*> ConnectSpace : ConnectSpaces)
    {
        //分岐しているなら分岐侵入を適用
        if (ConnectSpaces.Num() > 2)
            if (!ConnectSpace.Value->IsBranchFromEnter()) continue;

        ThisSpace->AddMoveDirectionStruct(ConnectSpace.Key, ConnectSpace.Value);
    }
}

/// <summary>
/// 各マスの初期設定
/// </summary>
void UGameBoard::InitializeSpaces()
{
    //ワープマスの移動先を設定
    for (AWarpSpace* SettingWarpSpace : WarpSpaces)
    {
        int WarpId = SettingWarpSpace->GetWarpID();
        for (AWarpSpace* TargetWarpSpace : WarpSpaces)
        {
            if (SettingWarpSpace == TargetWarpSpace) continue;
            if (WarpId == TargetWarpSpace->GetWarpID())
            {
                SettingWarpSpace->SetGoToSpace(TargetWarpSpace);
            }
        }
    }
}


/// <summary>
/// 指定したマスを返す
/// </summary>
/// <param> 指定するインデックス </param>
/// <return> マスの基底クラスのポインタ </return>
ABaseSpace* UGameBoard::GetSpace(int32 IndexX, int32 IndexY)
{
	if (IndexY < 0) return nullptr;
	if (IndexY >= SpaceTiles.Num()) return nullptr;

	TArray<ABaseSpace*> SpaceTileLine = SpaceTiles[IndexY];

	if (IndexX  < 0) return nullptr;
	if (IndexX >= SpaceTileLine.Num()) return nullptr;

	return SpaceTileLine[IndexX];
}

/// <summary>
/// 全てのイベントマスにメッセージ表示のイベントインターフェースを設定する
/// </summary>
void UGameBoard::SetMessageEventInterfaceForEventSpaces(const TScriptInterface<IMessageInterface> Interface)
{
    for (TArray<ABaseSpace*> Line : SpaceTiles)
    {
        for (ABaseSpace*& Element : Line)
        {
            if (!IsValid(Element))continue;
            if (!Element->IsEventSpace())continue;
            Element->SetMessageEventInterface(Interface);
        }
    }
}


/// <summary>
/// ゴールマスにイベントインターフェースを設定する
/// </summary>
void UGameBoard::SetGoalInterface(const TScriptInterface<IGoalInterface> Interface)
{
    if(!IsValid(GoalSpace)) return;
    GoalSpace->SetGoalEventInterface(Interface);
}