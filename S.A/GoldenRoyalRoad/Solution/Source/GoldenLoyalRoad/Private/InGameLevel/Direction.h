﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Direction.generated.h"


UENUM(BlueprintType)
enum class UDirection : uint8
{
	None = 0,    //なし
	Down = 2,    //下
	Left = 4,    //左
	Right = 6,   //右
	Up = 8,       //上
	
	Center = 5,  //中心
	Both = 10,   //両方
};

/// <summary>
/// 反対方向を返す
/// </summary>
/// <param> 方向 </param>
/// <returns> 反対方向 </returns>
UDirection GetBoth(UDirection Direction);