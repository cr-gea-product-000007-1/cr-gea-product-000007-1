﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/EnemyBasePiece.h"

/// <summary>
/// 初期設定
/// </summary>
void AEnemyBasePiece::Initialize()
{
	ABasePiece::Initialize();

	PieceName = TEXT("コンピューター");
}

/// <summary>
/// ターンを開始する
/// </summary>
void AEnemyBasePiece::TurnStart()
{
	ABasePiece::TurnStart();

	RollDiceTime = 0.0f;
	ThrowDiceTime = 0.0f;
	SelectBranchTime = 0.0f;
}

/// <summary>
/// サイコロを転がす
/// </summary>
/// <return> 終了 = true </return>
bool AEnemyBasePiece::RollDice(float DeltaTime)
{
	RollDiceTime += DeltaTime;
	if(RollDiceTime >= RollDiceMaxTime)
		return true;

	return false;
}

/// <summary>
/// サイコロを投げる
/// </summary>
/// <return> 終了 = true </return>
bool AEnemyBasePiece::ThrowDice(float DeltaTime)
{
	ThrowDiceTime += DeltaTime;
	if (ThrowDiceTime >= ThrowDiceMaxTime)
		return true;

	return false;
}

/// <summary>
/// 分岐を選ぶ
/// </summary>
/// <return> 終了 = true </return>
bool AEnemyBasePiece::SelectBranch(float DeltaTime)
{
	SelectBranchTime += DeltaTime;
	if (SelectBranchTime >= SelectBranchMaxTime)
	{
		SelectBranchTime = 0.0f;
		return true;
	}

	return false;
}