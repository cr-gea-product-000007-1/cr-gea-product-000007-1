﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/PlayerPiece.h"


/// <summary>
/// 初期設定
/// </summary>
void APlayerPiece::Initialize()
{
	ABasePiece::Initialize();

	MyPieceType = PieceType::Player;
	PieceName = TEXT("プレイヤー");
}

/// <summary>
/// ターンを開始する
/// </summary>
void APlayerPiece::TurnStart()
{
	ABasePiece::TurnStart();
	bDiceRollInput = false;
	bDiceThrowInput = false;
}

/// <summary>
/// サイコロを転がす
/// </summary>
/// <return> 終了 = true </return>
bool APlayerPiece::RollDice(float DeltaTime)
{
	if (bDiceRollInput)
		return true;
	else
		return false;
}

/// <summary>
/// サイコロを投げる
/// </summary>
/// <return> 終了 = true </return>
bool APlayerPiece::ThrowDice(float DeltaTime)
{
	if (bDiceThrowInput)
		return true;
	else
		return false;
}

/// <summary>
/// 分岐を選ぶ
/// </summary>
/// <return> 終了 = true </return>
bool APlayerPiece::SelectBranch(float DeltaTime)
{
	if (bBranchDecisionInput) {
		bBranchDecisionInput = false;
		return true;
	}
	return false;
}

/// <summary>
/// カメラ移動ウィジットを開く
/// </summary>
void APlayerPiece::OpenCameraMoveWidget()
{
	CameraInterface->CameraMoveStart();
}

/// <summary>
/// カメラ移動ウィジットを閉じる
/// </summary>
void APlayerPiece::CloseCameraMoveWidget()
{
	CameraInterface->CameraMoveEnd();
}

/// <summary>
/// キーの入力処理
/// </summary>
/// <param> 入力されたキー </param>
void APlayerPiece::InputKey(UKeyType Key)
{
	switch (Key)
	{
	case UKeyType::Dicide:
		//サイコロを転がす
		if (TurnPhase == TurnPhaseType::Dice && DicePhase == DicePhaseType::RollInput)
			bDiceRollInput = true;

		//サイコロの目を決定
		if (TurnPhase == TurnPhaseType::Dice && DicePhase == DicePhaseType::Roll)
			bDiceThrowInput = true;

		//分岐の移動先を決定
		if (TurnPhase == TurnPhaseType::Move && MovePhase == MovePhaseType::SelectBranch)
		{
			if (CameraInterface->IsCameraMove()) break;
			bBranchDecisionInput = true;
		}
		break;

	case UKeyType::Up:
		//分岐の移動先を決定
		if (TurnPhase == TurnPhaseType::Move && MovePhase == MovePhaseType::SelectBranch)
		{
			SetSelectBranchDirection(UDirection::Up);
		}
		break;

	case UKeyType::Down:
		//分岐の移動先を決定
		if (TurnPhase == TurnPhaseType::Move && MovePhase == MovePhaseType::SelectBranch)
		{
			SetSelectBranchDirection(UDirection::Down);
		}
		break;

	case UKeyType::Left:
		//分岐の移動先を決定
		if (TurnPhase == TurnPhaseType::Move && MovePhase == MovePhaseType::SelectBranch)
		{
			SetSelectBranchDirection(UDirection::Left);
		}
		break;

	case UKeyType::Right:
		//分岐の移動先を決定
		if (TurnPhase == TurnPhaseType::Move && MovePhase == MovePhaseType::SelectBranch)
		{
			SetSelectBranchDirection(UDirection::Right);
		}
		break;
	}
}

/// <summary>
/// 指定した方向に分岐を選択する
/// </summary>
/// <param> 設定する方向 </param>
void APlayerPiece::SetSelectBranchDirection(UDirection Direction)
{
	if (!NowSpace->ExitMoveDirection(Direction)) return;
	if (GetBoth(Direction) == NowDirection) return;

		SelectBranchDirection = Direction;
		UIInterface->SelectBranch(Direction);
}