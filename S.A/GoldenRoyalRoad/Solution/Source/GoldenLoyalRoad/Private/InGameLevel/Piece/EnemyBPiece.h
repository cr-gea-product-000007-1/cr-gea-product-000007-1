﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGameLevel/Piece/EnemyBasePiece.h"
#include "EnemyBPiece.generated.h"

/**
 * 
 */
UCLASS()
class AEnemyBPiece : public AEnemyBasePiece
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	virtual void Initialize() override;

protected:
	/// <summary>
	/// 分岐を選ぶ
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool SelectBranch(float DeltaTime) override;
	
};
