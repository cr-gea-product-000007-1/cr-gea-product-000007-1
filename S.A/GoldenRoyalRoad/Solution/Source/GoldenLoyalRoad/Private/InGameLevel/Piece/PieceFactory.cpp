﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/PieceFactory.h"
#include "../../../PathConstant.h"


/// <summary>
/// コマの生成
/// </summary>
/// <param> 生成するコマの種類 </param>
/// <return> 生成したコマ </return>
ABasePiece* UPieceFactory::CreatePiece(PieceType CreatePieceType)
{
	ABasePiece* NewPiece = nullptr;

	switch (CreatePieceType)
	{
	case PieceType::Player:
	{
		FString Path = Path::PlayerPiecePath;
		TSubclassOf<class ABasePiece> BaseClass = TSoftClassPtr<ABasePiece>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(BaseClass))
		{
			NewPiece = GetWorld()->SpawnActor<ABasePiece>(BaseClass);
		}
		break;
	}

	case PieceType::Enemy1:
	{
		FString Path = Path::Enemy1PiecePath;
		TSubclassOf<class ABasePiece> BaseClass = TSoftClassPtr<ABasePiece>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(BaseClass))
		{
			NewPiece = GetWorld()->SpawnActor<ABasePiece>(BaseClass);
		}
		break;
	}

	case PieceType::Enemy2:
	{
		FString Path = Path::Enemy2PiecePath;
		TSubclassOf<class ABasePiece> BaseClass = TSoftClassPtr<ABasePiece>(FSoftObjectPath(*Path)).LoadSynchronous();
		if (IsValid(BaseClass))
		{
			NewPiece = GetWorld()->SpawnActor<ABasePiece>(BaseClass);
		}
		break;
	}

	}

	return NewPiece;
}