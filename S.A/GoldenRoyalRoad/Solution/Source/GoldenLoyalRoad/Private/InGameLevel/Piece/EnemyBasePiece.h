﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGameLevel/Piece/BasePiece.h"
#include "EnemyBasePiece.generated.h"

//サイコロを転がすまでの時間
const float RollDiceMaxTime = 1.2f;

//サイコロを投げるまでの時間
const float ThrowDiceMaxTime = 1.5f;

//分岐を選択するまでの時間
const float SelectBranchMaxTime = 1.0f;

//分岐を選択する入力が複数回目の時の開始時間の短縮
const float SecondSelectBranchStartTime = 0.4f;

/**
 * 
 */
UCLASS()
class AEnemyBasePiece : public ABasePiece
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 初期設定
	/// </summary>
	virtual void Initialize() override;

	/// <summary>
	/// ターンを開始する
	/// </summary>
	void TurnStart() override;

protected:
	/// <summary>
	/// サイコロを転がす
	/// </summary>
	/// <return> 終了 = true </return>
	bool RollDice(float DeltaTime) override;

	/// <summary>
	/// サイコロを投げるさせる
	/// </summary>
	/// <return> 終了 = true </return>
	bool ThrowDice(float DeltaTime) override;

	/// <summary>
	/// 分岐を選ぶ
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool SelectBranch(float DeltaTime) override;

private:
	/// <summary>
	/// サイコロを転がすまでの時間
	/// </summary>
	float RollDiceTime;

	/// <summary>
	/// サイコロを投げるまでの時間
	/// </summary>
	float ThrowDiceTime;

protected:
	/// <summary>
	/// 分岐を選択するまでの時間
	/// </summary>
	float SelectBranchTime;
};