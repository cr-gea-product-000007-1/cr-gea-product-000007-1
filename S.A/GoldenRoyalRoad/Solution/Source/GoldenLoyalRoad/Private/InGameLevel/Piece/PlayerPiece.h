﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGameLevel/Piece/BasePiece.h"
#include "PlayerPiece.generated.h"

/**
 * 
 */
UCLASS()
class APlayerPiece : public ABasePiece
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 初期設定
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// ターンを開始する
	/// </summary>
	void TurnStart() override;

	/// <summary>
	/// キーの入力処理
	/// </summary>
	/// <param> 入力されたキー </param>
	void InputKey(UKeyType Key) override;

private:
	/// <summary>
	/// 指定した方向に分岐を選択する
	/// </summary>
	/// <param> 設定する方向 </param>
	void SetSelectBranchDirection(UDirection Direction);

protected:
	/// <summary>
	/// サイコロを転がす
	/// </summary>
	/// <return> 終了 = true </return>
	bool RollDice(float DeltaTime) override;

	/// <summary>
	/// サイコロを投げる
	/// </summary>
	/// <return> 終了 = true </return>
	bool ThrowDice(float DeltaTime) override;

	/// <summary>
	/// 分岐を選ぶ
	/// </summary>
	/// <return> 終了 = true </return>
	bool SelectBranch(float DeltaTime) override;

	/// <summary>
	/// カメラ移動ウィジットを開く
	/// </summary>
	void OpenCameraMoveWidget() override;

	/// <summary>
	/// カメラ移動ウィジットを閉じる
	/// </summary>
	void CloseCameraMoveWidget() override;

private:
	/// <summary>
	/// サイコロを転がすキーが入力されたか
	/// </summary>
	bool bDiceRollInput;

	/// <summary>
	/// サイコロを投げるキーが入力されたか
	/// </summary>
	bool bDiceThrowInput;

	/// <summary>
	/// 分岐を決定するキーが入力されたか
	/// </summary>
	bool bBranchDecisionInput;
};
