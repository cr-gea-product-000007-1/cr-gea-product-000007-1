﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Spaces/BaseSpace.h"
#include "Misc/EnumRange.h"
#include "UObject/ScriptInterface.h"
#include "../DiceInterface.h"
#include "../PieceInterface.h"
#include "../UIInterface.h"
#include "../MoneyRankInterface.h"
#include "../CameraInterface.h"
#include "../MessageInterface.h"
#include "../../KeyType.h"
#include "BasePiece.generated.h"

UENUM(BlueprintType)
enum class PieceType : uint8
{
	Player, //操作キャラ
	Enemy1, //敵キャラ1
	Enemy2, //敵キャラ2

	End UMETA(Hidden),
};
ENUM_RANGE_BY_COUNT(PieceType, PieceType::End);

/// <summary>
/// マスの上に配置する時に座標
/// </summary>
const FVector SpaceCenterLocation(0.0f, 0.0f, 90.0f);

/// <summary>
/// マスの端に配置する時に座標
/// </summary>
const FVector SpaceSideStartLocation(-60.0f, 60.0f, 30.0f);

/// <summary>
/// マスの端に複数いるときにずれる座標
/// </summary>
const FVector SpaceSideAddLocation(40.0f, 0.0f, 0.0f);


UCLASS()
class ABasePiece : public ACharacter
{
	GENERATED_BODY()

protected:
	enum class TurnPhaseType
	{
		NotTurn,    //他のプレイヤーの手番
		TurnStart,  //ターン開始処理
		Dice,       //サイコロを投げる
		Move,       //移動
		SpaceEvent, //マスのイベント
		TurnEnd,    //ターン終了処理
		TurnEnded,  //ターン終了している
	};

	enum class DicePhaseType
	{
		None,       //なし
		Start,      //開始
		RollInput,  //転がす入力待ち
		Roll,       //転がす
		Throw,      //投げる
		ThrowEnd,   //投げ終わり
		End,        //終了
	};

	enum class MovePhaseType
	{
		None,               //なし
		Start,              //開始
		MoveStart,          //移動開始
		MoveAnimation,      //移動アニメーション
		NextMove,           //次の移動に

		SelectBranchStart,  //分岐選択開始
		SelectBranch,       //分岐選択
		SelectBranchEnd,    //分岐選択終了

		BackStart,          //逆走開始
		BackAnimation,      //逆走アニメーション
		NextBack,           //次の逆走に

		End,                //終了
	};

	enum class SpaceEventPhaseType
	{
		Start,      //開始
		SpaceEvent, //マスのイベント中
		End,        //終了
	};

public:
	// Sets default values for this character's properties
	ABasePiece();

	/// <summary>
	/// 初期設定
	/// </summary>
	virtual void Initialize();

	/// <summary>
	/// コマの種類を返す
	/// </summary>
	/// <return> コマの種類 </return>
	PieceType GetPieceType() { return MyPieceType; };

	/// <summary>
	/// 自分がプレイヤーかを返す
	/// </summary>
	/// <return> プレイヤー = true </return>
	bool IsPlayer() { return MyPieceType == PieceType::Player; };

	/// <summary>
	/// ターンを開始する
	/// </summary>
	virtual void TurnStart() { TurnPhase = TurnPhaseType::TurnStart; };

	/// <summary>
	/// 移動を開始する
	/// </summary>
	virtual void MoveStart();

	/// <summary>
	/// さいころを投げる入力待ちかを返す
	/// </summary>
	/// <return> 入力待ち = true </return>
	bool IsThrowInput();

	/// <summary>
	/// さいころを投げている最中かを返す
	/// </summary>
	/// <return> 投げている = true </return>
	bool IsThrow();

	/// <summary>
	/// 分岐選択中かを返す
	/// </summary>
	/// <return> 分岐選択中 = true </return>
	bool IsSelectBranch();

	/// <summary>
	/// 自分のターンかを返す
	/// </summary>
	/// <return> ターン中 = true </return>
	bool IsMyTurn();

	/// <summary>
	/// 自分のターンが終了したかを返す
	/// </summary>
	/// <return> 終了 = true </return>
	bool IsTurnEnd() { return TurnPhase == TurnPhaseType::TurnEnded; }

	/// <summary>
	/// ターンを終了
	/// </summary>
	void TurnEnd() { TurnPhase = TurnPhaseType::NotTurn; };

	/// <summary>
	/// ゴール状態にする
	/// </summary>
	void Goal() { bIsGoal = true; };

	/// <summary>
	/// ゴール状態かを返す
	/// </summary>
	/// <return>ゴール=true</return>
	bool IsGoal() { return bIsGoal; };

	/// <summary>
	/// キーの入力処理
	/// </summary>
	/// <param> 入力されたキー </param>
	virtual void InputKey(UKeyType Key);

	/// <summary>
	/// 今いるマスを取得する
	/// </summary>
	FString& GetPieceName() { return PieceName; }

	/// <summary>
	/// IDを設定する
	/// </summary>
	void SetID(int NewID) { ID = NewID; }

	/// <summary>
	/// IDを取得する
	/// </summary>
	int GetID() { return ID; }

	/// <summary>
	/// 所持金を取得する
	/// </summary>
	int GetMoney() { return Money; }

	/// <summary>
	/// 今いるマスを設定する
	/// </summary>
	void SetNowSpace(ABaseSpace* TargetSpace);

	/// <summary>
	/// 今いるマスを取得する
	/// </summary>
	ABaseSpace* GetNowSpace() { return NowSpace; };

	/// <summary>
	/// 今いるマスの上に移動する
	/// </summary>
	/// <return> 移動完了 = true </return>
	bool GoToNowSpace();

	/// <summary>
	/// 今いるマスの行動場所に瞬間移動する
	/// </summary>
	void TeleportNowSpaceActive();

	/// <summary>
	/// 今いるマスの待機場所に瞬間移動する
	/// </summary>
	void TeleportNowSpaceIdel();

	/// <summary>
	/// 今いるマスの待機場所を設定する
	/// </summary>
	void SetSpaceIdelNum(int Num) { SpaceIdelNum = Num; }

	/// <summary>
	/// 今いるマスの待機場所を返す
	/// </summary>
	int GetSpaceIdelNum() { return SpaceIdelNum; }

	/// <summary>
	/// 今いるマスの待機場所をずらす
	/// </summary>
	void SlideSpaceIdel() { SpaceIdelNum--; }

	/// <summary>
	/// 行動中か
	/// </summary>
	bool IsActive(){ return bIsActive; }

	/// <summary>
	/// 向きを設定する
	/// </summary>
	void SetDirection(UDirection Direction);

	/// <summary>
	/// 向きを取得する
	/// </summary>
	UDirection GetDirection() { return NowDirection; }

	/// <summary>
	/// 移動できるマス数を設定する
	/// </summary>
	void SetMoveSpaceNum(int SpaceNum);

	/// <summary>
	/// 逆走するマス数を設定する
	/// </summary>
	void SetBackSpaceNum(int SpaceNum);

	/// <summary>
	/// 移動してきたマスの記録を消す
	/// </summary>
	void ResetSpaceLog() { SpaceLog.Empty(); }

	/// <summary>
	/// お金を入手する
	/// </summary>
	/// <param> 増やすお金 </param>
	void AddMoney(int MoneyNum);

	/// <summary>
	/// 所持金を減らす
	/// </summary>
	/// <param> 減らすお金 </param>
	void LostMoney(int MoneyNum);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// サイコロを投げる
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool RollDice(float DeltaTime);

	/// <summary>
	/// サイコロの目を確定させる
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool ThrowDice(float DeltaTime);

	/// <summary>
	/// 分岐を選ぶ
	/// </summary>
	/// <return> 終了 = true </return>
	virtual bool SelectBranch(float DeltaTime);

	/// <summary>
	/// カメラ移動ウィジットを開く
	/// </summary>
	virtual void OpenCameraMoveWidget();

	/// <summary>
	/// カメラ移動ウィジットを閉じる
	/// </summary>
	virtual void CloseCameraMoveWidget();

private :
	/// <summary>
	/// サイコロを転がすイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool DiceEvent(float DeltaTime);

	/// <summary>
	/// 移動するイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool MoveEvent(float DeltaTime);

	/// <summary>
	/// マスのイベント
	/// </summary>
	/// <return> 終了 = true </return>
	bool SpaceEvent(float DeltaTime);

public:	
	// Called every frame
	void Update(float DeltaTime);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/// <summary>
	/// サイコロのイベントインターフェースを設定する
	/// </summary>
	void SetDiceEventInterface(const TScriptInterface<IDiceInterface> Interface) { DiceInterface = Interface; };

	/// <summary>
	/// サイコロのイベントインターフェースを設定する
	/// </summary>
	void SetPieceEventInterface(const TScriptInterface<IPieceInterface> Interface) { PieceInterface = Interface; };

	/// <summary>
	/// UIのイベントインターフェースを設定する
	/// </summary>
	void SetUIEventInterface(const TScriptInterface<IUIInterface> Interface) { UIInterface = Interface; };

	/// <summary>
	/// 所持金のイベントインターフェースを設定する
	/// </summary>
	void SetMoneyRankEventInterface(const TScriptInterface<IMoneyRankInterface> Interface) { MoneyRankInterface = Interface; };

	/// <summary>
	/// メッセージ表示のイベントインターフェースを設定する
	/// </summary>
	void SetMessageEventInterface(const TScriptInterface<IMessageInterface> Interface) { MessageInterface = Interface; };

	/// <summary>
	/// カメラのイベントインターフェースを設定する
	/// </summary>
	void SetCameraInterface(const TScriptInterface<ICameraInterface> Interface) { CameraInterface = Interface; };

	/// <summary>
	/// 待機アニメーション
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void IdleAnimation();

	/// <summary>
	/// ジャンプアニメーション
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void JumpAnimation();

	/// <summary>
	/// ワープに入るアニメーション
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	bool InWarpAnimation();

	/// <summary>
	/// ワープから出るアニメーション
	/// </summary>
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	bool OutWarpAnimation();

	/// <summary>
	/// 今いるマスで待機状態にするアニメーション
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void IdelNowSpaceAnimation();

	/// <summary>
	/// 今いるマスで行動状態にするアニメーション
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ActiveNowSpaceAnimation();

public:
	/// <summary>
	/// 移動速度
	/// </summary>
	UPROPERTY(EditAnywhere)
	float MoveSpeed;

protected:
	/// <summary>
	/// コマの種類
	/// </summary>
	PieceType MyPieceType;

	/// <summary>
	/// ターンフェーズ
	/// </summary>
	TurnPhaseType TurnPhase;

	/// <summary>
	/// サイコロのフェーズ
	/// </summary>
	DicePhaseType DicePhase;

	/// <summary>
	/// 移動のフェーズ
	/// </summary>
	MovePhaseType MovePhase;

	/// <summary>
	/// マスのイベントのフェーズ
	/// </summary>
	SpaceEventPhaseType SpaceEventPhase;

	/// <summary>
	/// ゴールしたか
	/// イベント中ではなく終了後にゴール状態にする
	/// </summary>
	bool bIsGoal;

	/// <summary>
	/// 名前
	/// </summary>
	FString PieceName;

	/// <summary>
	/// 所持金
	/// </summary>
	int Money;

	/// <summary>
	/// ID
	/// （管理インデックス）
	/// </summary>
	int ID;

	/// <summary>
	/// 現在いるマス
	/// </summary>
	UPROPERTY()
	ABaseSpace* NowSpace;

	/// <summary>
	/// 現在の向き
	/// </summary>
	UPROPERTY()
	UDirection NowDirection;

	/// <summary>
	/// イベントを実行しているマス
	/// </summary>
	UPROPERTY()
	ABaseSpace* EventSpace;

	/// <summary>
	/// 移動できる残りマス
	/// </summary>
	int MoveSpaceNum;

	/// <summary>
	/// 現在のマスでの待機位置
	/// </summary>
	int SpaceIdelNum;

	/// <summary>
	/// 行動中か
	/// </summary>
	bool bIsActive;

	/// <summary>
	/// 分岐の進行方向
	/// </summary>
	UPROPERTY()
	UDirection SelectBranchDirection;

	/// <summary>
	/// 移動してきたマス
	/// </summary>
	UPROPERTY()
	TArray<ABaseSpace*> SpaceLog;

	/// <summary>
	/// 移動してきた方向
	/// </summary>
	UPROPERTY()
	TArray<UDirection> DirectionLog;

	/// <summary>
	/// サイコロのイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IDiceInterface> DiceInterface;

	/// <summary>
	/// コマのイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IPieceInterface> PieceInterface;

	/// <summary>
	/// UIのイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IUIInterface> UIInterface;

	/// <summary>
	/// 所持金のイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IMoneyRankInterface> MoneyRankInterface;

	/// <summary>
	/// メッセージ表示のイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<IMessageInterface> MessageInterface;

	/// <summary>
	/// カメラののイベントインターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<ICameraInterface> CameraInterface;
};
