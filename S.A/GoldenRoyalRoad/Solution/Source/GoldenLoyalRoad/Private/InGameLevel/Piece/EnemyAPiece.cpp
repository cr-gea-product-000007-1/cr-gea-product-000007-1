﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/EnemyAPiece.h"


/// <summary>
/// 初期設定
/// </summary>
void AEnemyAPiece::Initialize()
{
	ABasePiece::Initialize();

	MyPieceType = PieceType::Enemy1;
	PieceName = TEXT("コンピューターA");
}

/// <summary>
/// 分岐を選ぶ
/// </summary>
/// <return> 終了 = true </return>
bool AEnemyAPiece::SelectBranch(float DeltaTime)
{
	if (!IsValid(NowSpace)) return false;
	
	//時間がたっていたら選択開始
	SelectBranchTime += DeltaTime;
	if (SelectBranchTime < SelectBranchMaxTime) return false;
	SelectBranchTime = 0.0f;

	//選択候補のうち、ゴールまでの距離が短いものを選択
	TMap<UDirection, FMoveDirectionStruct> BranchDirections = NowSpace->GetAllMoveDirectionsFillter(GetBoth(NowDirection));
	FMoveDirectionStruct ThinkBranchDirection = BranchDirections.begin().Value();
	for (TTuple<UDirection, FMoveDirectionStruct> BranchDirection : BranchDirections)
	{
		if (ThinkBranchDirection.GoalMinDistance > BranchDirection.Value.GoalMinDistance)
			ThinkBranchDirection = BranchDirection.Value;
	}

	//選んでいる方向と進みたい方向が一致していたら進む
	if (SelectBranchDirection == ThinkBranchDirection.MoveDirection) {
		return true;
	}
	else {
		SelectBranchDirection = ThinkBranchDirection.MoveDirection;
		UIInterface->SelectBranch(SelectBranchDirection);

		//もう一度、短縮したタイマーで選ぶまで待機
		SelectBranchTime = SecondSelectBranchStartTime;
	}

	return false;
}