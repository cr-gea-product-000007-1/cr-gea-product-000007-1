﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/EnemyBPiece.h"


/// <summary>
/// 初期設定
/// </summary>
void AEnemyBPiece::Initialize()
{
	ABasePiece::Initialize();

	MyPieceType = PieceType::Enemy2;
	PieceName = TEXT("コンピューターB");
}

/// <summary>
/// 分岐を選ぶ
/// </summary>
/// <return> 終了 = true </return>
bool AEnemyBPiece::SelectBranch(float DeltaTime)
{
	if (!IsValid(NowSpace)) return false;

	//時間がたっていたら選択開始
	SelectBranchTime += DeltaTime;
	if (SelectBranchTime < SelectBranchMaxTime) return false;
	SelectBranchTime = 0.0f;

	//選択候補のうち、ゴールまでの距離が長いものを選択
	TMap<UDirection, FMoveDirectionStruct> BranchDirections = NowSpace->GetAllMoveDirectionsFillter(GetBoth(NowDirection));
	FMoveDirectionStruct ThinkBranchDirection = BranchDirections.begin().Value();
	for (TTuple<UDirection, FMoveDirectionStruct> BranchDirection : BranchDirections)
	{
		//距離が長い方を選ぶ
		if (ThinkBranchDirection.GoalMaxDistance < BranchDirection.Value.GoalMaxDistance)
			ThinkBranchDirection = BranchDirection.Value;

		//同じ距離なら先に遠回りをする
		if (ThinkBranchDirection.GoalMaxDistance == BranchDirection.Value.GoalMaxDistance) {
			if (ThinkBranchDirection.GoalMinDistance < BranchDirection.Value.GoalMinDistance) {
				ThinkBranchDirection = BranchDirection.Value;
			}
		}
	}

	//選んでいる方向と進みたい方向が一致していたら進む
	if (SelectBranchDirection == ThinkBranchDirection.MoveDirection) {
		return true;
	}
	else {
		SelectBranchDirection = ThinkBranchDirection.MoveDirection;
		UIInterface->SelectBranch(SelectBranchDirection);

		//もう一度、短縮したタイマーで選ぶまで待機
		SelectBranchTime = SecondSelectBranchStartTime;
	}

	return false;
}