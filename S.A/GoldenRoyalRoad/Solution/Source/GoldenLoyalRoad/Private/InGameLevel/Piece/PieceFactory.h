﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BasePiece.h"
#include "PieceFactory.generated.h"

/**
 * 
 */
UCLASS()
class UPieceFactory : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// コマの生成
	/// </summary>
	/// <param> 生成するコマの種類 </param>
	/// <return> 生成したコマ </return>
	ABasePiece* CreatePiece(PieceType CreatePieceType);
	
};
