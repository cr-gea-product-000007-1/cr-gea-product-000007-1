﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/Piece/BasePiece.h"
#include "../../../GameConstant.h"


/// <summary>
/// 開始時の所持金
/// </summary>
const int StartMoney = 10000;


// Sets default values
ABasePiece::ABasePiece()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABasePiece::BeginPlay()
{
	Super::BeginPlay();
	
	Money = StartMoney;
	bIsActive = false;
}

// Called every frame
void ABasePiece::Update(float DeltaTime)
{	
	switch (TurnPhase)
	{
	case ABasePiece::TurnPhaseType::NotTurn:
		break;

	case ABasePiece::TurnPhaseType::TurnStart:
		DicePhase = ABasePiece::DicePhaseType::Start;
		PieceInterface->UpdateNowSpaceidelPiece(this);
		TeleportNowSpaceActive();
		ActiveNowSpaceAnimation();

		TurnPhase = ABasePiece::TurnPhaseType::Dice;
		break;

	case ABasePiece::TurnPhaseType::Dice:
		if (DiceEvent(DeltaTime))
			MoveStart();
		break;

	case ABasePiece::TurnPhaseType::Move:
		if (MoveEvent(DeltaTime))
			TurnPhase = ABasePiece::TurnPhaseType::SpaceEvent;
		break;

	case ABasePiece::TurnPhaseType::SpaceEvent:
		if (SpaceEvent(DeltaTime))
		{
			if (MoveSpaceNum == 0)
			{
				TurnPhase = ABasePiece::TurnPhaseType::TurnEnd;
			}
			else
				MoveStart();
		}
		break;

	case ABasePiece::TurnPhaseType::TurnEnd:
		SpaceIdelNum = PieceInterface->GetNowSpacePieceNum(this);
		TeleportNowSpaceIdel();
		IdelNowSpaceAnimation();
		TurnPhase = ABasePiece::TurnPhaseType::TurnEnded;
		break;

	case ABasePiece::TurnPhaseType::TurnEnded:
		//ゲームマネージャーの方からターン終了を確認し次第NotTurnに遷移させる
		break;
	}
}

// Called to bind functionality to input
void ABasePiece::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/// <summary>
/// 初期設定
/// </summary>
void ABasePiece::Initialize()
{
	TurnPhase = ABasePiece::TurnPhaseType::NotTurn;
	DicePhase = ABasePiece::DicePhaseType::None;
	MovePhase = ABasePiece::MovePhaseType::None;
}

/// <summary>
/// 移動を開始する
/// </summary>
void ABasePiece::MoveStart()
{
	TurnPhase = ABasePiece::TurnPhaseType::Move;
	MovePhase = ABasePiece::MovePhaseType::Start;
	SpaceEventPhase = ABasePiece::SpaceEventPhaseType::Start;
}

/// <summary>
/// さいころを投げる入力待ちかを返す
/// </summary>
/// <return> 入力待ち = true </return>
bool ABasePiece::IsThrowInput()
{
	return (TurnPhase == TurnPhaseType::Dice && DicePhase == DicePhaseType::RollInput);
}

/// <summary>
/// さいころを投げている最中かを返す
/// </summary>
/// <return> 投げている = true </return>
bool ABasePiece::IsThrow()
{
	return (TurnPhase == TurnPhaseType::Dice && DicePhase == DicePhaseType::Roll);
}

/// <summary>
/// 分岐選択中かを返す
/// </summary>
/// <return> 分岐選択中 = true </return>
bool ABasePiece::IsSelectBranch()
{
	return (TurnPhase != TurnPhaseType::Move && MovePhase != MovePhaseType::SelectBranch);
}

/// <summary>
/// 自分のターンかを返す
/// </summary>
/// <return> ターン中 = true </return>
bool ABasePiece::IsMyTurn()
{
	return TurnPhase != TurnPhaseType::NotTurn;
};

/// <summary>
/// キーの入力処理
/// </summary>
/// <param> 入力されたキー </param>
void ABasePiece::InputKey(UKeyType Key)
{

}

/// <summary>
/// 今いるマスを設定する
/// </summary>
void ABasePiece::SetNowSpace(ABaseSpace* TargetSpace)
{
	if (!IsValid(TargetSpace)) return;
	NowSpace = TargetSpace;
}

/// <summary>
/// 今いるマスの上に移動する
/// </summary>
/// <return> 移動完了 = true </return>
bool ABasePiece::GoToNowSpace()
{
	if (!IsValid(NowSpace)) return false;

	FVector GoToLocation = NowSpace->GetActorLocation();
	GoToLocation += SpaceCenterLocation;

	FVector Move = GoToLocation - GetActorLocation();
	float Dist = FVector::Dist(GoToLocation , GetActorLocation());
	Move.Normalize(0.001f);
	Move *= MoveSpeed;

	if (Dist > MoveSpeed)
	{
		SetActorLocation(GetActorLocation() + Move);
		return false;
	}
	else {
		TeleportNowSpaceActive();
		return true;
	}
}

/// <summary>
/// 今いるマスの行動場所に瞬間移動する
/// </summary>
void ABasePiece::TeleportNowSpaceActive()
{
	if (!IsValid(NowSpace)) return;

	FVector Location = NowSpace->GetActorLocation();
	Location += SpaceCenterLocation;
	SetActorLocation(Location);
}

/// <summary>
/// 今いるマスの待機場所に瞬間移動する
/// </summary>
void ABasePiece::TeleportNowSpaceIdel()
{
	if (!IsValid(NowSpace)) return;

	FVector Location = NowSpace->GetActorLocation();
	Location += SpaceSideStartLocation + SpaceSideAddLocation * SpaceIdelNum;
	SetActorLocation(Location);
}

/// <summary>
/// 向きを設定する
/// </summary>
void ABasePiece::SetDirection(UDirection Direction)
{
	NowDirection = Direction;
	SetActorRotation(GameConstant::DirectionRotatos[Direction]);
}

/// <summary>
/// 移動できるマス数を設定する
/// </summary>
void ABasePiece::SetMoveSpaceNum(int SpaceNum)
{
	MoveSpaceNum = SpaceNum; 
	DiceInterface->SetDiceValue(SpaceNum);
}

/// <summary>
/// 逆走するマス数を設定する
/// </summary>
void ABasePiece::SetBackSpaceNum(int SpaceNum)
{
	MoveSpaceNum = -SpaceNum;
	DiceInterface->SetDiceValue(SpaceNum);
}

/// <summary>
/// お金を入手する
/// </summary>
/// <param> 増やすお金 </param>
void ABasePiece::AddMoney(int MoneyNum)
{
	Money += MoneyNum;
	MoneyRankInterface->AddMoney(this);
	PieceInterface->GetCoineEmmit(this, MoneyNum);
}

/// <summary>
/// 所持金を減らす
/// </summary>
/// <param> 減らすお金 </param>
void ABasePiece::LostMoney(int MoneyNum)
{
	Money -= MoneyNum;
	MoneyRankInterface->LostMoney(this);
	PieceInterface->LostCoineEmmit(this, MoneyNum);
}

/// <summary>
/// サイコロを転がすイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::DiceEvent(float DeltaTime)
{
	switch (DicePhase)
	{
	case ABasePiece::DicePhaseType::Start:
		DiceInterface->StartDice();
		DicePhase = ABasePiece::DicePhaseType::RollInput;
		break;

	case ABasePiece::DicePhaseType::RollInput:
		//転がす入力待ち
		if (RollDice(DeltaTime))
		{
			DiceInterface->RollDice();
			DicePhase = ABasePiece::DicePhaseType::Roll;
		}
		break;

	case ABasePiece::DicePhaseType::Roll:
		//投げる入力待ち
		if (ThrowDice(DeltaTime))
		{
			//投げるを追加
			DiceInterface->ThrowDice();
			DicePhase = ABasePiece::DicePhaseType::Throw;
		}
		break;

	case ABasePiece::DicePhaseType::Throw:
		//投げ終わったかを取得
		if (DiceInterface->IsThrowDiceEnd())
		{
			DicePhase = ABasePiece::DicePhaseType::ThrowEnd;
		}
		break;

	case ABasePiece::DicePhaseType::ThrowEnd:
		MoveSpaceNum = DiceInterface->GetDiceValue();
		DicePhase = ABasePiece::DicePhaseType::End;
		break;

	case ABasePiece::DicePhaseType::End:
		DicePhase = ABasePiece::DicePhaseType::None;
		return true;

	}
	return false;
}

/// <summary>
/// 移動するイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::MoveEvent(float DeltaTime)
{
	switch (MovePhase)
	{

	// --------------------------------------------------------
	//  開始処理
	// --------------------------------------------------------

	case ABasePiece::MovePhaseType::Start:
	{
		//移動可能数が負なら戻る
		if (MoveSpaceNum < 0)
		{
			if(SpaceLog.Num() == 0)
			{
				MoveSpaceNum = 0;
				DiceInterface->SetDiceValue(0);
				MovePhase = ABasePiece::MovePhaseType::End;
				return false;
			}
			MovePhase = ABasePiece::MovePhaseType::BackStart;
			break;
		}
		//選択候補取得
		TMap<UDirection, FMoveDirectionStruct> MoveDirections = NowSpace->GetAllMoveDirectionsFillter(GetBoth(NowDirection));
		//候補なしなら中断
		if (MoveDirections.Num() == 0)
		{
			MoveSpaceNum = 0;
			DiceInterface->SetDiceValue(0);
			MovePhase = ABasePiece::MovePhaseType::End;
			return false;
		}
		//候補から先頭のものを選ぶ
		SelectBranchDirection = NowSpace->GetAllMoveDirectionsFillter(GetBoth(NowDirection)).begin().Value().MoveDirection;

		if (!NowSpace->IsBranch(NowDirection)) // 今いるマスが分岐かの処理
		{
			MovePhase = ABasePiece::MovePhaseType::MoveStart;
		}
		else
			MovePhase = ABasePiece::MovePhaseType::SelectBranchStart;
		break;
	}


	// --------------------------------------------------------
	//  移動処理
	// --------------------------------------------------------

	case ABasePiece::MovePhaseType::MoveStart:
	{
		//選択肢の中から先頭のものを取得
		FMoveDirectionStruct MoveDirection = NowSpace->GetMoveDirection(SelectBranchDirection);
		SpaceLog.Add(NowSpace);
		DirectionLog.Add(MoveDirection.MoveDirection);
		ABaseSpace* NextSpace = MoveDirection.GoToSpace;
		NowSpace = NextSpace;
		SetDirection(MoveDirection.MoveDirection);

		MoveSpaceNum = DiceInterface->ValueCountDown();
		
		JumpAnimation();

		MovePhase = ABasePiece::MovePhaseType::MoveAnimation;
		break;
	}

	case ABasePiece::MovePhaseType::MoveAnimation:
		if (GoToNowSpace())
		{
			IdleAnimation();
			MovePhase = ABasePiece::MovePhaseType::NextMove;
		}
		break;

	case ABasePiece::MovePhaseType::NextMove:
		if (MoveSpaceNum > 0) 
		{
			MovePhase = ABasePiece::MovePhaseType::Start;
		}
		else
		{
			MovePhase = ABasePiece::MovePhaseType::End;
		}
		break;


	// --------------------------------------------------------
	//  分岐選択処理
	// --------------------------------------------------------

	case ABasePiece::MovePhaseType::SelectBranchStart:
		UIInterface->OpenSelectBranch();
		UIInterface->SelectBranch(SelectBranchDirection);
		OpenCameraMoveWidget();
		MovePhase = ABasePiece::MovePhaseType::SelectBranch;
		break;

	case ABasePiece::MovePhaseType::SelectBranch:
		if(SelectBranch(DeltaTime))
			MovePhase = ABasePiece::MovePhaseType::SelectBranchEnd;
		break;

	case ABasePiece::MovePhaseType::SelectBranchEnd:
		UIInterface->CloseSelectBranch();
		CloseCameraMoveWidget();
		MovePhase = ABasePiece::MovePhaseType::MoveStart;
		break;


	// --------------------------------------------------------
	//  戻る処理
	// --------------------------------------------------------

	case ABasePiece::MovePhaseType::BackStart:
	{
		if (SpaceLog.Num() == 0) return false;
		if (DirectionLog.Num() == 0) return false;
		//一つ前のマス取得
		ABaseSpace* BackSpace = SpaceLog.Last();
		UDirection BackDirection = DirectionLog.Last();
		SpaceLog.RemoveAt(SpaceLog.Num() - 1);
		DirectionLog.RemoveAt(DirectionLog.Num() - 1);

		NowSpace = BackSpace;
		SetDirection(GetBoth(BackDirection));

		MoveSpaceNum++;
		DiceInterface->SetDiceValue(-MoveSpaceNum);

		JumpAnimation();

		MovePhase = ABasePiece::MovePhaseType::BackAnimation;
		break;
	}

	case ABasePiece::MovePhaseType::BackAnimation:
		if (GoToNowSpace())
		{
			IdleAnimation();
			MovePhase = ABasePiece::MovePhaseType::NextBack;
		}
		break;

	case ABasePiece::MovePhaseType::NextBack:
		if (DirectionLog.Num() == 0) return false;
		//一つ前の向きを向く
		//分岐の進行方向にも影響するのでかえないこと
		SetDirection(DirectionLog.Last());
		if (MoveSpaceNum < 0)
		{
			MovePhase = ABasePiece::MovePhaseType::Start;
		}
		else
		{
			MovePhase = ABasePiece::MovePhaseType::End;
		}
		break;


	// --------------------------------------------------------
	//  終了処理
	// --------------------------------------------------------

	case ABasePiece::MovePhaseType::End:
		MovePhase = ABasePiece::MovePhaseType::None;
		return true;

	}
	return false;
}

/// <summary>
/// マスのイベント
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::SpaceEvent(float DeltaTime)
{
	switch (SpaceEventPhase)
	{
	case ABasePiece::SpaceEventPhaseType::Start:
		EventSpace = NowSpace;
		//マスにイベントを開始させる
		EventSpace->SpaceEventStart();
		SpaceEventPhase = ABasePiece::SpaceEventPhaseType::SpaceEvent;
		break;

	case ABasePiece::SpaceEventPhaseType::SpaceEvent:
		EventSpace->SpaceEvent(DeltaTime,this);
		if(!EventSpace->IsEvent())
			SpaceEventPhase = ABasePiece::SpaceEventPhaseType::End;
		break;

	case ABasePiece::SpaceEventPhaseType::End:
		return true;
	}
	return false;
}
/// <summary>
/// 今いるマスで待機状態にするアニメーション
/// </summary>
void ABasePiece::IdelNowSpaceAnimation_Implementation()
{
	bIsActive = false;
}

/// <summary>
/// 今いるマスで行動状態にするアニメーション
/// </summary>
void ABasePiece::ActiveNowSpaceAnimation_Implementation()
{
	bIsActive = true;
}

/// <summary>
/// サイコロを投げる
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::RollDice(float DeltaTime)
{
	return true;
}

/// <summary>
/// サイコロの目を確定させる
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::ThrowDice(float DeltaTime)
{
	return true;
}

/// <summary>
/// 分岐を選ぶ
/// </summary>
/// <return> 終了 = true </return>
bool ABasePiece::SelectBranch(float DeltaTime)
{
	return true;
}

/// <summary>
/// カメラ移動ウィジットを開く
/// </summary>
void ABasePiece::OpenCameraMoveWidget()
{
	//操作キャラ以外は開かない
}

/// <summary>
/// カメラ移動ウィジットを閉じる
/// </summary>
void ABasePiece::CloseCameraMoveWidget()
{
	//操作キャラ以外は開かない
}