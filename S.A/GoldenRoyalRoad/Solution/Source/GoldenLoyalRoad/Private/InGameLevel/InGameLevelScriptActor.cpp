// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/InGameLevelScriptActor.h"
#include "PlayerMenuController.h"
#include "Kismet/GameplayStatics.h"

// コンストラクタ
AInGameLevelScriptActor::AInGameLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AInGameLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	MyPhaseType = AInGameLevelScriptActor::PhaseType::Create;
	
	InGameManager = NewObject<UInGameManager>(this);
	InGameManager->Initialize();
}

void AInGameLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(InGameManager))
	{
		UE_LOG(LogTemp, Display, TEXT("GameManager Error"));
		return;
	}
	//フェーズごとの処理
	switch (MyPhaseType)
	{
	case AInGameLevelScriptActor::PhaseType::Create:
		if (InGameManager->Create())
			MyPhaseType = AInGameLevelScriptActor::PhaseType::GamePlay;
		break;

	case AInGameLevelScriptActor::PhaseType::GamePlay:
		if (InGameManager->GamePlay(DeltaTime))
		{
			MyPhaseType = AInGameLevelScriptActor::PhaseType::Result;
		}
		break;

	case AInGameLevelScriptActor::PhaseType::Result:
		if (InGameManager->Result(DeltaTime))
		{
			MyPhaseType = AInGameLevelScriptActor::PhaseType::End;
		}
		break;

	case AInGameLevelScriptActor::PhaseType::End:
		if (InGameManager->End())
		{
			if (IsValid(InGameManager))
			{
				InGameManager = nullptr;
			}
			UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		}
		break;

	default:
		break;
	}
}