// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Coin.h"
#include "Piece/BasePiece.h"
#include "CoinEmmiter.generated.h"

/**
 * 
 */
UCLASS()
class UCoinEmmiter : public UObject
{
	GENERATED_BODY()

public:
	/// <summary>
	/// コインの読み込み
	/// </summary>
	void LoadCoin();

	/// <summary>
	/// コイン入手
	/// </summary>
	void GetCoineEmmit(ABasePiece* Piece, int Money);

	/// <summary>
	/// コイン消失
	/// </summary>
	void LostCoineEmmit(ABasePiece* Piece, int Money);

private:
	/// <summary>
	/// 入手する銀コイン
	/// </summary>
	UPROPERTY()
	TSubclassOf<class ACoin> GetSilverCoin;

	/// <summary>
	/// 入手する金コイン
	/// </summary>
	UPROPERTY()
	TSubclassOf<class ACoin> GetGoldCoin;

	/// <summary>
	/// 消失する銀コイン
	/// </summary>
	UPROPERTY()
	TSubclassOf<class ACoin> LostSilverCoin;

	/// <summary>
	/// 消失する金コイン
	/// </summary>
	UPROPERTY()
	TSubclassOf<class ACoin> LostGoldCoin;
};
