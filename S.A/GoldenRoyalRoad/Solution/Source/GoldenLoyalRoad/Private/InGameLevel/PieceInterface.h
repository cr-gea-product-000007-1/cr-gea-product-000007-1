// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Piece/BasePiece.h"
#include "PieceInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPieceInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IPieceInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 同じマスにいるコマ数を取得するイベント
	/// </summary>
	/// <param> 判定するコマ </param>
	/// <return> コマ数 </return>
	virtual int GetNowSpacePieceNum(ABasePiece* Original) = 0;

	/// <summary>
	/// 同じマスにいる待機コマの位置更新
	/// </summary>
	/// <param> 移動するコマ </param>
	virtual void UpdateNowSpaceidelPiece(ABasePiece* Original) = 0;

	/// <summary>
	/// コマがお金を得る
	/// </summary>
	/// <param> コマ </param>
	/// <param> 金額 </param>
	virtual void GetCoineEmmit(ABasePiece* Piece, int Money) = 0;

	/// <summary>
	/// コマがお金を失う
	/// </summary>
	/// <param> コマ </param>
	/// <param> 金額 </param>
	virtual void LostCoineEmmit(ABasePiece* Piece, int Money) = 0;
};
