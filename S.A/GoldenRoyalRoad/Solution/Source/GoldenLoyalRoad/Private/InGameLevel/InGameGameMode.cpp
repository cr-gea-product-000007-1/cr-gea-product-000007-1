﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/InGameGameMode.h"
#include "PlayerMenuController.h"


/// <summary>
/// コンストラクタ
/// </summary>
AInGameGameMode::AInGameGameMode()
{
	DefaultPawnClass = nullptr;
	PlayerControllerClass = APlayerMenuController::StaticClass();
}