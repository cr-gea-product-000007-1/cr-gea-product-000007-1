// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CameraInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCameraInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ICameraInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// カメラの移動を開始する
	/// </summary>
	virtual void CameraMoveStart() = 0;

	/// <summary>
	/// カメラの移動を終了する
	/// </summary>
	virtual void CameraMoveEnd() = 0;

	/// <summary>
	/// カメラが移動しているかを返す
	/// </summary>
	/// <return>移動中 = true</return>
	virtual bool IsCameraMove() = 0;
};
