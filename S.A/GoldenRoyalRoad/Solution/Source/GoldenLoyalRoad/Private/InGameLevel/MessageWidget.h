﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "MessageWidget.generated.h"

/**
 * 
 */

UENUM()
enum class SendMessageType : uint8
{
	None,     //なし
	Input,    //キー入力で進む
	AutoTime, //一定時間で進む
};

UCLASS()
class UMessageWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()
	
public:
	/// <summary>
	/// 更新する
	/// </summary>
	void Update(float DeltaTime);

	/// <summary>
	/// メッセージを送る入力通知
	/// </summary>
	void SendMessageInputEvent();

	/// <summary>
	/// メッセージ本文を設定する
	/// </summary>
	/// <param>メッセージ本文</param>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetText(const FString& Text);

	/// <summary>
	/// メッセージの飛ばし方を設定する
	/// </summary>
	/// <param>メッセージの飛ばし方</param>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetSendMessageType(SendMessageType SendMessageType);

private:
	/// <summary>
	/// メッセージの飛ばし方
	/// </summary>
	SendMessageType MyMessageType;

	/// <summary>
	/// 時間計測
	/// </summary>
	float Timer;
};
