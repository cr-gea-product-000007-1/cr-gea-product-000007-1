﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Piece/BasePiece.h"
#include "Arrow.generated.h"

UCLASS()
class AArrow : public AActor
{
	GENERATED_BODY()

private:
	enum class ArrowPhaseType
	{
		Select,
		NotSelect,
	};

public:
	// Sets default values
	AArrow();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	/// <summary>
	/// 選ばれている状態にする
	/// </summary>
	void Select() { ArrowPhase = ArrowPhaseType::Select; };

	/// <summary>
	/// 選ばれていない状態にする
	/// </summary>
	void NotSelect() { ArrowPhase = ArrowPhaseType::NotSelect; };

	/// <summary>
	/// 表示させる
	/// </summary>
	void Visible();

	/// <summary>
	/// 表示をやめる
	/// </summary>
	void NotVisible();

private:
	/// <summary>
	/// 矢印のフェーズ
	/// </summary>
	ArrowPhaseType ArrowPhase;

	/// <summary>
	/// 矢印の矢の部分のメッシュ
	/// </summary>
	UPROPERTY()
	class UStaticMeshComponent* Arrow;

	/// <summary>
	/// 矢印の球の部分のメッシュ
	/// </summary>
	UPROPERTY()
	class UStaticMeshComponent* Ball;

	/// <summary>
	/// 選ばれている時に大きくなる大きさ
	/// </summary>
	float SelectAddScale;

	/// <summary>
	/// 経過時間
	/// </summary>
	float Timer;

	/// <summary>
	/// 時間で変わる大きさ
	/// </summary>
	float TimeScale;

	/// <summary>
	/// 表示状態
	/// </summary>
	bool bVisible = true;
};
