﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MessageWidget.h"
#include "MessageInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMessageInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IMessageInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// メッセージを表示
	/// </summary>
	/// <param> 内容 </param>
	virtual void OpenMessage(const FString& Text, SendMessageType MessageType) = 0;

	/// <summary>
	/// メッセージを閉じる
	/// </summary>
	virtual void CloseMessage() = 0;

	/// <summary>
	/// メッセージを表示中かを返す
	/// </summary>
	virtual bool IsMessage() = 0;
};
