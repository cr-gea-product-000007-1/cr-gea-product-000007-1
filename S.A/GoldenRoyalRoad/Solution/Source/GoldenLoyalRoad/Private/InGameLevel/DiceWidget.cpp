﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/DiceWidget.h"
#include "../../PathConstant.h"
#include "../../GameConstant.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"


/// <summary>
/// コンストラクタ
/// </summary>
UDiceWidget::UDiceWidget()
{

	for (int i = 0; i < GameConstant::DiceMaxValue; i++)
	{
		FString Path = Path::DiceImagePath[i];
		const TCHAR* TCharPath = *Path;

		ConstructorHelpers::FObjectFinder<UTexture2D> DataFile(TCharPath);
		if (IsValid(DataFile.Object))
		{
			ValueImages.Add(DataFile.Object);
		}
	}
}

/// <summary>
/// Imageの読み込み
/// </summary>
void UDiceWidget::LoadImage()
{
	Image = Cast<UImage>(GetWidgetFromName(TEXT("DiceImage")));
}

/// <summary>
/// 端に移動させるのイベント
/// </summary>
void UDiceWidget::SideMove_Implementation()
{

}

/// <summary>
/// 中心に移動させるのイベント
/// </summary>
void UDiceWidget::CenterMove_Implementation()
{

}

/// <summary>
/// 目を設定する
/// </summary>
void UDiceWidget::SetValue(int Value)
{
	//1から始まる目を0から始まるインデックスに合わせる
	Value--;
	if (Value < 0) return;
	if (Value > GameConstant::DiceMaxValue) return;
	if (!GetIsOpen()) Open();
	Image->SetBrushFromTexture(ValueImages[Value]);
}