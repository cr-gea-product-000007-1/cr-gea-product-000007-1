﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DiceInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDiceInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IDiceInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// サイコロを準備する
	/// </summary>
	virtual void StartDice() = 0;

	/// <summary>
	/// サイコロを転がすイベント
	/// </summary>
	virtual void RollDice() = 0;

	/// <summary>
	/// サイコロを投げるイベント
	/// </summary>
	virtual void ThrowDice() = 0;

	/// <summary>
	/// サイコロが振り終わったか
	/// </summary>
	virtual bool IsThrowDiceEnd() = 0;

	/// <summary>
	/// サイコロの目を設定する
	/// </summary>
	/// <return> サイコロの目 </return>
	virtual void SetDiceValue(int Value) = 0;

	/// <summary>
	/// サイコロの目を取得するイベント
	/// </summary>
	virtual int GetDiceValue() = 0;

	/// <summary>
	/// サイコロの目を1減らす
	/// </summary>
	/// <return> 減らした後の目 </return>
	virtual int ValueCountDown() = 0;
};
