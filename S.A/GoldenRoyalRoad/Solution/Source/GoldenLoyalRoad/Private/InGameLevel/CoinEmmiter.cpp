// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameLevel/CoinEmmiter.h"
#include "../../PathConstant.h"

/// <summary>
/// 銀コインの上限値
/// </summary>
const int Silver = 10000;

/// <summary>
/// コイン両方の上限値
/// </summary>
const int Mix = 30000;

/// <summary>
/// 通常金コインの上限値
/// </summary>
const int Gold = 50000;

/// <summary>
/// コインの読み込み
/// </summary>
void UCoinEmmiter::LoadCoin()
{
	FString GetSilverCoinPath = Path::GetSilverCoinPath;
	GetSilverCoin = TSoftClassPtr<ACoin>(FSoftObjectPath(*GetSilverCoinPath)).LoadSynchronous();

	FString GetGoldCoinPath = Path::GetGoldCoinPath;
	GetGoldCoin = TSoftClassPtr<ACoin>(FSoftObjectPath(*GetGoldCoinPath)).LoadSynchronous();

	FString LostSilverCoinPath = Path::LostSilverCoinPath;
	LostSilverCoin = TSoftClassPtr<ACoin>(FSoftObjectPath(*LostSilverCoinPath)).LoadSynchronous();

	FString LostGoldCoinPath = Path::LostGoldCoinPath;
	LostGoldCoin = TSoftClassPtr<ACoin>(FSoftObjectPath(*LostGoldCoinPath)).LoadSynchronous();
}

/// <summary>
/// コイン入手
/// </summary>
void UCoinEmmiter::GetCoineEmmit(ABasePiece* Piece, int Money)
{
	if (!IsValid(GetSilverCoin)) return;
	if (!IsValid(GetGoldCoin)) return;
	if (!IsValid(Piece)) return;
	if (Money <= 0) return;

	//生成したコインを入れる
	ACoin* NewCoin;

	if (Money <= Silver)
	{
		for (int i = 0; i < 4; i++)
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(GetSilverCoin);
			NewCoin->SetActorLocation(Piece->GetActorLocation());
			NewCoin->SetDelayTime((float)i * 0.2f);
		}
	}
	else if (Money <= Mix)
	{
		for (int i = 0; i < 8; i++)
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(GetSilverCoin);
			NewCoin->SetActorLocation(Piece->GetActorLocation());
			NewCoin->SetDelayTime((float)i * 0.1f);
		}
	}
	else if (Money <= Gold)
	{
		for (int i = 0; i < 10; i++)
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(GetGoldCoin);
			NewCoin->SetActorLocation(Piece->GetActorLocation());
			NewCoin->SetDelayTime((float)i * 0.08f);
		}
	}
	else  
	{
		for (int i = 0; i < 25; i++)
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(GetGoldCoin);
			NewCoin->SetActorLocation(Piece->GetActorLocation() + FVector(FMath::RandRange(-35.0f, 35.0f), FMath::RandRange(-35.0f, 35.0f), 0.0f));
			NewCoin->SetDelayTime((float)i * 0.025f);
		}
	}
}

/// <summary>
/// コイン消失
/// </summary>
void UCoinEmmiter::LostCoineEmmit(ABasePiece* Piece, int Money)
{
	if (!IsValid(LostSilverCoin)) return;
	if (!IsValid(LostGoldCoin)) return;
	if (!IsValid(Piece)) return;
	if (Money <= 0) return;

	for (int i = 0; i < 8; i++)
	{
		ACoin* NewCoin;
		if (Money <= Silver)
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(LostSilverCoin);
		}
		else if (Money <= Mix)
		{
			if (i % 2 == 0)
				NewCoin = GetWorld()->SpawnActor<ACoin>(LostSilverCoin);
			else
				NewCoin = GetWorld()->SpawnActor<ACoin>(LostGoldCoin);
		}
		else
		{
			NewCoin = GetWorld()->SpawnActor<ACoin>(LostGoldCoin);
		}
		NewCoin->SetActorLocation(Piece->GetActorLocation());
		NewCoin->SetDelayTime((float)i * 0.01f);

		//大金なら２倍生成
		if (Money > Gold) {
			ACoin* NewCoin2;
			NewCoin2 = GetWorld()->SpawnActor<ACoin>(LostGoldCoin);
			NewCoin2->SetActorLocation(Piece->GetActorLocation());
			NewCoin2->SetDelayTime((float)i * 0.01f);
		}
	}

}