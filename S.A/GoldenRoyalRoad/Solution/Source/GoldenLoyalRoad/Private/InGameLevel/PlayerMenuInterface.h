﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PlayerMenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPlayerMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IPlayerMenuInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	virtual void OnDecide() = 0;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	virtual void OnUp() = 0;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	virtual void OnDown() = 0;

	/// <summary>
	/// 左キーを押したときのイベント
	/// </summary>
	virtual void OnLeft() = 0;

	/// <summary>
	/// 右キーを押したときのイベント
	/// </summary>
	virtual void OnRight() = 0;
};
