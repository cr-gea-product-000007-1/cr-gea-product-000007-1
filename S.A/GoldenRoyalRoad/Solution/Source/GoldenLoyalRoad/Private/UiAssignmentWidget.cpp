﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UiAssignmentWidget.h"

void UUiAssignmentWidget::Open_Implementation()
{
	if(!IsInViewport())
		AddToViewport(0);
	bIsOpen = true;
}

void UUiAssignmentWidget::Close_Implementation()
{
}

void UUiAssignmentWidget::Closed_Implementation()
{
	bIsOpen = false;
	//ウィジットを閉じる
	RemoveFromParent();
}
