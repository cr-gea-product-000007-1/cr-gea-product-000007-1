﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UiAssignmentWidget.generated.h"

/**
 * 
 */
UCLASS()
class UUiAssignmentWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 開いた時のイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Open();

	/// <summary>
	/// 閉じた時のイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Close();

	/// <summary>
	/// 閉じ終わった時のイベント
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Closed();

	/// <summary>
	/// ウィジットを開いているかを取得する
	/// </summary>
	bool GetIsOpen() { return bIsOpen; }

protected:
	/// <summary>
	/// ウィジットを開いているか
	/// </summary>
	bool bIsOpen = false;

};
