﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "KeyType.generated.h"


UENUM(BlueprintType)
enum class UKeyType : uint8
{
	Dicide,
	Up,
	Down,
	Left,
	Right,
};