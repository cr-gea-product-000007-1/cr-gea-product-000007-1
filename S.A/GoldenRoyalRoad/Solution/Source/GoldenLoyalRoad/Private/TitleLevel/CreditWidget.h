// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UiAssignmentWidget.h"
#include "TitleLevelInterface.h"
#include "CreditWidget.generated.h"

/**
 * 
 */
UCLASS()
class UCreditWidget : public UUiAssignmentWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// 閉じるフラグ設定
	/// </summary>
	void CloseFrag();

	/// <summary>
	/// ウィジットを閉じる
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void CloseWidget();

	/// <summary>
	/// インターフェースを設定する
	/// </summary>
	void SetTitleLevelInterface(const TScriptInterface<ITitleLevelInterface>& Interface) { TitleLevelInterface = Interface; };

private:
	/// <summary>
	/// インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<ITitleLevelInterface> TitleLevelInterface;
	
};
