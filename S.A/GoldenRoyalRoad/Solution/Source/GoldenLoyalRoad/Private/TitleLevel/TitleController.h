﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TitleLevelInterface.h"
#include "TitleController.generated.h"

/**
 * 
 */
UCLASS()
class ATitleController : public APlayerController
{
	GENERATED_BODY()

protected:
	//SetupInputComponentの継承
	virtual void SetupInputComponent() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide();

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp();

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown();

public:
	/// <summary>
	/// インターフェースを設定する
	/// </summary>
	void SetTitleLevelInterface(const TScriptInterface<ITitleLevelInterface>& Interface) { TitleLevelInterface = Interface; };

private:
	/// <summary>
	/// インターフェース
	/// </summary>
	UPROPERTY()
	TScriptInterface<ITitleLevelInterface> TitleLevelInterface;
	
};
