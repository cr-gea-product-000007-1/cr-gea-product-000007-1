﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleLevel/TitleGameMode.h"
#include "TitleController.h"


/// <summary>
/// コンストラクタ
/// </summary>
ATitleGameMode::ATitleGameMode()
{
	DefaultPawnClass = nullptr;
	PlayerControllerClass = ATitleController::StaticClass();
}
