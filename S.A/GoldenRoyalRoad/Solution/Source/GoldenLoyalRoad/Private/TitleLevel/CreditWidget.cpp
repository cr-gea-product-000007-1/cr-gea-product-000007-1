// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleLevel/CreditWidget.h"


/// <summary>
/// 閉じるフラグ設定
/// </summary>
void UCreditWidget::CloseFrag()
{
	bIsOpen = false;
}

/// <summary>
/// ウィジットを閉じる
/// </summary>
void UCreditWidget::CloseWidget_Implementation()
{
	if (TitleLevelInterface == nullptr) return;
	TitleLevelInterface->CloseCredit();
}