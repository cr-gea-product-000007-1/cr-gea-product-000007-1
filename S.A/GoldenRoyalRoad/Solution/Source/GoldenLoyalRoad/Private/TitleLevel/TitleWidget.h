﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TitleWidget.generated.h"

/**
 * 
 */
UCLASS()
class UTitleWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/// <summary>
	/// ゲーム開始を選ぶ
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SelectStartEvent();

	/// <summary>
	/// クレジットを選ぶ
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SelectCreditEvent();

	/// <summary>
	/// ゲーム終了を選ぶ
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SelectEndEvent();
};
