﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "TitleLevelInterface.h"
#include "TitleWidget.h"
#include "TitleController.h"
#include "CreditWidget.h"
#include "TitleLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class ATitleLevelScriptActor : public ALevelScriptActor, public ITitleLevelInterface
{
	GENERATED_BODY()

	enum class TitleMenuPhase
	{
		Start,  //ゲーム開始
		Credit, //クレジット
		End,    //ゲーム終了
	};

protected:
	virtual void BeginPlay() override;

	/// <summary>
	/// 決定キーを押したときのイベント
	/// </summary>
	void OnDecide() override;

	/// <summary>
	/// 上キーを押したときのイベント
	/// </summary>
	void OnUp() override;

	/// <summary>
	/// 下キーを押したときのイベント
	/// </summary>
	void OnDown() override;

	/// <summary>
	/// クレジットを閉じるイベント
	/// </summary>
	void CloseCredit() override;

private:
	/// <summary>
	/// タイトルの選んでいる項目
	/// </summary>
	TitleMenuPhase TitleMenu;

	/// <summary>
	/// タイトルウィジット
	/// </summary>
	UPROPERTY()
	UTitleWidget* TitleWidget;

	/// <summary>
	/// クレジットウィジット
	/// </summary>
	UPROPERTY()
	UCreditWidget* CreditWidget;
};
