﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleLevel/TitleLevelScriptActor.h"
#include "../../PathConstant.h"
#include "Kismet/GameplayStatics.h"


void ATitleLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	TitleMenu = TitleMenuPhase::Start;

	FString TitlePath = Path::TitleWidgetPath;
	TSubclassOf<UTitleWidget> TitleWidgetClass = TSoftClassPtr<UTitleWidget>(FSoftObjectPath(*TitlePath)).LoadSynchronous();
	if (IsValid(TitleWidgetClass))
	{
		TitleWidget = CreateWidget<UTitleWidget>(GetWorld(), TitleWidgetClass);
		TitleWidget->AddToViewport();
		TitleWidget->SelectStartEvent();
	}

	FString CreditPath = Path::CreditWidgetPath;
	TSubclassOf<UCreditWidget> CreditWidgetClass = TSoftClassPtr<UCreditWidget>(FSoftObjectPath(*CreditPath)).LoadSynchronous();
	if (IsValid(CreditWidgetClass))
	{
		CreditWidget = CreateWidget<UCreditWidget>(GetWorld(), CreditWidgetClass);
		//CreditWidget->CloseFrag();
		//CreditWidget->Open();
		CreditWidget->SetTitleLevelInterface(this);
	}

	ATitleController* TitleController = Cast<ATitleController>(GetWorld()->GetFirstPlayerController());
	if (IsValid(TitleController))
	{
		TitleController->SetTitleLevelInterface(this);
	}
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ATitleLevelScriptActor::OnDecide()
{
	if (!IsValid(CreditWidget)) return;
	if (CreditWidget->GetIsOpen()) return;

	switch (TitleMenu)
	{
	case ATitleLevelScriptActor::TitleMenuPhase::Start:
		if (IsValid(TitleWidget))
		{
			TitleWidget->RemoveFromParent();
		}
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("InGameLevel"), true);
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::Credit:
		CreditWidget->Open();
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::End:
		//ゲームを閉じる
		UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
		break;
	}
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void ATitleLevelScriptActor::OnUp()
{
	if (!IsValid(TitleWidget)) return;

	switch (TitleMenu)
	{
	case ATitleLevelScriptActor::TitleMenuPhase::Start:
		TitleMenu = TitleMenuPhase::End;
		TitleWidget->SelectEndEvent();
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::Credit:
		TitleMenu = TitleMenuPhase::Start;
		TitleWidget->SelectStartEvent();
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::End:
		TitleMenu = TitleMenuPhase::Credit;
		TitleWidget->SelectCreditEvent();
		break;
	}
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void ATitleLevelScriptActor::OnDown()
{
	if (!IsValid(TitleWidget)) return;

	switch (TitleMenu)
	{
	case ATitleLevelScriptActor::TitleMenuPhase::Start:
		TitleMenu = TitleMenuPhase::Credit;
		TitleWidget->SelectCreditEvent();
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::Credit:
		TitleMenu = TitleMenuPhase::End;
		TitleWidget->SelectEndEvent();
		break;

	case ATitleLevelScriptActor::TitleMenuPhase::End:
		TitleMenu = TitleMenuPhase::Start;
		TitleWidget->SelectStartEvent();
		break;
	}
}


/// <summary>
/// クレジットを閉じるイベント
/// </summary>
void ATitleLevelScriptActor::CloseCredit()
{
	if (!IsValid(CreditWidget)) return;
	if (!CreditWidget->GetIsOpen()) return;
	CreditWidget->Close();
}