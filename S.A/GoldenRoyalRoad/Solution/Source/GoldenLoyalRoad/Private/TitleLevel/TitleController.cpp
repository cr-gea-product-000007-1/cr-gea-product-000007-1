﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleLevel/TitleController.h"


//SetupInputComponentの継承
void ATitleController::SetupInputComponent()
{
    Super::SetupInputComponent();
    check(InputComponent); //UE4のアサートマクロ

    if (IsValid(InputComponent))
    {
        InputComponent->BindAction("Decide", IE_Pressed, this, &ATitleController::OnDecide);
        InputComponent->BindAction("Up", IE_Pressed, this, &ATitleController::OnUp);
        InputComponent->BindAction("Down", IE_Pressed, this, &ATitleController::OnDown);
    }
}

/// <summary>
/// 決定キーを押したときのイベント
/// </summary>
void ATitleController::OnDecide()
{
    if (TitleLevelInterface == nullptr) return;

    TitleLevelInterface->OnDecide();
}

/// <summary>
/// 上キーを押したときのイベント
/// </summary>
void ATitleController::OnUp()
{
    if (TitleLevelInterface == nullptr) return;

    TitleLevelInterface->OnUp();
}

/// <summary>
/// 下キーを押したときのイベント
/// </summary>
void ATitleController::OnDown()
{
    if (TitleLevelInterface == nullptr) return;

    TitleLevelInterface->OnDown();
}