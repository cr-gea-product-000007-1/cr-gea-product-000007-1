﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Blueprint/UserWidget.h"
#include "BootLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class ABootLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	enum PhaseType
	{
		Idle,        //待機
		LevelChange, //レベル変更
	};

public:
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void BeginPlay() override;

private:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	ABootLevelScriptActor();

	/// <summary>
	/// 次のレベルに遷移する時間
	/// </summary>
	float NextLevelTime;

	/// <summary>
	/// フェーズ
	/// </summary>
	PhaseType Phase;

	/// <summary>
	/// メッセージのウィジット
	/// </summary>
	UPROPERTY()
	UUserWidget* BootWidget;
};
