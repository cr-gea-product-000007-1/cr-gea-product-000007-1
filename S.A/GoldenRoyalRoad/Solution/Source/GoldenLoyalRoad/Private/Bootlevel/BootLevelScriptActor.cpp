﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BootLevel/BootLevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../../PathConstant.h"

/// <summary>
/// 次のレベルに移動する時間
/// </summary>
const float NextLevelMaxTime = 3.0f;

/// <summary>
/// コンストラクタ
/// </summary>
ABootLevelScriptActor::ABootLevelScriptActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

/// <summary>
///　Tick関数の継承
/// 毎フレーム呼ばれる
/// </summary>
void ABootLevelScriptActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	switch (Phase)
	{
		// 時間経過
	case ABootLevelScriptActor::Idle:
		NextLevelTime += DeltaTime;
		if (NextLevelTime >= NextLevelMaxTime) {
			Phase = PhaseType::LevelChange;
		}
		break;

		//次のレベルへ移動
	case ABootLevelScriptActor::LevelChange:	
		if (IsValid(BootWidget))
		{
			BootWidget->RemoveFromParent();
		}
		UGameplayStatics::OpenLevel(GetWorld(), TEXT("TitleLevel"), true);
		break;

	}
}

/// <summary>
///　BeginPlay関数の継承
/// </summary>
void ABootLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	//ウインドサイズ設定
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), "r.SetRes 1280x720w");

	NextLevelTime = 0.0f;
	Phase = PhaseType::Idle;


	FString Path = Path::BootWidgetPath;
	TSubclassOf<UUserWidget> BootWidgetClass = TSoftClassPtr<UUserWidget>(FSoftObjectPath(*Path)).LoadSynchronous();
	if (IsValid(BootWidgetClass))
	{
		BootWidget = CreateWidget<UUserWidget>(GetWorld(), BootWidgetClass);
		BootWidget->AddToViewport();
	}
}

